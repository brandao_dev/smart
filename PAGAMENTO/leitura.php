<?php

//MES ANTERIOR
$mes = date('Y-m',strtotime('-1 months',strtotime(date('Y-m'))));
$_SESSION['referencia'] = utf8_encode(ucfirst(strftime('%B',strtotime('-1 months',strtotime(date('Y-m'))))));

$_SESSION['sql'] = conex()->query("SELECT id,smart,obs,data_rma,rma,pago,datatriagem,modelo,linha FROM codigo WHERE datatriagem LIKE '$mes%' UNION ALL SELECT id,smart,obs,data_rma,rma,pago,datatriagem,modelo,linha FROM lg WHERE datatriagem LIKE '$mes%' ORDER BY datatriagem")->fetchAll(PDO::FETCH_ASSOC);

$_SESSION['total'] = count($_SESSION['sql']);
$nulos = count(conex()->query("SELECT id FROM codigo WHERE rma NOT LIKE '' AND datatriagem LIKE '$mes%' AND obs LIKE '%AUTO; ' UNION ALL SELECT id FROM lg WHERE rma NOT LIKE '' AND datatriagem LIKE '$mes%' AND obs LIKE '%AUTO; '")->fetchAll(PDO::FETCH_ASSOC));
$partwait = count(conex()->query("SELECT id FROM codigo WHERE datatriagem LIKE '$mes%' AND obs LIKE '%PARTWAIT; ' UNION ALL SELECT id FROM lg WHERE datatriagem LIKE '$mes%' AND obs LIKE '%PARTWAIT; '")->fetchAll(PDO::FETCH_ASSOC));
$scrap = count(conex()->query("SELECT id FROM codigo WHERE datatriagem LIKE '$mes%' AND obs LIKE '%SCRAP; ' UNION ALL SELECT id FROM lg WHERE datatriagem LIKE '$mes%' AND obs LIKE '%SCRAP; '")->fetchAll(PDO::FETCH_ASSOC));

// INICIAR VARIAVEIS. PHP 8 ME DECEPCIONANDO
$pendente = 0;
$tbody = '';

foreach($_SESSION['sql'] as $reparo)
{ 
  #OBTER ULTIMO REPARO E QUANTIDADE 
  $r = explode('/',$reparo['obs']);
  $ultimo = trim(explode('=',end($r))[1]);
  $qtd = Count($r)-1;

  #DIAS PARA PAGO
  $pago = ($reparo['pago'] == null) ? '2020-05-06' : $reparo['pago'];  
  $intervalo = strtotime($pago) - strtotime($time);
  $dias = abs(floor($intervalo/(60*60*24)));

  #DEFINE GARANTIA
  $garantia = ($dias < 180) ? 'SIM' : 'NÃO';

  //SE NÂO FOR O PRIMEIRO REPARO
  if($qtd > 0)
  {
    //SE PAGO A MENOS DE 180 DIAS
    if($garantia == 'SIM')
    {
      //SE NÃO TIVER RMA *** OU *** TEM RMA E REPARO DIFERENTE DE TESTE
      if((empty($reparo['rma'])) || ($ultimo != "TESTADO_AUTO;" && !empty($reparo['rma'])))
      { 
        $pendente++;  
        $class = '';
        if($qtd == 2){$class = 'class="table-warning"';}
        elseif($qtd == 3){$class = 'class="table-orange"';}
        elseif($qtd == 4){$class = 'class="table-danger"';}
        elseif($qtd > 4){$class = 'class="table-purple"';}
        $link = "../CONSULTA/BUSCAR.php?smart={$reparo['smart']}";
        $tbody .=

          "<tr {$class}>
                <td>{$qtd} X</td>
                <td>{$dias} dias</td>
                <td>{$reparo['rma']}</td>
                <td>{$ultimo}</td>
                <td><a href={$link} target='blank'' class='text-primary font-weight-bold'>{$reparo['smart']}</a></td>
                <td>
                    <div class='btn-group btn-group-toggle shadow' data-toggle='buttons'>
                        <label class='btn btn-outline-danger active'>
                            <input type='radio' name='{$reparo['id']}' value='0' autocomplete='off' checked> SIM
                        </label>
                        <label class='btn btn-outline-danger'>
                            <input type='radio' name='{$reparo['id']}' value='1' autocomplete='off'> NÃO
                        </label>
                    </div>
                </td>
            </tr>";
      }
    }
  }
} 
?>
<script>
    sessionStorage.setItem('pendente', '<?php echo $pendente?>')
</script>