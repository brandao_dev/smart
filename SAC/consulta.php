<?php include '../CORE/Header2.php';?>
<script src="https://cdn.brandev.site/js/ag-grid-enterprise.min.js"></script>
	

<div class="row mb-3">
	<div class="col text-left">
		<button class="btn btn-cyan btn-lg shadow" id="selectedRows" onclick="sac()"><i class="far fa-arrow-alt-circle-right mr-2"></i>Escolha uma linha...</button>
	</div>
</div>

<div id="myGrid" style="position:absolute; width: 98%;height:80%" class="ag-theme-alpine"></div>
<script type="text/javascript">
// COLUNAS //////////////////////////////////////////
var columnDefs = 
[
  {headerName: "ALTERAÇÃO", field: "ALTERACAO", filter: "agDateColumnFilter", hide: true},
  {headerName: "ABERTO", field: "ABERTO", filter: "agSetColumnFilter", hide: true},
  {headerName: "AGENDA", field: "AGENDA", filter: "agDateColumnFilter"},
  {headerName: "ATENDENTE", field: "ATENDENTE", filter: "agSetColumnFilter", hide: true},
  {headerName: "ATENDIDO", field: "ATENDIDO", filter: "agSetColumnFilter", hide: true},
  {headerName: "BAIRRO", field: "BAIRRO", filter: "agTextColumnFilter", hide: true},
  {headerName: "CEP", field: "CEP", filter: "agTextColumnFilter", hide: true},
  {headerName: "CIDADE", field: "CIDADE", filter: "agTextColumnFilter", hide: true},
  {headerName: "CLIENTE", field: "CLIENTE", filter: "agTextColumnFilter"},
  {headerName: "CODIGO PEÇA", field: "CODIGOPECA", filter: "agTextColumnFilter", hide: true},
  {headerName: "COMPLEMENTO", field: "COMPLEMENTO", filter: "agTextColumnFilter", hide: true},
  {headerName: "COMPRA", field: "COMPRA", filter: "agDateColumnFilter", hide: true},
  {headerName: "COMPRA PEÇA", field: "COMPRAPECA", filter: "agDateColumnFilter", hide: true},
  {headerName: "CONSTATADO", field: "CONSTATADO", filter: "agTextColumnFilter", hide: true},
  {headerName: "CPF", field: "CPF", filter: "agTextColumnFilter", hide: true},
  {headerName: "DATA", field: "DATA", filter: "agDateColumnFilter", hide: true},
  {headerName: "COLETA", field: "COLETA", filter: "agDateColumnFilter"},
  {headerName: "DATA COMPRA", field: "DATACOMPRA", filter: "agDateColumnFilter", hide: true},
  {headerName: "DISPONIVEL", field: "DISPONIVEL", filter: "agSetColumnFilter", hide: true},
  {headerName: "ENDEREÇO", field: "ENDERECO", filter: "agTextColumnFilter", hide: true},
  {headerName: "FINAL", field: "FINAL", filter: "agDateColumnFilter", hide: true},
  {headerName: "LOCAL", field: "LOCAL", filter: "agSetColumnFilter"},
  {headerName: "MODELO", field: "MODELO", filter: "agTextColumnFilter"},
  {headerName: "NOTA", field: "NOTA", filter: "agTextColumnFilter", hide: true},
  {headerName: "OBS", field: "OBS", filter: "agTextColumnFilter", hide: true},
  {headerName: "OBSCOMPRA", field: "OBS", filter: "agTextColumnFilter", hide: true},
  {headerName: "ORDEM", field: "id", filter: "agTextColumnFilter", hide: true},
  {headerName: "PEÇA", field: "PECA", filter: "agTextColumnFilter", hide: true},
  {headerName: "QUEIXA", field: "QUEIXA", filter: "agTextColumnFilter", hide: true},
  {headerName: "SAC", field: "SAC", filter: "agTextColumnFilter"},
  {headerName: "SERVIÇO", field: "SERVICO", filter: "agTextColumnFilter", hide: true},
  {headerName: "SMART", field: "SMART", filter: "agTextColumnFilter"},
  {headerName: "STATUS", field: "STATUS", filter: "agSetColumnFilter"},
  {headerName: "TEL", field: "TEL", filter: "agTextColumnFilter", hide: true},
  {headerName: "USER", field: "USER", filter: "agSetColumnFilter", hide: true},
  {headerName: "VOLTAGEM", field: "VOLTAGEM", filter: "agSetColumnFilter", hide: true},
];

// SELECIONAR ITENS //////////////////////////////
function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    var imp = '';
    selectedRows.forEach( function(selectedRow, index) {
        if (index!==0) {
            selectedRowsString += ', ';
            imp += ', ';
        }
        selectedRowsString += selectedRow.SAC;
    });
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
    document.myform.busca.value = selectedRowsString;
}

// CARREGAR JSON //////////////////////////////////
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    // do http request to get our sample data - not using any framework to keep the example self contained.
    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'get.php');
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResult = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResult);
        }
    };
});

function sac()
{
	let sac = $('#selectedRows').html()
	if(sac != '<i class="far fa-arrow-alt-circle-right mr-2"></i>Escolha...')
	{
		window.open('../SAC/SAC.php?sac='+sac)
	}
}
</script>
<script src="/AGGRID/agGrid.js"></script>
<?php include('../CORE/Footer2.php')?>