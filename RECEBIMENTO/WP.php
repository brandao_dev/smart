<?php include("../CORE/Header2.php");

// MODO BOTOES
$_SESSION['modo'] = (isset($_GET['modo'])) ? $_GET['modo'] : 'SM';

//FALA
switch($_SESSION['modo'])
{
    case 'SM':
        $fala = 'Smart';
    break;
    case 'LG':
        $fala = 'LG';
    break;
    case 'WP':
        $fala = 'Whirlpool';
    break;
    default:
        $fala = 'Smart';
}

$query = "SELECT id,smart,enge,cor,voltagem,fabricante,linha,modelo FROM codigo WHERE data LIKE '$dia%' AND modo LIKE '{$_SESSION['modo']}' ORDER BY id DESC";
$data = conex()->query($query)->fetchAll(PDO::FETCH_ASSOC);
$total = Count($data);

//FALA
// $falas = htmlspecialchars(ucwords(strtolower($fala)));
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

?>
<!--Código custom -->
<script src="AJAX.js"></script>
<script src="https://cdn.brandev.site/js/jquery-barcode.js"></script>
<div id="player" style="display: none;"><?php echo $player?></div>

<!--
       d8888                   d8b
      d88888                   Y8P
     d88P888
    d88P 888 88888b.   .d88b.  888  .d88b.
   d88P  888 888 "88b d88""88b 888 d88""88b
  d88P   888 888  888 888  888 888 888  888
 d8888888888 888 d88P Y88..88P 888 Y88..88P
d88P     888 88888P"   "Y88P"  888  "Y88P"
             888
             888
             888
 -->

<div class="row mb-2">
    <div class="col-8">
        <!-- SEGUNDA LINHA DE FRASE DE APOIO *** //////////////////////// -->
        <div class="alert mt-3 alert-info font-weight-bold btn-block" id="caso"><i class="fas fa-handshake fa-lg mr-2"></i><?php echo 'Modo '.$_SESSION['modo'].' selecionado!';?></div>
    </div>
    <div class="col-4">
        <!-- TOTAL *** /////////////////////////// -->
        <button class="btn btn-lg btn-block btn-warning mt-3 font-weight-bold" id="total"><i class="fas fa-box mr-2"></i><?php echo $total?></button>
    </div>
</div>
<!-- 
8888888888          888              d8b                            888
888                 888              Y8P                            888
888                 888                                             888
8888888     8888b.  88888b.  888d888 888  .d8888b  8888b.  88888b.  888888  .d88b.
888            "88b 888 "88b 888P"   888 d88P"        "88b 888 "88b 888    d8P  Y8b
888        .d888888 888  888 888     888 888      .d888888 888  888 888    88888888
888        888  888 888 d88P 888     888 Y88b.    888  888 888  888 Y88b.  Y8b.
888        "Y888888 88888P"  888     888  "Y8888P "Y888888 888  888  "Y888  "Y8888
 -->
<form action="" method="GET">
    <div class="row mb-3">
        <div class="col">
            <div class="btn-group shadow d-flex" role="group" aria-label="Exemplo básico">
                <!-- WP -->
                <button type="submit" class="btn btn-outline-danger <?php if($_SESSION['modo'] == 'WP') echo 'active'?>" name="modo" value="WP" onclick="carga('WP'),vibrate(200),play()"><img src="../IMG/WP.png" width="25px" alt="" style="opacity: 60%;"> WP</button>
                <!-- SM -->
                <button type="submit" class="btn btn-outline-danger <?php if($_SESSION['modo'] == 'SM') echo 'active'?>" name="modo" value="SM" onclick="carga('SM'),vibrate(200),play()"><img src="../IMG/SM.png" width="25px" alt="" style="opacity: 60%;"> SM</button>
                <!-- LG -->
                <button type="submit" class="btn btn-outline-danger <?php if($_SESSION['modo'] == 'LG') echo 'active'?>" name="modo" value="LG" onclick="carga('LG'),vibrate(200),play()"><img src="../IMG/LG.png" width="25px" alt="" style="opacity: 60%;"> LG</button>
                <!-- CM -->
                <button type="submit" class="btn btn-outline-secondary" name="modo" value="CM" onclick="vibrate(200),play()"><img src="../IMG/CM.png" width="25px" alt="" style="opacity: 60%;"> CM</button>
            </div>
        </div>
    </div>
</form>

<!--
888               888
888               888
888               888
888       .d88b.  888888  .d88b.
888      d88""88b 888    d8P  Y8b
888      888  888 888    88888888
888      Y88..88P Y88b.  Y8b.
88888888  "Y88P"   "Y888  "Y8888
 -->

 <form action="" method="post" id="insert">

<div class="row">

<!-- MODO WP -->
<?php if($_SESSION['modo'] == 'WP') { ?>
    <div class=" col-12 col-lg-6  mt-3">
        <input list="not" type="number" name="nota" id="nota" class="form-control form-control-lg font-weight-bold" placeholder="NOTA FISCAL" autocomplete="off" required><datalist id="not">
            <?php $not = conex()->query("SELECT DISTINCT nota FROM codigo WHERE modo LIKE '{$_SESSION['modo']}' ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);
                foreach($not as $no)
                {
                    echo '<option value="'.$no['nota'].'"/>';
                }
            ?>
        </datalist>
    </div>
<div class="col-6 col-lg-3 mt-3">
    <input list="lot" type="number" name="lote" id="lote" class="form-control form-control-lg font-weight-bold" placeholder="LOTE" autocomplete="off" required><datalist id="lot">
        <?php $lot = conex()->query("SELECT DISTINCT nlote FROM codigo WHERE modo LIKE '{$_SESSION['modo']}' ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);
            foreach($lot as $l)
            {
                echo '<option value="'.$l['nlote'].'"/>';
            }
        ?>                
    <datalist>
    </div>
    <div class="col-6 col-lg-3 mt-3">
        <input type="text" name="enge" id="enge" class="form-control form-control-lg font-weight-bold" placeholder="ENGENHARIA" autocomplete="off" autofocus>
    </div>
    <div class="col-12 mt-3">
        <input type="text" class="form-control form-control-lg font-weight-bold" required id="cod" name="code" placeholder="CÓDIGO (Modelo+Serial)" autocomplete="off">
    </div>
<?php } ?>

<!-- MODO SM -->
<?php if($_SESSION['modo'] == 'SM') { ?>
    <div class="col-12 col-sm-6 mt-3">
        <select name="code" id="cod" class="form-control form-control-lg font-weight-bold" require>
            <option value="">MODELO...</option>
            <?php $cod = conex()->query("SELECT modelo FROM modelo");
                foreach($cod as $mod)
                {
                    echo '<option value="'.$mod['modelo'].'">'.$mod['modelo'].'</option>';
                }
            ?>    
        
        </select>
    </div>
    <div class="col-12 col-sm-6 mt-3">
        <input type="text" class="form-control form-control-lg font-weight-bold" id="serial" name="serial" placeholder="Serial" autocomplete="off">
    </div>
<?php } ?>

<!-- MODO LG -->
<?php if($_SESSION['modo'] == 'LG') { ?>

    <!-- NOTA -->
    <div class="mt-2 col-12 col-sm-6">
        <input list="not" type="text" name="nota" id="nota" class="form-control form-control-lg font-weight-bold" placeholder="NOTA FISCAL" autocomplete="off" required><datalist id="not">
            <?php $not = conex()->query("SELECT DISTINCT nota FROM codigo WHERE modo LIKE '{$_SESSION['modo']}' ORDER BY id DESC");
                foreach($not as $no)
                {
                    echo '<option value="'.$no['nota'].'"/>';
                }
            ?>
        </datalist>
    </div>
    <!-- ENGENHARIA -->
    <div class="col-12 col-sm-6 mt-2">
        <input type="text" name="enge" id="enge" class="form-control form-control-lg font-weight-bold" placeholder="ENGENHARIA" autocomplete="off" autofocus>
    </div>
    <div class="col-12 col-sm-6 mt-3">
        <select name="code" id="cod" class="form-control form-control-lg font-weight-bold" require>
            <option value="">MODELO...</option>
            <?php $cod = conex()->query("SELECT modelo FROM modelo");
                foreach($cod as $mod)
                {
                    echo '<option value="'.$mod['modelo'].'">'.$mod['modelo'].'</option>';
                }
            ?>    
        
        </select>
    </div>
    <div class="col-12 col-sm-6 mt-3">
        <input type="text" class="form-control form-control-lg font-weight-bold" id="serial" name="serial" placeholder="SERIAL" autocomplete="off">
    </div>
<?php } ?>

<!-- MODO CM -->
<?php if($_SESSION['modo'] == 'CM') { ?>
            <!-- MARCA -->
        <div class="col-12 col-sm-6 mt-3">
            <input type="text" name="marca" id="marca" class="form-control form-control-lg font-weight-bold" placeholder="MARCA" autocomplete="off" autofocus list="mar">
            <datalist id="mar">                
                <?php $query = conex()->query("SELECT DISTINCT(fabricante) FROM codigo UNION ALL SELECT DISTINCT(fabricante) FROM lg ORDER BY fabricante");
                    foreach($query as $fab)
                    {
                        echo '<option value="'.$fab['fabricante'].'"/>';
                    }
                ?>
            </datalist>
        </div>
            <!-- VOLTAGEM -->
        <div class="col-12 col-sm-6 mt-3">
            <div class="btn-group btn-group-toggle shadow d-flex" data-toggle="buttons">
                <label class="btn btn-outline-olive btn-lg w-100">
                    <input type="radio" name="voltagem" value="127 V" autocomplete="off" id="127" checked onclick="vibrate(100),play()"> 127V
                </label>
                <label class="btn btn-outline-olive btn-lg w-100">
                    <input type="radio" name="voltagem" value="220 V" autocomplete="off" id="220" onclick="vibrate(100),play()"> 220V
                </label>
                <label class="btn btn-outline-olive btn-lg w-100">
                    <input type="radio" name="voltagem" value="127~220" autocomplete="off" id="BI" onclick="vibrate(100),play()"> Bivolt
                </label>
            </div>
        </div>
            <!-- MODELO -->
        <div class="col-12 col-sm-6 mt-3">
            <input type="text" name="modelo" id="modelo" class="form-control form-control-lg font-weight-bold" placeholder="MODELO" autocomplete="off" list="mod">
            <datalist id="mod">                
                <?php $query = conex()->query("SELECT DISTINCT(modelo) FROM codigo UNION ALL SELECT DISTINCT(modelo) FROM lg ORDER BY modelo");
                    foreach($query as $fab)
                    {
                        echo '<option value="'.$fab['modelo'].'"/>';
                    }
                ?>                    
            </datalist>
        </div>

            <!-- SERIE -->
        <div class="col-12 col-sm-6 mt-3">
            <input type="text" name="serial" id="serial" class="form-control form-control-lg font-weight-bold" placeholder="SERIAL" autocomplete="off">
        </div>

            <!-- LINHA -->
        <div class="col-12 col-sm-6 mt-3">
            <input type="text" name="linha" id="linha" class="form-control form-control-lg font-weight-bold" placeholder="LINHA" autocomplete="off" list="linh">
            <datalist id='linh'>
                <?php $query = conex()->query("SELECT DISTINCT(linha) FROM codigo ORDER BY linha");
                    foreach($query as $lin)
                    {
                        echo '<option value="'.$lin['linha'].'"/>';
                    }
                ?>
            </datalist>
        </div>

        <!-- COR -->
        <div class="col-12 col-sm-6 mt-3">
            <input type="text" name="cor" id="cor" class="form-control form-control-lg font-weight-bold" placeholder="COR" autocomplete="off" list="core">
            <datalist id='core'>
                <?php $query = conex()->query("SELECT DISTINCT(cor) FROM codigo ORDER BY cor");
                    foreach($query as $cor)
                    {
                        echo '<option value="'.$cor['cor'].'"/>';
                    }
                ?>
            </datalist>
        </div>

    </div>

    <!-- GRUPO -->
    <div class="btn-group shadow d-flex row mt-3" data-toggle="buttons">
        <div class="col-12 col-sm-6 col-lg-3">
            <label class="btn btn-outline-indigo btn-lg w-100">
                <input type="radio" style="visibility:hidden;" name="grupo" value="R" autocomplete="off" onclick="vibrate(100),play()"><i class="fas fa-refrigerator mr-2"></i>Refrigeração
            </label>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <label class="btn btn-outline-indigo btn-lg w-100">
                <input type="radio" style="visibility:hidden;" name="grupo" value="C" autocomplete="off" onclick="vibrate(100),play()"><i class="fas fa-oven mr-2"></i>Cocção
            </label>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <label class="btn btn-outline-indigo btn-lg w-100">
                <input type="radio" style="visibility:hidden;" name="grupo" value="L" autocomplete="off" onclick="vibrate(100),play()"><i class="fas fa-dryer-alt mr-2"></i>Lavanderia
            </label>
        </div>
        <div class="col-12 col-sm-6 col-lg-3">
            <label class="btn btn-outline-indigo btn-lg w-100">
                <input type="radio" style="visibility:hidden;" name="grupo" value="A" autocomplete="off" checked onclick="vibrate(100),play()"><i class="fas fa-blender mr-2"></i>Outros
            </label>
        </div>
    </div>

    <!-- OBS -->
    <div class="row mt-3">
        <div class="col-12 mt-3">
            <input type="text" name="obs" id="obs" class="form-control form-control-lg font-weight-bold" placeholder="OBSERVAÇÕES" autocomplete="off">
        </div>
<?php } ?>

    <div class="col-12 mt-3">
        <button class="btn btn-primary shadow btn-block" type="submit" id="botao" onclick="vibrate(200),play()"><h5><i class="fas fa-plus-circle mr-2"></i>Adicionar</h5></button>
    </div>
</form>
<?php if($_SESSION['modo'] != 'CM') { ?>
<div class="col-12 d-xl-none mt-3">
    <button class="btn btn-success shadow btn-block" type="button" value="Scan" onclick="play(),getScan()"><h5 class="mt-1"><i class="fas fa-camera mr-2"></i>Camera</h5></button>
</div>
<?php } ?>
</div>
<!-- PLAYER DE AUDIO -->
<div style="display: none" id="click"></div>

<!-- BARCODE 1 -->
<input id=barcode type=text style="display: none" >
<hr>

<!--
88888888888          888               888
    888              888               888
    888              888               888
    888      .d88b.  88888b.   .d88b.  888  8888b.
    888     d8P  Y8b 888 "88b d8P  Y8b 888     "88b
    888     88888888 888  888 88888888 888 .d888888
    888     Y8b.     888 d88P Y8b.     888 888  888
    888      "Y8888  88888P"   "Y8888  888 "Y888888
 -->

<div class="row mt-2">
    <div class="col-lg-12">
        <table class="table table-striped table-bordered table-hover" id="tab">
            <!-- <input name="nex" id="minut" type="number" value="" style="display: none;"> -->
            <thead>
                <tr class="bg-secondary text-light">
                    <th><i class="fas fa-barcode-read fa-lg"></i></th>
                    <th><i class="fas fa-tags fa-lg"></i></th>
                    <th><i class="fas fa-folder fa-lg"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($data as $dat)
                    {
                        echo 
                        "<tr>
                            <td>".$dat['smart']."</td>
                            <td>".$dat['modelo']."</td>
                            <td>
                                <button type='button' class='btn btn-warning btn-block shadow' onclick='play(), vibrate(100), menu(\"".$dat['modelo']."\",\"".$dat['enge']."\",\"".$dat['cor']."\",\"".$dat['voltagem']."\",\"".$dat['fabricante']."\",\"".$dat['linha']."\",\"".$dat['smart']."\",\"".$dat['id']."\")'><i class='fas fa-folder-open'></i></button>
                            </td>
                        </tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>

<?php include("../CORE/Footer2.php")?>