<?php
include('../CORE/PDO.php');

//QUANTIDADE
$mes = date('Y-m');
$lavanderia = Count(conex()->query("SELECT id FROM codigo WHERE datatriagem LIKE '$mes%' AND grupo = 'L' UNION ALL SELECT id FROM lg WHERE datatriagem LIKE '$mes%' AND grupo = 'L'")->fetchAll(PDO::FETCH_ASSOC));
$coccao = Count(conex()->query("SELECT id FROM codigo WHERE datatriagem LIKE '$mes%' AND grupo = 'C' UNION ALL SELECT id FROM lg WHERE datatriagem LIKE '$mes%' AND grupo = 'C'")->fetchAll(PDO::FETCH_ASSOC));
$refrigeracao = Count(conex()->query("SELECT id FROM codigo WHERE datatriagem LIKE '$mes%' AND grupo = 'R' UNION ALL SELECT id FROM lg WHERE datatriagem LIKE '$mes%' AND grupo = 'R'")->fetchAll(PDO::FETCH_ASSOC));
$outros = Count(conex()->query("SELECT id FROM codigo WHERE datatriagem LIKE '$mes%' AND grupo = 'A' UNION ALL SELECT id FROM lg WHERE datatriagem LIKE '$mes%' AND grupo = 'A'")->fetchAll(PDO::FETCH_ASSOC));
$QTD = $lavanderia+$coccao+$refrigeracao+$outros;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PROCESSO PADRAO////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#DEFINE A TABELA DE ORIGEM DO ID
$_SESSION['id'] = strtoupper(substr($_POST['smart'],0,12));
// $_SESSION['id'] = strtoupper(substr('WC0378RF0023',0,12));
// $_SESSION['banco'] = ((substr($_SESSION['id'],0,1)) == 'W') ? 'codigo' : 'lg';


#NOVO METODO DE BUSCA BANCO
$_SESSION['banco'] = banco($_SESSION['id']);


#CERTIFICA SE EXISTE
if($_SESSION['banco'] != '')
{
	$_SESSION['cli'] = conex()->query("SELECT datatriagem,INICIO_TRIAGEM,smart,modelo,linha,etapa,serie1,fabricante,voltagem,cor,enge,temperatura,rma,obs,ALTA,CARGA FROM {$_SESSION['banco']} WHERE smart = '{$_SESSION['id']}'")->fetch(PDO::FETCH_ASSOC);
	$tip = $_SESSION['cli']['modelo'];

	//LE A TABELA MODELO PARA PROCURAR ALTERNATIVOS
	// $base = substr($tip, 0, 7);
	// if(conex()->query("SELECT modelo, tensao FROM modelo WHERE modelo LIKE '$base%' AND tensao = '127 V'")->fetch(PDO::FETCH_ASSOC))
	// {
	// 	$_SESSION['base127'] = conex()->query("SELECT modelo, tensao FROM modelo WHERE modelo LIKE '$base%' AND tensao = '127 V'")->fetch(PDO::FETCH_ASSOC)['modelo'];
	// }
	// else
	// {
	// 	$_SESSION['base127'] = '';
	// }
	
	// if(conex()->query("SELECT modelo, tensao FROM modelo WHERE modelo LIKE '$base%' AND tensao = '220 V'")->fetch(PDO::FETCH_ASSOC))
	// {
	// 	$_SESSION['base220'] = conex()->query("SELECT modelo, tensao FROM modelo WHERE modelo LIKE '$base%' AND tensao = '220 V'")->fetch(PDO::FETCH_ASSOC)['modelo'];
	// }
	// else
	// {
	// 	$_SESSION['base127'] = '';
	// }

	//COR ORIGINAL
	$original = conex()->query("SELECT cor,procel FROM modelo WHERE modelo LIKE '$tip'")->fetch(PDO::FETCH_ASSOC);


	#DEFINIR HISTORICO
	$historico = ($_SESSION['cli']['datatriagem'] != '0000-00-00 00:00:00') ? date_format((date_create($_SESSION['cli']['datatriagem'])),"d/m/Y") : false;


	#INICIO TRIAGEM
	$recente =  $_SESSION['cli']['INICIO_TRIAGEM'];

	#CRIA UM FORMATO DE DATA
	$NADA = '';
	$create = ($recente == '0000-00-00 00:00:00') ? date_create($NADA) : date_create($recente);

	#AJUSTA O FORMATO DE DATA
	$ini = date_format($create,"Y-m-d");
		
	$datetime1 = date_create($ini);
	$datetime2 = date_create($dia);
	$interval = date_diff($datetime1, $datetime2);
	$_SESSION['aging'] = $interval->format('%a');

	#ATENÇÂO: SE MUDAR O ICONE DA SESSÂO NÂO VAI GRAVAR
	$_SESSION['caso'] = '<i class="fas fa-thumbs-up fa-lg"></i> ';
	$fala = 'Produto OK. Aguardando definição!';

	//LER O SAC 

	$sac =  conex()->query("SELECT id, QUEIXA, CONSTATADO, PECA FROM sac WHERE SMART = '{$_SESSION['id']}' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
	$queixa = (isset($sac['QUEIXA'])) ? $sac['QUEIXA'] : '';
	$constatado = (isset($sac['CONSTATADO'])) ? $sac['CONSTATADO'] : '';
	$peca = (isset($sac['PECA'])) ? $sac['PECA'] : '';

	// $falas = (!empty($fala)) ? htmlspecialchars($fala) : htmlspecialchars('Ops. atenção no preenchimento');
	// $falas=rawurlencode($falas);
	// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
	// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
	$player = '';

	//PDF
	$pdf = file_exists("../VISTA/{$_SESSION['cli']['modelo']}.pdf") ? true : false;


	// CRIAR OBJETO
	$_SESSION['cli'] = array_merge($_SESSION['cli'],(array('caso'=>$_SESSION['caso'], 'pdf'=>$pdf, 'aging'=>$_SESSION['aging'], 'fala'=>$fala, 'queixa'=>$queixa, 'constatado'=>$constatado, 'peca'=>$peca, 'historico'=>$historico, 'player'=>$player, 'qtd'=>$QTD, 'lavanderia'=>$lavanderia, 'coccao'=>$coccao, 'refrigeracao'=>$refrigeracao, 'outros'=>$outros, 'original'=>$original)));

}

//ID NAO ENCONTRADO
else
{
	$_SESSION['caso'] = '<i class="fas fa-frown fa-lg mr-2"></i>';
	$fala = "Atenção ".NOME_MINUSCULO.", ID não reconhecido!";

	// $falas = (!empty($fala)) ? htmlspecialchars($fala) : htmlspecialchars('Ops. atenção no preenchimento');
	// $falas=rawurlencode($falas);
	// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
	// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
	$player = '';

	// CRIAR OBJETO
	$_SESSION['cli'] = array_merge(array('caso'=>$_SESSION['caso'], 'fala'=>$fala, 'player'=>$player));
	
}

//ENVIO JSON
echo json_encode($_SESSION['cli'],JSON_UNESCAPED_UNICODE);

?>