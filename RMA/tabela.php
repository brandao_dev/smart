<?php

// INCLUSO NA LEITURA.PHP, RECEBER.PHP E AGENDA.PHP

$table = null;
$sacs = null;
$data = null;
$_SESSION['sacs'] = null;
$local = null;

    // RELEITURA RENOVAR
    $data = conex()->query("SELECT ALTERACAO, ENDERECO, BAIRRO, NUMERO, CIDADE, SMART, SAC, MODELO, LOCAL, COLETA, ABERTO FROM sac WHERE STATUS LIKE '11.LABORATORIO' ORDER BY ALTERACAO DESC");
     
    foreach($data as $dat)
    {
        // FILTRAR O QUE ESTIVER AGUARDANDO REPARO / RMA NA ETAPA DO 
        if(conex()->query("SELECT id FROM codigo WHERE smart = '{$dat['SMART']}' AND etapa = '15.RMA'")->fetch(PDO::FETCH_ASSOC))
        {
            $sacs[] = $dat['SAC'];
            $local = str_replace(" ", "+", $dat['ENDERECO'].",".$dat['NUMERO']."-".$dat['BAIRRO']."-".$dat['CIDADE']);
            $table .= 
            "<tr>
                <td>
                    <a class='btn btn-purple shadow' href='../CONSULTA/BUSCAR.php?sac={$dat['SMART']}' target='blank'>{$dat['SMART']}</a>
                </td>
                <td class='d-none d-sm-table-cell'>
                    <a class='btn btn-warning shadow' href='../SAC/SAC.php?sac={$dat['SAC']}' target='blank'>{$dat['SAC']}</a>
                </td>
                <td class='d-none d-lg-table-cell'>
                    ".$dat['LOCAL']."
                </td>
                <td class='d-none d-md-table-cell'>".$dat['ALTERACAO']."</td>
                <td>
                    <input class='form-control' name='{$dat['SAC']}' value='".$dat['COLETA']."' autocomplete='off' placeholder='Agendado para...'>
                </td>
                <td class='d-none d-xl-table-cell'>
                    <a class='btn btn-pink shadow' href='https://www.google.com.br/maps/dir/R.+Galatea,+1560+-+Carandiru,+São+Paulo+-+SP,+02068-000,+Brasil/".$local."' target='_blank'>".$dat['ENDERECO'].", ".$dat['CIDADE']."</a>
                </td>
                <td class='d-none d-xl-table-cell'>
                    ".$dat['ABERTO']."X
                </td>
            </tr>";
        }
    }

    $_SESSION['sacs'] = $sacs;
    
?>