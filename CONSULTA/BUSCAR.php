<?php include("../CORE/Header2.php");?>
<script src="AJAX.js?<?php echo filemtime("AJAX.js");?>"></script>
<style>
    @media screen{
        .modal-dialog {
          max-width: 90%; /* New width for default modal */
        }
    }
</style>
<!-- PLAYER DE AUDIO -->
<div style="display: none" id="player"></div>

<!-- BARCODE 1 -->
<input id=barcode type=text style="display: none">

<!-- SEGUNDA LINHA DE FRASE DE APOIO *** /////////////////////////////////////////////////////////////////////////////////////// -->
<div class="alert mt-3 alert-info" id="caso"><i class="fas fa-unlock-alt fa-lg mr-2"></i>Pronto para nova entrada!</div>

<!-- 
8888888                            888    
  888                              888    
  888                              888    
  888   88888b.  88888b.  888  888 888888 
  888   888 "88b 888 "88b 888  888 888    
  888   888  888 888  888 888  888 888    
  888   888  888 888 d88P Y88b 888 Y88b.  
8888888 888  888 88888P"   "Y88888  "Y888 
                 888                      
                 888                      
				 888                      
-->
<form action="" id="vai" method="GET" name="vai">

<div class="row">
	<div class="mt-3 col-12 col-sm-4">
		<input type="text" list="serial" class="font-weight-bold form-control form-control-lg" placeholder="ID-Smart/Serial" name="busca" id="smart" autofocus autocomplete="off" required value="<?php if(isset($_GET['smart']))echo $_GET['smart'] ?>">
		<datalist id="serial">
			<?php
				$leitura = conex()->query("SELECT id,smart,serie1 FROM codigo UNION ALL SELECT id,smart,serie1 FROM lg ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);
				foreach($leitura as $res)
				{
					echo "<option value='{$res['smart']}'/>";
					echo "<option value='{$res['serie1']}'/>";
				}
			?>
		</datalist>	
	</div>
	<div class="mt-3 col-6 col-sm-4">
		<button class="btn btn-danger btn-block shadow" id="go" type="submit" onclick="vibrate(100),play()"><h5><i class="fas fa-check-square mr-2"></i>Confirma</h5></button>
	</div>
	<div class="mt-3 col-6 col-sm-4">
		<button class="btn btn-warning btn-block shadow" onclick="vibrate(100),play(),getScan()"><h5><i class="fas fa-camera mr-2"></i>Câmera</h5></button>
	</div>
</div>

</form>
<!-- 
8888888b.                        888          888             
888   Y88b                       888          888             
888    888                       888          888             
888   d88P 888d888  .d88b.   .d88888 888  888 888888  .d88b.  
8888888P"  888P"   d88""88b d88" 888 888  888 888    d88""88b 
888        888     888  888 888  888 888  888 888    888  888 
888        888     Y88..88P Y88b 888 Y88b 888 Y88b.  Y88..88P 
888        888      "Y88P"   "Y88888  "Y88888  "Y888  "Y88P"  
                                                               -->

<div class="row mt-3">
	<div class="col text-left">
		<h3 class="alert alert-brown"><i class="fas fa-plug mr-2"></i>Produto</h3>
	</div>
</div>
<div class="card shadow">
  <div class="card-body" style="background-color:#f2ede9;">
	<div class="row">
		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold bg-primary"><i class="fas fa-barcode-read mr-2"></i>Smart</span>
			</div>
			<input type="text" class="form-control" id="smarte" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-palette mr-2"></i>Cor</span>
			</div>
			<input type="text" class="form-control" id="cor" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-fingerprint mr-2"></i>Série</span>
			</div>
			<input type="text" class="form-control" id="serie1" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-bolt mr-2"></i>Voltagem</span>
			</div>
			<input type="text" class="form-control" id="voltagem" >		
		</div>
		<div class="col-12 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-align-left mr-2"></i>Linha</span>
			</div>
			<input type="text" class="form-control" id="linha" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-industry-alt mr-2"></i>Fabricante</span>
			</div>
			<input type="text" class="form-control" id="fabricante" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold bg-warning"><i class="fas fa-pencil-ruler mr-2"></i>Modelo</span>
			</div>
			<input type="text" class="form-control" id="modelo" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-user-hard-hat mr-2"></i>Engenharia</span>
			</div>
			<input type="text" class="form-control" id="enge" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold bg-success"><i class="fas fa-shoe-prints mr-2"></i>Etapa</span>
			</div>
			<input type="text" class="form-control" id="etapa" >		
		</div>
		<div class="col-12 col-lg-6 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="far fa-calendar-alt mr-2"></i>Criado em:</span>
			</div>
			<input type="text" class="form-control" id="data" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-6 col-xl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-user-plus mr-2"></i>Criado por:</span>
			</div>
			<input type="text" class="form-control" id="usuario" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-boxes mr-2"></i>Lote</span>
			</div>
			<input type="text" class="form-control" id="nlote" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 col-xxl-2 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-award mr-2"></i>Grade</span>
			</div>
			<input type="text" class="form-control" id="grade" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="far fa-clipboard mr-2"></i>Nota</span>
			</div>
			<input type="text" class="form-control" id="nota" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-2 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-chess mr-2"></i>Grupo</span>
			</div>
			<input type="text" class="form-control" id="grupo" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-passport mr-2"></i>EAN</span>
			</div>
			<input type="text" class="form-control" id="ean" >		
		</div>
		<div class="col-12 col-lg-4 col-xl-4 col-xxl-2 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-hamburger mr-2"></i>WebSac</span>
			</div>
			<input type="text" class="form-control" id="websac" >		
		</div>
	</div>
  </div>
</div>
<!-- 
.d8888b.                            d8b                   
d88P  Y88b                           Y8P                   
Y88b.                                                      
 "Y888b.    .d88b.  888d888 888  888 888  .d8888b  .d88b.  
    "Y88b. d8P  Y8b 888P"   888  888 888 d88P"    d88""88b 
      "888 88888888 888     Y88  88P 888 888      888  888 
Y88b  d88P Y8b.     888      Y8bd8P  888 Y88b.    Y88..88P 
 "Y8888P"   "Y8888  888       Y88P   888  "Y8888P  "Y88P"   
-->
<div class="row mt-3">
	<div class="col text-left">
		<h3 class="alert alert-primary"><i class="fas fa-wrench mr-2"></i>Serviço</h3>
	</div>
</div>
<div class="card shadow">
  <div class="card-body" style="background-color:#e6fafa;">
	<div class="row">
		<div class="col-12 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold bg-success"><i class="far fa-clipboard-list-check mr-2"></i>Descrição</span>
			</div>
			<textarea class="form-control" id="obs" ></textarea>		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-tools mr-2"></i>Reparo</span>
			</div>
			<input type="text" class="form-control" id="datatriagem" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-users-cog mr-2"></i>Reparo por</span>
			</div>
			<input type="text" class="form-control" id="usertriagem" >		
		</div>
		<div class="col-12 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-cloud-sun-rain mr-2"></i>Data RMA</span>
			</div>
			<input type="text" class="form-control" id="data_rma" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold bg-warning"><i class="fas fa-band-aid mr-2"></i>RMA</span>
			</div>
			<input type="text" class="form-control" id="rma" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-paint-roller mr-2"></i>Pintura</span>
			</div>
			<input type="text" class="form-control" id="data_pintura" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-spray-can mr-2"></i>Funilaria</span>
			</div>
			<input type="text" class="form-control" id="pintura" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-paint-brush mr-2"></i>Status Pintura</span>
			</div>
			<input type="text" class="form-control" id="pnt_local" >		
		</div>
		<div class="col-12 col-lg-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="far fa-flag mr-2"></i>Start Qualidade</span>
			</div>
			<input type="text" class="form-control" id="qualidade" >		
		</div>
		<div class="col-12 col-lg-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-medal mr-2"></i>Fim Qualidade</span>
			</div>
			<input type="text" class="form-control" id="obs_quali" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-user-tie mr-2"></i>Auditor</span>
			</div>
			<input type="text" class="form-control" id="user_qualidade" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-container-storage mr-2"></i>Alta</span>
			</div>
			<input type="text" class="form-control" id="alta" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class='fal fa-temperature-frigid mr-2'></i>Temperatura</span>
			</div>
			<input type="text" class="form-control" id="temperatura" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-shower mr-2"></i>Higiene</span>
			</div>
			<input type="text" class="form-control" id="higiene" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-balance-scale mr-2"></i>Limpeza</span>
			</div>
			<input type="text" class="form-control" id="limpeza" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-user-nurse mr-2"></i>Agente</span>
			</div>
			<input type="text" class="form-control" id="higienico" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-4 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-gas-pump mr-2"></i>Carga</span>
			</div>
			<input type="text" class="form-control" id="carga" >		
		</div>
		<div class="col-12 col-sm-6 col-lg-6 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-curling mr-2"></i>Compressor</span>
			</div>
			<input type="text" class="form-control" id="compressor" >		
		</div>
		<div class="col-12 col-lg-6 col-xl-4 col-xxl-3 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-cogs mr-2"></i>Peça</span>
			</div>
			<input type="text" class="form-control" id="peca" >		
		</div>
	</div>
  </div>
</div>
<!-- 
.d8888b.           d8b      888          
d88P  Y88b          Y8P      888          
Y88b.                        888          
 "Y888b.    8888b.  888  .d88888  8888b.  
    "Y88b.     "88b 888 d88" 888     "88b 
      "888 .d888888 888 888  888 .d888888 
Y88b  d88P 888  888 888 Y88b 888 888  888 
 "Y8888P"  "Y888888 888  "Y88888 "Y888888 
  -->
<div id="expedicao">
</div>

<!-- <div class="card shadow">
  <div class="card-body">
	<div class="row">
		<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-truck-loading mr-2"></i>Transferência</span>
			</div>
			<input type="text" class="form-control" id="dataexpedicao" readonly>		
		</div>
		<div class="col-12 col-sm-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-location mr-2"></i>Local</span>
			</div>
			<input type="text" class="form-control" id="localsaida" readonly>		
		</div>
		<div class="col-12 col-sm-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-walking mr-2"></i>Expedidor</span>
			</div>
			<input type="text" class="form-control" id="userexpedicao" readonly>		
		</div>
		<div class="col-12 col-sm-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-th-list mr-2"></i>Minuta</span>
			</div>
			<input type="text" class="form-control" id="minuta" readonly>		
		</div>
		<div class="col-12 col-sm-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-pallet mr-2"></i>Palete</span>
			</div>
			<input type="text" class="form-control" id="palete" readonly>		
		</div>
		<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold"><i class="fas fa-stopwatch mr-2"></i>Atualizado em:</span>
			</div>
			<input type="text" class="form-control" id="alteracao" readonly>		
		</div>
	</div>
  </div>
</div>   -->
<!-- 
8888888                                                            
  888                                                              
  888                                                              
  888   88888b.d88b.   8888b.   .d88b.   .d88b.  88888b.  .d8888b  
  888   888 "888 "88b     "88b d88P"88b d8P  Y8b 888 "88b 88K      
  888   888  888  888 .d888888 888  888 88888888 888  888 "Y8888b. 
  888   888  888  888 888  888 Y88b 888 Y8b.     888  888      X88 
8888888 888  888  888 "Y888888  "Y88888  "Y8888  888  888  88888P' 
                                    888                            
                               Y8b d88P                            
								"Y88P"                              
-->
<div id="im">	
</div>  
<!-- 
8888888888                   888               888 d8b      888          
888                          888               888 Y8P      888          
888                          888               888          888          
8888888    888  888 88888b.  888  .d88b.   .d88888 888  .d88888  8888b.  
888        `Y8bd8P' 888 "88b 888 d88""88b d88" 888 888 d88" 888     "88b 
888          X88K   888  888 888 888  888 888  888 888 888  888 .d888888 
888        .d8""8b. 888 d88P 888 Y88..88P Y88b 888 888 Y88b 888 888  888 
8888888888 888  888 88888P"  888  "Y88P"   "Y88888 888  "Y88888 "Y888888 
                    888                                                  
                    888                                                  
                    888                                                                              
-->
<div class="row">
	<div class="col-12 col-lg-6">
		<div id="vista">
		</div>
	</div>
	

<!-- 
888888b.            888          888    d8b               
888  "88b           888          888    Y8P               
888  .88P           888          888                      
8888888K.   .d88b.  888  .d88b.  888888 888 88888b.d88b.  
888  "Y88b d88""88b 888 d8P  Y8b 888    888 888 "888 "88b 
888    888 888  888 888 88888888 888    888 888  888  888 
888   d88P Y88..88P 888 Y8b.     Y88b.  888 888  888  888 
8888888P"   "Y88P"  888  "Y8888   "Y888 888 888  888  888  
-->
	<div class="col-12 col-lg-6">
		<div id="boletim">
		</div>
	</div>
</div>

<!-- 
.d8888b.                    
d88P  Y88b                   
Y88b.                        
 "Y888b.    8888b.   .d8888b 
    "Y88b.     "88b d88P"    
      "888 .d888888 888      
Y88b  d88P 888  888 Y88b.    
 "Y8888P"  "Y888888  "Y8888P  
-->

<div id="sc">
</div>
					
<!-- 
888                        
888                        
888                        
888       .d88b.   .d88b.  
888      d88""88b d88P"88b 
888      888  888 888  888 
888      Y88..88P Y88b 888 
88888888  "Y88P"   "Y88888 
                       888 
                  Y8b d88P 
				   "Y88P"   -->
<div class="row mt-3">
	<div class="col text-left">
		<h3 class="alert alert-dark"><i class="fas fa-history mr-2"></i>Histórico</h3>
	</div>
</div>
<div id="log">	
</div>

	
<?PHP include('../CORE/Footer2.php'); ?>