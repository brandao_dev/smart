<?php include('../CORE/Header2.php');?>
<style>
     .nav-tabs > a:hover{
     background-color: #08F;
     color: #fff;
     }
     .nav-tabs > a{
     color: #aaa;
     }
     .nav-tabs > a.active{
     color: #000;
     }
</style>

<script src="Permissao.js"></script>
<div id="player" style="display: none;"></div>


<!-- 
       d8888                   d8b          
      d88888                   Y8P          
     d88P888                                
    d88P 888 88888b.   .d88b.  888  .d88b.  
   d88P  888 888 "88b d88""88b 888 d88""88b 
  d88P   888 888  888 888  888 888 888  888 
 d8888888888 888 d88P Y88..88P 888 Y88..88P 
d88P     888 88888P"   "Y88P"  888  "Y88P"  p
             888                            
             888                            
             888                            
 P-->

<div class="row">
    <div class="col-12">
        <!-- SEGUNDA LINHA DE FRASE DE APOIO *** //////////////////////// -->
        <div class="alert mt-3 alert-info font-weight-bold btn-block" id="caso"><i class="fas fa-handshake fa-lg mr-2"></i><?php echo 'Olá Sr. '.$user.'!';?></div>
    </div>
</div>

<form action="" method="post" id="form">
<div class="row text-left">
     <div class="col-12 col-sm-6 col-xl-4">
          <label for="nome"><i class="fas fa-user-edit mr-2"></i>Nome</label>
          <select name="nome" placeholder="Escolha..." type="text" class="form-control form-control-lg font-weight-bold" autocomplete="off" required autofocus id="nome">
               <option value="">Escolha...</option>
               <?php
               $sac = conex()->query("SELECT nome FROM usuarios ORDER BY nome");
               foreach($sac as $name)
               {
                    echo "<option value='{$name['nome']}'>{$name['nome']}</option>";
               }
          ?>
          </select>
     </div>
     <div class="col-12 col-sm-6 col-xl-3">
     <label for="senha"><i class="fas fa-key mr-2"></i>Senha</label>
          <input type="text" class="form-control form-control-lg font-weight-bold" autocomplete="off" required placeholder="****" id="senha" name="senha">
     </div>
     <div class="col-12 col-sm-6 col-xl-3">
          <label for=""><i class="fas fa-toggle-on mr-2"></i>Status</label><br>
          <div class="btn-group btn-group-toggle btn-block" data-toggle="buttons">
               <label class="btn btn-outline-purple btn-lg shadow active" for="active">
                    <input type="radio" name="ativo" value="1" id="active" autocomplete="off" onclick="play(), vibrate()" checked><i class="fas fa-user-check mr-2"></i>Ativo
               </label>
               <label class="btn btn-outline-purple btn-lg shadow" for="blocked">
                    <input type="radio" name="ativo" value="0" id="blocked" autocomplete="off" onclick="play(), vibrate()"><i class="fas fa-user-slash mr-2"></i>Inativo
               </label>
          </div>
     </div>
     <div class="col-12 col-sm-6 col-xl-2">
          <label for=""><i class="fas fa-flag-checkered mr-2"></i>Concluir</label>
          <button type="submit" class="btn btn-block btn-lg btn-success shadow" onclick="play(), vibrate()"><i class="fas fa-vote-yea mr-2"></i>Gravar</button>
     </div>
</div>

<!-- GERAR MENUS CONFORME GRUPO DO SQL -->
<nav class="font-weight-bold h5 mt-4">
     <div class="nav nav-tabs bg-dark" id="nav-tab" role="tablist">
          <?php 
               $i = '';
               $grupo = conex()->query("SELECT DISTINCT(grupo),ico1 FROM menu ORDER BY id")->fetchAll(PDO::FETCH_ASSOC);
               $menu = '';
               foreach($grupo as $opt)
               {
                    $active = (empty($i)) ? 'active' : '';$i++;
                    echo '<a class="nav-item nav-link '.$active.'" id="'.$opt['grupo'].'" data-toggle="tab" href="#nav-'.$opt['grupo'].'" role="tab" aria-controls="nav-'.$opt['grupo'].'" aria-selected="true"><i class="'.$opt['ico1'].' mr-2"></i>'.$opt['grupo'].'</a>';

                    $links = conex()->query("SELECT code, opcao, ico2 FROM menu WHERE grupo = '{$opt['grupo']}' ORDER BY lista,id")->fetchAll(PDO::FETCH_ASSOC);
                    $opco = '';

                         foreach($links as $link)
                         {
                              $opco .= "
                              <div class='col-12 btn-group-toggle'>
                                   <label class='btn btn-lg btn-outline-secondary mb-3 shadow' for='".$link['code'].$link['code']."'>
                                        <input type='checkbox' name='padrao' id='".$link['code'].$link['code']."' value='".$link['code']."' onclick='play(),radio(\"".$link['code']."\")' autocomplete='off' disabled='disabled'><i class='fas fa-home'></i>
                                   </label>
                                   <label class='btn btn-lg btn-outline-secondary mb-3 shadow' for='".$link['code']."'>
                                        <input type='checkbox' name='permissao[]' value='".$link['code']."' id='".$link['code']."' onclick='play(),reabilita(\"".$link['code']."\")'><i class='".$link['ico2']." mr-2'></i>".$link['opcao']."
                                   </label>
                              </div>";
                         }

                    $menu .= '
                    <div class="tab-pane fade show '.$active.'" id="nav-'.$opt['grupo'].'" role="tabpanel" aria-labelledby="nav-'.$opt['grupo'].'-tab">
                         <div class="text-left row ml-1 mt-3" data-toggle="buttons">'.$opco.'
                         </div>
                    </div>';
               }
          ?>
     </div>
</nav>
<div class="tab-content shadow" id="nav-tabContent">
     <?php echo $menu ?>
</div>
</form>

<?php include('../CORE/Footer2.php'); ?>