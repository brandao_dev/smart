<?php
	include("../CORE/CONN.php");
	//O campo usuário e senha preenchido entra no if para validar
	if((!empty($_POST['nome'])) && (!empty($_POST['senha'])))
	{
		$usuario = strtoupper($_POST['nome']);
		$senha = $_POST['senha'];

		//Buscar na tabela usuario o usuário que corresponde com os dados digitado no formulário
		$resultado = conex()->query("SELECT * FROM usuarios WHERE nome = '$usuario' && senha = '$senha'")->fetch(PDO::FETCH_ASSOC);

		//Encontrado um usuario na tabela usuário com os mesmos dados digitado no formulário
		if($resultado == true)
		{
			if($resultado['status'] == 1)
			{
				$_SESSION['permissao'] = $resultado['permissao'];
				$_SESSION['padrao'] = $resultado['padrao'];
				$_SESSION['MM_Username'] = $resultado['nome'];
				$_SESSION['MM_UserGroup'] = $resultado['grupo'];
				// $_SESSION['token'] = $resultado['token'];
				// $_SESSION['chat'] = $resultado['chat'];

				//PAGINA PADRÃO
				include ('../CORE/menu2.php');
				$local = conex()->query("SELECT url FROM menu WHERE code = '{$resultado['padrao']}'")->fetch(PDO::FETCH_ASSOC)['url'];

				function processa($where)
				{
					$celula = '';
						foreach($where as $tmp)
						{
							#AJUSTA O FORMATO DE DATA
							$intervalo = strtotime($tmp['ALTERACAO']) - strtotime(date('Y-m-d'));
							$dias = abs(floor($intervalo/(60*60*24)));
							if(($dias >= 30)&&($dias < 60))
							{
								$css = 'bg-warning';
								$ico = '<i class="fas fa-frown-open mr-2"></i>';
							}
							elseif(($dias >= 60)&&($dias < 90))
							{
								$css = 'bg-orange';
								$ico = '<i class="fas fa-dizzy mr-2"></i>';
							}
							elseif($dias >= 90)
							{
								$css = 'bg-danger';
								$ico = '<i class="fas fa-skull mr-2"></i>';
							}
							else
							{
								$css = '';
								$ico = '<i class="fas fa-grin mr-2"></i>';
							}
							//TESTE CONEXÃO (APURAR ERROS)
							$temp = $tmp['SMART'];
							if($linha = conex()->query("SELECT linha FROM codigo WHERE smart = '$temp' UNION ALL SELECT linha FROM lg WHERE smart = '$temp'")->fetch(PDO::FETCH_ASSOC))
							{
								$celula .= "<tr class=".$css."><td><a href='../SAC/SAC.php?sac=".$tmp['SAC']."'>".$tmp['SAC']."</a></td><td class='d-none d-md-table-cell'>".$tmp['SMART']."</td><td>".$linha['linha']."</td><td>".$ico.$dias."</td><td class='d-none d-md-table-cell'>".$tmp['STATUS']."</td></tr>";
							}
						}
						return  '
								<h1>Olá '.$_SESSION['MM_Username'].'!</h1>
								<h5>Confira abaixo os produtos em LABORATÓRIO e/ou AGENDADO:</h5>
								<table class="table table-striped table-bordered table-hover table-sm mt-3">
									<thead class="thead-dark">
										<th>SAC</th><th class="d-none d-md-table-cell">ID SMART</th><th>LINHA</th><th>PRAZO</th><th class="d-none d-md-table-cell">STATUS</th>
									</thead>
									<tbody>'.$celula.'</tbody>
								</table>
								<div class="text-center">
								</div>';

				}

				if(($resultado['grupo'] == 'ADM')||($resultado['grupo'] == 'AGE')||($resultado['grupo'] == 'GALATEA')||($resultado['grupo'] == 'SAC'))
				{
					$where = conex()->query("SELECT id,ALTERACAO,SAC,SMART,STATUS FROM sac WHERE (STATUS LIKE '11.%' OR STATUS LIKE '04.%') ORDER BY id");	
					if($where->rowCount() > 0)
					{
						$div = processa($where);
						$fala = 'Existem alguns produtos em LABORATÓRIO precisando da sua atenção';
						$logon = 4;
						$erro = '';
					}
					else
					{
						$div = '';
						$fala = 'Parabéns. Não existe nenhum produto devolvido em análise';
						$logon = 3;
						$erro = '';
					}
					
				}
				elseif($resultado['grupo'] == 'LOJA')
				{
					$where = conex()->query("SELECT id,ALTERACAO,SAC,SMART FROM sac WHERE STATUS LIKE '11.%' AND LOCAL = '$usuario' ORDER BY id");
					if($where->rowCount() > 0)
					{
						$div = processa($where);
						$fala = 'Existem alguns produtos em LABORATÓRIO precisando da sua atenção';
						$logon = 4;
						$erro = '';
					}
					else
					{
						$div = '';
						$fala = 'Parabéns. Não existe nenhum produto devolvido em análise';
						$logon = 3;
						$erro = '';
					}
				}
				else
				{
					$erro  = '';
					$div = '';
					$fala = '';
					$logon = 2;
				}
			}
			else
			{
				//USUARIO BLOQUEADO
				$erro = 'Usuario Bloqueado';
				$div = '';
				$fala = 'Usuario Bloqueado';
				$logon = 1;
				$local = null;
			}
		}
		else
		{
			//USUARIO OU SENHA INVALIDOS
				$erro = 'Usuario ou Senha inválido';
				$div = '';
				$fala = 'Usuario ou Senha inválido';
				$logon = 1;
				$local = null;
		}
    }
    else
    {
		//USER OU SENHA NAO INFORMADO
				$erro = 'Usuario ou Senha não informados';
				$div = '';
				$fala = 'Usuario ou Senha não informados';
				$logon = 1;
				$local = null;
    }


//FALA
// if(!empty($fala))
// {
// 	$falas = htmlspecialchars($fala);
// 	$falas=rawurlencode($falas);
// 	$html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// 	$player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
// }
// else
// {
	$player = '';
// }

//JSON
$soma = ['div'=>$div, 'local'=>$local, 'erro'=>$erro, 'logon'=>$logon, 'fala'=>$player];
print json_encode($soma,JSON_UNESCAPED_UNICODE);

?>