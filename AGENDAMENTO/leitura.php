  <?php  include ('../CORE/Header2.php');
    $ano = date('Y');
    $hoje = date('d/m/Y');
    $classe = null;

    // LISTA ATENDIMENTOS
    $sql = conex()->query("SELECT id, ALTERACAO, AGENDA, SAC, BAIRRO, CIDADE, ENDERECO, NUMERO, MODELO, ATENDIDO, STATUS FROM sac WHERE STATUS = '01.ABERTO' OR STATUS = '04.AGENDADO' OR STATUS = '02.NEGADO' OR STATUS = '09.TRATATIVA' OR (SUBSTR(AGENDA,-10,10) LIKE '$hoje' AND ATENDIDO = 1) ORDER BY STATUS")->fetchAll(PDO::FETCH_ASSOC);

    $irregulares = Count(conex()->query("SELECT id FROM sac WHERE STATUS = '01.ABERTO' OR STATUS = '02.NEGADO' OR STATUS = '09.TRATATIVA' OR (STATUS = '04.AGENDADO' AND SUBSTR(AGENDA,-4,4) NOT LIKE '$ano')")->fetchAll(PDO::FETCH_ASSOC));

    $agendados = Count(conex()->query("SELECT id FROM sac WHERE STATUS = '04.AGENDADO' AND SUBSTR(AGENDA,-4,4) LIKE '$ano'")->fetchAll(PDO::FETCH_ASSOC));

    $atendidos = Count(conex()->query("SELECT id FROM sac WHERE (SUBSTR(AGENDA,-10,10) LIKE '$hoje' AND ATENDIDO = 1)")->fetchAll(PDO::FETCH_ASSOC));

    $pendentes = $irregulares - $atendidos;

    $tabela = null;
    foreach($sql as $linha)
    {
      $classe = ($linha['ATENDIDO'] == 1 && substr($linha['AGENDA'],-10) == $hoje) ? 'class="bg-warning"' : '';
      $agenda = (substr($linha['AGENDA'],-4) != $ano) ? '---' :  substr($linha['AGENDA'],-10);
      $color = ($linha['STATUS'] != '04.AGENDADO' || substr($linha['AGENDA'],-4,4) != $ano) ? "danger" : 'success'; 
      // BUSCAR BOLETIM
      $boletim = file_exists("../BOLETIM/{$linha['MODELO']}.pdf") ? "<button class='btn btn-success shadow' onclick='abre(\"BOLETIM\",\"{$linha['MODELO']}\")'>SIM</button>" : "<button class='btn btn-danger shadow' onclick='upload(\"b{$linha['id']}\",\"BOLETIM\",\"{$linha['MODELO']}\")'>NÃO</button>";
      $vista = file_exists("../VISTA/{$linha['MODELO']}.pdf") ? "<button class='btn btn-success' onclick='abre(\"VISTA\",\"{$linha['MODELO']}\")'>SIM</button>" : "<button class='btn btn-danger' onclick='upload(\"{$linha['id']}\",\"VISTA\",\"{$linha['MODELO']}\")'>NÃO</button>";
      $tabela .= 
        "<tr ".$classe.">
          <th>".$linha['STATUS']."</th>
          <td>".$agenda."</td>
          <td><a class='btn btn-".$color." shadow' href='../SAC/SAC.php?sac={$linha['SAC']}' target='blank'>{$linha['SAC']}</a></td>
          <td><a class='btn btn-primary shadow' href='https://www.google.com.br/maps/dir/R.+Galatea,+1560+-+Carandiru,+São+Paulo+-+SP,+02068-000,+Brasil/{$linha['ENDERECO']},{$linha['NUMERO']},{$linha['BAIRRO']},{$linha['CIDADE']}' target='blank'>{$linha['BAIRRO']}, {$linha['CIDADE']}</a></td>
          <td><div id='b".$linha['id']."'>{$boletim}</div></td>
          <td><div id='{$linha['id']}'>{$vista}</div></td>
        </tr>";
    }
?>