<?php include("../CORE/Header2.php");?>     
<!--Sweet Alert 2 -->     
<!--Código custom -->          
<script src="main.js"></script>   
<div id="player" style="display: none;"></div>

<!-- 
       d8888                   d8b          
      d88888                   Y8P          
     d88P888                                
    d88P 888 88888b.   .d88b.  888  .d88b.  
   d88P  888 888 "88b d88""88b 888 d88""88b 
  d88P   888 888  888 888  888 888 888  888 
 d8888888888 888 d88P Y88..88P 888 Y88..88P 
d88P     888 88888P"   "Y88P"  888  "Y88P"  
             888                            
             888                            
             888                            
 -->

<div class="row">
    <div class="col-8">
        <!-- SEGUNDA LINHA DE FRASE DE APOIO *** //////////////////////// -->
        <div class="alert mt-3 alert-info font-weight-bold btn-block" id="caso"><i class="fas fa-handshake fa-lg mr-2"></i><?php echo 'Olá Sr. '.$user.'!';?></div>
    </div>
    <div class="col-4">
        <!-- TOTAL *** /////////////////////////// -->
        <button class="btn btn-lg btn-block btn-warning mt-3 font-weight-bold" id="total"><i class="fas fa-shopping-cart mr-2"></i>0</button>
    </div>
</div>

<!-- 

888b     d888 d8b                   888             
8888b   d8888 Y8P                   888             
88888b.d88888                       888             
888Y88888P888 888 88888b.  888  888 888888  8888b.  
888 Y888P 888 888 888 "88b 888  888 888        "88b 
888  Y8P  888 888 888  888 888  888 888    .d888888 
888   "   888 888 888  888 Y88b 888 Y88b.  888  888 
888       888 888 888  888  "Y88888  "Y888 "Y888888 

 -->

<form action="" method="post" id="mform">	
<div class="row">
	<div class="col-12 col-sm-6 mt-3">
		<input list="id" class="form-control form-control-lg font-weight-bold" name="min" type="text" autocomplete="off" required id="ne1" placeholder="Pedido"/>
			<datalist id="id">
				<?PHP
  				$sac = conex()->query("SELECT minuta FROM pedido WHERE user = '$user' GROUP BY minuta DESC")->fetchAll(PDO::FETCH_ASSOC);
				foreach($sac as $resultado)
					{
						echo "<option value='{$resultado['minuta']}'/>";
					}
				?>
		</datalist>
	</div>
	<div class="col-6 col-sm-3 mt-3">
            <button type="button" class='btn btn-warning btn-block shadow' id="mbutton" onclick="play(), vibrate(300)"><h5 class="mt-1"><i class="far fa-clipboard-list fa-lg mr-2"></i>Abrir</h5></button>
    </div>
    <div class="col-6 col-sm-3 mt-3">
            <button type="button" class='btn btn-danger btn-block shadow'><h5 class="mt-1" onclick="play(), vibrate(300), reset()"><i class="far fa-clipboard-list fa-lg mr-2"></i>Fechar</h5></button>
	</div>
</div>
        <input name="option" type="number" value="0" style="display: none;">
</form>

<!-- 
8888888b.                    888    d8b                   
888  "Y88b                   888    Y8P                   
888    888                   888                          
888    888  .d88b.  .d8888b  888888 888 88888b.   .d88b.  
888    888 d8P  Y8b 88K      888    888 888 "88b d88""88b 
888    888 88888888 "Y8888b. 888    888 888  888 888  888 
888  .d88P Y8b.          X88 Y88b.  888 888  888 Y88..88P 
8888888P"   "Y8888   88888P'  "Y888 888 888  888  "Y88P"       
 -->
<hr>
<form id="formulario" action="" method="POST">
    <div class="row mt-2">
        <div class="col-6">
            <h5 class="alert alert-dark alert-block"><i class="fas fa-user mr-2"></i><?php echo $user?></h5>
        </div>
        <div class="col-6">
            <input type="text" class="form-control form-control-lg font-weight-bold" list="data" required id="destino" name="destino" placeholder="Destino" autocomplete="off">
            <datalist id="data">
                <option value="Disk TV"></option>
                <option value="Leo Saldão"></option>
                <option value="Liquitudo"></option>
                <option value="Mix Prime"></option>
                <option value="Orion"></option>
                <option value="Saldão Mbassi"></option>
            </datalist>
        </div>
    </div>

<!-- 
       d8888      888 d8b          d8b                                    
      d88888      888 Y8P          Y8P                                    
     d88P888      888                                                     
    d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
   d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
  d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
 d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     
 -->

	<div class="row">
		<div class="col-12 col-sm-6 col-lg-4 text-lg-center input-group-lg mt-3">
			<input type="text" class="form-control font-weight-bold mt-2 mt-sm-0" name="smart" id="smart" autocomplete="off" required autofocus list="lis" placeholder="Insira o ID">
			<datalist id="lis">
				<?php
					foreach((conex()->query("SELECT smart,id FROM codigo UNION ALL SELECT smart,id FROM lg ORDER BY id DESC")) as $result)
					{
						echo "<option value='".$result['smart']."'/>";
					}
				?>
			</datalist>
		</div>
		<div class="col-12 col-sm-6 col-lg-4 mt-3">
			<button class="btn btn-primary shadow btn-block" type="button" id="botao" onclick="vibrate(200),play()"><h5><i class="fas fa-plus-circle mr-2"></i>Adicionar</h5></button>
        </div>
        <input name="option" type="number" value="1" style="display: none;">
        <input name="min" id="ne2" type="number" value="" style="display: none;">
		<div class="col-12 col-lg-4 mt-3">
			<button class="btn btn-success shadow btn-block" type="button" value="Scan" onclick="play(),getScan()"><h5 class="mt-1"><i class="fas fa-camera mr-2"></i>Camera</h5></button>
		</div>
	</div>
</form>

<!-- PLAYER DE AUDIO -->
<div style="display: none" id="click"></div>

<!-- BARCODE 1 -->
<INPUT id=barcode type=text style="display: none" >

<!-- RESERVA -->
<div style="display: none;" id="reserva"></div>
<hr>
<!-- 

88888888888          888               888          
    888              888               888          
    888              888               888          
    888      .d88b.  88888b.   .d88b.  888  8888b.  
    888     d8P  Y8b 888 "88b d8P  Y8b 888     "88b 
    888     88888888 888  888 88888888 888 .d888888 
    888     Y8b.     888 d88P Y8b.     888 888  888 
    888      "Y8888  88888P"   "Y8888  888 "Y888888  

 -->
                
<div class="row mt-2">
    <div class="col-lg-12">                    
        <table class="table table-striped table-bordered table-hover" id="tab">
            <!-- <input name="nex" id="minut" type="number" value="" style="display: none;"> -->
            <thead>
                <tr class="bg-secondary text-light">
                    <th><i class="far fa-barcode-read fa-lg mr-2"></i></th>  
                    <th><i class="fas fa-shopping-cart fa-lg mr-2"></i></th>
                    <th><i class="fas fa-trash-alt fa-lg mr-2"></i></th>
                </tr> 
            </thead> 
            <tbody>  
            </tbody>
        </table>                    
    </div>
</div>

<?php include("../CORE/Footer2.php")?>