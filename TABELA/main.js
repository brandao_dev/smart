var columnDefs = 
  [
    {headerName: "AGENTE", field: "agente", filter: "agSetColumnFilter", hide: true},
    {headerName: "ALTA", field: "ALTA", filter: "agSetColumnFilter", hide: true},
    {headerName: "ALTERAÇÃO", field: "agora", filter: "agDateColumnFilter", hide: true},
    {headerName: "ALT. HORA", field: "HORA", filter: "agTextColumnFilter", hide: true},
    {headerName: "COR", field: "cor", filter: "agTextColumnFilter", hide: true},
    {headerName: "DATA AGING", field: "INICIO_TRIAGEM", filter: "agDateColumnFilter", hide: true},
    {headerName: "DATA ENTRADA", field: "data", filter: "agDateColumnFilter", hide: true},
    {headerName: "DATA EXPEDIÇÃO", field: "dataexpedicao", filter: "agDateColumnFilter", hide: true},
    {headerName: "DATA HIGIENE", field: "higiene", filter: "agDateColumnFilter", hide: true},
    {headerName: "DATA PINTURA", field: "DATA_PINTURA", filter: "agDateColumnFilter", hide: true},
    {headerName: "DATA QUALIDADE", field: "qualidade", filter: "agDateColumnFilter", hide: true},
    {headerName: "DATA RMA", field: "datarma", filter: "agDateColumnFilter", hide: true},
    {headerName: "DATA TRIAGEM", field: "datatriagem", filter: "agDateColumnFilter"},
    {headerName: "ENGENHARIA", field: "enge", filter: "agTextColumnFilter", hide: true},
    {headerName: "ETAPA", field: "etapa", filter: "agSetColumnFilter", menuTabs: ['filterMenuTab'],},
    {headerName: "FABRICANTE", field: "fabricante", filter: "agSetColumnFilter", filterParams: {values: filtro('fabricante')}},
    {headerName: "GRADE", field: "grade", filter: "agSetColumnFilter", hide: true},
    {headerName: "GRUPO", field: "grupo", filter: "agSetColumnFilter", hide: true},
    {headerName: "LINHA", field: "linha", filter: "agTextColumnFilter"},
    {headerName: "LIMPEZA", field: "limpeza", filter: "agSetColumnFilter", hide: true},
    {headerName: "LOCAL SAIDA", field: "localsaida", filter: "agTextColumnFilter", hide: true},
    {headerName: "LOTE", field: "nlote", filter: "agNumberColumnFilter", hide: true},
    {headerName: "MINUTA ENTRADA", field: "minuta", filter: "agNumberColumnFilter", hide: true},
    {headerName: "MINUTA SAIDA", field: "m2", filter: "agNumberColumnFilter", hide: true},
    {headerName: "MODELO", field: "modelo", filter: "agTextColumnFilter"},
    {headerName: "MODO", field: "modo", filter: "agSetColumnFilter", hide: true},
    {headerName: "PALETE", field: "palet", filter: "agTextColumnFilter", hide: true},
    {headerName: "PINTURA", field: "PINTURA", filter: "agTextColumnFilter", hide: true},
    {headerName: "PINTURA FASE", field: "pnt_local", filter: "agSetColumnFilter", hide: true},
    {headerName: "REFRI", field: "CARGA", filter: "agSetColumnFilter", hide: true},
    {headerName: "REPARO", field: "obs", filter: "agTextColumnFilter", hide: true},
    {headerName: "RMA", field: "rma", filter: "agTextColumnFilter", hide: true},
    {headerName: "SERIAL ORIGINAL", field: "serie1", filter: "agTextColumnFilter", hide: true},
    {headerName: "SERIAL SMART", field: "smart", filter: "agSetColumnFilter", filterParams: {values: function(params) { setTimeout(function() { params.success(filtro('smart')); }, 500); }}, menuTabs: ['filterMenuTab'],},
    {headerName: "TENSAO", field: "voltagem", filter: "agTextColumnFilter", hide: true},
    {headerName: "USER HIGIENE", field: "higienico", filter: "agSetColumnFilter", hide: true},
    {headerName: "USER QUALIDADE", field: "user_qualidade", filter: "agSetColumnFilter", hide: true},
    {headerName: "USER TRIAGEM", field: "usertriagem", filter: "agSetColumnFilter", hide: true}
  ];
  
  var gridOptions = {
    columnDefs: columnDefs,
    defaultColDef: {
      flex: 1,
      minWidth: 150,
      sortable: true,
      resizable: true,
    },
    // use the server-side row model
    rowModelType: 'serverSide',
  
    // fetch 100 rows at a time
    cacheBlockSize: 40,
  
    animateRows: true,
    // debug: true
  
    onFilterChanged: onFilterChanged,
  };
  
  var fakeServer;
  var selectedCountries = null;
  
  function onFilterChanged() {
    var countryFilterModel = gridOptions.api.getFilterModel()['etapa'];
    var selected = countryFilterModel && countryFilterModel.values;
  
    if (!areEqual(selectedCountries, selected)) {
      selectedCountries = selected;
  
      console.log('Refreshing sports filter');
      var sportFilter = gridOptions.api.getFilterInstance('smart');
      sportFilter.refreshFilterValues();
    }
  }
  
  function areEqual(a, b) {
    if (a == null && b == null) {
      return true;
    }
    if (a != null || b != null) {
      return false;
    }
  
    return (
      a.length === b.length &&
      a.every(function (v, i) {
        return b[i] === v;
      })
    );
  }
  
  function getCountryValuesAsync(params) {
    var countries = fakeServer.getCountries();
  
    // simulating real server call with a 500ms delay
    setTimeout(function () {
      params.success(countries);
    }, 500);
  }
  
  function getSportValuesAsync(params) {
    var sports = fakeServer.getSports(selectedCountries);
  
    // simulating real server call with a 500ms delay
    setTimeout(function () {
      params.success(sports);
    }, 500);
  }
  
  function ServerSideDatasource(server) {
    return {
      getRows: function (params) {
        console.log('[Datasource] - rows requested by grid: ', params.request);
  
        // get data for request from our fake server
        var response = server.getData(params.request);
  
        // simulating real server call with a 500ms delay
        setTimeout(function () {
          if (response.success) {
            // supply rows for requested block to grid
            params.successCallback(response.rows, response.lastRow);
          } else {
            params.failCallback();
          }
        }, 500);
      },
    };
  }
  
  // setup the grid after the page has finished loading
  document.addEventListener('DOMContentLoaded', function () {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);
  
    agGrid
      .simpleHttpRequest({
        url:
          '/TABELA/GET.php',
      })
      .then(function (data) {
        // setup the fake server with entire dataset
        fakeServer = new FakeServer(data);
  
        // create datasource with a reference to the fake server
        var datasource = new ServerSideDatasource(fakeServer);
  
        // register the datasource with the grid
        gridOptions.api.setServerSideDatasource(datasource);
      });
  });

  function filtro(filtro)
  {
    // $.ajax({
    //   data: 'filtro='+filtro,
    //   type: 'POST',
    //   dataType: 'json',
    //   async: true,
    //   url: '/TABELA/filtro.php',
    //   success: function(este){
    //     console.log(este)
    //     // let array = $.map(este,function(value,index){
    //     //   return [value]
    //     // })
    //     // return print_r(array)
    //   }

    // })
  
  }