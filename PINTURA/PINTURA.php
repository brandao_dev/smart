<?php include("../CORE/Header2.php");?>     
<!--Sweet Alert 2 -->     
<!--Código custom -->          
<script src="AJAX.js"></script>   
<div id="player" style="display: none;"></div>

<!-- 
       d8888                   d8b          
      d88888                   Y8P          
     d88P888                                
    d88P 888 88888b.   .d88b.  888  .d88b.  
   d88P  888 888 "88b d88""88b 888 d88""88b 
  d88P   888 888  888 888  888 888 888  888 
 d8888888888 888 d88P Y88..88P 888 Y88..88P 
d88P     888 88888P"   "Y88P"  888  "Y88P"  
             888                            
             888                            
             888                            
 -->




<div class="row">
    <div class="col-8 col-md-5">
        <!-- SEGUNDA LINHA DE FRASE DE APOIO *** //////////////////////// -->
        <div class="alert alert-info font-weight-bold btn-block" id="caso"><i class="fas fa-handshake fa-lg mr-2"></i><?php echo 'Olá Sr. '.$user.'!';?></div>
    </div>
    <div class="col-4 col-md-2">
        <!-- TOTAL *** /////////////////////////// -->
        <button class="btn btn-lg btn-block btn-warning font-weight-bold" id="total"><i class="fas fa-print-roller mr-2"></i><i class="far fa-calendar-alt mr-2"></i>0</button>
    </div>
    <div class="col-12 col-md-5">
        <div class="btn-group btn-group-toggle shadow d-flex" data-toggle="buttons">
            <label class="btn btn-outline-danger btn-lg" id="interno">
                <input type="radio" name="int" value="1" autocomplete="off" id="interno" checked onclick="vibrate(100),play(), lista(1)" class="w-100"><i class="fas fa-inbox-in mr-2"></i>Interno
            </label>
            <label class="btn btn-outline-danger btn-lg" id="saida">
                <input type="radio" name="int" value="2" autocomplete="off" id="saida" onclick="vibrate(100),play(), lista(2)" class="w-100"><i class="fas fa-sign-out-alt mr-2"></i>Saída
            </label>
            <label class="btn btn-outline-danger btn-lg" id="retorno">
                <input type="radio" name="int" value="3" autocomplete="off" id="retorno" onclick="vibrate(100),play(), lista(3)" class="w-100"><i class="fas fa-sign-in-alt mr-2"></i>Retorno
            </label>
        </div>
    </div>
</div>

<hr>

<form id="formulario" action="" method="POST">
    <input type="text" style="display: none;" id="cache" name="int">
    <div class="row mt-2">
        <div class="col-6">
            <select type="text" class="form-control form-control-lg font-weight-bold" required id="servico" name="servico" placeholder="Serviço" autocomplete="off">
                <option value="">SERVIÇO...</option>
                <option value="POLIMENTO">POLIMENTO</option>
                <option value="RETOQUE">RETOQUE</option>
                <option value="RESTAURACAO">RESTAURAÇÃO</option>
                <option value="CONSERTO_INTERNO">CONSERTO_INTERNO</option>
                <option value="PINTURA_PARCIAL">PINTURA_PARCIAL</option>
                <option value="PINTURA_COMPLETA">PINTURA_COMPLETA</option>
                <option value="1 PORTA">1 PORTA</option>
                <option value="2 PORTAS">2 PORTAS</option>
                <option value="3 PORTAS">3 PORTAS</option>
        </select>
        </div>
        <div class="col-6">
            <select type="text" class="form-control form-control-lg font-weight-bold" id="cor" name="cor" placeholder="Cor" autocomplete="off">
                <option value="">COR...</option>
                <option value="ORIGINAL">ORIGINAL</option>
                <option value="AZUL PERSO.">AZUL PERSO.</option>
                <option value="BRANCO PERSO.>">BRANCO PERSO.</option>
                <option value="CINZA PERSO.">CINZA PERSO.</option>
                <option value="GRAFITE PERSO.">GRAFITE PERSO.</option>
                <option value="INOX PERSO.">INOX PERSO.</option>
                <option value="PRATA PERSO.">PRATA PERSO.</option>
                <option value="PRETO PERSO.">PRETO PERSO.</option>
                <option value="TITANIO PERSO.">TITANIO PERSO.</option>
                <option value="VERDE PERSO.">VERDE PERSO.</option>
                <option value="VERMELHO PERSO.">VERMELHO PERSO.</option>
            </select>
        </div>
    </div>

<!-- 
       d8888      888 d8b          d8b                                    
      d88888      888 Y8P          Y8P                                    
     d88P888      888                                                     
    d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
   d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
  d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
 d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     
 -->

	<div class="row">
		<div class="col-12 col-sm-6 col-lg-4 text-lg-center input-group-lg mt-3">
			<input type="text" class="form-control font-weight-bold mt-2 mt-sm-0" name="smart" id="smart" autocomplete="off" required autofocus list="lis" placeholder="Insira o ID">
			<datalist id="lis">
				<?php
					foreach((conex()->query("SELECT smart,id FROM codigo UNION ALL SELECT smart,id FROM lg ORDER BY id DESC")) as $result)
					{
						echo "<option value='".$result['smart']."'/>";
					}
				?>
			</datalist>
		</div>
		<div class="col-12 col-sm-6 col-lg-4 mt-3">
			<button class="btn btn-primary shadow btn-block" type="submit" id="botao" onclick="vibrate(200),play()"><h5><i class="fas fa-plus-circle mr-2"></i>Adicionar</h5></button>
        </div>
</form>
		<div class="col-12 col-lg-4 mt-3">
			<button class="btn btn-success shadow btn-block" type="button" value="Scan" onclick="play(),getScan()"><h5 class="mt-1"><i class="fas fa-camera mr-2"></i>Camera</h5></button>
		</div>
	</div>

<!-- PLAYER DE AUDIO -->
<div style="display: none" id="click"></div>

<!-- BARCODE 1 -->
<INPUT id=barcode type=text style="display: none" >
<hr>
<!-- 

88888888888          888               888          
    888              888               888          
    888              888               888          
    888      .d88b.  88888b.   .d88b.  888  8888b.  
    888     d8P  Y8b 888 "88b d8P  Y8b 888     "88b 
    888     88888888 888  888 88888888 888 .d888888 
    888     Y8b.     888 d88P Y8b.     888 888  888 
    888      "Y8888  88888P"   "Y8888  888 "Y888888  

 -->
                
<div class="row mt-2">
    <div class="col-lg-12">                    
        <table class="table table-striped table-bordered table-hover" id="tab">
            <!-- <input name="nex" id="minut" type="number" value="" style="display: none;"> -->
            <thead>
                <tr class="bg-secondary text-light">
                    <th><i class="far fa-barcode-read fa-lg mr-2"></i></th>  
                    <th><i class="fas fa-paint-roller fa-lg mr-2"></i></th>
                    <th><i class="fas fa-trash-alt fa-lg mr-2"></i></th>
                </tr> 
            </thead> 
            <tbody>  
            </tbody>
        </table>                    
    </div>
</div>

<?php include("../CORE/Footer2.php")?>