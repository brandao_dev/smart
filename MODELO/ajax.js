// 888               d8b 888                              
// 888               Y8P 888                              
// 888                   888                              
// 888       .d88b.  888 888888 888  888 888d888  8888b.  
// 888      d8P  Y8b 888 888    888  888 888P"       "88b 
// 888      88888888 888 888    888  888 888     .d888888 
// 888      Y8b.     888 Y88b.  Y88b 888 888     888  888 
// 88888888  "Y8888  888  "Y888  "Y88888 888     "Y888888
$(function(){
    $('#modelo').on('change', function(){
        if($(this).val().length > 0)
        {
            let chave = $(this).val()
            ajax(chave)
        }
    })
})

// CARREGA AUTOMATICO URL VAR
const get = window.location.search.split('=')[1]
if(get != undefined)
{
    window.onload = ajax(get)
}

function ajax(chave){
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        async: true,
        url: 'leitura.php',
        data: 'modelo='+chave,
        success: function(data) {

            //LIMPA LABEL
            $('label').removeClass('active')
            
            //LIMPA SESSÂO
            sessionStorage.clear()

            //PADRAO
            $('#player').html(data.player)
            $('#caso').html(data.caso)

            //SE MODELO EXISTENTE NO BD
            if(data[0] != undefined)
            {
                //SETA SESSION
                sessionStorage.setItem('id',data[0].id)
                //INPUTS
                $('#data').val(data[0].DATA)
                $('#alteracao').val(data[0].ALTERACAO)
                $('#user').val(data[0].USUARIO)
                $('#linha').val(data[0].procel+'/'+data[0].linha)
                $('#fabricante').val(data[0].fabricante)
                $('#cor').val(data[0].cor)
                $('#ean').val(data[0].EAN)    
                $('#websac').val(data[0].websac)    
                //PROCEL
                $('label[for="'+data[0].procel+'"]').addClass('active')
                $('#'+data[0].procel).prop('checked',true)

                //TENSÃO - REMOVE BARRA EM BIVOLT POR CAUSA DE ERRO EM ID JS COM BARRA
                let tensao
                if(data[0].tensao.includes('/'))
                {
                    tensao = data[0].tensao.replace("/","")
                }
                else if(data[0].tensao.includes(' '))
                {
                    tensao = data[0].tensao.replace(" ","")
                }
                else
                {
                    tensao = data[0].tensao
                }
                //label do for
                $('label[for="'+tensao+'"]').addClass('active')
                $("input:radio").removeAttr("checked")
                $("#"+tensao).attr("checked",true)
                console.log(tensao)
            }
        },
        error: function() {
            console.log('deu ruim')
        }
    })
}
// 8888888b.                                                                
// 888   Y88b                                                               
// 888    888                                                               
// 888   d88P 888d888  .d88b.   .d8888b  .d88b.  .d8888b  .d8888b   8888b.  
// 8888888P"  888P"   d88""88b d88P"    d8P  Y8b 88K      88K          "88b 
// 888        888     888  888 888      88888888 "Y8888b. "Y8888b. .d888888 
// 888        888     Y88..88P Y88b.    Y8b.          X88      X88 888  888 
// 888        888      "Y88P"   "Y8888P  "Y8888   88888P'  88888P' "Y888888
$(function(){
    $('#form').on('submit',function(e){
        e.preventDefault()

        //LINHA VALIDATE
        if($('#linha').val() == '' || $('#linha').val() == null)
        {
            Swal.fire(
            'Atenção!!',
            'Escolha a <strong>LINHA</strong> deste modelo',
            'error'
          )
        }
        else
        {
            //VOLTAGEM VALIDATE
            if($('input[name=tensao]:checked').val() == undefined)
            {
                Swal.fire(
                    'Atenção!!',
                    'Escolha uma <strong>VOLTAGEM</strong> para o modelo',
                    'question'
                )
            }
            else
            {
                //FABRICANTE
                if($('#fabricante').val() == '' || $('#fabricante').val() == null)
                {
                    Swal.fire(
                        'Atenção!!',
                        'Escolha um <strong>FABRICANTE</strong> para o modelo',
                        'info'
                    )
                }
                else
                {
                    //COR
                    if($('#cor').val() == '' || $('#cor').val() == null)
                    {
                        Swal.fire(
                            'Atenção!!',
                            'Escolha uma <strong>COR</strong> para o modelo',
                            'info'
                        )
                    }
                    else
                    {
                        //EAN INVALIDO
                        if($('#ean').val().length > 0 && $('#ean').val().length < 12)
                        {
                            Swal.fire(
                                'Estranho...',
                                'Este número de <strong>EAN</strong> não parece ser válido. Confirma?',
                                'question',
                            )
                        }
                        else
                        {
                            //WEBSAC
                            if($('#websac').val().length > 0 && $('#websac').val().length < 3)
                            {
                                Swal.fire(
                                    'Estranho...',
                                    'Este número de <strong>WEBSAC</strong> está fora do padrão. Confirma?',
                                    'question',
                                )
                            }
                            else
                            {
                                Swal.fire(
                                    {
                                        title: 'Tudo Certo!!',
                                        html: 'Podemos confirmar o registro deste modelo?',
                                        imageUrl: '../IMG/MOD/CONFIRMA.jpg',
                                        imageWidth: 400,
                                        imageHeight: 200,
                                        customClass: 'swal',
                                        showCancelButton: true,
                                        confirmButtonText: '<i class="far fa-thumbs-up mr-2"></i>Sim, confirmo!',
                                        cancelButtonText: '<i class="far fa-thumbs-down mr-2"></i>Melhor não...',
                                        allowOutsideClick: false
                                    }
                                ).then
                                (
                                (result) =>
                                {
                                    if(result.isConfirmed) 
                                    {
                                        //AJAX
                                        let data = $(this).serialize()
                                        $.ajax({
                                            dataType: 'json',
                                            type: 'POST',
                                            data: data,
                                            async: true,
                                            url: 'processa.php',
                                            success: function(data){
        
                                                //LIMPA LABEL 
                                                $('label').removeClass('active')
                                                //LIMPA INPUTS
                                                $(':input').val('')
                                                //LIMPA RADIOS
                                                $('input[type=radio]').prop('checked',false);
                                                //LIMPAR SESSÂO
                                                sessionStorage.clear()
    
    
                                                $('#caso').html(data.caso)
                                                $('#player').html(data.player)
                                            },
                                            error: function(){
                                                console.log('deu ruim')  
                                            }
                                        })
                                    }
                                }
                                )
                            }
                        }
                    }
                }
            }
            
        }       
    })    
})

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

function vibrate(ms){
    navigator.vibrate(ms);
  }

//   8888888888                   888          d8b         
//   888                          888          Y8P         
//   888                          888                      
//   8888888    888  888  .d8888b 888 888  888 888 888d888 
//   888        `Y8bd8P' d88P"    888 888  888 888 888P"   
//   888          X88K   888      888 888  888 888 888     
//   888        .d8""8b. Y88b.    888 Y88b 888 888 888     
//   8888888888 888  888  "Y8888P 888  "Y88888 888 888  

function excluir(){
    let id = sessionStorage.getItem('id');
    console.log(id)
    if(id != null)
    {
        Swal.fire(
            {
                title: 'Certeza??',
                html: 'Deseja mesmo excluir definitivamente esse modelo?',
                imageUrl: '../IMG/MOD/APAGA.jpg',
                imageWidth: 400,
                imageHeight: 200,
                customClass: 'swal',
                showCancelButton: true,
                confirmButtonText: '<i class="far fa-thumbs-up mr-2"></i>Sim, desejo!',
                cancelButtonText: '<i class="far fa-thumbs-down mr-2"></i>Melhor não...',
                allowOutsideClick: false
            }
        ).then
        (
        (result) =>
        {
            if(result.isConfirmed) 
            {
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: 'excluir.php',
                    async: true,
                    data: 'id='+id,
                    success: function(data){
            
                        //LIMPA LABEL 
                        $('label').removeClass('active')
                        //LIMPA INPUTS
                        $(':input').val('')
                        //LIMPA RADIOS
                        $('input[type=radio]').prop('checked',false);
                        //LIMPAR SESSÂO
                        sessionStorage.clear()

                        $('#caso').html(data.caso)
                        $('#player').html(data.player)
                    },
                    error: function(){
                        console.log('no')
                    }
                })
            }
        }
        )
    }
    else
    {
        Swal.fire(
            'Atenção!',
            'Abra um modelo existente antes de tentar <strong>EXCLUIR</strong>',
            'error'
        )
    }

}