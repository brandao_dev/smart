﻿<?PHP 
require('../CORE/PDO.php');

#DEFINIR QUAL BANCO USAR
$modelo = $_GET['modelo'];
#QUERY TABELA
$tabela = conex()->query("SELECT * FROM modelo WHERE modelo LIKE '$modelo'")->fetch(PDO::FETCH_ASSOC);
//FORNECEDOR
$fornecedor = ($tabela['fabricante'] == 'CONSUL')||($tabela['fabricante'] == 'BRASTEMP') ? 'Whirlpool' : '';

?>

<!doctype html>
<html>
	
<head>
<meta charset="utf-8">
<title>ERP SMART</title>
</head>
	
<style>
	.container {position: relative;}
	#image {position: absolute;}
	#imetro {position: absolute;left: 40px;top: 40px;}
	#energia {position: absolute;left: 390px;top: 20px;font-family: 'Arial Black', 'sans-serif';font-size: 60px}
	#volt {position: absolute;left: 850px;top: 41px;}
	#linha {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 38px;left: 390px;top: 105px;}
	#fornecedor {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 35px;left: 390px;top: 155px;}
	#marca {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 35px;left: 390px;top: 205px;}
	#modelo {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 40px;left: 390px;top: 270px;}
	/* #centrifuga {position: absolute;font-family: Impact;font-size: 35px;left: 55px;top: 365px;} */
	#energetica {position: absolute;font-family: Impact;font-size: 35px;left: 800px;top: 365px;}
	
	/*MAIS EFICIENTE*/
	#mais {position:absolute; font-family: Tahoma, Genova;font-size: 38px;left: 55px;top: 360px}
	
	#a {position: absolute;left: 810px;top: 412px;}
	#b {position: absolute;left: 810px;top: 508px;}
	#c {position: absolute;left: 810px;top: 614px;}
	#d {position: absolute;left: 810px;top: 680px;}
	#e {position: absolute;left: 810px;top: 777px;}
	#a2 {position: absolute;left: 590px;top: 412px;}
	#b2 {position: absolute;left: 590px;top: 508px;}
	#c2 {position: absolute;left: 590px;top: 614px;}
	#d2 {position: absolute;left: 590px;top: 680px;}
	#e2 {position: absolute;left: 590px;top: 777px;}
	
	/*MENOS EFICIENTE*/
	#menos {position:absolute; font-family: Tahoma, Genova;font-size: 38px;left: 55px;top: 875px}
	
	
	/*VOLUME*/
	/* #quadroVolume {position: absolute;left: 405px;top: 960px;}
	#kg {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 60px;left: 750px;top: 970px;}
	#capacidade {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 55px;left: 440px;top: 1060px;} */
	
	/*AGUA*/
	/* #quadroAgua {position: absolute;left: 690px;top: 960px;}
	#agua {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 65px;left: 750px;top: 960px;}
	#ciclo {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 35px;left: 760px;top: 1040px;}
	#consumoAgua {position: absolute;font-family: 'impact';font-size: 29px;left: 720px;top: 1100px;}
	 */
	/*LOGO PROCEL*/
	/* #procel {position: absolute;left: 100px;top: 1220px;} */

	
	/*INSTRUÇÔES*/
	#instrucoes {position: absolute;font-family: 'impact';font-size: 28px;left: 55px;top: 1480px;}
</style>
	
<body onload="window.print()">
<div class="container">
  <img id="image" src="GABA2.jpg" width="100%">
  <label id="imetro"><img src="IMETRO.png" width="32%"></label>
  <label id="energia">ENERGIA</label>
  <label id="volt"><img src="<?PHP echo substr($tabela['tensao'],0,3).'.png';?>" width="132px"></label>
  <label id="linha"><?PHP echo $tabela['linha'];?></label>
  <label id="fornecedor"><?PHP echo $fornecedor;?></label>
  <label id="marca">Marca: <?PHP echo $tabela['fabricante'];?></label>
  <label id="modelo">Modelo: <?PHP echo $tabela['modelo'];?></label>

  <!-- <label id="centrifuga">Desempenho Global(Eficiência energética, de centrifugação, de lavagem e consumo de água)</label> -->
	
  <label id="mais">Mais eficiente</label>
	
	
	<!--A-->
	<?PHP if($tabela['eficiencia'] == 'A') { ?>	
  <label id="a"><img src="A.jpg" width="160px"></label>
	
	<!--B-->
	<?PHP } if($tabela['eficiencia'] == 'B') { ?>
  <label id="b"><img src="B.jpg" width="160px"></label>
	
	<!--C-->
	<?PHP } if($tabela['eficiencia'] == 'C') { ?>
  <label id="c"><img src="C.jpg" width="160px"></label>
	
	<!--D-->
	<?PHP } if($tabela['eficiencia'] == 'D') { ?>
  <label id="d"><img src="D.jpg" width="160px"></label>
	
	<!--E-->
	<?PHP } if($tabela['eficiencia'] == 'E') { ?>
  <label id="e"><img src="E.jpg" width="160px"></label>
	<?php } ?>
	
  <label id="menos">Menos eficiente</label>

	<!--CONSUMO ENERGIA-->
  	<label id="kwh" style='position: absolute;font-family: Impact;font-size: 25px;left: 422px;top: 983px'>
	  Consumo de energia
	</label>
	<label id="consumo" style='position: absolute;font-family:"Arial Black";font-size: 65px;left: 435px;top: 1005px;'>
  		<?PHP echo $tabela['carga'];?>
	</label>
  	<label id="qf" style='position: absolute;font-family:"Arial Black";font-size: 30px;left: 450px;top: 1090px;'>
	  kWh/ciclo
	</label>
  	<label id="quadroConsumo" style='position: absolute;left: 400px;top: 960px'>
	  <img src="KWH.png" width="260px">
	</label>
	
	<!--CAPACIDADE LAVAGEM-->
  	<label id="quadroVolume" style="position: absolute;left: 680px;top: 960px">
	  <img src="KWH.png" width="260px">
	</label>
  	<label id="kg" style="position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 50px;left: 710px;top: 970px">
	  <?PHP echo $tabela['tipo']?>Kg
	</label>
  	<label style="position: absolute;font-family: 'Arial', 'sans-serif';font-size: 25px;left: 740px;top: 1050px">
		Capacidade
	</label>
  	<label style="position: absolute;font-family: 'Arial', 'sans-serif';font-size: 25px;left: 740px;top: 1090px">
		de Lavagem
	</label>
	
	<!--CONSUMO AGUA-->
	<label style="position: absolute;left: 680px;top: 1200px">
		<img src="KWH.png" width="260px">
	</label>
	<label style="position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 70px;left: 710px;top: 1200px">
		<?PHP echo $tabela['corrente'];?>
	</label>
	<label style="position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 35px;left: 755px;top: 1280px">
		L/ciclo
	</label>
	<label style="position: absolute;font-family: 'impact';font-size: 29px;left: 705px;top: 1330px">
		Consumo de Água
	</label>
	
	<!--SEGURANÇA-->
  	<label style="position: absolute;left: 400px;top: 1200px"><img src="KWH.png" width="260px"></label>
  	<label style="position: absolute;font-family: 'impact';font-size: 35px;left: 450px;top: 1230px">Segurança</label>
  	<label style="position: absolute;font-family: 'impact';font-size: 35px;left: 437px;top: 1280px">Desempenho</label>
	
	<!--PROCEL PBE-->
  	<label style="position: absolute;left: 100px;top: 950px"><img src="ICON.png" width="200px"></label>
	<label style="position: absolute;left: 100px;top: 1200px"><img src='PBE.png' width='200px'></label>


  <label id="instrucoes">Instruções de instalação e recomendações de uso, leia o Manual do aparelho</label>
</div>
</body>
</html>
<?PHP echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=../IMPRESSAO/IMP.php'>";?>