<?php
include_once '../CORE/PDO.php';


$smart = (isset($_POST['smart'])) ? strtoupper($_POST['smart']) : '';
$ex = (isset($_POST['ex'])) ? true : false;
$ano = "2020-07";

//DEFINIÇÂO ENTRADA/ SAIDA

if(!empty($_POST['int']))
{
    $retornos = null;
    // PEGAR O NUMERO DE RETORNOS DE SAC DO ID. BLOQUEIO TOTAL SE PASSAR DE 1X ABERTOS
    if($query = conex()->query("SELECT id,ABERTO FROM sac WHERE SMART = '$smart' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC))
    { 
        $retornos = $query['ABERTO'];
    }
    else
    {
        $retonos = 0;
    }
    if($retornos > 1 && $equipe != 'ADM' && $_POST['int'] == 1)
    {
        $int = 1;
        $caso = '<i class="fas fa-exclamation-circle mr-2"></i>Produto bloqueado!';
        $fala = 'Produto bloqueado!';
        $data = $retornos;
    }
    else
    {

        $int = $_POST['int'];
    
        //VERIFICA BANCO    
        $banco = banco($smart);
    
        //CERTIFICA SE EXISTE
        if($sql = conex()->query("SELECT modelo,etapa,rma FROM $banco WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC))
        {        
            //ENTRADA OU SAIDA
            switch($int)
            {
                case 1:
    
                    //SE FOR EXCLUIR
                    if($ex == true)
                    {
                        conex()->prepare("UPDATE $banco SET qualidade = '', obs_quali = '', user_qualidade = '$user', etapa = '03.REPARADO' WHERE smart = '$smart'")->execute();
                        $caso = '<i class="fas fa-ban fa-lg mr-2"></i>Qualidade Cancelada!';
                        $fala = 'Qualidade Cancelada!';
        
                        #CHAMA FUNCAO LOG	
                        setLOG('QUALIDADE',$sql['rma'],"Qualidade cancelada",$smart,"03.REPARADO");
                    }
    
                    //SE FOR ADICIONAR
                    else
                    {
                        //SE A ETAPA É REPARADO OU QUALIDADE
                        if(($sql['etapa'] == '03.REPARADO')||($sql['etapa'] == '05.QUALIDADE')||($sql['etapa'] == '04.HIGIENIZADO'))
                        {
                            conex()->prepare("UPDATE $banco SET qualidade = '$time', obs_quali = '', user_qualidade = '$user', etapa = '05.QUALIDADE' WHERE smart = '$smart'")->execute();
                            $caso = '<i class="fas fa-hourglass-start fa-lg mr-2"></i>Qualidade iniciada!';
                            $fala = 'Qualidade iniciada!';
            
                            #CHAMA FUNCAO LOG	
                            setLOG('QUALIDADE',$sql['rma'],"Qualidade iniciada",$smart,"05.QUALIDADE");
                        }
    
                        //ETAPA NÃO É REPARADO NEM QUALIDADE
                        else
                        {
                            $caso = '<i class="fas fa-exclamation-circle fa-lg mr-2"></i>Não passou pelo reparo!';
                            $fala = 'Não passou pelo reparo!';
            
                            #CHAMA FUNCAO LOG	
                            setLOG('QUALIDADE',$sql['rma'],"Produto não foi reparado",$smart,"05.QUALIDADE");
    
                        }
                    }
                    $data = conex()->query("SELECT smart, modelo FROM codigo WHERE qualidade > '$ano%' AND obs_quali = '' UNION ALL SELECT smart, modelo FROM lg WHERE qualidade > '$ano%' AND obs_quali = ''")->fetchAll(PDO::FETCH_ASSOC);
                break;
        
                case 2:
                    if($ex == true)
                    {
                        //CERTIFICA SE ID ESTA NA RESERVA
                        if(conex()->query("SELECT smart FROM $banco WHERE smart = '$smart' AND qualidade > '$ano%'")->fetch(PDO::FETCH_ASSOC))
                        {
                            conex()->prepare("UPDATE $banco SET qualidade = '', obs_quali = '', user_qualidade = '$user', etapa = '02.AGUARD.REPARO' WHERE smart = '$smart'")->execute();
                            $caso = '<i class="fas fa-ban fa-lg mr-2"></i>Qualidade reprovada!';
                            $fala = 'Qualidade reprovada!';
                            #CHAMA FUNCAO LOG	
                            setLOG('QUALIDADE',$sql['rma'],"Qualidade reprovada",$smart,"02.AGUARD.REPARO");
                        }
                        else
                        {                    
                            $caso = '<i class="fas fa-exclamation-circle fa-lg mr-2"></i>Impossível reprovar produto não iniciado!';
                            $fala = 'Impossível reprovar produto não iniciado!';
                            #CHAMA FUNCAO LOG	
                            setLOG('QUALIDADE',$sql['rma'],"Impossível reprovar produto não iniciado",$smart,"*");
                        }
                    }
                    else
                    {
                        //CERTIFICA SE ID ESTA NA RESERVA
                        if(conex()->query("SELECT smart FROM $banco WHERE smart = '$smart' AND qualidade > '$ano%'")->fetch(PDO::FETCH_ASSOC))
                        {
                            conex()->prepare("UPDATE $banco SET obs_quali = '$time', user_qualidade = '$user' WHERE smart = '$smart'")->execute();
                            $caso = '<i class="fas fa-flag-checkered fa-lg mr-2"></i>Qualidade aprovada!';
                            $fala = 'Qualidade aprovada!';
                            #CHAMA FUNCAO LOG	
                            setLOG('QUALIDADE',$sql['rma'],"Qualidade aprovada",$smart,"05.QUALIDADE");
                        }
                        else
                        {                    
                            $caso = '<i class="fas fa-exclamation-circle fa-lg mr-2"></i>Impossível aprovar produto não iniciado!';
                            $fala = 'Impossível aprovar produto não iniciado!';
                            #CHAMA FUNCAO LOG	
                            setLOG('QUALIDADE',$sql['rma'],"Impossível aprovar produto que não foi iniciado",$smart,"*");
                        }
                        
        
                    }
                    $data = conex()->query("SELECT smart, modelo FROM codigo WHERE qualidade > '$ano%' AND obs_quali != '' AND obs_quali > '$ano%' AND etapa = '05.QUALIDADE' UNION ALL SELECT smart, modelo FROM lg WHERE qualidade > '$ano%' AND obs_quali != '' AND obs_quali > '$ano%' AND etapa = '05.QUALIDADE'")->fetchAll(PDO::FETCH_ASSOC);
                break;
            }
        }
        else
        {       
            $caso = '<i class="fas fa-exclamation-circle fa-lg mr-2"></i>ID não existe';
            $fala = 'ID nao existe';
            $data = conex()->query("SELECT smart, modelo FROM codigo WHERE qualidade > '$ano%' AND obs_quali != '' AND obs_quali > '$ano%' AND etapa = '05.QUALIDADE' UNION ALL SELECT smart, modelo FROM lg WHERE qualidade > '$ano%' AND obs_quali != '' AND obs_quali > '$ano%' AND etapa = '05.QUALIDADE'")->fetchAll(PDO::FETCH_ASSOC);
        }
    }
}
else
{
    $int = 0;
    $caso = '<i class="fas fa-exclamation-circle mr-2"></i>Fase não escolhida!';
    $fala = 'Fase não escolhida!';
    $data = '';
}



//FALA
// $falas = htmlspecialchars(ucwords(strtolower($fala)));
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

//JSON
$total = Count(conex()->query("SELECT smart, modelo FROM codigo WHERE qualidade > '$ano%' AND obs_quali = '' UNION ALL SELECT smart, modelo FROM lg WHERE qualidade > '$ano%' AND obs_quali = ''")->fetchAll(PDO::FETCH_ASSOC));

$data = array_merge(['sql'=>$data, 'caso'=>$caso, 'fala'=>$player, 'total'=>$total, 'int'=>$int]);
print json_encode($data, JSON_UNESCAPED_UNICODE);