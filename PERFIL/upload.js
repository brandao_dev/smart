function perfil()
{
    swal.fire({
        title: 'Imagem do seu perfil',
        showConfirmButton: false,
        html: 
        '<span id="file-name"><i class="fas fa-hand-point-down mr-2"></i>Primeiro capture a imagem</span>'+
        '<form method="post" action="" enctype="multipart/form-data" id="myform">'+ 
            '<div class="row">'+               
                '<div class="col-6">'+
                    '<label for="file" class="btn btn-lg btn-block btn-primary shadow mt-3" onclick="play(),vibrate(100)"><i class="fas fa-portrait mr-2"></i>Carregar</label>'+
                    '<input type="file" id="file" name="file"  style="display:none"/>'+ 
                '</div>'+
                '<div class="col-6">'+
                    '<button type="submit" class="btn btn-lg btn-block btn-success shadow mt-3" onclick="play(),vibrate(100)"><i class="fas fa-save fa-lg mr-2"></i>Salvar</button>'+ 
                '</div>'+
            '</div>'+
        '</form>',
        backdrop: `rgba(0,0,0,0.8)`,
    }) 
    

    var $input = document.getElementById('file');
    // $fileName = document.getElementById('file-name');
    
    $input.addEventListener('change', function(){
    // $fileName.textContent = 'Ok. Agora clique em Salvar';
    $('#file-name').html('Ok. Agora clique em Salvar<i class="fas fa-hand-point-down ml-2"></i>')
    });

    // ==================================================================================
    // Image
    // ==================================================================================

    $(function(){
    
        $("#myform").on('submit',function(e){
            e.preventDefault()
    
            var fd = new FormData();
            var files = $('#file')[0].files;
            
            // Check file selected or not
            if(files.length > 0 ){
               fd.append('file',files[0]);
    
               $.ajax({
                  url: '/PERFIL/imagem.php',
                  type: 'post',
                  data: fd,
                  contentType: false,
                  processData: false,
                  success: function(){
                    window.open('../index.php', '_self')
                  },
               });
            }else{
               alert("Capture a foto antes de salvar.");
            }
        });
    });
}

//VIBRAR
function vibrate(ms){
	navigator.vibrate(ms);
  }