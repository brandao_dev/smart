
$(function() 
{
    $(document).ajaxStart(function(){
      $("#wait").css("display", "block");
      $("#quadro").css("display", "none");
    });
    $(document).ajaxStop(function(){
      $("#wait").css("display", "none");
    }); 
    $('#login').on('submit',function(e)
    {
        e.preventDefault();
        var con = $(this).serialize();
        $.ajax(
        {
            type: 'POST',
            dataType: 'json',
            url: 'LOGIN/PROC.php',
            async: true,
            data: con,
            success: function(confirma)
            {
                $('#erro').html(confirma.erro);
                $('#player').html(confirma.fala);
                switch(confirma.logon)
                {
                    //ERRO
                    case 1:
                        $("#quadro").css("display", "block");
                    break;

                    //SUCESSO
                    case 2:
                        window.location='../'+confirma.local
                    break;

                    //OK LOJA
                    case 3:
                        Swal.fire
                        (
                            {
                                title: 'Parabéns!!...',
                                html: "<div>Não existe nenhum produto devolvido em análise.</div>",
                                imageUrl: '../IMG/FESTA.jpg',
                                imageWidth: 650,
                                imageHeight: 300,
                                confirmButtonText: '<i class="far fa-thumbs-up mr-2"></i>Ok!',
                                allowOutsideClick: false
                            }
                        ).then
                        (
                            (result) =>
                            {
                                if(result.isConfirmed) 
                                {
                                    window.location='../'+confirma.local
                                }
                            }	
                        )
                    break;

                    //LISTA LOJA
                    case 4:
                        Swal.fire
                        (
                            {
                                html: confirma.div,
                                customClass: 'swal',
                                confirmButtonText: '<i class="far fa-thumbs-up mr-2"></i>Ok. Estou acompanhando!',
                                allowOutsideClick: false
                            }
                        ).then
                        (
                            (result) =>
                            {
                                if(result.isConfirmed) 
                                {
                                    window.location='../'+confirma.local
                                }
                            }	
                        )
                    break;
                }
            },
            error: function(confirma)
            {
                console.log('deu ruim');
                console.log(confirma.local)
                console.log(confirma.errp)
                console.log(confirma.logon)
            }
        }
            )
        
    }
    );
}
);

// MANDA TELEGRAM
function telegram(user)
{
    if(user.length == 0)
    {
        Swal.fire({
            title: "Por favor",
            text: "Escreva seu nome para que posssamos entrar em contato!",
            icon: "info",
            backdrop: `rgba(50,50,50,1)`
        }   
        )
    }
    else
    {
        var msg = "Eu, "+user+", não lembro da minha senha";
        var token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
        //SMART LOGIN CHANNEL
        var chat = '-1001268762199';
        async function file_get_contents(uri, callback) {
            let res = await fetch(uri),
                ret = await res.text(); 
            return callback ? callback(ret) : ret; // a Promise() actually.
        }
        file_get_contents("https://api.telegram.org/bot"+token+"/sendMessage?chat_id="+chat+"&text="+msg);

    }
}
