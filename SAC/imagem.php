<?php require ('../CORE/PDO.php');

$id = $_POST['id'];

$caminho = '../BOOK/'.$id.'/';
mkdir($caminho);
$img = glob($caminho.'*.jpg');

//CONTAGEM RECEBE +1 DO STRTOK ATÉ O PONTO DO END EXPLODE DE BARRA DO END DO IMG
$contagem = strtok(end(explode('/',end($img))),'.')+1;

#SALVAR FOTO ++++++++++++++++++++++++++++++++++++++++
if(!empty($_FILES['image']['tmp_name']))
{
    $file_name = $_FILES['image']['tmp_name'];
}
else
{
    $file_name = $_FILES['cam']['tmp_name'];
}

$file_name = imagecreatefromjpeg($file_name);


//PEGA COMPRIMENTO
$comprimento = imagesx($file_name);
//PEGA ALTURA
$altura = imagesy($file_name);

//SE ALTURA É MAIOR OU IGUAL COMPRIMENTO
if($altura < ($comprimento * 0.75)) 
{
    $novo_comprimento = $altura * 1.33;
    $corte_comprimento = ($comprimento - $novo_comprimento)/2;
    $adaptada = imagecrop($file_name, ['x' => $corte_comprimento, 'y' => 0, 'width' => $novo_comprimento, 'height' => $altura]);
}
else 
{
    $nova_altura = $comprimento * 0.75;
    $corte_altura = ($altura - $nova_altura)/2;
    $adaptada = imagecrop($file_name, ['x' => 0, 'y' => $corte_altura, 'width' => $comprimento, 'height' => $nova_altura]);
}

$adaptada =  imagescale($adaptada, 1280, 960 );


$foto = imagejpeg($adaptada,$caminho.$contagem.'.jpg', 100);

// echo

// '<br>ID - '.$id.
// '<br>CAMINHO - '.$caminho.
// '<br>CONTAGEM - '.$contagem;
header('refresh:0;url=editar.php?id='.$id);
        
        
?>        