<?php
include '../CORE/PDO.php';

$destino = (isset($_POST['destino'])) ? $_POST['destino'] : '';
$etapa = (($destino == 'CAMARES')||($destino == 'CHICO PONTES')||($destino == 'GALATEA')) ? '12.TRANSFERIDO' : '08.LOJA';
$option = (isset($_POST['option'])) ? $_POST['option'] : '';
$smart = (isset($_POST['smart'])) ? strtoupper($_POST['smart']) : '';
$banco = banco($smart);
$min = (!empty($_POST['min'])) ? $_POST['min'] : '';

//MINUTA
if($option == 0)
{
    #NOVO PEDIDO
    if($min == '')
        {
            if($min = conex()->query("SELECT minuta FROM pedido ORDER BY minuta DESC")->fetch(PDO::FETCH_ASSOC))
            {                
                $min = $min['minuta']+1;
            }
            else
            {
                $min = 1;
            }
            $caso = '<i class="fas fa-plus-square fa-lg mr-2"></i>Nova Minuta Gerada';
            $fala = 'Nova Minuta Gerada';
        }
        #EDITAR PEDIDO
        else
        {
            if(conex()->query("SELECT minuta FROM pedido ORDER BY minuta DESC")->fetch(PDO::FETCH_ASSOC)['minuta'])
            {
                $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Editar Minuta';
                $fala = 'Editar Minuta';
            }
        }
}

//PRODUTO
elseif($option == 1)
{ 
    //INSERI QUALIDADE PARA PEGAR TEMPO MINIMO MAS DESISTI. FALTA COMPROMETIMENTO DE FUNCIONARIOS
    $sql = conex()->query("SELECT smart, linha, modelo, rma, qualidade FROM $banco WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC);

    //TERNARIA
    $query = conex()->query("SELECT id,SAC FROM sac WHERE SMART LIKE '{$sql['smart']}' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
    $sac = ($query) ? $query['SAC'] : '';

    //SE SMART EXISTE
    if($sql['smart'] == $smart)
    {
        $linha = $sql['linha'];
        $modelo = $sql['modelo'];

        //BUSCA EAN
        $ean = conex()->query("SELECT EAN,websac FROM modelo WHERE modelo = '$modelo'")->fetch(PDO::FETCH_ASSOC);

        //UPDATE
        if(conex()->query("SELECT smart FROM pedido WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC))
        {
            conex()->prepare("UPDATE pedido SET minuta = '$min', destino='$destino', user='$user', reserva='$sac', pedido='$time' WHERE smart = '$smart'")->execute();
        }

        //INSERT
        else
        {
            conex()->prepare("INSERT INTO pedido (smart, linha, modelo, minuta, pedido, destino, ean, websac, user, reserva) VALUES ('$smart', '$linha', '$modelo', '$min', '$time', '$destino', '{$ean['EAN']}', '{$ean['websac']}', '$user', '$sac')")->execute();
        }

        //ATUALIZA SAC SE HOUVER
        if(fnmatch('*.*', $sql['rma']))
        {
            conex()->prepare("UPDATE sac SET STATUS = '15.FINALIZADO' WHERE SMART = '$smart'")->execute();
            conex()->prepare("UPDATE $banco SET rma = '15.FINALIZADO' WHERE smart = '$smart'")->execute();
        }

        //BANCO PRINCIPAL, ETAPA
        conex()->prepare("UPDATE $banco SET etapa = '$etapa' WHERE smart = '$smart'")->execute();

        #CHAMA FUNCAO LOG	
        setLOG("SAIDA",'15.FINALIZADO',"minuta=$min, destino=$destino",$smart,"$etapa");           

        //RESERVA
        // $reserva = conex()->query("SELECT reserva FROM pedido WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC)['reserva'];

        $caso = '<i class="fas fa-check-circle fa-lg mr-2"></i>Produto adicionado!';
        $fala = 'Produto adicionado';
        
    }
    else
    {
        $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>ID não encontrado!';
        $fala = 'ID não encontrado';
    }
    
}

//EXCLUIR
elseif($option == 2)
{
    conex()->prepare("DELETE FROM pedido WHERE smart ='$smart'")->execute();

    #CHAMA FUNCAO LOG	
    setLOG("SAIDA",'*',"EXCLUIDO DA TRANSFERENCIA",$smart,"*"); 

    $caso = '<i class="fas fa-trash-alt fa-lg mr-2"></i>Produto Removido!';
    $fala = 'Produto Removido';
}

//RESETAR
elseif($option == 3)
{
    $caso = '<i class="fas fa-thumbs-up fa-lg mr-2"></i>Pedido concluído!';
    $fala = 'Pedido concluído!';
    $pedido = $min;
    $min = '';
}
//RMA
if(isset($sql))
{
    $rma = (fnmatch('*.*', $sql['rma'])) ? $sql['rma'] : '';
}
else
{
    $rma = '';
}

//FALA
// $falas = (!empty($fala)) ? htmlspecialchars($fala) : htmlspecialchars('Ops. atenção no preenchimento');
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

// $reserva = (isset($reserva)) ? $reserva : '';
$pedido = (isset($pedido)) ? $pedido : '';

//JSON
$data = conex()->query("SELECT * FROM pedido WHERE minuta LIKE '$min' ORDER BY alteracao DESC")->fetchAll(PDO::FETCH_ASSOC);
// unset($option, $smart, $min, $caso);
// array_push($data,$tot,$plus);
$plus = ['caso'=>$caso,'minuta'=>$min,'total'=>Count($data), 'fala'=>$player, 'user'=>$user, 'pedido'=>$pedido, 'rma'=>$rma];
$data = array_merge($data,$plus);
print json_encode($data, JSON_UNESCAPED_UNICODE);