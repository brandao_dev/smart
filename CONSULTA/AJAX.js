
// CARREGA AUTOMATICO URL VAR
const get = window.location.search.split('=')[1]
if(get != undefined)
{
    window.onload = ajax(get)
}

function ajax(data)
{
    $.ajax(
        {   
            type: 'GET',
            dataType: 'json',
            url: 'PROCESSA.php',
            async: true,
            data: 'serial='+data,
            success: function(data)
            {
                //PRODUTO
                $('#ean').val(data.ean.EAN);
                $('#websac').val(data.ean.websac);
                $('#smart').val('');
                $('#grupo').val(data.sql.grupo);
                $('#player').html(data.fala);
                $('#caso').html(data.caso);
                $('#data').val(data.sql.data);
                $('#smarte').val(data.sql.smart);
                $('#cor').val(data.sql.cor);
                $('#serie1').val(data.sql.serie1);
                $('#voltagem').val(data.sql.voltagem);
                $('#fabricante').val(data.sql.fabricante);
                $('#linha').val(data.sql.linha);
                $('#modelo').val(data.sql.modelo);
                $('#enge').val(data.sql.enge);
                $('#reparo').val(data.sql.reparo);
                $('#usuario').val(data.sql.usuario);
                $('#etapa').val(data.sql.etapa);
                $('#nlote').val(data.sql.nlote);
                $('#nota').val(data.sql.nota);
                $('#grade').val(data.sql.grade);
                $('#nota').val(data.sql.nota);

                //SERVIÇO
                $('#datatriagem').val(data.sql.datatriagem);
                $('#usertriagem').val(data.sql.usertriagem);
                $('#obs').html(data.sql.obs);
                $('#rma').val(data.sql.rma);
                $('#carga').val(data.sql.carga);
                $('#data_pintura').val(data.sql.DATA_PINTURA);
                $('#alta').val(data.sql.ALTA);
                $('#pintura').val(data.sql.PINTURA);
                $('#qualidade').val(data.sql.qualidade);
                $('#peca').val(data.sql.peca);
                $('#obs_quali').val(data.sql.obs_quali);
                $('#user_qualidade').val(data.sql.user_qualidade);
                $('#saco').val(data.saco);
                $('#higiene').val(data.sql.higiene);
                $('#higienico').val(data.sql.higienico);
                $('#limpeza').val(data.sql.limpeza);
                $('#temperatura').val(data.sql.temperatura);
                $('#compressor').val(data.sql.COMPRESSOR);
                $('#carga').val(data.sql.CARGA);
                $('#data_rma').val(data.sql.data_rma);
                // $('#pnt_local').val(data.sql.pnt_local);
                if(data.sql.pnt_local == 1){$('#pnt_local').val('INTERNO');}
                if(data.sql.pnt_local == 2){$('#pnt_local').val('EXT-SAIDA');}
                if(data.sql.pnt_local == 3){$('#pnt_local').val('EXT-RETORNO');}
                if(data.sql.pnt_local == 0){$('#pnt_local').val('SEM PINTURA');}

                //ENTRADA E SAIDA
                $('#expedicao').empty()
                if(data.pedido != false)
                {
                    $.each(data.pedido, function(ind,ele){
                        if(ele.destino == 'GALATEA')
                        {
                            posicao = '<h3 class="alert alert-success"><i class="fas fa-sign-in-alt mr-2"></i>Entrada</h3>'
                        }
                        else
                        {
                            posicao = '<h3 class="alert alert-success"><i class="fas fa-sign-out-alt mr-2"></i>Saída</h3>'
                        }
                        $('#expedicao').append(
                            '<div class="row mt-3">'+
                                '<div class="col text-left">'+posicao+'</div>'+
                            '</div>'+
                            '<div class="card shadow">'+
                            '<div class="card-body" style="background-color:#f2fced">'+
                                '<div class="row">'+
                                    '<div class="col-12 col-md-6 col-xxl-3 input-group mt-3">'+
                                        '<div class="input-group-prepend">'+
                                            '<span class="input-group-text font-weight-bold"><i class="fas fa-truck-loading mr-2"></i>Transferência</span>'+
                                        '</div>'+
                                        '<input type="text" class="form-control" value="'+ele.alteracao+'" >'+		
                                    '</div>'+
                                    '<div class="col-12 col-md-6 col-xxl-3 input-group mt-3">'+
                                        '<div class="input-group-prepend">'+
                                            '<span class="input-group-text font-weight-bold"><i class="fas fa-location mr-2"></i>Local</span>'+
                                        '</div>'+
                                        '<input type="text" class="form-control"  value="'+ele.destino+'">'+		
                                    '</div>'+
                                    '<div class="col-12 col-sm-6 col-xxl-3 input-group mt-3">'+
                                        '<div class="input-group-prepend">'+
                                            '<span class="input-group-text font-weight-bold"><i class="fas fa-walking mr-2"></i>Expedidor</span>'+
                                        '</div>'+
                                        '<input type="text" class="form-control"  value="'+ele.user+'" >'+		
                                    '</div>'+
                                    '<div class="col-12 col-sm-6 col-xxl-3 input-group mt-3">'+
                                        '<div class="input-group-prepend">'+
                                            '<span class="input-group-text font-weight-bold"><i class="fas fa-th-list mr-2"></i>Minuta</span>'+
                                        '</div>'+
                                        '<input type="text" class="form-control" value="'+ele.minuta+'" >'+		
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '</div>')
                    })
                    
                }

                //IMAGENS
                $('#im').empty()
                if(data.img != false)
                {
                    $('#im').append(
                        '<div class="row mt-3">'+
                            '<div class="col text-left">'+
                                '<h3 class="alert alert-orange"><i class="fas fa-images mr-2"></i>Imagens</h3>'+
                            '</div>'+	
                        '</div>')
                    $.each(data.img,function(index,element){
                        $('#im').append('<div class="row"><div class="col-12 col-sm-6 col-md-4 col-xl-2 mb-3"><img src="'+element+'" class="w-100 rounded shadow"></div></div>');
                    })
                }
                else
                {
                    $('#im').append(
                        '<div class="row mt-3">'+
                            '<div class="col text-left">'+
                                '<h3 class="alert alert-secondary"><i class="fas fa-images mr-2"></i>Imagem <span class="badge badge-dark shadow">Indisponível</span></h3>'+
                            '</div>'+	
                        '</div>')
                }

                //PDF
                $('#vista').empty()
                if(data.pdf != false)
                {
                    $('#vista').append(
                        '<div class="row mt-3" data-toggle="modal" data-target="#consulta">'+
                            '<div class="col text-left">'+
                                '<h3 class="alert alert-olive"><i class="fas fa-bomb mr-2"></i>Vista Explodida <span class="badge badge-danger shadow">Disponível</span></h6></h3>'+
                            '</div>'+
                            '<div class="modal fade" id="consulta" tabindex="-1" role="dialog" aria-labelledby="con" aria-hidden="true">'+
                                '<div class="modal-dialog" role="document">'+
                                    '<div class="modal-content">'+
                                        '<div class="modal-header">'+
                                            '<h5 class="modal-title" id="con"><i class="fas fa-bomb mr-2"></i>Vista Explodida</h5>'+
                                            '<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">'+
                                            '<span aria-hidden="true">&times;</span>'+
                                            '</button>'+
                                        '</div>'+
                                        '<div class="modal-body">'+
                                            '<embed type="application/pdf" src="../VISTA/'+data.sql.modelo+'.pdf" width="100%" frameborder="0" height="500" style="height: 85vh;"></embed>'+
                                        '</div>'+
                                        '<div class="modal-footer">'+
                                            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'
                    )
                }
                else
                {
                    $('#vista').append(
                        '<div class="row mt-3">'+
                            '<div class="col text-left">'+
                                '<h3 class="alert alert-secondary"><i class="fas fa-bomb mr-2"></i>Vista Explodida <span class="badge badge-dark shadow">Ausente</span></h6></h3>'+
                            '</div>'+
                        '</div>'
                    )
                }

                //PDF
                $('#boletim').empty()
                if(data.bol != false)
                {
                    $('#boletim').append(
                        '<div class="row mt-3" data-toggle="modal" data-target="#bol" id="boletim">'+
                        '<div class="col text-left">'+
                            '<h3 class="alert alert-orange"><i class="fas fa-clipboard-list mr-2"></i>Boletim Técnico <span class="badge badge-danger shadow">Disponível</span></h6></h3>'+
                        '</div>'+
                        '<div class="modal fade" id="bol" tabindex="-1" role="dialog" aria-labelledby="bole" aria-hidden="true">'+
                        '<div class="modal-dialog" role="document">'+
                            '<div class="modal-content">'+
                            '<div class="modal-header">'+
                                '<h5 class="modal-title" id="bole"><i class="fas fa-clipboard-list mr-2"></i>Boletim Técnico</h5>'+
                                '<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">'+
                                '<span aria-hidden="true">&times;</span>'+
                                '</button>'+
                            '</div>'+
                            '<div class="modal-body">'+
                                '<embed type="application/pdf" src="../BOLETIM/'+data.sql.modelo+'.pdf" width="100%" frameborder="0" height="500" style="height: 85vh;"></embed>'+
                            '</div>'+
                            '<div class="modal-footer">'+
                                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>'+
                            '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '</div>'
                    )
                }
                else
                {
                    $('#boletim').append(
                        '<div class="row mt-3">'+
                            '<div class="col text-left">'+
                                '<h3 class="alert alert-secondary"><i class="fas fa-clipboard-list mr-2"></i>Boletim Técnico <span class="badge badge-dark shadow">Ausente</span></h6></h3>'+
                            '</div>'+
                        '</div>'
                    )
                }

                //LOG
                $('#log').empty();
                $.each(data.log,function(ind,ele){
                    n = data.log.length - ind;
                    //DEFINIR ICONE
                    switch(ele.ACTION)
                    {
                        case 'REGISTRO':
                            ico = '<i class="fas fa-baby mr-2"></i>';
                        break;
                        case 'PINTURA':
                            ico = '<i class="fas fa-paint-roller mr-2"></i>';
                        break;
                        case 'QUALIDADE':
                            ico = '<i class="fas fa-clipboard-check mr-2"></i>';
                        break;
                        case 'REPARO':
                            ico = '<i class="fas fa-tools mr-2"></i>';
                        break;
                        case 'SAIDA':
                        case 'PEDIDO':
                            ico = '<i class="fas fa-door-open mr-2"></i>';
                        break;
                        case 'SAC':
                            ico = '<i class="fas fa-headset mr-2"></i>';
                        break;
                        case 'IMPRESSAO':
                        case 'ETIQUETA':
                        case 'PROCEL':
                            ico = '<i class="far fa-barcode-read mr-2"></i>';
                        break;
                        case 'HIGIENE':
                            ico = '<i class="fas fa-shower mr-2"></i>';
                        break;
                        default :
                            ico = '<i class="far fa-sticky-note mr-2"></i>';
                        break;
                    }
                    $('#log').append(
                    '<div class="card shadow mb-3"><div class="card-body" style="background-color:#f2f2f2">'+
                        '<h4 class="text-left">'+
                            ico+n+' - '+ele.ACTION+
                        '</h4>'+
                        '<div class="row">'+
                            '<div class="col-12 col-md-6 col-xl-3 input-group mt-3">'+
                                '<div class="input-group-prepend">'+
                                    '<span class="input-group-text font-weight-bold bg-warning">DATA</span>'+
                                '</div>'+
                                '<input type="text" class="form-control"  value="'+ele.DATA+'">'+
                            '</div>'+
                            '<div class="col-12 col-md-6 col-xl-3 input-group mt-3">'+
                                '<div class="input-group-prepend">'+
                                    '<span class="input-group-text font-weight-bold bg-success">STATUS</span>'+
                                '</div>'+
                                '<input type="text" class="form-control"  value="'+ele.TABELA+'">'+
                            '</div>'+
                            '<div class="col-12 col-md-6 col-xl-3 input-group mt-3">'+
                                '<div class="input-group-prepend">'+
                                    '<span class="input-group-text font-weight-bold bg-pink">ETAPA</span>'+
                                '</div>'+
                                '<input type="text" class="form-control"  value="'+ele.CAMPO+'">'+
                            '</div>'+
                            '<div class="col-12 col-xl-3 input-group mt-3">'+
                                '<div class="input-group-prepend">'+
                                    '<span class="input-group-text font-weight-bold bg-info">USER</span>'+
                                '</div>'+
                                '<input type="text" class="form-control"  value="'+ele.USER+'">'+
                            '</div>'+
                            '<div class="col-12 input-group mt-3">'+
                                '<div class="input-group-prepend">'+
                                    '<span class="input-group-text font-weight-bold bg-danger">AÇÕES</span>'+
                                '</div>'+
                                '<textarea class="form-control" >'+ele.COLUNAS+'</textarea>'+
                            '</div>'+
                        '</div>'+
                    '</div></div>'
                    )

                })

                //SAC
                $('#sc').empty();
                if(data.sac.length != 0)
                {
                    $('#sc').append(
                        '<div class="row mt-3">'+
                        '<div class="col text-left">'+
                            '<h3 class="alert alert-purple"><i class="fas fa-user-headset mr-2"></i>SAC</h3>'+
                        '</div>'+
                    '</div>')
                    for(var i = 0; i < data.sac.length; i++)
                    {
                        $('#sc').append(
                        '<div class="card shadow mb-3">'+
                        '<div class="card-body" style="background-color:#fbebff">'+
                            '<div class="row">'+
                                '<div class="col-6 col-xxl-2 input-group mt-3">'+
                                    '<div class="input-group-prepend">'+
                                        '<span class="input-group-text font-weight-bold bg-warning">NUMERO</span>'+
                                    '</div>'+
                                        '<input type="text" class="form-control"  value="'+data.sac[i].SAC+'">'+
                                '</div>'+
                                '<div class="col-6 col-xxl-2 input-group mt-3">'+
                                    '<a href="../SAC/SAC.php?sac='+data.sac[i].SAC+'" class="btn btn-block btn-cyan shadow" target="_blank"><i class="fas fa-bullseye-pointer mr-2"></i>ABRIR</a>'+
                                '</div>'+
                                '<div class="col-12 col-sm-6 col-xxl-2 input-group mt-3">'+
                                    '<div class="input-group-prepend">'+
                                        '<span class="input-group-text font-weight-bold bg-olive">STATUS</span>'+
                                    '</div>'+
                                        '<input type="text" class="form-control"  value="'+data.sac[i].STATUS+'">'+
                                '</div>'+
                                '<div class="col-12 col-sm-6 col-xxl-3 input-group mt-3">'+
                                    '<div class="input-group-prepend">'+
                                        '<span class="input-group-text font-weight-bold">DATA</span>'+
                                    '</div>'+
                                        '<input type="text" class="form-control"  value="'+data.sac[i].DATA+'">'+
                                '</div>'+
                                '<div class="col-12 col-xxl-3 input-group mt-3">'+
                                    '<div class="input-group-prepend">'+
                                        '<span class="input-group-text font-weight-bold">CLIENTE</span>'+
                                    '</div>'+
                                        '<input type="text" class="form-control"  value="'+data.sac[i].CLIENTE+'">'+
                                '</div>'+
                                '<div class="col-12 input-group mt-3">'+
                                    '<div class="input-group-prepend">'+
                                        '<span class="input-group-text font-weight-bold">QUEIXA</span>'+
                                    '</div>'+
                                        '<textarea class="form-control" >'+data.sac[i].QUEIXA+'</textarea>'+
                                '</div>'+
                                '<div class="col-12 input-group mt-3">'+
                                    '<div class="input-group-prepend">'+
                                        '<span class="input-group-text font-weight-bold">CONSTATADO</span>'+
                                    '</div>'+
                                        '<textarea class="form-control" >'+data.sac[i].CONSTATADO+'</textarea>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '</div>'
                        )
                    }  
                }                  
            },
        }
    )

}
// 888888b.
// 888  "88b
// 888  .88P
// 8888888K.  888  888 .d8888b   .d8888b  8888b.  888d888
// 888  "Y88b 888  888 88K      d88P"        "88b 888P"
// 888    888 888  888 "Y8888b. 888      .d888888 888
// 888   d88P Y88b 888      X88 Y88b.    888  888 888
// 8888888P"   "Y88888  88888P'  "Y8888P "Y888888 888

$(function()
    {
        $('#vai').on('submit',function(e)
            {
                e.preventDefault()
                var data = $('#smart').val();
                ajax(data)
            }
        )
    }
)

// 888     888 d8b 888
// 888     888 Y8P 888
// 888     888     888
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"
//   Y88o88P   888 888  888 888     .d888888 888
//    Y888P    888 888 d88P 888     888  888 888
//     Y8P     888 88888P"  888     "Y888888 888

function vibrate(ms){
    navigator.vibrate(ms);
  }

// 888888b.                                           888
// 888  "88b                                          888
// 888  .88P                                          888
// 8888888K.   8888b.  888d888  .d8888b  .d88b.   .d88888  .d88b.
// 888  "Y88b     "88b 888P"   d88P"    d88""88b d88" 888 d8P  Y8b
// 888    888 .d888888 888     888      888  888 888  888 88888888
// 888   d88P 888  888 888     Y88b.    Y88..88P Y88b 888 Y8b.
// 8888888P"  "Y888888 888      "Y8888P  "Y88P"   "Y88888  "Y8888

if(window.location.hash.substr(1,2) == "zx"){
    var bc = window.location.hash.substr(3);
    localStorage["barcode"] = decodeURI(window.location.hash.substr(3))
    window.close();
    self.close();
    window.location.href = "about:blank";//In case self.close isn't allowed
}
var changingHash = false;
function onbarcode(event){
    switch(event.type){
        case "hashchange":{
            if(changingHash == true){
                return;
            }
            var hash = window.location.hash;
            if(hash.substr(0,3) == "#zx"){
                hash = window.location.hash.substr(3);
                changingHash = true;
                window.location.hash = event.oldURL.split("\#")[1] || ""
                changingHash = false;
                processBarcode(hash);
            }

            break;
        }
        case "storage":{
            window.focus();
            if(event.key == "barcode"){
                window.removeEventListener("storage", onbarcode, false);
                processBarcode(event.newValue);
            }
            break;
        }
        default:{
            console.log(event)
            break;
        }
    }
}
window.addEventListener("hashchange", onbarcode, false);

function getScan(){
    var href = window.location.href;
    var ptr = href.lastIndexOf("#");
    if(ptr>0){
        href = href.substr(0,ptr);
    }
    window.addEventListener("storage", onbarcode, false);
    setTimeout('window.removeEventListener("storage", onbarcode, false)', 15000);
    localStorage.removeItem("barcode");
    //window.open  (href + "#zx" + new Date().toString());

    if(navigator.userAgent.match(/Firefox/i)){
        //Used for Firefox. If Chrome uses this, it raises the "hashchanged" event only.
        window.location.href =  ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
    }else{
        //Used for Chrome. If Firefox uses this, it leaves the scan window open.
        window.open   ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
    }
}

function processBarcode(bc){
        document.getElementById("smart").value = bc;
        $('html, body').animate({scrollTop:0},500);
   
    //put your code in place of the line above.
}