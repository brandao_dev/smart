<?php
include '../CORE/Header2.php';

$sql = conex()->query("SELECT smart, linha, modelo, enge, ALTERACAO FROM codigo WHERE linha LIKE 'REF%' ORDER BY ALTERACAO DESC LIMIT 27")->fetchAll(PDO::FETCH_ASSOC);


?>
<script type='text/javascript'>
<?php
$js_array = json_encode($sql);
echo "var sql = ". $js_array . ";\n";
?>
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="PRODUCAO.js"></script>
<style> .swal {width:950px !important; } </style>

<div class="row">
	<div class="col-12 col-xl-4 mt-2">
		<div class="card">
		<div class="card-body shadow">
			<h2 class="card-title text-left"><i class="fas fa-person-dolly mr-2"></i>Em operação</h2>
			<div id="aging" style="width: auto; height: 460px"></div>
		</div>
		</div>
	</div>
	<div class="col-12 col-xl-8 mt-2">
		<div class="card">
			<div class="card-body shadow">
				<h2 class="card-title text-left"><i class="fas fa-running mr-2"></i>Em atividade</h2>
				<table class="table table-bordered table-striped table-sm">
					<thead class="thead-dark">
						<tr>
							<th>SMART</th>
							<th>LINHA</th>
							<th>MODELO</th>
							<th>ENGENHARIA</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$i=0;
						foreach($sql as $linha)
						{
							if($i<12){
								echo
								'<tr>
									<td>'.$linha['smart'].'</td>
									<td>'.$linha['linha'].'</td>
									<td>'.$linha['modelo'].'</td>
									<td>'.$linha['enge'].'</td>
								</tr>';
							};
							$i++;
						} ?>
					</tbody>
				</table>				
			</div>
		</div>
	</div>
	<div class="col-4 mt-4">
		<div class="card" style="height: 330px;">
			<div class="card-body shadow">
				<h2 class="card-title text-left"><i class="fas fa-boxes mr-2"></i>Aguardando Reparo</h2>
				<table class="table table-bordered table-striped table-sm">
					<thead class="bg-cyan">
						<tr>
							<th>CATEGORIA</th>
							<th>QUANTIDADE</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>REFRIGERAÇÃO</td>
							<td>281</td>
						</tr>
						<tr>
							<td>COCÇÃO</td>
							<td>10</td>
						</tr>
						<tr>
							<td>LAVANDERIA</td>
							<td>41</td>
						</tr>
						<tr>
							<td>OUTROS</td>
							<td>77</td>
						</tr>
						<tr class="bg-cyan font-weight-bold">
							<td>TOTAL</td>
							<td>409</td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>
	</div>
	<div class="col-8 mt-4">
		<div class="card" style="height: 330px;">
			<div class="card-body shadow">
				<div class="row">
					<div class="col-12 col-sm-3 col-xl-3">
						<h2><i class="fas fa-chart-line mr-2 text-left"></i>Meta %</h2>
					</div>
					<div class="col-12 col-sm-9 col-xl-9">
						<h2><i class="fas fa-chart-area mr-2"></i>Performance</h2>
					</div>
					<div class="col-3 col-xl-3">
						<div id="1"></div>
					</div>
					<div class="col-9 col-xl-9">					
						<div id="2" style="width: 100%; height: 50%"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<?php include('../CORE/Footer2.php')?>