<?php include('leitura.php')?>
<!-- ==================================================== -->
<script src="upload.js"></script>
<style>.swal {width:950px !important;}</style>
<!-- ==================================================== -->
<div class="row">
  <div class="col-3">
    <div class="alert alert-block alert-primary"><i class="fas fa-ghost mr-2"></i>Irregulares: <?php echo $pendentes?></div>
  </div>
  <div class="col-3">
    <div class="alert alert-block alert-primary"><i class="fas fa-comment-smile mr-2"></i>Confirmados: <?php echo $agendados?></div>
  </div>
  <div class="col-3">
    <div class="alert alert-block alert-primary"><i class="fas fa-stamp mr-2"></i>Atendidos: <?php echo $atendidos?></div>
  </div>
  <div class="col-3">
    <button type="button" class=" btn btn-lg btn-block btn-primary shadow" onclick='location.reload()'><i class="fas fa-recycle mr-2"></i>Atualizar</button>
  </div>
</div>
<table class="table table-striped table-bordered">
  <thead class="bg-secondary text-light">
    <tr>
      <th scope="col">STATUS</th>
      <th scope="col">AGENDA</th>
      <th scope="col">O.S.</th>
      <th scope="col">LOCAL</th>
      <th scope="col">BOLETIM</th>
      <th scope="col">VISTA</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $tabela?>
  </tbody>
</table>

<?php include ('../CORE/Footer2.php')?>