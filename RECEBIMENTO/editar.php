<?PHP include('../CORE/PDO.php');

if(isset($_GET['id']))
{
    $id = $_GET['id'];

    $caminho = '../BOOK/'.$id.'/';
    $img = glob($caminho.'*.jpg');
}
    
?>  
<!DOCTYPE HTML>
<html lang='pt-br'>
<head>
<meta charset='UTF-8'>
<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
<meta name='autor' content='A. Brandão Full Stack Developer 2020 / PHPOO, Composer, NodeJs, SASS, Gulp, MySQL, CSS, HTML5, JavaScript'>
<meta name='format-detection' content='telephone-no'>
<title>ERP - SMART</title>
<link rel='shortcut icon' href='../IMG/SICON.png'>
<link rel='stylesheet' href='https://brandev.tech/css/bootstrap.css'>
<link href='https://cdn.brandev.site/css/all.css' rel='stylesheet'>
<script src="https://cdn.brandev.site/js/sweetalert2.all.min.js"></script>
<link rel="stylesheet" href="https://cdn.brandev.site/css/sweetalert2.min.css">
<script src="https://cdn.brandev.site/js/jquery.js"></script>
<script src="https://cdn.brandev.site/js/bootstrap.js"></script>
<script src="https://cdn.brandev.site/js/popper.js"></script>
</head>
<body  class='text-center'>
<!-- AUDIO PADRÂO -->
<audio id="click" src="../SOM/click.mp3"></audio>
<script>
      function play() {
        var click = document.getElementById("click");
        click.play();
      }
</script>
<!-- MARGEM DO CORPO -->
<div class='container-fluid'>
<!-- 
88888888888888888888888888888888888888888888888888888888888

                  888                  d8b          
                  888                  Y8P          
                  888                               
 .d88b.   8888b.  888  .d88b.  888d888 888  8888b.  
d88P"88b     "88b 888 d8P  Y8b 888P"   888     "88b 
888  888 .d888888 888 88888888 888     888 .d888888 
Y88b 888 888  888 888 Y8b.     888     888 888  888 
 "Y88888 "Y888888 888  "Y8888  888     888 "Y888888 
     888                                            
Y8b d88P                                            
 "Y88P"                                                               

888888888888888888888888888888888888888888888888888888888888
 -->
<form action="grava.php" method="POST">
<div class="row mt-4">
    <?php foreach($img as $im) { ?>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card-group">
                <div class="card mt-2 shadow">
                    <img src="<?php echo $im;?>" class="card-img-top">
                    <div class="card-body bg-light">
                        <div class="form-check">
                            <input class="form-check-input" name="delete[]" type="checkbox" value="<?php echo $im?>" id="<?php echo $im?>">
                            <label class="form-check-label" for="<?php echo $im?>">
                            <i class="fas fa-trash-alt mr-2"></i>Excluir
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<hr>
<input type="text" value="<?php echo $id?>" name="id" style="display: none;">

<div class="row mt-3 justify-content-center">
    <div class="col-10">
        <button type="submit" class="btn btn-lg btn-block btn-danger shadow"><h4><i class="fas fa-trash fa-lg mr-2"></i>Excluir</h4></button>
    </div>

</form>
</div>
<hr>

<!-- 
8888888888888888888888888888888888888888888888888888888

                            888          888 
                            888          888 
                            888          888 
88888b.d88b.   .d88b.   .d88888  8888b.  888 
888 "888 "88b d88""88b d88" 888     "88b 888 
888  888  888 888  888 888  888 .d888888 888 
888  888  888 Y88..88P Y88b 888 888  888 888 
888  888  888  "Y88P"   "Y88888 "Y888888 888 

8888888888888888888888888888888888888888888888888888888
 -->
 <div class="row mt-3 justify-content-center">
    <div class="col-10">
        <!-- Botão para acionar modal -->
        <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modalExemplo">
        <h4><i class="fas fa-camera mr-2"></i>Adicionar Fotos</h4>
        </button>

        <!-- Modal -->
        <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-camera mr-2"></i>Adicionar Foto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <h5><span id="file-name" class="font-weight-bold"></span></h5>

                    <form action="imagem.php" method="post" enctype="multipart/form-data">
                        <!-- FOTO -->
                            <div class="col">
                                <label for="input-file" class="btn btn-lg btn-block btn-primary shadow mt-3"><i class="fas fa-search fa-lg mr-2"></i>ARQUIVO</label>
                                <input type="file" id="input-file" style="display:none" name="image" accept="image/*" value="">
                            </div>
                            <div class="col">
                                <label for="input-cam" class="btn btn-lg btn-block btn-warning shadow mt-3"><i class="fas fa-camera fa-lg mr-2"></i>CÂMERA</label>
                                <input type="file" id="input-cam" style="display:none" name="cam" accept="image/*" value="" capture="camera">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-lg btn-block btn-success shadow mt-3"><i class="fas fa-save fa-lg mr-2"></i>GRAVAR</button>
                            </div>
                            <input type="text" style="display: none;" name="id" value="<?php echo $id?>">
                    </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-undo-alt mr-2"></i>Cancelar</button>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <hr>
    <div class="row mt-3 justify-content-center">
    <div class="col-10">
        <!-- Botão para acionar modal -->
        <button type="button" class="btn btn-secondary btn-block" onclick="window.close()">
        <h4><i class="fas fa-check mr-2"></i>Concluir</h4>
        </button>
    </div>
    </div>
</div>
<br>

</div>
<!-- 
8888888888888888888888888888888888888888888888888888888888888888

                          d8b          888    
                          Y8P          888    
                                       888    
.d8888b   .d8888b 888d888 888 88888b.  888888 
88K      d88P"    888P"   888 888 "88b 888    
"Y8888b. 888      888     888 888  888 888    
     X88 Y88b.    888     888 888 d88P Y88b.  
 88888P'  "Y8888P 888     888 88888P"   "Y888 
                              888             
                              888             
                              888             

888888888888888888888888888888888888888888888888888888888888888
 -->
<script>
    var $input = document.getElementById('input-file'),
        $cam = document.getElementById('input-cam'),
        $fileName = document.getElementById('file-name');

    $input.addEventListener('change', function(){
        $fileName.textContent = 'Imagem carregada com sucesso!';
    });
    $cam.addEventListener('change', function(){
        $fileName.textContent = 'Imagem capturada com sucesso!';
    });
</script>

<?php include('../CORE/Footer2.php')?>