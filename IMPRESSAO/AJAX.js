// CARREGA AUTOMATICO URL VAR
const get = window.location.search.split('=')[1]
if(get != undefined)
{
    window.onload = ajax(get)
}

function ajax(data)
{
    // SESSÂO PARA SMART ID NO PROCEL
    sessionStorage.setItem('smart',data)
    $.ajax(
    {
        type: 'POST',
        dataType: 'json',
        url: 'LEITURA.php',
        data: 'id='+data,
        async: true,
        success: function(data)
        {
            $('#caso').html(data.caso);
            $('#player').html(data.fala);
            $('#option1').prop('checked',false);
            $('#la').removeClass();
            $('#la').addClass('btn btn-outline-primary w-100 btn-lg');
            $('#option2').prop('checked',false);
            $('#lb').removeClass();
            $('#lb').addClass('btn btn-outline-success w-100 btn-lg');
            $('#option3').prop('checked',false);
            $('#lc').removeClass();
            $('#lc').addClass('btn btn-outline-danger w-100 btn-lg');
            $('#linha').html(data.sql.linha+' - '+data.sql.fabricante+' - '+data.sql.cor);
            $('#modelo').html('MODELO - '+data.sql.modelo+' / ENG-'+data.sql.enge);
            $('#serial').html('SERIAL - '+data.sql.smart);
            if(data.sql.grade != undefined && data.sql.grade != '' && data.sql.grade != null)
            {
                $('#grade').html('<img src="../IMG/IMP/'+data.sql.grade+'.png" width="60px">');
            }
            $('#ean').html('EAN - '+data.model.EAN);
            $('#voltagem').html(data.sql.voltagem);
            $('#tabela').empty();
            $('#procel').empty();

            //IMAGENS
            $('#im').html('');
            $.each(data.img,function(index,element){
                $('#im').append('<img src="'+element+'" class="rounded shadow" width="400px">');
            })
            
            //BARRAS
            $(document).ready(function(){
            $("#2").barcode(data.sql.smart,"code128",{barWidth:3,barHeight:60,fontSize: 0});
            $("#3").barcode(data.model.EAN,"code128",{barWidth:3,barHeight:60,fontSize: 0});
            });

            //REFRIGERAÇÃO (APENAS REFRIGERADOR E FREEZER)
            if(data.model.procel == 'R' && ((data.model.linha.substring(0,3) == 'REF') ||(data.model.linha.substring(0,3) == 'FRE')))
            {
                //SELECIONAR CARGA ORIGINAL OU ALTERNATIVA
                if(data.model.tipo == data.sql.CARGA ||data.sql.CARGA == '')
                {
                    carga = data.model.carga.split('.')[0];
                }
                else
                {
                    carga = data.model.carga.split('.')[1]
                }
                let refri;
                if(data.sql.CARGA == '')
                {
                    refri = data.model.tipo
                }
                else
                {
                    refri = data.sql.CARGA
                }
                $('#tabela').append(
                '<tr>'+
                    '<td style="border: 1px solid  #000 !important;">CARG.REF</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+carga+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">VOL.T.BRU</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.voltb+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">DEGELO</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.dgelo+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td style="border: 1px solid  #000 !important;">TP.REFRIG</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+refri+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">VOL.B.RFG</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.volbr+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">POTENCIA</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.potencia+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td style="border: 1px solid  #000 !important;">TENSÃO</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.tensao+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">VOL.B.FRZ</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.volbf+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">ISOL.TERM</td>'+
                    '<td style="border: 1px solid  #000 !important;">C-PENTANO</td>'+
                '</tr>'+
                '<tr>'+
                    '<td style="border: 1px solid  #000 !important;">FREQUENCIA</td>'+
                    '<td style="border: 1px solid  #000 !important;">60Hz</td>'+
                    '<td style="border: 1px solid  #000 !important;">VOL.T.ARM</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.volta+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">CORRENTE</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.corrente+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td style="border: 1px solid  #000 !important;">CAP.C.24H</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.cap+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">VOL.A.FRZ</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.volaf+'</td>'+
                    '<td style="border: 1px solid  #000 !important;">VOL.A.FRG</td>'+
                    '<td style="border: 1px solid  #000 !important;">'+data.model.volar+'</td>'+
                '</tr>'
                )
            }
            procelGrupos(data)
        },
        error: function(data)
        {
            console.log('Deu ruim'+data)
        }
    }
    )
}

//BUSCA AUTOMATICA ID
$(function()
{
    $("#etiqueta").on("keyup",function()
    {
        ajax($(this).val())
    }
    )
    
}
)

// BUSCAAUTOMATICA MODELO
$(function()
{    
    $('#modeloProcel').on('keyup', function()
    {
        let modelo = $('#modeloProcel').val()        
        $('#procel').empty()


        $('#model').find('option').each(function()
        { 
            if($(this).val() == modelo)
            {               
                $.ajax(
                    {
                        type: 'POST',
                        dataType: 'json',
                        async: true,
                        url: 'modelo.php',
                        data: 'modelo='+modelo,
                        success: function (data){
                            console.log('ok')
        
                            procelGrupos(data)
                        }
                    }
                )
            }
        })
    })
})

// .d8888b.                        888          
// d88P  Y88b                       888          
// 888    888                       888          
// 888        888d888  8888b.   .d88888  .d88b.  
// 888  88888 888P"       "88b d88" 888 d8P  Y8b 
// 888    888 888     .d888888 888  888 88888888 
// Y88b  d88P 888     888  888 Y88b 888 Y8b.     
//  "Y8888P88 888     "Y888888  "Y88888  "Y8888  

function grade(grade,id)
{
    if(grade != null)
    {
        $('#grade').html('<img src="../IMG/IMP/'+grade+'.png" width="60px">')
    }
    $.ajax(
        {
            type: 'POST',
            dataType: 'json',
            async: true,
            url: 'GRADE.php',
            data: {grade:grade,id:id},
            success: function(data){
                $('#caso').html(data.caso);
                $('#player').html(data.fala);
            }
        }
    )
}

 
// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

function vibrate(ms){
    navigator.vibrate(ms);
  }

//   8888888b.                                     888 
//   888   Y88b                                    888 
//   888    888                                    888 
//   888   d88P 888d888  .d88b.   .d8888b  .d88b.  888 
//   8888888P"  888P"   d88""88b d88P"    d8P  Y8b 888 
//   888        888     888  888 888      88888888 888 
//   888        888     Y88..88P Y88b.    Y8b.     888 
//   888        888      "Y88P"   "Y8888P  "Y8888  888 

function procel()
{
    //VOLUME REFRIGERADOR
    let volumeRefrigerador = $('#volr').val();
    if(volumeRefrigerador.length < 4)
    {
        Swal.fire(
            {
                title: 'Erro',
                html: 'Formato inválido ou vazio para o </p><i class="fas fa-turkey mr-2"></i>Volume do Refrigerador!',
                imageUrl: '../IMG/IMP/REFRIGERADOR.jpg',
                imageWidth: 400,
                imageHeight: 200
            }
        )
    }  
    else
    {              
        //VOLUME CONGELADOR
        let volumeCongelador = $('#volc').val();
        if(volumeCongelador.length < 4)
        {
            Swal.fire(
                {
                    title: 'Erro',
                    html: 'Formato inválido ou vazio para o </p><i class="fas fa-ice-cream mr-2"></i>Volume do Congelador!',
                    imageUrl: '../IMG/IMP/CONGELADOR.jpg',
                    imageWidth: 400,
                    imageHeight: 200
                }
            )
        }   
        else
        {     
            //VOLUME TOTAL
            let volumeTotal = $('#volt').val();
            if(volumeTotal.length < 4)
            {
                Swal.fire(
                    {
                        title: 'Erro',
                        html: 'Formato inválido ou vazio para o </p><i class="fas fa-flask mr-2"></i>Volume Total Bruto!',
                        imageUrl: '../IMG/IMP/TOTAL.jpg',
                        imageWidth: 400,
                        imageHeight: 200
                    }
                )
            }
            else
            {        
                //EFICIENCIA
                if(
                    $("#A:checked").length == 0 &&
                    $("#B:checked").length == 0 &&
                    $("#C:checked").length == 0 &&
                    $("#D:checked").length == 0 &&
                    $("#E:checked").length == 0
                    )
            
                {
                    Swal.fire(
                        {
                            title: 'Erro',
                            html: 'Não foi escolhida a </p><i class="fas fa-tachometer-alt mr-2"></i>EFICIENCIA!',
                            imageUrl: '../IMG/IMP/EFICIENCIA.jpg',
                            imageWidth: 400,
                            imageHeight: 200
                        }
                    )
                }  
                else
                {           
                    //DEGELO
                    if(
                        $("#AUTOMATICO:checked").length == 0 &&
                        $("#SEMI-AUTOMATICO:checked").length == 0 &&
                        $("#MANUAL:checked").length == 0
                        )
                
                    {
                        Swal.fire(
                            {
                                title: 'Erro',
                                html: 'Não foi escolhido o </p><i class="fas fa-icicles mr-2"></i>DEGELO!',
                                imageUrl: '../IMG/IMP/DEGELO.jpg',
                                imageWidth: 400,
                                imageHeight: 200
                            }
                        )
                    }   
                    else
                    {               
                        //KWHM
                        let kwhm = $('#kwhm').val();
                        if(kwhm.length < 3)
                        {
                            Swal.fire(
                                {
                                    title: 'Erro',
                                    html: 'Formato inválido ou vazio para </p><i class="fas fa-bolt mr-2"></i>KWh/mês!',
                                    imageUrl: '../IMG/IMP/KWHM.jpg',
                                    imageWidth: 400,
                                    imageHeight: 200
                                }
                            )
                        }  
                        else
                        {                   
                            //TEMPERATURA
                            let temperatura = $('#temp').val();
                            if(temperatura.length < 3)
                            {
                                Swal.fire(
                                    {
                                        title: 'Erro',
                                        html: 'Formato inválido ou vazio para </p><i class="fas fa-temperature-low mr-2"></i>TEMPERATURA!',
                                        imageUrl: '../IMG/IMP/TEMPERATURA.jpg',
                                        imageWidth: 400,
                                        imageHeight: 200
                                    }
                                )
                            } 
                            else
                            {                          
                                //REGISTRO
                                let registro = $('#regi').val();
                                if(registro.length < 11)
                                {
                                    Swal.fire(
                                        {
                                            title: 'Erro',
                                            html: 'Formato inválido ou vazio para </p><i class="fas fa-stamp mr-2"></i>REGISTRO!',
                                            imageUrl: '../IMG/IMP/REGISTRO2.jpg',
                                            imageWidth: 400,
                                            imageHeight: 200
                                        }
                                    )
                                }
                                else
                                {
                                    let dados = $('#form').serialize()
                                    let modelo = $('input[name="modelo"]').val()
                                    $.ajax(
                                        {
                                            type: 'POST',
                                            dataType: 'json',
                                            url: 'PROCEL.php',
                                            async: true,
                                            data: dados,
                                            success: function(){
                                                window.open('../PROCEL/PROCEL.php?modelo='+modelo);
                                            },
                                            error: function(){
                                                console.log('caca')
                                            }
                                        }
                                    )
                                    }
                                }
                            } 
                        } 
                    }
                } 
            }  
        }
    }

// 8888888b.                                     888         .d8888b.  
// 888   Y88b                                    888        d88P  Y88b 
// 888    888                                    888        888    888 
// 888   d88P 888d888  .d88b.   .d8888b  .d88b.  888        888        
// 8888888P"  888P"   d88""88b d88P"    d8P  Y8b 888        888        
// 888        888     888  888 888      88888888 888 888888 888    888 
// 888        888     Y88..88P Y88b.    Y8b.     888        Y88b  d88P 
// 888        888      "Y88P"   "Y8888P  "Y8888  888         "Y8888P"     

function procelc()
{
    //CONSUMO
    let consumo = $('#consumo').val();
    if(consumo.length < 4)
    {
        Swal.fire(
            {
                title: 'Erro',
                html: 'Formato inválido ou vazio para o </p><i class="fas fa-gas-pump mr-2"></i>Consumo!',
                imageUrl: '../IMG/IMP/CONSUMO.jpg',
                imageWidth: 400,
                imageHeight: 200
            }
        )
    }  
    else
    {              
        //VOLUME
        let volume = $('#volume').val();
        if(volume.length < 3)
        {
            Swal.fire(
                {
                    title: 'Erro',
                    html: 'Formato inválido ou vazio para o </p><i class="fas fa-ruler-combined mr-2"></i>Volume!',
                    imageUrl: '../IMG/IMP/VOLUME.jpg',
                    imageWidth: 400,
                    imageHeight: 200
                }
            )
        }   
        else
        {     
            //RENDIMENTO
            let rendimento = $('#rendimento').val();
            if(rendimento.length < 2)
            {
                Swal.fire(
                    {
                        title: 'Erro',
                        html: 'Formato inválido ou vazio para o </p><i class="fas fa-donate mr-2"></i>Rendimento!',
                        imageUrl: '../IMG/IMP/RENDIMENTO.jpg',
                        imageWidth: 400,
                        imageHeight: 200
                    }
                )
            }
            else
            {        
                //EFICIENCIA1
                if(
                    $("#A1:checked").length == 0 &&
                    $("#B1:checked").length == 0 &&
                    $("#C1:checked").length == 0 &&
                    $("#D1:checked").length == 0 &&
                    $("#E1:checked").length == 0
                    )
            
                {
                    Swal.fire(
                        {
                            title: 'Erro',
                            html: '<i class="fas fa-tachometer-alt mr-2"></i>Eficiência dos Queimadores não escolhida!',
                            imageUrl: '../IMG/IMP/EFICIENCIA1.jpg',
                            imageWidth: 400,
                            imageHeight: 200
                        }
                    )
                }  
                else
                {          
                    //EFICIENCIA1
                    if(
                        $("#A2:checked").length == 0 &&
                        $("#B2:checked").length == 0 &&
                        $("#C2:checked").length == 0 &&
                        $("#D2:checked").length == 0 &&
                        $("#E2:checked").length == 0
                        )
                
                    {
                        Swal.fire(
                            {
                                title: 'Erro',
                                html: '<i class="fas fa-tachometer-alt mr-2"></i>Eficiência do Forno não escolhida!',
                                imageUrl: '../IMG/IMP/EFICIENCIA2.jpg',
                                imageWidth: 400,
                                imageHeight: 200
                            }
                        )
                    }
                    else
                    {          
                        //TIPO
                        if(
                            $("#ELETRICO:checked").length == 0 &&
                            $("#GAS:checked").length == 0
                            )
                    
                        {
                            Swal.fire(
                                {
                                    title: 'Erro',
                                    html: '<i class="fas fa-plug mr-2"></i>Tipo não escolhido!',
                                    imageUrl: '../IMG/IMP/TIPO.jpg',
                                    imageWidth: 400,
                                    imageHeight: 200
                                }
                            )
                        }   
                        else
                        {           
                            //GAS
                            if(
                                $("#GLP:checked").length == 0 &&
                                $("#GNV:checked").length == 0
                                )
                        
                            {
                                Swal.fire(
                                    {
                                        title: 'Erro',
                                        html: '<i class="fas fa-monument mr-2"></i>GAS não escolhido!',
                                        imageUrl: '../IMG/IMP/GAS.jpg',
                                        imageWidth: 400,
                                        imageHeight: 200
                                    }
                                )
                            } 
                            else
                            {                          
                                //REGISTRO
                                let registro = $('#regi').val();
                                if(registro.length < 11)
                                {
                                    Swal.fire(
                                        {
                                            title: 'Erro',
                                            html: 'Formato inválido ou vazio para o </p><i class="fas fa-stamp mr-2"></i>REGISTRO!',
                                            imageUrl: '../IMG/IMP/REGISTRO.jpg',
                                            imageWidth: 400,
                                            imageHeight: 200
                                        }
                                    )
                                }
                                else
                                {
                                    let dados = $('#form').serialize();
                                    let modelo = $('input[name="modelo"]').val()
                                    $.ajax(
                                        {
                                            type: 'POST',
                                            dataType: 'json',
                                            url: 'PROCEL-C.php',
                                            async: true,
                                            data: dados,
                                            success: function(){
                                                window.open('../PROCEL/PROCEL-C.php?modelo='+modelo);
                                            },
                                            error: function(){
                                                console.log('caca')
                                            }
                                        }
                                    )
                                }
                            } 
                        }
                    }
                }
            } 
        }  
    }
}

// 8888888b.                                     888        888      
// 888   Y88b                                    888        888      
// 888    888                                    888        888      
// 888   d88P 888d888  .d88b.   .d8888b  .d88b.  888        888      
// 8888888P"  888P"   d88""88b d88P"    d8P  Y8b 888        888      
// 888        888     888  888 888      88888888 888 888888 888      
// 888        888     Y88..88P Y88b.    Y8b.     888        888      
// 888        888      "Y88P"   "Y8888P  "Y8888  888        88888888 

function procelL()
{
    //CONSUMO
    let consumo = $('#consumo').val();
    if(consumo.length < 4)
    {
        Swal.fire(
            'Atenção!',
            'Formato inválido Consumo de Energia!<p><strong>Exemplo: 0,25</strong>',
            'error'
        )
    }  
    else
    {              
        //CAPACIDADE
        let capacidade = $('#capacidade').val();
        if(capacidade.length < 3)
        {
            Swal.fire(
                'Atenção!',
                'Formato inválido ou vazio para o Capacidade de Lavagem<p><strong>Exemplo: 8,0</strong>!',
                'error'
            )
        }   
        else
        {     
            //AGUA
            let agua = $('#agua').val();
            if(agua.length < 2)
            {
                Swal.fire(
                    'Atenção!',
                    'Formato inválido ou vazio para o Consumo de Água</p><strong>Exemplo: 105,7</strong>!',
                    'error'
                )
            }
            else
            {        
                //EFICIENCIA1
                if(
                    $("#A:checked").length == 0 &&
                    $("#B:checked").length == 0 &&
                    $("#C:checked").length == 0 &&
                    $("#D:checked").length == 0 &&
                    $("#E:checked").length == 0
                    )
            
                {
                    Swal.fire(
                        {
                            title: 'Erro',
                            html: '<i class="fas fa-tachometer-alt mr-2"></i>Eficiência Global não escolhida!',
                            imageUrl: '../IMG/IMP/EFICIENCIA1.jpg',
                            imageWidth: 400,
                            imageHeight: 200
                        }
                    )
                }  
                else
                { 
                    let dados = $('#form').serialize();
                    let modelo = $('input[name="modelo"]').val()
                    $.ajax(
                        {
                            type: 'POST',
                            url: 'PROCEL-L.php',
                            async: true,
                            data: dados,
                            success: function(){
                                window.open('../PROCEL/PROCEL-L.php?modelo='+modelo);
                            },
                            error: function(){
                                console.log('caca')
                            }
                        }
                    )
                }
            } 
        }  
    }
}
// .d8888b.                                              
// d88P  Y88b                                             
// 888    888                                             
// 888        888d888 888  888 88888b.   .d88b.  .d8888b  
// 888  88888 888P"   888  888 888 "88b d88""88b 88K      
// 888    888 888     888  888 888  888 888  888 "Y8888b. 
// Y88b  d88P 888     Y88b 888 888 d88P Y88..88P      X88 
//  "Y8888P88 888      "Y88888 88888P"   "Y88P"   88888P' 
//                             888                        
//                             888                        
//                             888                        

function procelGrupos(data)
{
    console.log(data.model.modelo)
    //PROCEL
if(data.model.procel == 'R')
{
    $('#procel').append(
        '<div class="form-group col-12">'+
            '<label for="volr"><i class="fas fa-turkey mr-2"></i>Volume do Refrigerador</label>'+
            '<input type="text" class="form-control" id="volr" name="volr" placeholder="000,0"/>'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="volc"><i class="fas fa-ice-cream mr-2"></i>Volume do Congelador</label>'+
            '<input type="text" class="form-control" id="volc" name="volc" placeholder="000,0">'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="volt"><i class="fas fa-flask mr-2"></i>Volume Total</label>'+
            '<input type="text" class="form-control" id="volt" name="volt" placeholder="000,0">'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="efic"><i class="fas fa-tachometer-alt mr-2"></i>Eficiência</label></br>'+
            '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons" id="efic">'+
                '<label class="btn btn-outline-success" id="AA">'+
                    '<input type="radio" name="eficiencia" id="A" value="A" autocomplete="off" class="w-100"> A'+
                '</label>'+
                '<label class="btn btn-outline-teal" id="BB">'+
                    '<input type="radio" name="eficiencia" id="B" value="B" autocomplete="off" class="w-100"> B'+
                '</label>'+
                '<label class="btn btn-outline-warning" id="CC">'+
                    '<input type="radio" name="eficiencia" id="C" value="C" autocomplete="off" class="w-100"> C'+
                '</label>'+
                '<label class="btn btn-outline-orange" id="DD">'+
                    '<input type="radio" name="eficiencia" id="D" value="D" autocomplete="off" class="w-100"> D'+
                '</label>'+
                '<label class="btn btn-outline-danger" id="EE">'+
                    '<input type="radio" name="eficiencia" id="E" value="E" autocomplete="off" class="w-100"> E'+
                '</label>'+
            '</div>'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="dege"><i class="fas fa-icicles mr-2"></i>Degelo</label></br>'+
            '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons" id="efic">'+
                '<label class="btn btn-outline-indigo" id="1AUTOMATICO">'+
                    '<input type="radio" name="degelo" id="AUTOMATICO" value="AUTOMATICO" autocomplete="off" class="w-100"> AUTOMATICO'+
                '</label>'+
                '<label class="btn btn-outline-primary" id="1SEMI-AUTOMATICO">'+
                    '<input type="radio" name="degelo" id="SEMI-AUTOMATICO" value="SEMI-AUTOMATICO" autocomplete="off" class="w-100"> SEMI-AUTOMATICO'+
                '</label>'+
                '<label class="btn btn-outline-cyan" id="1MANUAL">'+
                    '<input type="radio" name="degelo" id="MANUAL" value="MANUAL" autocomplete="off" class="w-100"> MANUAL'+
                '</label>'+
            '</div>'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="kwhm"><i class="fas fa-bolt mr-2"></i>KWh/mês</label>'+
            '<input type="number" class="form-control" id="kwhm" name="kwhm" placeholder="00.0">'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="temp"><i class="fas fa-temperature-low mr-2"></i>Temperatura</label>'+
            '<input type="number" class="form-control" id="temp" name="temp" placeholder="00">'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="regi"><i class="fas fa-stamp mr-2"></i>Registro</label>'+
            '<input type="text" class="form-control" id="regi" name="regi" placeholder="000000/0000">'+
        '</div>'+
        '<input type="hidden" value="'+data.model.modelo+'" name="modelo"/>'+
        '<div class="col-12 mt-4">'+
            '<button class="btn btn-lg btn-block btn-danger shadow" id="bot" type="button" onclick="procel()"><i class="fas fa-tags mr-2"></i>PROCEL</button>'+
        '</div>'
    )

    if(data.model.volbr != ''){$('#volr').val(data.model.volbr)};
    if(data.model.volbf != ''){$('#volc').val(data.model.volbf)};
    if(data.model.voltb != ''){$('#volt').val(data.model.voltb)};
    if(data.model.kwhm != ''){$('#kwhm').val(data.model.kwhm)};
    if(data.model.temperatura != ''){$('#temp').val(data.model.temperatura)};
    if(data.model.registro != ''){$('#regi').val(data.model.registro)};

    //ATIVAR CHECKBOX CONFORME LEITURA    
    if(data.model.eficiencia != '')
    {
        $('#'+data.model.eficiencia).prop('checked',true);
        $('#'+data.model.eficiencia+data.model.eficiencia).addClass('active');
    }
    //ATIVAR CHECKBOX CONFORME LEITURA  
    if(data.model.dgelo != '')
    {  
        $('#'+data.model.dgelo).prop('checked',true);
        $('#1'+data.model.dgelo).addClass('active');
    }
    console.log('DEGELO='+data.model.dgelo)
}
else if(data.model.procel == 'C')
{
    //PROCEL
    $('#procel').append(

        //CONSUMO
        '<div class="form-group col-12 col-xl-6">'+
            '<label for="consumo"><i class="fas fa-gas-pump mr-2"></i>Consumo</label>'+
            '<input type="text" class="form-control" id="consumo" name="consumo" placeholder="0,000"/>'+
        '</div>'+

        //VOLUME
        '<div class="form-group col-12 col-xl-6">'+
            '<label for="volume"><i class="fas fa-ruler-combined mr-2"></i>Volume</label>'+
            '<input type="text" class="form-control" id="volume" name="volume" placeholder="00,0">'+
        '</div>'+

        //RENDIMENTO
        '<div class="form-group col-12 col-xl-6">'+
            '<label for="rendimento"><i class="fas fa-donate mr-2"></i>Rendimento</label>'+
            '<input type="text" class="form-control" id="rendimento" name="rendimento" placeholder="00">'+
        '</div>'+

        //REGISTRO
        '<div class="form-group col-12 col-xl-6">'+
            '<label for="regi"><i class="fas fa-stamp mr-2"></i>Registro</label>'+
            '<input type="text" class="form-control" id="regi" name="regi" placeholder="000000/0000">'+
        '</div>'+

        //EFICIENCIA QUEIMADORES
        '<div class="form-group col-12">'+
            '<label for="efic1"><i class="fas fa-tachometer-alt mr-2"></i>Eficiência dos Queimadores de Mesa</label></br>'+
            '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons" id="efic1">'+
                '<label class="btn btn-outline-success" id="A1A1">'+
                    '<input type="radio" name="eficiencia1" id="A1" value="A" autocomplete="off" class="w-100"> A'+
                '</label>'+
                '<label class="btn btn-outline-teal" id="B1B1">'+
                    '<input type="radio" name="eficiencia1" id="B1" value="B" autocomplete="off" class="w-100"> B'+
                '</label>'+
                '<label class="btn btn-outline-warning" id="C1C1">'+
                    '<input type="radio" name="eficiencia1" id="C1" value="C" autocomplete="off" class="w-100"> C'+
                '</label>'+
                '<label class="btn btn-outline-orange" id="D1D1">'+
                    '<input type="radio" name="eficiencia1" id="D1" value="D" autocomplete="off" class="w-100"> D'+
                '</label>'+
                '<label class="btn btn-outline-danger" id="E1E1">'+
                    '<input type="radio" name="eficiencia1" id="E1" value="E" autocomplete="off" class="w-100"> E'+
                '</label>'+
            '</div>'+
        '</div>'+

        //EFICIENCIA FORNO
        '<div class="form-group col-12">'+
            '<label for="efic"><i class="fas fa-tachometer-alt mr-2"></i>Eficiência do Forno</label></br>'+
            '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons" id="efic">'+
                '<label class="btn btn-outline-success" id="A2A2">'+
                    '<input type="radio" name="eficiencia2" id="A2" value="A" autocomplete="off" class="w-100"> A'+
                '</label>'+
                '<label class="btn btn-outline-teal" id="B2B2">'+
                    '<input type="radio" name="eficiencia2" id="B2" value="B" autocomplete="off" class="w-100"> B'+
                '</label>'+
                '<label class="btn btn-outline-warning" id="C2C2">'+
                    '<input type="radio" name="eficiencia2" id="C2" value="C" autocomplete="off" class="w-100"> C'+
                '</label>'+
                '<label class="btn btn-outline-orange" id="D2D2">'+
                    '<input type="radio" name="eficiencia2" id="D2" value="D" autocomplete="off" class="w-100"> D'+
                '</label>'+
                '<label class="btn btn-outline-danger" id="E2E2">'+
                    '<input type="radio" name="eficiencia2" id="E2" value="E" autocomplete="off" class="w-100"> E'+
                '</label>'+
            '</div>'+
        '</div>'+

        //TIPO
        '<div class="form-group col-12">'+
            '<label for="tipo"><i class="fas fa-plug mr-2"></i>Tipo</label></br>'+
            '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons" id="tipo">'+
                '<label class="btn btn-outline-indigo" id="1GAS">'+
                    '<input type="radio" name="tipo" id="GAS" value="GAS" autocomplete="off" class="w-100"> GÁS'+
                '</label>'+
                '<label class="btn btn-outline-indigo" id="1ELETRICO">'+
                    '<input type="radio" name="tipo" id="ELETRICO" value="ELETRICO" autocomplete="off" class="w-100"> ELÉTRICO'+
                '</label>'+
            '</div>'+
        '</div>'+

        //GAS
        '<div class="form-group col-12">'+
            '<label for="g"><i class="fas fa-monument mr-2"></i>Gás</label></br>'+
            '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons" id="g">'+
                '<label class="btn btn-outline-pink" id="1GLP">'+
                    '<input type="radio" name="gas" id="GLP" value="GLP" autocomplete="off" class="w-100"> GLP'+
                '</label>'+
                '<label class="btn btn-outline-pink" id="1GNV">'+
                    '<input type="radio" name="gas" id="GNV" value="GNV" autocomplete="off" class="w-100"> GNV'+
                '</label>'+
            '</div>'+
        '</div>'+

        //MODELO
        '<input style="display:none" value="'+data.model.modelo+'" name="modelo"/>'+
        '<div class="col-12 mt-4">'+

        //BOTÃO
            '<button class="btn btn-lg btn-block btn-danger shadow" id="bot" type="button" onclick="procelc()"><i class="fas fa-tags mr-2"></i>PROCEL</button>'+
        '</div>'
    )
    
    //SE NAO CERTIFICAR QUE VALOR ESTA VAZIO, A MASK NÃO FUNCIONA
    if(data.model.corrente != ''){$('#consumo').val(data.model.corrente)}
    if(data.model.cap != ''){$('#volume').val(data.model.cap)}
    if(data.model.rendimento != ''){$('#rendimento').val(data.model.carga)}
    if(data.model.registro != ''){$('#regi').val(data.model.registro)}

    //ATIVAR CHECKBOX EFICIENCIA QUEIMADORES   
    if(data.model.eficiencia != '')
    { 
        let Q1 = data.model.eficiencia.substring(0,1);
        $('#'+Q1+'1').prop('checked',true);
        $('#'+Q1+'1'+Q1+'1').addClass('active');
        let F2 = data.model.eficiencia.substring(1);
        $('#'+F2+'2').prop('checked',true);
        $('#'+F2+'2'+F2+'2').addClass('active');
    }

    //ATIVAR CHECKBOX TIPO   
    if(data.model.tipo != '')
    { 
        $('#'+data.model.tipo).prop('checked',true);
        $('#1'+data.model.tipo).addClass('active');
    }

    //ATIVAR CHECKBOX GÁS  
    if(data.model.gas != '')
    {  
        $('#'+data.model.gas).prop('checked',true);
        $('#1'+data.model.gas).addClass('active');
    }
}
else if(data.model.procel == 'L')
{
    $('#procel').empty()
    $('#procel').append(
        '<div class="form-group col-12 col-xxxl-4">'+
            '<label for="volr"><i class="fas fa-bolt mr-2"></i>Consumo de Energia</label>'+
            '<div class="input-group">'+
                '<input type="text" class="form-control" id="consumo" name="consumo" value="'+data.model.carga+'" placeholder="0,00"/>'+
                '<div class="input-group-append">'+
                  '<span class="input-group-text">kWh/ciclo</span>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="form-group col-12 col-xxxl-4">'+
            '<label for="volc"><i class="fas fa-washer mr-2"></i>Capacidade de Lavagem</label>'+
            '<div class="input-group">'+
                '<input type="text" class="form-control" id="capacidade" name="capacidade" value="'+data.model.tipo+'" placeholder="00">'+
                '<div class="input-group-append">'+
                '<span class="input-group-text">kg</span>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="form-group col-12 col-xxxl-4">'+
            '<label for="volt"><i class="fas fa-faucet-drip mr-2"></i>Consumo de Água</label>'+            
            '<div class="input-group">'+
                '<input type="text" class="form-control" id="agua" name="agua" value="'+data.model.corrente+'" placeholder="000,0">'+
                '<div class="input-group-append">'+
                '<span class="input-group-text">L/ciclo</span>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="form-group col-12">'+
            '<label for="efic"><i class="fas fa-tachometer-alt mr-2"></i>Eficiência</label></br>'+
            '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons" id="efic">'+
                '<label class="btn btn-outline-success" id="AA">'+
                    '<input type="radio" name="eficiencia" id="A" value="A" autocomplete="off" class="w-100"> A'+
                '</label>'+
                '<label class="btn btn-outline-teal" id="BB">'+
                    '<input type="radio" name="eficiencia" id="B" value="B" autocomplete="off" class="w-100"> B'+
                '</label>'+
                '<label class="btn btn-outline-warning" id="CC">'+
                    '<input type="radio" name="eficiencia" id="C" value="C" autocomplete="off" class="w-100"> C'+
                '</label>'+
                '<label class="btn btn-outline-orange" id="DD">'+
                    '<input type="radio" name="eficiencia" id="D" value="D" autocomplete="off" class="w-100"> D'+
                '</label>'+
                '<label class="btn btn-outline-danger" id="EE">'+
                    '<input type="radio" name="eficiencia" id="E" value="E" autocomplete="off" class="w-100"> E'+
                '</label>'+
            '</div>'+
        '</div>'+
        '<input type="hidden" value="'+data.model.modelo+'" name="modelo"/>'+
        '<div class="col-12 mt-4">'+
            '<button class="btn btn-lg btn-block btn-danger shadow" id="bot" type="button" onclick="procelL()"><i class="fas fa-tags mr-2"></i>PROCEL</button>'+
        '</div>'
    )

    //ATIVAR CHECKBOX CONFORME LEITURA    
    if(data.model.eficiencia != '')
    {
        $('#'+data.model.eficiencia).prop('checked',true);
        $('#'+data.model.eficiencia+data.model.eficiencia).addClass('active');
    }
}
else if(data.model.procel == '')
{
    $('#procel').append('<h4>Atenção. O <a href=""  data-toggle="modal" data-target="#modalExemplo">PROCEL</a> deste modelo não foi definido. Clique <a href="../MODELO.php" target="blank">aqui</a> para definir. Depois volte para imprimir o PROCEL.</h4>')

}
else
{
    $('#procel').append('<h4>Atenção. Não foi desenvolvido PROCEL para esta linha de Produtos. Em caso de dúvida entre em contato com o administrador</h4>')

}
//MASK FUNCTION
$(function() {
    VMasker($("#consumo")).maskPattern('9,999');
    VMasker($("#volume")).maskPattern('99,9');
    VMasker($("#rendimento")).maskPattern('99');
    VMasker($("#regi")).maskPattern('999999/9999');
    VMasker($("#volr")).maskPattern('999,9');
    VMasker($("#volc")).maskPattern('999,9');
    VMasker($("#volt")).maskPattern('999,9');
    VMasker($("#kwhm")).maskPattern('99.9');
    VMasker($("#temp")).maskPattern('-99');
    VMasker($("#capacidade")).maskPattern('99,9');
    VMasker($("#agua")).maskPattern('999,9');
});

}