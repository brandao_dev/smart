<?php include ('../CORE/Header2.php');
$abertos = count(conex()->query("SELECT id FROM sac WHERE STR_TO_DATE(SUBSTR(AGENDA,-10,10),'%d/%m/%Y') = '$dia' AND ATENDIDO = 0")->fetchAll(PDO::FETCH_ASSOC));
$fechados = count(conex()->query("SELECT id FROM sac WHERE STR_TO_DATE(SUBSTR(AGENDA,-10,10),'%d/%m/%Y') = '$dia' AND ATENDIDO = 1")->fetchAll(PDO::FETCH_ASSOC));
?>
<!-- ==================================================== -->
<script src="upload.js"></script>
<script src="../AGING/BADGE.js"></script>
<style>.swal {width:950px !important;}</style>
<!-- ==================================================== -->
<div class="row">
  <div class="col-12 col-sm-6">
    <div class="alert alert-block alert-pink font-weight-bold">
      <h5>
        <i class="fas fa-siren-on mr-2"></i>Abertos: <span class="ml-2 badge badge-danger"><?php echo $abertos?></span>
      </h5>
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="alert alert-block alert-success font-weight-bold">
      <h5>
       <i class="far fa-shield-check mr-2"></i>Concluídos: <span class="ml-2 badge badge-success"><?php echo $fechados?></span>
      </h5>
    </div>
  </div>
</div>

<div class="row mb-3 mt-2">

  <?php
    // PEGAR ATENDIMENTOS DE ULTIMA AGENDA MAIOR OU IGUAL A HOJE
    $sql = conex()->query("SELECT id, ALTERACAO, AGENDA, SAC, BAIRRO, CIDADE, ENDERECO, NUMERO, MODELO, SMART, CLIENTE, TEL, QUEIXA, ATENDIDO, STATUS  FROM sac WHERE STR_TO_DATE(SUBSTR(AGENDA,-10,10),'%d/%m/%Y') = '$dia' ORDER BY ALTERACAO")->fetchAll(PDO::FETCH_ASSOC);
    foreach($sql as $linha)
    {
      // BUSCAR BOLETIM
      $boletim = file_exists("../BOLETIM/{$linha['MODELO']}.pdf") ? "<button class='btn btn-cyan shadow' onclick='play(),vibrate(100),abre(\"BOLETIM\",\"{$linha['MODELO']}\")'>SIM</button>" : "<button class='btn btn-danger shadow' onclick='upload(\"b{$linha['id']}\",\"BOLETIM\",\"{$linha['MODELO']}\")'>NÃO</button>";
      $vista = file_exists("../VISTA/{$linha['MODELO']}.pdf") ? "<button class='btn btn-cyan' onclick='play(),vibrate(100),abre(\"VISTA\",\"{$linha['MODELO']}\")'>SIM</button>" : "<button class='btn btn-danger' onclick='upload(\"{$linha['id']}\",\"VISTA\",\"{$linha['MODELO']}\")'>NÃO</button>";
      $percurso[$linha['id']] = $linha['ENDERECO'].',+'.$linha['NUMERO'].',+'.$linha['BAIRRO'].',+'.$linha['CIDADE'];
    ?>

    <!-- //CARD CINZA SE ATENDIDO FOR DIFERENTE DE ZERO -->
    <div class="col-12 col-lg-6 col-xxxl-3 mt-3" id="<?php echo $linha['SMART']?>" <?php if($linha['ATENDIDO'] != 0) echo 'style="-webkit-filter: grayscale(100%)"'?>>
      <div class='card border-brown shadow mb-4'>
        <div class='card-header bg-brown'>
          <div class="row">
            <div class="col-2">
            <!-- ORDEM -->
              <h4><span class="badge badge-pill badge-warning" id='<?php echo 'o'.$linha['id']?>'></span></h4>
            </div>
            <!-- SELECT -->
            <div class="col-2">
              <div class='btn-group-toggle' data-toggle='buttons'>
                <label class='btn btn-outline-orange'  onclick='play(),vibrate(100),order("<?php echo $linha['id']?>")'>
                  <input type='checkbox' id='<?php echo 'i'.$linha['id']?>' value='<?php echo $linha['id']?>' name='ordem[]'><i class='fas fa-check'></i>
                </label>
              </div>
            </div>
            <div class="col">
              <label class="h4 text-light">
                <?php echo ($linha['ATENDIDO'] == 0) ?  $linha['SAC'].' - '.$linha['MODELO'] : '<a class="btn btn-cyan" href="../SAC/SAC.php?sac='.$linha['SAC'].'" target="_blank">'.$linha['SAC'].'</a> - '.$linha['STATUS'];?>
              </label>
            </div>
          </div>
        </div>

        <div class='card-body bg-caqui'>

          <!-- CLIENTE -->
          <h5 class='card-title'>
            <div class="row justify-content-around">
              <div class="col-12 mb-3">
                <a class='btn btn-cyan btn-block shadow' 
                  href='https://www.google.com.br/maps/dir/R.+Galatea,+1560+-+Carandiru,+São+Paulo+-+SP,+02068-000,+Brasil/
                  <?php echo $linha['ENDERECO'].','.$linha['NUMERO'].','.$linha['BAIRRO'].','.$linha['CIDADE']?>'
                  target='blank' onclick="play(),vibrate(100)"><i class="fas fa-map-signs mr-2"></i></i><?php echo $linha['ENDERECO'].', '.$linha['NUMERO'].'<br>'.$linha['BAIRRO'].', '.$linha['CIDADE']?>
                </a>
              </div>              
              <div class="col-12 text-left">

                <!-- CLIENTE -->
                <p class="text-danger"><i class="fas fa-user mr-2"></i><?php echo $linha['CLIENTE']?></p>

                <p class="text-dark h3">
                <!-- TELEFONE -->
                  <a onclick='play(),vibrate(100)' class="btn btn-purple shadow mr-3" href="tel:<?php echo $linha['TEL']?>"><i class="fas fa-phone-alt fa-2x"></i></a>
                <!-- WHATSAPP -->
                  <a onclick='play(),vibrate(100)' class="btn btn-success shadow mr-3" href="https://api.whatsapp.com/send?phone=55<?php echo preg_replace('/[^0-9]/', '', $linha['TEL'])?>" target="_blank"><i class="fab fa-whatsapp fa-2x"></i></a><?php echo $linha['TEL']?></p><hr>
                <?php 
                  $expo = null;
                  $expo = explode('/',$linha['QUEIXA']);
                  foreach($expo as $q)
                  {
                    echo "<p class='text-secondary'><i class='fas fa-comment-exclamation mr-2'></i>{$q}</p><hr>";
                  }
                ?>
                
              </div>
              <div class="col">
                <button class='btn btn-cyan btn-block shadow' onclick='play(),vibrate(100),abre("BOLETIM","<?php echo $linha['MODELO']?>")'><i class="far fa-newspaper mr-2"></i>BOLETIM</button>
              </div>
              <div class="col">
                <button class='btn btn-cyan btn-block shadow' onclick='play(),vibrate(100),abre("VISTA","<?php echo $linha['MODELO']?>")'><i class="fas fa-project-diagram mr-2"></i>VISTA</button>
              </div>
            </div>
          </h5>
          <hr>
          <div class="row">
            <div class="col-12 col-sm-6 mt-3">
              <button type="button" class='btn btn-lg btn-warning btn-block' onclick='play(),vibrate(100),atendido("<?php echo $linha['SMART']?>")'><i class="fas fa-thumbs-up mr-2"></i>ATENDIDO</button>
            </div>
            <div class="col-12 col-sm-6 mt-3">
              <button type="button" class='btn btn-lg btn-danger btn-block' onclick='play(),vibrate(100),conclusao("02.NEGADO","<?php echo $linha['SMART']?>")'><i class="fas fa-thumbs-down mr-2"></i>NEGADO</button>
            </div>
          </div>
        </div>
      </div>
      </div>
<?php } ?>
    <div class="col-12 mb-3">
      <button class="btn btn-block btn-orange btn-lg shadow" onclick='play(),vibrate(100),percurso(<?php echo json_encode($percurso)?>)'><i class="fas fa-map-marked-alt mr-2"></i>Rota</button>
    </div>
</div>
<?php include ('../CORE/Footer2.php')?>