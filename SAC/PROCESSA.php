<?php
include_once '../CORE/PDO.php';

$sac = (isset($_POST['sac'])) ? strtoupper($_POST['sac']) : '';
$smart = strtoupper($_POST['smart']);
$modelo = (isset($_POST['modelo'])) ? strtoupper($_POST['modelo']): '';
$voltagem = (isset($_POST['voltagem'])) ? $_POST['voltagem'] : '';
$loja = $_POST['locais'];
$compra = (!empty($_POST['compra'])) ? $_POST['compra'] : '';
$nome = strtoupper($_POST['nome']);
$telefone1 = $_POST['telefone1'];
$telefone2 = (isset($_POST['telefone2'])) ? $_POST['telefone2'] : '';
$cep = (isset($_POST['cep'])) ? $_POST['cep'] : '';
$rua = $_POST['rua'];
$bairro = $_POST['bairro'];
$numero = $_POST['numero'];
$complemento = (isset($_POST['complemento'])) ? $_POST['complemento'] : '';
$cidade = $_POST['cidade'];
$nota = $_POST['nota'];

//QUERY GERAL DO SAC
$mysql = "SELECT AGENDA, PECA, QUEIXA, STATUS, CONSTATADO, ABERTO FROM sac WHERE SAC = '$sac'";
if(conex()->query($mysql)->fetch(PDO::FETCH_ASSOC))
{
    $sql = conex()->query($mysql)->fetch(PDO::FETCH_ASSOC);
    $pecaX = $sql['PECA'];
    $agendaX = $sql['AGENDA'];
    $queixaX = $sql['QUEIXA'];
    $aberto = $sql['ABERTO'];
}
else
{
    $pecaX = '';
    $agendaX = '';
    $queixaX = '';
    $aberto = 1;
}

//FUNÇÂO ATUALIZAR DB PRINCIPAL
function atualizaBD($banco,$status,$time,$smart)
{
    conex()->prepare("UPDATE $banco SET rma = '$status', data_rma = '$time', etapa = '15.RMA' WHERE smart = '$smart'")->execute();
};


//CASOS ESPECIAIS
$status = $_POST['status'];

$agenda = (isset($_POST['agenda'])) ? $_POST['agenda']: '';
if(!empty($agendaX))
{
    if(!empty($agenda))
    {
        $agenda = $agendaX.'_'.$agenda;
    }
    else
    {
        $agenda = $agendaX;
    }
}

$queixa = (!empty($_POST['queixa'])) ? $time.'='.str_replace("/"," ",strtoupper($_POST['queixa'])) : '';
                
// TEM HISTORICO DA CONSTATADO
if(!empty($sql['CONSTATADO']))
{
    $constatado = (empty($_POST['constatado'])) ? $constatado = $sql['CONSTATADO'] : $sql['CONSTATADO'].'/'.$time.'='.$_POST['tecnico'].'.'.strtoupper($_POST['constatado']);
}
//NÂO TEM HISTORICO CONSTATADO
else
{
    $constatado = (empty($_POST['constatado'])) ? '' : $time.'='.$_POST['tecnico'].'.'.strtoupper($_POST['constatado']); 
}

#CHECAR SE PEÇA FOI MODIFICADA OU ACRESCIDA PARA MANDAR TELEGRAM
$peca = (isset($_POST['peca'])) ? $_POST['peca'] : '';
if($peca != $pecaX && ($equipe == 'AGE' || $equipe == 'ADM'))
{
    //MUDA STATUS PARA PARTWAIT
    $status = '03.PARTWAIT';
    $msg = "Eu, ".$user.", estou adicionado/alterando um pedido de peça pelo atendimento: ".$sac;
    $token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
    //SMART LOGIN CHANNEL
    $chat = '-1001210942378';
    file_get_contents("https://api.telegram.org/bot".$token."/sendMessage?chat_id=".$chat."&text=".$msg);
}

#ATUALIZAR O RMA, DATA RMA e STATUS DO BANCO DO PRODUTO
$banco = banco($smart);

if(empty($modelo))
{
    
    #TELEGRAM
    $msg = "Falha. Modelo ausente";
    $token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
    //SMART LOGIN CHANNEL
    $chat = '-1001496650485';
    file_get_contents("https://api.telegram.org/bot".$token."/sendMessage?chat_id=".$chat."&text=".$msg);

    ##RETORNO
    $fala = 'Falha. Modelo ausente';
    $caso = '<i class="fas fa-medal mr-2"></i>Falha. Modelo ausente';

}
else
{
    // 8888888                                    888    
    //   888                                      888    
    //   888                                      888    
    //   888   88888b.  .d8888b   .d88b.  888d888 888888 
    //   888   888 "88b 88K      d8P  Y8b 888P"   888    
    //   888   888  888 "Y8888b. 88888888 888     888    
    //   888   888  888      X88 Y8b.     888     Y88b.  
    // 8888888 888  888  88888P'  "Y8888  888      "Y888 
    
    
    if(empty($sac))
    {
    
        #GERAR NUMERO DE SAC
        $sac = 'S0';
        do{
            $sequencia = substr($sac,1,10);
            $sequencia++;
            $sac = 'S'.$sequencia;
        }while(conex()->query("SELECT SAC FROM sac WHERE SAC = '$sac'")->fetch(PDO::FETCH_ASSOC));
        
        #INSERT SAC
        switch($equipe)
        {
            ##ACRESCIMO DE CONSTATADO E PECA
            case 'ADM':
                conex()->prepare("INSERT INTO sac (COMPRA,AGENDA,SAC,SMART,MODELO,VOLTAGEM,ATENDENTE,USER,CLIENTE,NUMERO,TEL,TEL2,ENDERECO,BAIRRO,CEP,CIDADE,COMPLEMENTO,QUEIXA,LOCAL,STATUS,CONSTATADO,PECA,NOTA,ABERTO) VALUES ('$compra','$agenda','$sac','$smart','$modelo','$voltagem','$user','$user','$nome','$numero','$telefone1','$telefone2','$rua','$bairro','$cep','$cidade','$complemento','$queixa','$loja','$status','$constatado','$peca','$nota',1)")->execute();

                #LOG
                setLOG("SAC",$status,"compra=$compra, agenda=$agenda, sac=$sac, modelo=$modelo, voltagem=$voltagem, atendente=$user, user=$user, cliente=$nome, numero=$numero, telefone=$telefone1, telefone=$telefone2, endereço=$rua, bairro=$bairro, cep=$cep, cidade=$cidade, complemento=$complemento, queixa=$queixa, loja=$loja, constatado=$constatado, peça=$peca, rma=$status, data_rma=$time, nota=$nota, aberto=1X",$smart,"15.RMA");
            break;
    
            #PADRAO
            case 'SAC':
                conex()->prepare("INSERT INTO sac (COMPRA,AGENDA,SAC,SMART,MODELO,VOLTAGEM,ATENDENTE,USER,CLIENTE,NUMERO,TEL,TEL2,ENDERECO,BAIRRO,CEP,CIDADE,COMPLEMENTO,QUEIXA,LOCAL,STATUS,NOTA,ABERTO) VALUES ('$compra','$agenda','$sac','$smart','$modelo','$voltagem','$user','$user','$nome','$numero','$telefone1','$telefone2','$rua','$bairro','$cep','$cidade','$complemento','$queixa','$loja','$status','$nota',1)")->execute();
                #LOG
                setLOG("SAC",$status,"compra=$compra, agenda=$agenda, sac=$sac, modelo=$modelo, voltagem=$voltagem, atendente=$user, user=$user, cliente=$nome, numero=$numero, telefone=$telefone1, telefone=$telefone2, endereço=$rua, bairro=$bairro, cep=$cep, cidade=$cidade, complemento=$complemento, queixa=$queixa, loja=$loja, status=$status, rma=$status, data_rma=$time, nota=$nota, aberto=1X",$smart,"15.RMA");
            break;
    
            #REMOÇÂO DE AGENDA
            case 'LOJA':
                conex()->prepare("INSERT INTO sac (COMPRA,SAC,SMART,MODELO,VOLTAGEM,ATENDENTE,USER,CLIENTE,NUMERO,TEL,TEL2,ENDERECO,BAIRRO,CEP,CIDADE,COMPLEMENTO,QUEIXA,LOCAL,STATUS,NOTA,ABERTO) VALUES ('$compra','$sac','$smart','$modelo','$voltagem','$user','$user','$nome','$numero','$telefone1','$telefone2','$rua','$bairro','$cep','$cidade','$complemento','$queixa','$loja','$status','$nota',1)")->execute();
                #LOG BANCO - AÇÃO PRINCIPAL - STATUS SAC - CAMPOS AFETADOS - SMART ID - ETAPA
                setLOG("SAC",$status,"compra=$compra, sac=$sac, modelo=$modelo, voltagem=$voltagem, atendente=$user, user=$user, cliente=$nome, numero=$numero, telefone=$telefone1, telefone=$telefone2, endereço=$rua, bairro=$bairro, cep=$cep, cidade=$cidade, complemento=$complemento, queixa=$queixa, loja=$loja, status=$status, rma=$status, data_rma=$time, nota=$nota, aberto=1X",$smart,"15.RMA");
            break;
        }
    
        ##RETORNO
        $fala = 'Parabéns! Atendimento '.$sac.' Registrado';
        $caso = '<i class="fas fa-medal mr-2"></i>Parabéns! Atendimento '.$sac.' registrado!';
    
        #TELEGRAM
        $msg = "OS ".$sac.", ABERTA por ".$user." da loja ".$loja." foi registrado com sucesso como ".$status." para o ID=".$smart.", MODELO=".$modelo;
        $token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
        //SMART LOGIN CHANNEL
        $chat = '-1001496650485';
        file_get_contents("https://api.telegram.org/bot".$token."/sendMessage?chat_id=".$chat."&text=".$msg);
    }
    
    // 888     888               888          888             
    // 888     888               888          888             
    // 888     888               888          888             
    // 888     888 88888b.   .d88888  8888b.  888888  .d88b.  
    // 888     888 888 "88b d88" 888     "88b 888    d8P  Y8b 
    // 888     888 888  888 888  888 .d888888 888    88888888 
    // Y88b. .d88P 888 d88P Y88b 888 888  888 Y88b.  Y8b.     
    //  "Y88888P"  88888P"   "Y88888 "Y888888  "Y888  "Y8888  
    //             888                                        
    //             888                                        
    //             888                                        
    
    else
    {
    
        #UPDATE STATUS E DATA CODIGO /LG SE HOUVE ALTERAÇÃO DE STATUS
        if($sql['STATUS'] != $status)
        {
            atualizaBD($banco,$status,$time,$smart);
    
            //CASO O STATUS SEJA ABERTO, GRAVA SEQUENCIA
            if($status == '01.ABERTO')
            {
                #ACRESCENTA 1 EM QUALQUER SITUAÇÃO
                $aberto++;
            }
        }
    
        //HISTORICO DA QUEIXA POR SER ATUALIZAÇÃO JA EXISTE QUEIXA POIS É OBRIGATORIO NO INSERT
        if($queixaX == true)
        {
            //SE TIVER QUEIXA NOVA
            if(!empty($queixa))
            {
                $queixa = $queixaX.'/'.$queixa;
            }
            else
            {
                $queixa = $queixaX;
            } 
        }
    
        //SE A COMPRA ESTIVER VAZIA RECOLHE A COMPRA ANTERIOR
        if(empty($compra))
        {
            $compra = conex()->query("SELECT COMPRA FROM sac WHERE SAC = '$sac'")->fetch((PDO::FETCH_ASSOC))['COMPRA'];
        } 
    
        switch($equipe)
        {
            case 'ADM':
    
                #ACRESCIDO CONSTATADO E PEÇA
                conex()->prepare("UPDATE sac SET COMPRA = '$compra', AGENDA = '$agenda',USER = '$user', CLIENTE = '$nome', NUMERO = '$numero', TEL = '$telefone1',TEL2 = '$telefone2', ENDERECO = '$rua', BAIRRO = '$bairro', CEP = '$cep', CIDADE = '$cidade', COMPLEMENTO = '$complemento', QUEIXA = '$queixa', LOCAL = '$loja', STATUS = '$status', CONSTATADO = '$constatado', PECA = '$peca', NOTA = '$nota', ABERTO='$aberto', ATENDIDO = 0 WHERE SAC = '$sac'")->execute();
    
                #LOG BANCO - AÇÃO PRINCIPAL - STATUS SAC - CAMPOS AFETADOS - SMART ID - ETAPA
                setLOG("SAC", "$status", "COMPRA=$compra, AGENDA=$agenda, USER=$user, CLIENTE=$nome, NUMERO=$numero, TEL=$telefone1, TEL2=$telefone2, ENDERECO=$rua, BAIRRO=$bairro, CEP=$cep, CIDADE=$cidade, COMPLEMENTO=$complemento, QUEIXA=$queixa, LOCAL=$loja, STATUS=$status, SAC=$sac,CONSTATADO=$constatado, PECA=$peca, etapa=$status, datarma=$time, nota=$nota, ABERTO=$aberto, ATENDIDO=0", $smart,"15.RMA");
        
                #UPDATE CODIGO/LG PEÇA
                conex()->prepare("UPDATE $banco SET peca = '$peca' WHERE smart = '$smart'")->execute();
    
            break;
    
            case 'SAC':
                #PADRAO
                conex()->prepare("UPDATE sac SET COMPRA = '$compra', AGENDA = '$agenda',USER = '$user', CLIENTE = '$nome', NUMERO = '$numero', TEL = '$telefone1',TEL2 = '$telefone2', ENDERECO = '$rua', BAIRRO = '$bairro', CEP = '$cep', CIDADE = '$cidade', COMPLEMENTO = '$complemento', QUEIXA = '$queixa', LOCAL = '$loja', STATUS = '$status', NOTA = '$nota', ABERTO='$aberto', ATENDIDO = 0 WHERE SAC = '$sac'")->execute();
    
                #LOG
                setLOG("SAC", "$status", "COMPRA=$compra, AGENDA=$agenda, USER=$user, CLIENTE=$nome, NUMERO=$numero, TEL=$telefone1, TEL2=$telefone2, ENDERECO=$rua, BAIRRO=$bairro, CEP=$cep, CIDADE=$cidade, COMPLEMENTO=$complemento, QUEIXA=$queixa, LOCAL=$loja, STATUS=$status, SAC=$sac, etapa=$status, datarma=$time, nota=$nota, ABERTO=$aberto, ATENDIDO=0", $smart,"15.RMA");
            break;
    
            case 'LOJA':
                #REMOVIDO AGENDA
                conex()->prepare("UPDATE sac SET COMPRA = '$compra', USER = '$user', CLIENTE = '$nome', NUMERO = '$numero', TEL = '$telefone1',TEL2 = '$telefone2', ENDERECO = '$rua', BAIRRO = '$bairro', CEP = '$cep', CIDADE = '$cidade', COMPLEMENTO = '$complemento', QUEIXA = '$queixa', LOCAL = '$loja', STATUS = '$status', NOTA = '$nota', ABERTO='$aberto', ATENDIDO = 0 WHERE SAC = '$sac'")->execute();
    
                #LOG
                setLOG("SAC", "$status", "COMPRA=$compra, USER=$user, CLIENTE=$nome, NUMERO=$numero, TEL=$telefone1, TEL2=$telefone2, ENDERECO=$rua, BAIRRO=$bairro, CEP=$cep, CIDADE=$cidade, COMPLEMENTO=$complemento, QUEIXA=$queixa, LOCAL=$loja, STATUS=$status, SAC=$sac,etapa=$status, datarma=$time, nota=$nota, ABERTO=$aberto, ATENDIDO = 0", $smart,"15.RMA");
            break;
    
            case 'AGE':
                #LIMITADO UPDATE SAC APENAS PEÇA E CONSTATADO
                conex()->prepare("UPDATE sac SET CONSTATADO = '$constatado', PECA = '$peca', STATUS = '$status' WHERE SAC = '$sac'")->execute();
    
                #LOG
                setLOG("SAC","$status","CONSTATADO=$constatado, PECA=$peca, STATUS=$status, rma=$status, etapa=$status, datarma=$time",$smart,"15.RMA");
    
                #UPDATE CODIGO/LG PEÇA
                conex()->prepare("UPDATE $banco SET peca = '$peca' WHERE smart = '$smart'")->execute();
            break;
    
        }
    
        #TELEGRAM
        $msg = "OS ".$sac.", EDITADA por ".$user." da loja ".$loja." foi atualizado com sucesso como ".$status." para o ID=".$smart.", MODELO=".$modelo;
        $token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
        //SMART LOGIN CHANNEL
        $chat = '-1001496650485';
        file_get_contents("https://api.telegram.org/bot".$token."/sendMessage?chat_id=".$chat."&text=".$msg);
    
        ##RETORNO
        $fala = 'Muito bem! Atendimento '.$sac.' Atualizado';
        $caso = '<i class="fas fa-medal mr-2"></i>Muito bem! Atendimento '.$sac.' Atualizado!';
    }
    
    #INSERT CODIGO/LG
    atualizaBD($banco,$status,$time,$smart);
}

//FALA
// $falas = (!empty($fala)) ? htmlspecialchars($fala) : htmlspecialchars('Ops. atenção no preenchimento');
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

//JSON
$soma = ['caso'=>$caso,'fala'=>$player];
print json_encode($soma, JSON_UNESCAPED_UNICODE);