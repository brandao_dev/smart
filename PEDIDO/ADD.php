<?php
include_once '../CORE/PDO.php';

$destino = (isset($_POST['destino'])) ? $_POST['destino'] : '';
$option = (isset($_POST['option'])) ? $_POST['option'] : '';
$smart = (isset($_POST['smart'])) ? strtoupper($_POST['smart']) : '';
$min = (!empty($_POST['min'])) ? $_POST['min'] : '';

//MINUTA
if($option == 0)
{
    #NOVO PEDIDO
    if($min == '')
        {
            $min = conex()->query("SELECT minuta FROM pedido ORDER BY minuta DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC)['minuta'];
            $min++;
            $caso = '<i class="fas fa-plus-square fa-lg mr-2"></i>Novo Pedido Gerado';
            $fala = 'Novo Pedido Gerado';
        }
        #EDITAR PEDIDO
        else
        {
            if($teste = conex()->query("SELECT minuta FROM pedido WHERE minuta = '$min' LIMIT 1")->fetch(PDO::FETCH_ASSOC)['minuta'])
            {
                $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Editar Pedido';
                $fala = 'Editar Pedido';
            }
        }
}

//PRODUTO
elseif($option == 1)
{
    //SE MINUTA NÃO ESTA VAZIA
    if($min != '')
    {
        //SE SMART NÃO ESTA VAZIO
        if($smart != '')
        {
            $banco = (substr($smart,0,1) == 'W') ? 'codigo' : 'lg';
            $sql = conex()->query("SELECT smart,linha, modelo FROM $banco WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC);

            //SE SMART EXISTE
            if($sql['smart'] == $smart)
            {
                $linha = $sql['linha'];
                $modelo = $sql['modelo'];

                //BUSCA EAN, WEBSAC
                $ean = conex()->query("SELECT EAN,websac FROM modelo WHERE modelo = '$modelo'")->fetch(PDO::FETCH_ASSOC);

                //SE SMART JA EXISTE NO PEDIDO
                
                //SE TIVER DESTINO
                if($destino != '')
                {
                    //UPDATE
                    if(conex()->query("SELECT smart FROM pedido WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC))
                    {
                        conex()->prepare("UPDATE pedido SET minuta = '$min', destino='$destino', user='$user' WHERE smart = '$smart'")->execute();
                    }
    
                    //INSERT
                    else
                    {
                        conex()->prepare("INSERT INTO pedido (smart, linha, modelo, minuta, pedido, destino, ean, websac, user, reserva) VALUES ('$smart', '$linha', '$modelo', '$min', '$time', '$destino', '{$ean['EAN']}', '{$ean['websac']}', '$user', '$user')")->execute();
                    }           

                    //RESERVA
                    $reserva = conex()->query("SELECT reserva FROM pedido WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC)['reserva'];
    
                    $caso = '<i class="fas fa-check-circle fa-lg mr-2"></i>Produto adicionado!';
                    $fala = 'Produto adicionado';
                }     
                else
                {  
                    $caso = '<i class="fas fa-map-marked-alt fa-lg mr-2"></i>Faltou o Destino!';
                    $fala = 'Faltou o Destino';
                } 
            }
            else
            {
                $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>ID não encontrado!';
                $fala = 'ID não encontrado';
            }
        }
        else
        {
            $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>ID não escolhido!';
            $fala = 'ID não escolhido';
        }
    }
    else
    {
        $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Pedido não definido!';
        $fala = 'Pedido não definido';
    }
     
}

//EXCLUIR
elseif($option == 2)
{
    //RESERVA
    $reserva = conex()->query("SELECT reserva FROM pedido WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC)['reserva'];

    if($reserva != '')
    #APENAS REMOVE A MINUTA MAS MANTEM O PRODUTO EM RESERVA
    {   
        conex()->prepare("UPDATE pedido SET minuta = '', user = '', destino = '' WHERE smart = '$smart'")->execute();
    }
    #NÃO TEM RESERVA, DELETA A LINHA
    else
    {
        conex()->prepare("DELETE FROM pedido WHERE smart ='$smart'")->execute();
    }
    $caso = '<i class="fas fa-trash-alt fa-lg mr-2"></i>Produto Removido!';
    $fala = 'Produto Removido';
}

//RESETAR
elseif($option == 3)
{
    $caso = '<i class="fas fa-thumbs-up fa-lg mr-2"></i>Pedido concluído!';
    $fala = 'Pedido concluído!';
    $pedido = $min;
    $min = '';
}

//FALA
// $falas = htmlspecialchars(ucwords(strtolower($fala)));
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

$reserva = (isset($reserva)) ? $reserva : '';
$pedido = (isset($pedido)) ? $pedido : '';

//JSON
$data = conex()->query("SELECT * FROM pedido WHERE minuta LIKE '$min' ORDER BY alteracao DESC")->fetchAll(PDO::FETCH_ASSOC);
// unset($option, $smart, $min, $caso);
// array_push($data,$tot,$plus);
$plus = ['caso'=>$caso,'minuta'=>$min,'total'=>Count($data), 'fala'=>$player, 'reserva'=>$reserva, 'user'=>$user, 'pedido'=>$pedido];
$data = array_merge($data,$plus);
print json_encode($data, JSON_UNESCAPED_UNICODE);