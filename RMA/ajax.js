// 8888888b.           888                      
// 888  "Y88b          888                      
// 888    888          888                      
// 888    888  8888b.  888888  8888b.  .d8888b  
// 888    888     "88b 888        "88b 88K      
// 888    888 .d888888 888    .d888888 "Y8888b. 
// 888  .d88P 888  888 Y88b.  888  888      X88 
// 8888888P"  "Y888888  "Y888 "Y888888  88888P' 

jQuery(function($){
	$.datepicker.regional['pt-BR'] = {
			closeText: 'Fechar',
			prevText: 'Mes Anterior',
			nextText: 'Proximo Mes;',
			currentText: 'Hoje',
			monthNames: ['Janeiro','Fevereiro','Marco','Abril','Maio','Junho',
			'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
			'Jul','Ago','Set','Out','Nov','Dez'],
			dayNames: ['Domingo','Segunda-feira','Terca-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			weekHeader: 'Sm',
			dateFormat: 'dd/mm/yy',
			firstDay: 0,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});
$( function() {
  $("input[name^='S']").datepicker();
} );


//        d8888      888 d8b          d8b                                    
//       d88888      888 Y8P          Y8P                                    
//      d88P888      888                                                     
//     d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
//    d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
//   d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
//  d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
// d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     

$(function(){
    $('#form').on('submit', function(e){
        e.preventDefault();
        data = $(this).serialize();

        // SE PREENCHIDO
        $.ajax(
            {
                type: 'POST',
                dataType: 'json',
                url: 'receber.php',
                async: true,
                data: data,
                success: function(data){
                    console.log(data)
                    
                    //RETORNO BÁSICO
                    $('#smart').val('');
                    $('#player').html(data.fala);
                    $('#caso').html(data.caso);
                    $('#table').empty()
                    $('#table').append(data.tabela);
                    $("input[name^='S']").datepicker();
                },
                error: function(){
                    console.log('erro')
                }
            }
        )

    })
})

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

function vibrate(ms){
    navigator.vibrate(ms);
  }

//        d8888                                 888          
//       d88888                                 888          
//      d88P888                                 888          
//     d88P 888  .d88b.   .d88b.  88888b.   .d88888  8888b.  
//    d88P  888 d88P"88b d8P  Y8b 888 "88b d88" 888     "88b 
//   d88P   888 888  888 88888888 888  888 888  888 .d888888 
//  d8888888888 Y88b 888 Y8b.     888  888 Y88b 888 888  888 
// d88P     888  "Y88888  "Y8888  888  888  "Y88888 "Y888888 
//                   888                                     
//              Y8b d88P                                     
//               "Y88P"                                      

$(function(){
    $('#btn').on('click', function(e){
        e.preventDefault();

        // SEGUNDO FORMULARIO
        agendas = $('#agendas').serialize();
        $.ajax({
            data: agendas,
            type: 'POST',
            dataType: 'json',
            async: true,
            url: 'agenda.php',
            success: function(dados){
                $('#caso').html(dados.caso);
                $('#player').html(dados.fala);
                $('#table').empty()
                $('#table').append(dados.tabela);
                $("input[name^='S']").datepicker();
                console.log('acerio')
            },
            error: function(){
                console.log('erru')
            }
        })
    })
})

// 888b     d888                            
// 8888b   d8888                            
// 88888b.d88888                            
// 888Y88888P888  8888b.  88888b.   8888b.  
// 888 Y888P 888     "88b 888 "88b     "88b 
// 888  Y8P  888 .d888888 888  888 .d888888 
// 888   "   888 888  888 888 d88P 888  888 
// 888       888 "Y888888 88888P"  "Y888888 
//                        888               
//                        888               
//                        888               

// function mapa(local)
// {
//     console.log(local)
//     swal.fire(
//         {
//             title: 'Mapa',
//             html: '<iframe  width="100%" height="700" src="https://maps.google.com/maps?q='+local+'&output=embed"></iframe>',
//             backdrop: 'rgba(0,0,0,0.8)',
//             customClass: 'swal',
//         }
//     )
// }