<?PHP include('../CORE/Header2.php');?>
<script src="ajax.js?<?php echo filemtime('ajax.js')?>"></script>
<div id="player" style="display: none;"></div>
<!-- TAMAHHO SWAL -->
<style> .swal {width:550px !important; } </style>

<!-- SEGUNDA LINHA DE FRASE DE APOIO *** /////////////////////////////////////////////////////////////////////////////////////// -->
<div class="alert mt-3 alert-info font-weight-bold" id="caso"><i class="fas fa-unlock-alt fa-lg mr-2"></i>Pronto para nova entrada!</div>

<!-- 
8888888                            888    
  888                              888    
  888                              888    
  888   88888b.  88888b.  888  888 888888 
  888   888 "88b 888 "88b 888  888 888    
  888   888  888 888  888 888  888 888    
  888   888  888 888 d88P Y88b 888 Y88b.  
8888888 888  888 88888P"   "Y88888  "Y888 
                 888                      
                 888                      
				 888                      
-->
<form action="" id="form" method="POST" name="form">

<div class="row">
	<div class="mt-3 col-12">
		<input type="text" list="mod" class="font-weight-bold form-control form-control-lg" placeholder="Digite o modelo..." name="modelo" id="modelo" autofocus autocomplete="off" required value="<?php if(isset($_GET['modelo']))echo $_GET['modelo']?>">
		<datalist id="mod">
			<?php
				$leitura = conex()->query("SELECT modelo FROM modelo ORDER BY modelo");
				foreach($leitura as $res)
				{
					echo "<option value='{$res['modelo']}'/>";
				}
			?>
		</datalist>	
	</div>
</div>

<!-- 
8888888b.                        888          888             
888   Y88b                       888          888             
888    888                       888          888             
888   d88P 888d888  .d88b.   .d88888 888  888 888888  .d88b.  
8888888P"  888P"   d88""88b d88" 888 888  888 888    d88""88b 
888        888     888  888 888  888 888  888 888    888  888 
888        888     Y88..88P Y88b 888 Y88b 888 Y88b.  Y88..88P 
888        888      "Y88P"   "Y88888  "Y88888  "Y888  "Y88P"  
                                                               -->

<div class="card shadow mt-3">
  <div class="card-body">
	<div class="row">

<!-- DATA -->
	<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="far fa-calendar-alt mr-2"></i>Criado:</span>
		</div>
		<input type="text" class="form-control" id="data" readonly placeholder="Data">		
	</div>

<!-- ALTERAÇÃO -->
	<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="far fa-calendar-edit mr-2"></i>Alterado em:</span>
		</div>
		<input type="text" class="form-control" id="alteracao" readonly placeholder="Data">		
	</div>

<!-- USUARIO -->
	<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="fas fa-user-hard-hat mr-2"></i>Criado por:</span>
		</div>
		<input type="text" class="form-control" id="user" readonly placeholder="Usuário">		
	</div>	

<!-- LINHA -->
	<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="fas fa-align-left mr-2"></i>Linha</span>
		</div>
		<select class="form-control" id="linha" name="linha" required>
			<option value="">Escolha...</option>
			<option value="R/ADEGA">ADEGA</option>
			<option value="A/AR CONDICIONADO">AR CONDICIONADO</option>
			<option value="C/ASSADOR">ASSADOR</option>
			<option value="A/BATEDEIRA">BATEDEIRA</option>
			<option value="A/BOMBA DE AGUA">BOMBA DE ÁGUA</option>
			<option value="A/CAFETEIRA">CAFETEIRA</option>
			<option value="R/CERVEJEIRA">CERVEJEIRA</option>
			<option value="A/CHALEIRA">CHALEIRA</option>
			<option value="C/CHAPA">CHAPA</option>
			<option value="C/CHURRASQUEIRA">CHURRASQUEIRA</option>
			<option value="A/CILINDRO">CILINDRO</option>
			<option value="A/CLIMATIZADOR">CLIMATIZADOR</option>
			<option value="C/COIFA ILHA">COIFA ILHA</option>
			<option value="C/COIFA PAREDE">COIFA PAREDE</option>
			<option value="A/CONDENSADOR">CONDENSADOR</option>
			<option value="C/COOKTOP INDUÇÃO">COOKTOP INDUÇÃO</option>
			<option value="C/COOKTOP 2 BOCAS">COOKTOP 2 BOCAS</option>
			<option value="C/COOKTOP 4 BOCAS">COOKTOP 4 BOCAS</option>
			<option value="C/COOKTOP 5 BOCAS">COOKTOP 5 BOCAS</option>
			<option value="A/DEPURADOR DE AR">DEPURADOR DE AR</option>
			<option value="A/ESTUFA">ESTUFA</option>
			<option value="A/EVAPORADOR">EVAPORADOR</option>
			<option value="A/EXPOSITOR">EXPOSITOR</option>
			<option value="C/FOGÃO 4 BOCAS">FOGÃO 4 BOCAS</option>
			<option value="C/FOGÃO 5 BOCAS">FOGÃO 5 BOCAS</option>
			<option value="C/FOGÃO 6 BOCAS">FOGÃO 6 BOCAS</option>
			<option value="C/FOGÃO 6 BOCAS">FOGÃO 6 BOCAS</option>
			<option value="C/FOGÃO INDUSTRIAL">FOGÃO INDUSTRIAL</option>
			<option value="C/FORNO INDUSTRIAL">FORNO INDUSTRIAL</option>
			<option value="C/FORNO A GÁS">FORNO A GÁS</option>
			<option value="C/FORNO ELÉTRICO">FORNO ELÉTRICO</option>
			<option value="R/FREEZER H.">FREEZER H.</option>
			<option value="R/FREEZER V.">FREEZER V.</option>
			<option value="R/FRIGOBAR">FRIGOBAR</option>
			<option value="C/FRITADEIRA">FRITADEIRA</option>
			<option value="L/LAVA-LOUÇAS">LAVA-LOUÇAS</option>
			<option value="L/LAVA E SECA">LAVA E SECA</option>
			<option value="L/LAVADORA">LAVADORA</option>
			<option value="L/LAV. ALTA PRESSÃO">LAV. ALTA PRESSÃO</option>
			<option value="A/LIQUIDIFICADOR">LIQUIDIFICADOR</option>
			<option value="R/MAQUINA DE BEBIDAS">MAQUINA DE BEBIDAS</option>
			<option value="A/MAQUINA DE MACARRÃO">MAQUINA DE MACARRÃO</option>
			<option value="C/MICROONDAS">MICROONDAS</option>
			<option value="A/MIXER">MIXER</option>
			<option value="A/MOEDOR">MOEDOR</option>
			<option value="A/PROCESSADOR">PROCESSADOR</option>
			<option value="A/PURIFICADOR">PURIFICADOR</option>
			<option value="R/REFRIGERADOR">REFRIGERADOR</option>
			<option value="A/SOVADEIRA">SOVADEIRA</option>
			<option value="L/TANQUINHO">TANQUINHO</option>
		</select>		
	</div>

<!-- FABRICANTE -->
	<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="fas fa-industry-alt mr-2"></i>Fabricante</span>
		</div>
		<select class="form-control" id="fabricante" name="fabricante" required>
			<option value="">Escolha...</option>
			<option value="ANODILAR">ANODILAR</option>
			<option value="APELSON">APELSON</option>
			<option value="ARKE">ARKE</option>
			<option value="ART DES CAVES">ART DES CAVES</option>
			<option value="ATLAS">ATLAS</option>
			<option value="BRASTEMP">BRASTEMP</option>
			<option value="BRITANIA">BRITANIA</option>
			<option value="BUILT">BUILT</option>
			<option value="CADENCE">CADENCE</option>
			<option value="CASAVITRA">CASAVITRA</option>
			<option value="CATA">CATA</option>
			<option value="COLORMAQ">COLORMAQ</option>
			<option value="CONSUL">CONSUL</option>
			<option value="CONTINENTAL">CONTINENTAL</option>
			<option value="COTHERM">COTHERM</option>
			<option value="CROYDON">CROYDON</option>
			<option value="DAKO">DAKO</option>
			<option value="DYNASTY">DYNASTY</option>
			<option value="EASYCOOLER">EASYCOOLER</option>
			<option value="EDANCA">EDANCA</option>
			<option value="ELECTROLUX">ELECTROLUX</option>
			<option value="ESMALTEC">ESMALTEC</option>
			<option value="FALMEC">FALMEC</option>
			<option value="FAMASTIL">FAMASTIL</option>
			<option value="FERRARI">FERRARI</option>
			<option value="FISCHER">FISCHER</option>
			<option value="FOGATTI">FOGATTI</option>
			<option value="FRANKE">FRANKE</option>
			<option value="FUNDIFERRO">FUNDIFERRO</option>
			<option value="GE">GE</option>
			<option value="GOODYEAR">GOODYEAR</option>
			<option value="HIDRO">HIDRO</option>
			<option value="INTECH">INTECH</option>
			<option value="ITATIAIA">ITATIAIA</option>
			<option value="KARCHER">KARCHER</option>
			<option value="KITCHENAID">KITCHENAID</option>
			<option value="LAYR">LAYR</option>
			<option value="LG">LG</option>
			<option value="MASTERFRIO">MASTERFRIO</option>
			<option value="MUELLER">MUELLER</option>
			<option value="MARCHESONE">MARCHESONE</option>
			<option value="METALFRIO">METALFRIO</option>
			<option value="NARDELLI">NARDELLI</option>
			<option value="PHILCO">PHILCO</option>
			<option value="PHILIPS">PHILIPS</option>
			<option value="PLAZA HERCULES">PLAZA HERCULES</option>
			<option value="PRAXIS">PRAXIS</option>
			<option value="SAMSUNG">SAMSUNG</option>
			<option value="SCHULZ">SCHULZ</option>
			<option value="SUGGAR">SUGGAR</option>
			<option value="TEDESCO">TEDESCO</option>
			<option value="TEKNA">TEKNA</option>
			<option value="TERRA ACQUA">TERRA ACQUA</option>
			<option value="TITA">TITA</option>
			<option value="TRAMONTINA">TRAMONTINA</option>
			<option value="TRON">TRON</option>
			<option value="VENAX">VENAX</option>
			<option value="WAP">WAP</option>
			<option value="ZEEX">ZEEX</option>
		</select>			
	</div>

<!-- COR -->
	<div class="col-12 col-md-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="fas fa-palette mr-2"></i>Cor</span>
		</div>
		<select class="form-control" id="cor" name="cor" required>
			<option value="">Escolha...</option>
			<option value="AMARELO">AMARELO</option>
			<option value="AZUL">AZUL</option>
			<option value="BRANCO">BRANCO</option>
			<option value="CINZA">CINZA</option>
			<option value="GRAFITE">GRAFITE</option>
			<option value="INOX">INOX</option>
			<option value="LARANJA">LARANJA</option>
			<option value="PLATINUM">PLATINUM</option>
			<option value="PRATA">PRATA</option>
			<option value="PRETO">PRETO</option>
			<option value="ROSA">ROSA</option>
			<option value="ROXO">ROXO</option>
			<option value="TITANIO">TITANIO</option>
			<option value="VERDE">VERDE</option>
			<option value="VERMELHO">VERMELHO</option>
		</select>		
	</div>

<!-- PROCEL -->
	<!-- <div class="col-12 col-md-6 col-lg-6 col-xxxl-3 mt-3 btn-group btn-group-toggle" data-toggle="buttons">
		<label class="btn btn-outline-orange font-weight-bold shadow" for="R">
			<input type="radio" name="procel" id="R" value="R" autocomplete="off"><i class="fas fa-refrigerator mr-2"></i>R
		</label>
		<label class="btn btn-outline-orange font-weight-bold shadow" for="C">
			<input type="radio" name="procel" id="C" value="C" autocomplete="off"><i class="fas fa-oven mr-2"></i>C
		</label>
		<label class="btn btn-outline-orange font-weight-bold shadow" for="L">
			<input type="radio" name="procel" id="L" value="L" autocomplete="off"><i class="fas fa-dryer-alt mr-2"></i>L
		</label>
		<label class="btn btn-outline-orange font-weight-bold shadow" for="A">
			<input type="radio" name="procel" id="A" value="A" autocomplete="off"><i class="fas fa-air-conditioner mr-2"></i>A
		</label>
	</div> -->

<!-- TENSÃO -->
	<div class="col-12 col-xl-4 mt-3 btn-group btn-group-toggle" data-toggle="buttons">
		<label class="btn btn-outline-teal font-weight-bold shadow" for="127V">
			<input type="radio" name="tensao" id="127V" value="127 V" autocomplete="off"><i class="fas fa-bolt mr-2"></i>127V
		</label>
		<label class="btn btn-outline-teal font-weight-bold shadow" for="220V">
			<input type="radio" name="tensao" id="220V" value="220 V" autocomplete="off"><i class="fas fa-bolt mr-2"></i>220V
		</label>
		<label class="btn btn-outline-teal font-weight-bold shadow" for="127220">
			<input type="radio" name="tensao" id="127220" value="127/220" autocomplete="off"><i class="fas fa-bolt mr-2"></i>Bivolt
		</label>
		</label>
		<label class="btn btn-outline-teal font-weight-bold shadow" for="NA">
			<input type="radio" name="tensao" id="NA" value="N/A" autocomplete="off"><i class="fas fa-bolt mr-2"></i>N/A
		</label>
	</div>

<!-- EAN -->
	<div class="col-12 col-sm-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="fas fa-barcode-read mr-2"></i>EAN</span>
		</div>
		<input type="number" class="form-control" id="ean" name="ean" placeholder="Números">		
	</div>

<!-- WEBSAC -->
	<div class="col-12 col-sm-6 col-xl-4 input-group mt-3">
		<div class="input-group-prepend">
			<span class="input-group-text font-weight-bold"><i class="fas fa-hamburger mr-2"></i>WebSac</span>
		</div>
		<input type="number" class="form-control" id="websac" name="websac" placeholder="Números">		
	</div>

	</div>
  </div>
</div>

<div class="row mt-3">
	<div class="mt-3 col-12 col-sm-6">
		<button class="btn btn-lg btn-danger btn-block shadow" type="button" onclick="vibrate(100),play(),excluir()"><h5><i class="fas fa-trash-alt mr-2"></i>Excluir</h5></button>
	</div>
	<div class="mt-3 col-12 col-sm-6">
		<button class="btn btn-lg btn-success btn-block shadow" type="submit" onclick="vibrate(100),play()"><h5><i class="far fa-save mr-2"></i>Salvar</h5></button>
	</div>
</div>

</form>
<br>
<?PHP include('../CORE/Footer2.php'); ?>