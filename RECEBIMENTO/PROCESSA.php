<?php
include '../CORE/PDO.php';

// $nota = '15415125';
$nota = (!empty($_POST['nota'])) ? $_POST['nota'] : '';
// $lote = '0503';
$lote = (!empty($_POST['lote'])) ? substr('000'.$_POST['lote'],-4) : '';
// $enge = '52';
$enge = (!empty($_POST['enge'])) ? $_POST['enge'] : '';
// $code = 'CPB36ABANA26852685472';
$code = (!empty($_POST['code'])) ? strtoupper($_POST['code']) : '';
$ex = (!empty($_POST['ex'])) ? true : false;
$smart = (!empty($_POST['smart'])) ? $_POST['smart'] : '';
$serial = (!empty($_POST['serial'])) ? $_POST['serial'] : '';
// CM
$marca = (!empty($_POST['marca'])) ? $_POST['marca'] : '';
$voltagem = (!empty($_POST['voltagem'])) ? $_POST['voltagem'] : '';
$modelo = (!empty($_POST['modelo'])) ? $_POST['modelo'] : '';
$obs = (!empty($_POST['obs'])) ? $_POST['obs'] : '';
$grupo = (!empty($_POST['grupo'])) ? $_POST['grupo'] : '';
$cor = (!empty($_POST['cor'])) ? $_POST['cor'] : '';
$linha = (!empty($_POST['linha'])) ? $_POST['linha'] : '';


    switch($_SESSION['modo'])
    {
    
    // 888       888 8888888b.  
    // 888   o   888 888   Y88b 
    // 888  d8b  888 888    888 
    // 888 d888b 888 888   d88P 
    // 888d88888b888 8888888P"  
    // 88888P Y88888 888        
    // 8888P   Y8888 888        
    // 888P     Y888 888        
    
        case 'WP':
            //EXCLUIR
            if($ex == true)
            {
                conex()->prepare("DELETE FROM codigo WHERE smart = '$smart'")->execute();
                $caso = '<i class="fas fa-times-circle fa-lg mr-2"></i>Registro removido!';
                $fala = 'Registro removido!';
                #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                setLOG("REGISTRO",'*',"Excluido",$smart,"*");
            }
            else
            {
                //SE PREENCHEU NOTA
                if(!empty($nota))
                {
                    //SE PREENCHEU LOTE
                    if(!empty($lote))
                    {
                        //SE PREENCHEU CODIGO E TEM TAMANHO ADEQUADO
                        if(!empty($code))
                        {
                            //PEGA MODELO DO COMEÇO
                            $modelo = substr($code,0,10);
                            $serial = substr($code,10,20);
                            #BUSCA DE MODELO SE EXISTIR	
                            if($sql = conex()->query("SELECT modelo,linha,cor,tensao,fabricante,GRUPO, procel FROM modelo WHERE modelo LIKE '$modelo'")->fetch(PDO::FETCH_ASSOC))
                            {
                                //EXISTE PROCEL
                                if(!empty($sql['procel']))
                                {
                                    //SERIAL REPETIDO
                                    if(conex()->query("SELECT serie1 FROM codigo WHERE serie1 = '$serial'")->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Serial repetido!';
                                        $fala = 'Serial repetido!';
                                    }
                                    else
                                    {
                                        
                                        //GARANTIR ID UNICO E SEQUENCIAL
                                        $total = 0;
                                        $ini = 'W';
                                        do
                                        {
                                            $total++;
                                            $total = substr('000'.$total,-3);
                                            $subSmart = $lote.$sql['procel'].$total;
                                        }
                                        //ACRESCENTAR MAIS 1 NO TOTAL ENQUANTO FOR ENCONTRADO ID COMEÇANDO COM W, MESMO LOTE E TERMINANDO COM TOTAL
                                        while(conex()->query("SELECT smart FROM codigo WHERE smart LIKE '$ini%$lote%$total'")->fetch(PDO::FETCH_ASSOC));
                                        //RESULTADO
                                        $smart = $ini.substr($sql['fabricante'],0,1).$subSmart;

                                        //GARANTIR ID COM 10 CASAS
                                        if(strlen($smart) != 10)
                                        {
                                            $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Erro no cadastro. Confira!';
                                            $fala = 'Erro no cadastro. Confira!';
                                            #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                                            setLOG("REGISTRO","*","Erro no cadastro",$smart,"*");

                                        }
                                        else
                                        {                
                                            //GRAVA
                                            conex()->prepare("INSERT INTO codigo(data, modelo, cor, smart, linha, fabricante, serie1, voltagem, nota, nlote, etapa, usuario, enge, localsaida, grupo, modo)VALUES('$time', '$modelo', '{$sql['cor']}', '$smart', '{$sql['linha']}', '{$sql['fabricante']}', '$serial', '{$sql['tensao']}', '$nota', '$lote', '01.RECEBIDO', '$user', '$enge', 'GALATEA', '{$sql['procel']}', 'WP')")
                                            ->execute();
                                            $caso = '<i class="fas fa-thumbs-up fa-lg mr-2"></i>ID gerado com sucesso!';
                                            $fala = 'ID gerado com sucesso!';
                                            #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                                            setLOG("REGISTRO","*","ID Gerado",$smart,"01.RECEBIDO");
                                        }
                                    }
    
                                }
                                else
                                {
                                    $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Falta PROCEL no modelo';
                                    $fala = 'Falta PROCEL no modelo';
                                    #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                                    setLOG("REGISTRO",'*',"Tentativa falha. Falta procel no modelo",$smart,"*");
    
                                }
                
                            }
                            else
                            {
                                $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Modelo Ausente!';
                                $fala = 'Modelo Ausente!';
                            }
                        }
                        else
                        {
                            $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Código Ausente!';
                            $fala = 'Código Ausente!';
                        }
                
                    }
                    else
                    {
                        $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Faltou definir o lote!';
                        $fala = 'Faltou definir o lote!';
                    }
                }
                else
                {
                    $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Nota Fiscal Ausente!';
                    $fala = 'Nota Fiscal Ausente!';
                }
            
            }
        break;
    
    // 888       .d8888b.  
    // 888      d88P  Y88b 
    // 888      888    888 
    // 888      888        
    // 888      888  88888 
    // 888      888    888 
    // 888      Y88b  d88P 
    // 88888888  "Y8888P88 
    
    
        case 'LG':
            //EXCLUIR
            if($ex == true)
            {
                conex()->prepare("DELETE FROM codigo WHERE smart = '$smart'")->execute();
                $caso = '<i class="fas fa-times-circle fa-lg mr-2"></i>Registro removido!';
                $fala = 'Registro removido!';
                #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                setLOG("REGISTRO",'*',"Excluido",$smart,"*");
            }
            else
            {
                //SE PREENCHEU CODIGO
                if(!empty($code))
                {
                    //SE PREENCHEU SERIAL
                    if(!empty($serial))
                    {
                        #BUSCA DE MODELO SE EXISTIR	
                        if($sql = conex()->query("SELECT modelo,linha,cor,tensao,fabricante,GRUPO,procel FROM modelo WHERE modelo LIKE '$code'")->fetch(PDO::FETCH_ASSOC))
                        {
                            // VERIFICA PROCEL
                            if(!empty($sql['procel']))
                            {
                                //SERIAL REPETIDO
                                if(conex()->query("SELECT serie1 FROM codigo WHERE serie1 = '$serial'")->fetch(PDO::FETCH_ASSOC))
                                {
                                    $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Serial repetido!';
                                    $fala = 'Serial repetido!';
                                }
                                else
                                {
                                    $ini = 'LR';
                                    //PROCURAR ID COMEÇANCO COM LR
                                    $smart = 'LR0500'.$sql['procel'].'000';
                                        do
                                        {
                                            //PEGAR SO NUMEROS
                                            $sequencia = preg_replace("/[^0-9]/", "", $smart);
                                            //ADICIONAR 1
                                            $sequencia++;$sequencia = substr(('000000'.$sequencia),-7);
                                            //SEPARAR LOTE
                                            $lote = substr($sequencia,0,4);
                                            //SEPARAR ORDEM
                                            $ordem = substr($sequencia,-3);
                                            //CRIAR ID
                                            $smart = 'LR'.$lote.$sql['procel'].$ordem;

                                        }
                                        while(conex()->query("SELECT smart FROM codigo WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC));
            
                                    //GRAVA
                                    conex()->prepare("INSERT INTO codigo(data, modelo, cor, smart, linha, fabricante, serie1, voltagem, nota, nlote, etapa, usuario, enge, localsaida, grupo, modo)VALUES('$time', '$code', '{$sql['cor']}', '$smart', '{$sql['linha']}', '{$sql['fabricante']}', '$serial', '{$sql['tensao']}', '$nota', '$lote', '01.RECEBIDO', '$user', '$enge', 'GALATEA', '{$sql['procel']}', 'LG')")
                                    ->execute();
                                    $caso = '<i class="fas fa-thumbs-up fa-lg mr-2"></i>ID gerado com sucesso!';
                                    $fala = 'ID gerado com sucesso!';
                                    #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                                    setLOG("REGISTRO",'*',"ID Gerado",$smart,"01.RECEBIDO");
                                }
    
                            }
                            else
                            {
                                $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Falta PROCEL no modelo';
                                $fala = 'Falta PROCEL no modelo';
                                #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                                setLOG("REGISTRO",'*',"Tentativa falha. Falta procel no modelo",$smart,"*");
    
                            }
            
                        }
                        else
                        {
                            $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Modelo não existe na tabela!';
                            $fala = 'Modelo não existe na tabela!';
                        }
                    }
                    else
                    {
                        $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Serial ausente!';
                        $fala = 'Serial Ausente!';
    
                    }
                }
                else
                {
                    $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Modelo Ausente!';
                    $fala = 'Modelo Ausente!';
                    $lote = '';
                }
            
            }
        break;
    
    
    // .d8888b.  888b     d888 
    //d88P  Y88b 8888b   d8888 
    //Y88b.      88888b.d88888 
    // "Y888b.   888Y88888P888 
    //    "Y88b. 888 Y888P 888 
    //      "888 888  Y8P  888 
    //Y88b  d88P 888   "   888 
    // "Y8888P"  888       888 
    
         
        case 'SM':
            //EXCLUIR
            if($ex == true)
            {
                conex()->prepare("DELETE FROM codigo WHERE smart = '$smart'")->execute();
                $caso = '<i class="fas fa-times-circle fa-lg mr-2"></i>Registro removido!';
                $fala = 'Registro removido!';
                #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                setLOG("REGISTRO",'*',"Excluido",$smart,"*");
            }
            else
            {
                //SE PREENCHEU CODIGO E TEM TAMANHO ADEQUADO
                if(!empty($code))
                {
                    if(!empty($serial))
                    {
                        #BUSCA DE MODELO SE EXISTIR	
                        if($sql = conex()->query("SELECT modelo,linha,cor,tensao,fabricante,GRUPO, procel FROM modelo WHERE modelo LIKE '$code'")->fetch(PDO::FETCH_ASSOC))
                        {
                            //VERIFICA PROCEL
                            if(!empty($sql['procel']))
                            {
                                //SERIAL REPETIDO
                                if(conex()->query("SELECT serie1 FROM codigo WHERE serie1 = '$serial'")->fetch(PDO::FETCH_ASSOC))
                                {
                                    $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Serial repetido!';
                                    $fala = 'Serial repetido!';
                                }
                                else
                                {
                                    $ini = 'SM';
                                    //PROCURAR ID COMEÇANCO COM LR
                                    $smart = 'SM0500'.$sql['procel'].'000';
                                        do
                                        {
                                            //PEGAR SO NUMEROS
                                            $sequencia = preg_replace("/[^0-9]/", "", $smart);
                                            //ADICIONAR 1
                                            $sequencia++;$sequencia = substr(('000000'.$sequencia),-7);
                                            //SEPARAR LOTE
                                            $lote = substr($sequencia,0,4);
                                            //SEPARAR ORDEM
                                            $ordem = substr($sequencia,-3);
                                            //CRIAR ID
                                            $smart = 'SM'.$lote.$sql['procel'].$ordem;

                                        }
                                        while(conex()->query("SELECT smart FROM codigo WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC));
            
                                    //GRAVA
                                    conex()->prepare("INSERT INTO codigo(data, modelo, cor, smart, linha, fabricante, serie1, voltagem, nota, nlote, etapa, usuario, enge, localsaida, grupo, modo)VALUES('$time', '$code', '{$sql['cor']}', '$smart', '{$sql['linha']}', '{$sql['fabricante']}', '$serial', '{$sql['tensao']}', '$nota', '$lote', '01.RECEBIDO', '$user', '$enge', 'GALATEA', '{$sql['procel']}', 'SM')")
                                    ->execute();
                                    $caso = '<i class="fas fa-thumbs-up fa-lg mr-2"></i>ID gerado com sucesso!';
                                    $fala = 'ID gerado com sucesso!';
                                    #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                                    setLOG("REGISTRO",'*',"ID Gerado",$smart,"01.RECEBIDO");
                                }
                                
                            }
                            else
                            {
                                $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Falta PROCEL no modelo';
                                $fala = 'Falta PROCEL no modelo';
                                #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                                setLOG("REGISTRO",'*',"Tentativa falha. Falta procel no modelo",$smart,"*");

                            }

            
                        }
                        else
                        {
                            $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Modelo não existe na tabela!';
                            $fala = 'Modelo não existe na tabela!';
                        }
                    }
                    else
                    {
                        $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Serial ausente!';
                        $fala = 'Serial Ausente!';

                    }
                }
                else
                {
                    $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Modelo Ausente!';
                    $fala = 'Modelo Ausente!';
                    $lote = '';
                }
                
            
            }
        break;
    
    
    // .d8888b.  888b     d888 
    // d88P  Y88b 8888b   d8888 
    // 888    888 88888b.d88888 
    // 888        888Y88888P888 
    // 888        888 Y888P 888 
    // 888    888 888  Y8P  888 
    // Y88b  d88P 888   "   888 
    //  "Y8888P"  888       888 
    
         
        case 'CM':
            //EXCLUIR
            if($ex == true)
            {
                conex()->prepare("DELETE FROM codigo WHERE smart = '$smart'")->execute();
                $caso = '<i class="fas fa-times-circle fa-lg mr-2"></i>Registro removido!';
                $fala = 'Registro removido!';
                #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                setLOG("REGISTRO",'*',"Excluido",$smart,"*");
            }
            else
            {
                //SERIAL REPETIDO
                if(conex()->query("SELECT serie1 FROM codigo WHERE serie1 = '$serial'")->fetch(PDO::FETCH_ASSOC))
                {
                    $caso = '<i class="fas fa-exclamation-triangle fa-lg mr-2"></i>Serial repetido!';
                    $fala = 'Serial repetido!';
                }
                else
                {
                    $ini = 'CM';
                    //PROCURAR ID COMEÇANCO COM LR
                    $smart = 'CM0500'.$grupo.'000';
                        do
                        {
                            //PEGAR SO NUMEROS
                            $sequencia = preg_replace("/[^0-9]/", "", $smart);
                            //ADICIONAR 1
                            $sequencia++;$sequencia = substr(('000000'.$sequencia),-7);
                            //SEPARAR LOTE
                            $lote = substr($sequencia,0,4);
                            //SEPARAR ORDEM
                            $ordem = substr($sequencia,-3);
                            //CRIAR ID
                            $smart = 'CM'.$lote.$grupo.$ordem;

                        }
                        while(conex()->query("SELECT smart FROM codigo WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC));

                    //GRAVA
                    conex()->prepare("INSERT INTO codigo(data, modelo, cor, smart, linha, fabricante, serie1, voltagem, etapa, usuario, localsaida, grupo, modo, obs)VALUES('$time', '$modelo', '$cor', '$smart', '$linha', '$marca', '$serial', '$voltagem', '01.RECEBIDO', '$user', 'CAMARES', '$grupo', 'CM', '$obs')")
                    ->execute();
                    $caso = '<i class="fas fa-thumbs-up fa-lg mr-2"></i>ID gerado com sucesso!';
                    $fala = 'ID gerado com sucesso!';
                    $lote = '';
                    #CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
                    setLOG("REGISTRO",'*',"obs=$obs",$smart,"01.RECEBIDO");
                }
                
            
            }
        break;
    }



//FALA
// $falas = htmlspecialchars(ucwords(strtolower($fala)));
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

//APENAS LEITURA DO DIA SE LOTE VAZIO
if(empty($lote))
{
    $query = "SELECT id,smart,enge,cor,voltagem,fabricante,linha,modelo FROM codigo WHERE data LIKE '$dia%' AND modo LIKE '{$_SESSION['modo']}' ORDER BY id DESC";
    $total = Count(conex()->query("SELECT id FROM codigo WHERE data LIKE '$dia%' AND modo LIKE '{$_SESSION['modo']}'")->fetchAll(PDO::FETCH_ASSOC));
}
else
{
    $query = "SELECT id,smart,enge,cor,voltagem,fabricante,linha,modelo FROM codigo WHERE nlote = '$lote' AND modo LIKE '{$_SESSION['modo']}' ORDER BY id DESC";
    $total = Count(conex()->query("SELECT id FROM codigo WHERE nlote = '$lote' AND modo LIKE '{$_SESSION['modo']}'")->fetchAll(PDO::FETCH_ASSOC));
}

//JSON
$data = conex()->query($query)->fetchAll(PDO::FETCH_ASSOC);
$soma = ['caso'=>$caso, 'total'=>$total, 'lote'=>$lote, 'data'=>$data,'fala'=>$player];
print json_encode($soma, JSON_UNESCAPED_UNICODE);