<?php include '../CORE/Header2.php';?>
<script src="https://cdn.brandev.site/js/ag-grid-enterprise.min.js"></script>
<div id="myGrid" style="position:absolute; width: 98%;height:80%" class="ag-theme-alpine"></div>
<script type="text/javascript">		
// COLUNAS //////////////////////////////////////////
var columnDefs = 
[
  {headerName: "DATA", field: "DATA", filter: "agDateColumnFilter"},
  {headerName: "USER", field: "USER", filter: "agSetColumnFilter"},
  {headerName: "IP", field: "IP", filter: "agSetColumnFilter", hide: true},
  {headerName: "SMART", field: "SMART", filter: "agTextColumnFilter"},
  {headerName: "PROCESSO", field: "ACTION", filter: "agSetColumnFilter"},
  {headerName: "ETAPA", field: "CAMPO", filter: "agSetColumnFilter"},
  {headerName: "SAC", field: "TABELA", filter: "agSetColumnFilter"},
  {headerName: "AÇÕES", field: "COLUNAS", filter: "agTextColumnFilter"},
  {headerName: "ENDEREÇO", field: "ENDERECO", filter: "agTextColumnFilter", hide: true}
];


// SELECIONAR ITENS //////////////////////////////
function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    var imp = '';
    selectedRows.forEach( function(selectedRow, index) {
        if (index!==0) {
            selectedRowsString += ', ';
            imp += ', ';
        }
        selectedRowsString += selectedRow.smart;
        imp += selectedRow.smart;
    });
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
    document.myform.busca.value = selectedRowsString;
    document.formulario.imp.value = imp;
}
</script>		

<script>
// CARREGAR JSON //////////////////////////////////
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    // do http request to get our sample data - not using any framework to keep the example self contained.
    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'get.php');
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResult = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResult);
        }
    };
});
</script>
<script src="/AGGRID/agGrid.js"></script>
<?php include('../CORE/Footer2.php')?>