// 888b     d888 d8b                   888             
// 8888b   d8888 Y8P                   888             
// 88888b.d88888                       888             
// 888Y88888P888 888 88888b.  888  888 888888  8888b.  
// 888 Y888P 888 888 888 "88b 888  888 888        "88b 
// 888  Y8P  888 888 888  888 888  888 888    .d888888 
// 888   "   888 888 888  888 Y88b 888 Y88b.  888  888 
// 888       888 888 888  888  "Y88888  "Y888 "Y888888 

$(function()
{		
	$('#mbutton').on('click',function() 
	{
		var data = $('#mform').serialize();
		$.ajax
		(
			{
				type: 'POST',
				dataType: 'json',
				url: 'processa.php',
				async: true,
				data: data,
				success: function(data)
				{
					$('#caso').html(data.caso);
					$('#player').html(data.fala);
					$('#ne1').val(data.minuta);
					$('#ne2').val(data.minuta);
					$('#total').html('<i class="fas fa-shopping-cart mr-2"></i>'+data.total);
					$('#tab tbody').html('');
					for(var i = 0; i<data.total; i++)
					{
						// var classe = (data[i].reserva != data[i].user) ? "class='bg-warning'" : ''; 
						$("#tab tbody").append(
							"<tr>"+
								"<td>"+data[i].smart+"</td>"+
								"<td>"+data[i].linha+"</td>"+
								"<td class='d-none d-xl-table-cell'>"+data[i].reserva+"</td>"+
								"<td>"+"<button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td>"+
							"</tr>"
						);
					}
	},
				error: function(data){
					$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Pedido inexistente");
				}
			}
		);
	});
}
);

//        d8888      888 d8b          d8b                                    
//       d88888      888 Y8P          Y8P                                    
//      d88P888      888                                                     
//     d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
//    d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
//   d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
//  d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
// d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     

	$(function() 
		{			
    		$('#formulario').on('submit',function(e) 
				{	
					e.preventDefault();
					if($('#ne1').val() != '')
					{
						var data = $(this).serialize();
						$.ajax
						(
							{
								type: 'POST',
								dataType: 'json',
								url: 'processa.php',
								async: true,
								data: data,
								success: function(data)
								{
									if(data.rma.length != 0)
									{       
										Swal.fire({
										  title: 'Atenção!',
										  html: '<div class="row"><div class="col-12 h1">Este produto é RMA:</br>'+data.rma+'</div></div>',         
										  type: 'warning',
										  confirmButtonText: 'Ok'
										})
									}
									// $('#reserva').html(data.reserva);
									$('#smart').val('');
									$('#player').html(data.fala);
									$('#ne1').val(data.minuta);
									$('#ne2').val(data.minuta);
									$('#destino').val(data[0].destino);
									$('#caso').html(data.caso);
									$('#total').html('<i class="fas fa-forklift mr-2"></i>'+data.total);
									$('#tab tbody').html('');
									for(var i = 0; i<data.total; i++)
									{
										// var classe = (data[i].reserva != data[i].user) ? "class='bg-warning'" : ''; 
										$("#tab tbody").append(
										"<tr>"+
											"<td>"+data[i].smart+"</td>"+
											"<td>"+data[i].linha+"</td>"+
											"<td class='d-none d-xl-table-cell'>"+data[i].reserva+"</td>"+
											"<td>"+"<button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td>"+
										"</tr>"
										);
									}			
								},
								error: function(){
									$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro. ID não encontrado");
								}
							}
						)
					}
					else
					{
						swal.fire(
							'Atenção',
							'Abra uma <strong>nova</strong> minuta ou <strong>escolha</strong> uma já existente',
							'error'
						)
					}
    			}
			);
		}
	)

// 8888888888                   888          d8b         
// 888                          888          Y8P         
// 888                          888                      
// 8888888    888  888  .d8888b 888 888  888 888 888d888 
// 888        `Y8bd8P' d88P"    888 888  888 888 888P"   
// 888          X88K   888      888 888  888 888 888     
// 888        .d8""8b. Y88b.    888 Y88b 888 888 888     
// 8888888888 888  888  "Y8888P 888  "Y88888 888 888  

function excluir(id){        
	Swal.fire({
	  icon: 'question',
	  title: 'Confirma a exclusão do registro: '+id+" ?", 
	  showCancelButton: true,
	  confirmButtonText: 'Confirma',
	  cancelButtonText: 'Cancela'
	}).then((result) => {
	  if (result.value) {            
		this.del(id);             
		//y mostramos un msj sobre la eliminación  
		Swal.fire(
		  'Eliminado!',
		  'O Produto foi excluido do pedido.',
		  'success'
		)
	  }
	})                
} 
//Procedimiento BORRAR.
function del(id){
	min = $('#ne1').val(),
	$.ajax
	(
		{
			type: 'POST',
			dataType: 'json',
			url: 'processa.php',
			async: true,
			data: {smart:id, option:2, min:min},
			success: function(data)
			{
				$('#caso').html(data.caso);
				$('#player').html(data.fala);
				$('#ne1').val(data.minuta);
				$('#ne2').val(data.minuta);
				$('#destino').val(data[0].destino);
				$('#total').html('<i class="fas fa-shopping-cart mr-2"></i>'+data.total);
				$('#tab tbody').html('');
				for(var i = 0; i<data.total; i++)
				{
					// var classe = (data[i].reserva != data[i].user) ? "class='bg-warning'" : ''; 
					$("#tab tbody").append(
						"<tr>"+
							"<td>"+data[i].smart+"</td>"+
							"<td>"+data[i].linha+"</td>"+
							"<td class='d-none d-xl-table-cell'>"+data[i].reserva+"</td>"+
							"<td>"+"<button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td>"+
						"</tr>"
					);
				}
},
			error: function(data){
				$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro. Minuta não encontrada");
			}
		}
	);
}

// 8888888b.                             888    
// 888   Y88b                            888    
// 888    888                            888    
// 888   d88P  .d88b.  .d8888b   .d88b.  888888 
// 8888888P"  d8P  Y8b 88K      d8P  Y8b 888    
// 888 T88b   88888888 "Y8888b. 88888888 888    
// 888  T88b  Y8b.          X88 Y8b.     Y88b.  
// 888   T88b  "Y8888   88888P'  "Y8888   "Y888 

function reset(){       
	Swal.fire({
		title: 'Confirma a Conclusão do Pedido?',         
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Confirma',
		cancelButtonText: 'Cancela'
	}).then((result) => {
		if (result.value) {            
		this.fechar();  
		Swal.fire(
			'Parabéns!',
			'Pedido Concluído.',
			'success'
		)
		}
	})               
} 
//Procedimiento BORRAR.
function fechar(){
	min = $('#ne1').val(),
	$.ajax
	(
		{
			type: 'POST',
			dataType: 'json',
			url: 'processa.php',
			async: true,
			data: {option:3,min:min},
			success: function(data)
			{
				$('#caso').html(data.caso);
				$('#player').html(data.fala);
				$('#ne1').val('');
				$('#ne2').val('');
				$('#total').html('');
				$('#tab tbody').html('');    
				// MANDA TELEGRAM
				var msg = "O Pedido n°"+data.pedido+" do cliente "+data.user+" foi conclído com êxito!";
				var token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
				var chat = '-1001435954908';
				async function file_get_contents(uri, callback) {
					let res = await fetch(uri),
						ret = await res.text(); 
					return callback ? callback(ret) : ret; // a Promise() actually.
				}
				file_get_contents("https://api.telegram.org/bot"+token+"/sendMessage?chat_id="+chat+"&text="+msg);
				
			},
			error: function(){
				$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Falha na conclusão!");
			}
		}
	);
}

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

    function vibrate(ms){
		navigator.vibrate(ms);
	  }

// 888888b.                                           888          
// 888  "88b                                          888          
// 888  .88P                                          888          
// 8888888K.   8888b.  888d888  .d8888b  .d88b.   .d88888  .d88b.  
// 888  "Y88b     "88b 888P"   d88P"    d88""88b d88" 888 d8P  Y8b 
// 888    888 .d888888 888     888      888  888 888  888 88888888 
// 888   d88P 888  888 888     Y88b.    Y88..88P Y88b 888 Y8b.     
// 8888888P"  "Y888888 888      "Y8888P  "Y88P"   "Y88888  "Y8888  

    if(window.location.hash.substr(1,2) == "zx"){
        var bc = window.location.hash.substr(3);
        localStorage["barcode"] = decodeURI(window.location.hash.substr(3))
        window.close();
        self.close();
        window.location.href = "about:blank";//In case self.close isn't allowed
    }
    var changingHash = false;
    function onbarcode(event){
        switch(event.type){
            case "hashchange":{
                if(changingHash == true){
                    return;
                }
                var hash = window.location.hash;
                if(hash.substr(0,3) == "#zx"){
                    hash = window.location.hash.substr(3);
                    changingHash = true;
                    window.location.hash = event.oldURL.split("\#")[1] || ""
                    changingHash = false;
                    processBarcode(hash);
                }

                break;
            }
            case "storage":{
                window.focus();
                if(event.key == "barcode"){
                    window.removeEventListener("storage", onbarcode, false);
                    processBarcode(event.newValue);
                }
                break;
            }
            default:{
                console.log(event)
                break;
            }
        }
    }
    window.addEventListener("hashchange", onbarcode, false);

    function getScan(){
        var href = window.location.href;
        var ptr = href.lastIndexOf("#");
        if(ptr>0){
            href = href.substr(0,ptr);
        }
        window.addEventListener("storage", onbarcode, false);
        setTimeout('window.removeEventListener("storage", onbarcode, false)', 15000);
        localStorage.removeItem("barcode");
        //window.open  (href + "#zx" + new Date().toString());

        if(navigator.userAgent.match(/Firefox/i)){
            //Used for Firefox. If Chrome uses this, it raises the "hashchanged" event only.
            window.location.href =  ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
        }else{
            //Used for Chrome. If Firefox uses this, it leaves the scan window open.
            window.open   ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
        }
    }

    function processBarcode(bc){
			document.getElementById("smart").value = bc;
			$('html, body').animate({scrollTop:0},500);
       
        //put your code in place of the line above.
	}

	// 8888888b.          d8b          888    
	// 888   Y88b         Y8P          888    
	// 888    888                      888    
	// 888   d88P 888d888 888 88888b.  888888 
	// 8888888P"  888P"   888 888 "88b 888    
	// 888        888     888 888  888 888    
	// 888        888     888 888  888 Y88b.  
	// 888        888     888 888  888  "Y888 
	
	function printar()
	{
		print = $('#ne1').val(),
		window.open("minuta.php?min="+print)
		// window.location="minuta.php?min="+print	
	}