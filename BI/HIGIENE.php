<?php include '../CORE/Header2.php';

$mes = date("Y-m");
$total = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%'")->fetchAll(PDO::FETCH_ASSOC));

?>
<script>
    setTimeout(function() {
    location.reload();
    }, 600000);
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">      google.load("visualization", "1.0", {packages:["bar"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        let total = '<?php echo $total?>'
        var data = google.visualization.arrayToDataTable([
          ['', 'Total'],
          ['Produção geral da equipe', parseInt(total)]
        ]);

        var options = {         
        legend: { position: "none" },
          vAxis: {
            viewWindowMode:'explicit',
            viewWindow: {
              max:500,
              min:1
            }
        },
          bars: 'vertical', // Required for Material Bar Charts.
            width: 1900,
            height: 900
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_material'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
  </script>

<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>

<div class="row mt-5">
  <div class="col">
    <div id="barchart_material" class="mt-3"></div>
  </div>
</div>
<!--NOME DO MES -->
<h1 style="position: absolute;left: 6%; top: 89%;"><i class="fas fa-chart-bar mr-2"></i>Produção de <?php echo ucfirst(strftime('%B',strtotime('today')))?></h1>
<?php include "../CORE/Footer.php"?>