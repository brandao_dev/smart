<?php

include('../CORE/PDO.php');

$option = isset($_POST['option']) ? $_POST['option'] : null;
$smart = isset($_POST['id']) ? $_POST['id'] : null;
$tecnico = isset($_POST['tecnico']) ? $_POST['tecnico'] : null;
$const = isset($_POST['constatado']) ? strtoupper($_POST['constatado']) : null;
$pec = isset($_POST['peca']) ? strtoupper($_POST['peca']) : null;
//TEMPERATURAS
$T0 =  (!empty($_POST['T0'])) ? $_POST['T0'] : '';
$T1 =  (!empty($_POST['T1'])) ? $_POST['T1'] : '';
$temperatura = $T0.'.'.$T1;
//GAS
$gas = isset($_POST['gas']) ? $_POST['gas'] : '';
//ALTA
$alta = isset($_POST['alta']) ? $_POST['alta'] : '';
// SERVICO
$servico = empty($_POST['servico']) ? '' : implode('; ',$_POST['servico']);

$banco = banco($smart);



$desc = conex()->query("SELECT obs FROM $banco WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC)['obs'];
//BARRA PARA SEPARAR FUTURO ARRAY SE TIVER ALGUMA DESCRIÇÃO
$barra = (empty($desc)) ? '' : '/';
//SE NOVO SERVIÇO ESTA VAZIO COLOCA TESTADO AUTO, SE NÃO COLOCA O NOVO SERVIÇO
$desc .= (empty($servico)) ? $barra.$time.'=TESTADO_AUTO; ' : $barra.$time.'='.$servico.'; ';


$sql = conex()->query("SELECT id, CONSTATADO, SAC, PECA FROM sac WHERE SMART LIKE '$smart' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);

// TEM HISTORICO DA CONSTATADO
if(!empty($sql['CONSTATADO']))
{
    $constatado = (empty($const)) ? $constatado = $sql['CONSTATADO'] : $sql['CONSTATADO'].'/'.$time.'='.$tecnico.'.'.strtoupper($const);
}
//NÂO TEM HISTORICO CONSTATADO
else
{
    $constatado = (empty($const)) ? '' : $time.'='.$tecnico.'.'.strtoupper($const); 
}


#CHECAR SE PEÇA FOI MODIFICADA OU ACRESCIDA PARA MANDAR TELEGRAM
$peca = null;
$peca = (isset($pec)) ? $pec : '';
if(!empty($sql['PECA']))
{
   if($peca != $sql['PECA'])
   {
       //MUDA STATUS PARA PARTWAIT
       $status = '03.PARTWAIT';
       $msg = "Eu, ".$user.", estou adicionado/alterando um pedido de peça pelo atendimento: ".$sql['SAC'];
       $token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
       //SMART LOGIN CHANNEL
       $chat = '-1001210942378';
       file_get_contents("https://api.telegram.org/bot".$token."/sendMessage?chat_id=".$chat."&text=".$msg);
   }
}

$etapa = null;
switch($option)
{
    case '11.LABORATORIO':
    case '05.OBSERVACAO':
    case '09.TRATATIVA':
    case '06.ORCAMENTO':
    case '02.NEGADO':
        // BANCO
        $etapa = '15.RMA';
        conex()->prepare("UPDATE $banco SET peca = '$peca', etapa = '$etapa' WHERE smart = '$smart'")->execute();
    break;
    case '15.FINALIZADO':
        // BANCO
        $etapa = '03.REPARADO';
        conex()->prepare("UPDATE $banco SET peca = '$peca', etapa = '$etapa', temperatura = '$temperatura', obs = '$desc', ALTA = '$alta', CARGA = '$gas' WHERE smart = '$smart'")->execute();
    break;
    case '03.PARTWAIT':
        // BANCO
        $etapa = '03.PARTWAIT';
        conex()->prepare("UPDATE $banco SET peca = '$peca', etapa = '$etapa', temperatura = '' WHERE smart = '$smart'")->execute();
    break;
    break;

    
}
// SAC
conex()->prepare("UPDATE sac SET CONSTATADO = '$constatado', PECA = '$peca', ATENDIDO = 1, STATUS = '$option' WHERE SMART = '$smart' ORDER BY id DESC LIMIT 1")->execute();  
// LOG
setLOG("SAC", $etapa, "CONSTATADO=$const, PECA=$pec, ALTA=$alta, GAS=$gas, TEMPERATURA=$temperatura, REPARO=$servico", $smart, $option);

$retorno = ['cons'=>$const, 'peca'=>$pec, 'smart'=>$smart];
print json_encode($retorno, JSON_UNESCAPED_UNICODE);
?>