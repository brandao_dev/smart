<?php 

// error_reporting(0);
require '../vendor/autoload.php';
include('../CORE/PDO.php');

// use JsonSchema\Uri\Retrievers\FileGetContents;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = array(
  'borders' => array(
      'outline' => array(
          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
          'color' => array('argb' => '00000000'),
      ),
  ),
);

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

// DATA
$sheet->setCellValue('A1', 'DATA')->getColumnDimension('A')->setAutoSize(true);
$sheet->getStyle('A1')->applyFromArray($styleArray);
$sheet->getStyle('A1')->getFont('A1')->setBold(true);

#SMART
$sheet->setCellValue('B1', 'SMART')->getColumnDimension('B')->setAutoSize(true);
$sheet->getStyle('B1')->applyFromArray($styleArray);
$sheet->getStyle('B1')->getFont('B1')->setBold(true);

#LINHA
$sheet->setCellValue('C1', 'LINHA')->getColumnDimension('C')->setAutoSize(true);
$sheet->getStyle('C1')->applyFromArray($styleArray);
$sheet->getStyle('C1')->getFont('C1')->setBold(true);

#MODELO
$sheet->setCellValue('D1', 'MODELO')->getColumnDimension('D')->setAutoSize(true);
$sheet->getStyle('D1')->applyFromArray($styleArray);
$sheet->getStyle('D1')->getFont('D1')->setBold(true);


#PARA CADA ITEM DE TODA A LISTA DE RECEBIMENTO
$cel = 2;
$total = 0;
foreach($_SESSION['sql'] as $smart)
{
	#RECEBE UM POST CORRESPONDENTE A SMART DE CADA ITEM DA LISTA GERAL
	$item = (isset($_POST[$smart['id']])) ? $_POST[$smart['id']] : '';

	##PEGA SOMENTE OS PRODUTOS QUE NÃO FORAM RECUSADOS
	if($item != 1)
	{
        conex()->prepare("UPDATE codigo SET pago = '$dia' WHERE id = '{$smart['id']}'")->execute();
        $sheet->setCellValue('A'.$cel, $smart['datatriagem']);
        $sheet->setCellValue('B'.$cel, $smart['smart']);
        $sheet->setCellValue('C'.$cel, $smart['linha']);
        $sheet->setCellValue('D'.$cel, $smart['modelo']);
        $cel++;
		    $total++;
	}
	
}

$writer = new Xlsx($spreadsheet);
unlink('../PAGAMENTO/PLANILHAS/fechamento.xlsx');
$writer->save('../PAGAMENTO/PLANILHAS/fechamento.xlsx');

print json_encode($total, JSON_UNESCAPED_UNICODE);
    
?>