﻿<?PHP
include('../CORE/Header.php');


$processo = $_POST['nome'];

function distinct($etapa1,$etapa2,$fabricante1,$fabricante2,$linha1,$linha2,$localsaida)
{
    return conex()->query("SELECT DISTINCT(modelo) FROM codigo WHERE (etapa LIKE '$etapa1%' OR etapa LIKE '$etapa2%') AND (fabricante LIKE '$fabricante1%' OR fabricante LIKE '$fabricante2%') AND (linha LIKE '$linha1%' OR linha LIKE '$linha2%') AND localsaida LIKE '%$localsaida' ORDER BY modelo")->fetchAll(PDO::FETCH_ASSOC);
}

function distinctOutros($etapa1,$etapa2,$localsaida)
{
    return conex()->query("SELECT DISTINCT(modelo) FROM codigo WHERE (etapa LIKE '$etapa1%' OR etapa LIKE '$etapa2%') AND (fabricante LIKE 'BRASTEMP' OR fabricante LIKE 'CONSUL') AND (linha NOT LIKE 'REFR%' AND linha NOT LIKE 'ADEG%' AND linha NOT LIKE 'FREEZ%' AND linha NOT LIKE 'FRIGO%' AND linha NOT LIKE 'LAVADORA%' AND linha NOT LIKE 'FOG%' AND linha NOT LIKE 'COO%' AND linha NOT LIKE 'CERV%') AND localsaida LIKE '%$localsaida' ORDER BY modelo")->fetchAll(PDO::FETCH_ASSOC);
}

function distincTotal($etapa1,$etapa2,$localsaida)
{
    return conex()->query("SELECT DISTINCT(modelo) FROM codigo WHERE (etapa LIKE '$etapa1%' OR etapa LIKE '$etapa2%') AND (fabricante LIKE 'BRASTEMP' OR fabricante LIKE 'CONSUL' OR fabricante LIKE 'KITCHENAID' ) AND localsaida LIKE '%$localsaida' ORDER BY modelo")->fetchAll(PDO::FETCH_ASSOC);
}

function conti($coisa,$mode,$etapa1,$etapa2,$fabricante1,$fabricante2,$linha1,$linha2,$localsaida)
{
    return conex()->query("SELECT $coisa FROM codigo WHERE modelo LIKE '$mode' AND (etapa LIKE '$etapa1%' OR etapa LIKE '$etapa2%') AND (fabricante LIKE '$fabricante1%' OR fabricante LIKE '$fabricante2%') AND (linha LIKE '$linha1%' OR linha LIKE '$linha2%') AND localsaida LIKE '%$localsaida' ORDER BY '$coisa'");
}

switch($processo)
{
    case 'refri_galatea' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'CERVE';$ln2 = 'REFRI';$lsd = 'GALATEA';
        $titulo = "REFRIGERADOR/CERJEVEIRA WHIRLPOOL GALATEA";
    break;
    case 'refri_capitao' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'CERVE';$ln2 = 'REFRI';$lsd = 'CAPITAO';
        $titulo = "REFRIGERADOR/CERJEVEIRA WHIRLPOOL CAPITÃO";
        break;
    case 'refri_camares' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'CERVE';$ln2 = 'REFRI';$lsd = 'CAMARES';
        $titulo = "REFRIGERADOR/CERJEVEIRA WHIRLPOOL CAMARES";
        break;
    case 'refri_cce' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'CERVE';$ln2 = 'REFRI';$lsd = 'CCE';
        $titulo = "REFRIGERADOR/CERJEVEIRA WHIRLPOOL CCE";
        break;
    case 'refri_tudo' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'CERVE';$ln2 = 'REFRI';$lsd = '';
        $titulo = "REFRIGERADOR/CERJEVEIRA WHIRLPOOL TOTAL";
        break;
    case 'refri_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'CERVE';$ln2 = 'REFRI';$lsd = 'GALATEA';
        $titulo = "REFRIGERADOR/CERJEVEIRA WHIRLPOOL PARTWAIT";
        break;
    case 'free_galatea' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FREEZ';$ln2 = 'FREEZ';$lsd = 'GALATEA';
        $titulo = "FREEZER HORIZONAL/VERTICAL WHIRLPOOL GALATEA";
        break;
    case 'free_capitao' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FREEZ';$ln2 = 'FREEZ';$lsd = 'CAPITAO';
        $titulo = "FREEZER HORIZONAL/VERTICAL WHIRLPOOL CAPITÃO";
        break;
    case 'free_camares' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FREEZ';$ln2 = 'FREEZ';$lsd = 'CAMARES';
        $titulo = "FREEZER HORIZONAL/VERTICAL WHIRLPOOL CAMARES";
        break;
    case 'free_cce' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FREEZ';$ln2 = 'FREEZ';$lsd = 'CCE';
        $titulo = "FREEZER HORIZONAL/VERTICAL WHIRLPOOL CCE";
        break;
    case 'free_tudo' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FREEZ';$ln2 = 'FREEZ';$lsd = '';
        $titulo = "FREEZER HORIZONAL/VERTICAL WHIRLPOOL TOTAL";
        break;
    case 'free_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FREEZ';$ln2 = 'FREEZ';$lsd = 'GALATEA';
        $titulo = "FREEZER HORIZONAL/VERTICAL WHIRLPOOL PARTWAIT";
        break;
    case 'frigo_galatea' :
        $et1 = '02.AGUA';$et2 = '01';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FRIGO';$ln2 = 'ADEG';$lsd = 'GALATEA';
        $titulo = "FRiGOBAR WHIRLPOOL GALATEA";
        break;
    case 'frigo_capitao' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FRIGO';$ln2 = 'ADEG';$lsd = 'CAPITAO';
        $titulo = "FRiGOBAR WHIRLPOOL CAPITÃO";
        break;
    case 'frigo_camares' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FRIGO';$ln2 = 'ADEG';$lsd = 'CAMARES';
        $titulo = "FRiGOBAR WHIRLPOOL CAMARES";
        break;
    case 'frigo_cce' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FRIGO';$ln2 = 'ADEG';$lsd = 'CCE';
        $titulo = "FRiGOBAR WHIRLPOOL CCE";
        break;
    case 'frigo_tudo' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FRIGO';$ln2 = 'ADEG';$lsd = '';
        $titulo = "FRiGOBAR WHIRLPOOL TOTAL";
        break;
    case 'frigo_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FRIGO';$ln2 = 'ADEG';$lsd = 'GALATEA';
        $titulo = "FRiGOBAR WHIRLPOOL PARTWAIT";
        break;
    case 'lava_galatea' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'LAVADORA';$ln2 = 'LAVADORA';$lsd = 'GALATEA';
        $titulo = "LAVADORA WHIRLPOOL GALATEA";
        break;
    case 'lava_capitao' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'LAVADORA';$ln2 = 'LAVADORA';$lsd = 'CAPITAO';
        $titulo = "LAVADORA WHIRLPOOL CAPITÃO";
        break;
    case 'lava_camares' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'LAVADORA';$ln2 = 'LAVADORA';$lsd = 'CAMARES';
        $titulo = "LAVADORA WHIRLPOOL CAMARES";
        break;
    case 'lava_cce' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'LAVADORA';$ln2 = 'LAVADORA';$lsd = 'CCE';
        $titulo = "LAVADORA WHIRLPOOL CCE";
        break;
    case 'lava_tudo' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'LAVADORA';$ln2 = 'LAVADORA';$lsd = '';
        $titulo = "LAVADORA WHIRLPOOL TOTAL";
        break;
    case 'lava_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'LAVADORA';$ln2 = 'LAVADORA';$lsd = 'GALATEA';
        $titulo = "LAVADORA WHIRLPOOL PARTWAIT";
        break;
    case 'fog_galatea' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FOG';$ln2 = 'FOG';$lsd = 'GALATEA';
        $titulo = "FOGÃO WHIRLPOOL GALATEA";
        break;
    case 'fog_capitao' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FOG';$ln2 = 'FOG';$lsd = 'CAPITAO';
        $titulo = "FOGÃO WHIRLPOOL CAPITÃO";
        break;
    case 'fog_camares' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FOG';$ln2 = 'FOG';$lsd = 'CAMARES';
        $titulo = "FOGÃO WHIRLPOOL CAMARES";
        break;
    case 'fog_cce' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FOG';$ln2 = 'FOG';$lsd = 'CCE';
        $titulo = "FOGÃO WHIRLPOOL CCE";
        break;
    case 'fog_tudo' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FOG';$ln2 = 'FOG';$lsd = '';
        $titulo = "FOGÃO WHIRLPOOL TOTAL";
        break;
    case 'fog_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$fb1 = 'BRASTEMP';$fb2 = 'CONSUL';$ln1 = 'FOG';$ln2 = 'FOG';$lsd = 'GALATEA';
        $titulo = "FOGÃO WHIRLPOOL PARTWAIT";
        break;
    case 'kit_galatea' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'KITCHENAID';$fb2 = 'KITCHENAID';$ln1 = '';$ln2 = '';$lsd = 'GALATEA';
        $titulo = "KITCHENAID WHIRLPOOL GALATEA";
        break;
    case 'kit_capitao' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'KITCHENAID';$fb2 = 'KITCHENAID';$ln1 = '';$ln2 = '';$lsd = 'CAPITAO';
        $titulo = "KITCHENAID WHIRLPOOL CAPITÃO";
        break;
    case 'kit_camares' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'KITCHENAID';$fb2 = 'KITCHENAID';$ln1 = '';$ln2 = '';$lsd = 'CAMARES';
        $titulo = "KITCHENAID WHIRLPOOL CAMARES";
        break;
    case 'kit_cce' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'KITCHENAID';$fb2 = 'KITCHENAID';$ln1 = '';$ln2 = '';$lsd = 'CCE';
        $titulo = "KITCHENAID WHIRLPOOL CCE";
        break;
    case 'kit_tudo' :
        $et1 = '01';$et2 = '02.AGUA';$fb1 = 'KITCHENAID';$fb2 = 'KITCHENAID';$ln1 = '';$ln2 = '';$lsd = '';
        $titulo = "KITCHENAID WHIRLPOOL TOTAL";
        break;
    case 'kit_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$fb1 = 'KITCHENAID';$fb2 = 'KITCHENAID';$ln1 = '';$ln2 = '';$lsd = 'GALATEA';
        $titulo = "KITCHENAID WHIRLPOOL PARTWAIT";
        break;
    case 'out_galatea' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'GALATEA';
        $titulo = "OUTROS WHIRLPOOL GALATEA";
        break;
    case 'out_capitao' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'CAPITAO';
        $titulo = "OUTROS WHIRLPOOL CAPITÃO";
        break;
    case 'out_camares' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'CAMARES';
        $titulo = "OUTROS WHIRLPOOL CAMARES";
        break;
    case 'out_cce' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'CCE';
        $titulo = "OUTROS WHIRLPOOL CCE";
        break;
    case 'out_tudo' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = '';
        $titulo = "OUTROS WHIRLPOOL TOTAL";
        break;
    case 'out_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$lsd = 'GALATEA';
        $titulo = "OUTROS WHIRLPOOL PARTWAIT";
        break;
    case 'total_galatea' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'GALATEA';
        $titulo = "TOTAL WHIRLPOOL GALATEA";
        break;
    case 'total_capitao' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'CAPITAO';
        $titulo = "TOTAL WHIRLPOOL CAPITÃO";
        break;
    case 'total_camares' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'CAMARES';
        $titulo = "TOTAL WHIRLPOOL CAMARES";
        break;
    case 'total_cce' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = 'CCE';
        $titulo = "TOTAL WHIRLPOOL CCE";
        break;
    case 'total_tudo' :
        $et1 = '01';$et2 = '02.AGUA';$lsd = '';
        $titulo = "TOTAL WHIRLPOOL TOTAL";
        break;
    case 'total_pw' :
        $et1 = '10.PART';$et2 = '10.PART';$lsd = 'GALATEA';
        $titulo = "TOTAL WHIRLPOOL PARTWAIT";
        break;
}
if(fnmatch('out*',$processo))
{
    $modelo = distinctOutros($et1,$et2,$lsd);
}
elseif(fnmatch('*total',$processo))
{
    $modelo = distinctTotal($et1,$et2,$lsd);
}
else
{
    $modelo = distinct($et1,$et2,$fb1,$fb2,$ln1,$ln2,$lsd);
}
?>
    <br>

<div class="f100">
<div class="mediolaranja"><?php echo $titulo;?></div><div class="medioroxo">TOTAL: <?php echo $_SESSION[$processo]; $Cont = 0;?></div>
<div align="rigth"><a href="<?php echo DIRPAGE.'BI/PROCESSAR.php'?>"><img src="<?php echo DIRIMG.'rep.png'?>" width="50" alt=""></a></div>
</div>

<fieldset class="round">
<div class="divTable">
    <div class="divBody" style="background-color: #ccc">
        <div class="f100">
        <?php foreach($modelo as $mode){foreach($mode as $chave => $valor) { ?>
            <?php $Cont = $Cont+1; $cla = ($Cont % 2 == 0) ? "background-color: #999" : "background-color: #fff"?>
        <div class="divRow">
            <div class="card radius">
                <div class="divCell mediocinza" width="10%">
                    <?php echo $valor.' - '.conti('modelo',$valor,$et1,$et2,$fb1,$fb2,$ln1,$ln2,$lsd)->rowCount()?>
                </div>
                <div class="divCell">
                    <form action="<?php echo DIRPAGE.'CONSULTA/BUSCAR.php'?>" target="_blank" method="post">
                    <select class="radius medioroxo" name="q" id="q">
                        <?php foreach(conti('smart',$valor,$et1,$et2,$fb1,$fb2,$ln1,$ln2,$lsd)->fetchAll(PDO::FETCH_ASSOC) as $snarf)
                            {
                                foreach ($snarf as $key => $smart)
                                {
                                    $Cont++; $cla = ($Cont % 2 == 0) ? "background-color: #999" : "background-color: #777";
                                    echo '<option style="'.$cla.'" value="'.$smart.'">'.$smart.'</option>';
                                }
                            }
                        ?>
                    </select>
                    <button type="submit">
                        <img src="<?php echo DIRIMG.'inicial.png'?>" width="50" alt="" style="vertical-align: middle">
                    </button>
                    </form>
                </div>
            </div>
        </div>
       <?php } } ?>
        </div>
    </div>
    </fieldset>
</div>
<?PHP include('../CORE/Footer.php'); ?>