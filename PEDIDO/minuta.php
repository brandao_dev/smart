<?php include_once('../CORE/PDO.php');

$Busca = conex()->query("SELECT ALTERACAO,smart,modelo,linha,cor,localsaida FROM codigo WHERE m2 LIKE '{$_SESSION['m2']}' UNION ALL SELECT ALTERACAO,smart,modelo,linha,cor,localsaida FROM lg WHERE m2 LIKE '{$_SESSION['m2']}' ORDER BY ALTERACAO DESC");
$row_Busca = $Busca->fetchAll(PDO::FETCH_ASSOC);
$total = Count($row_Busca);

?>

<!DOCTYPE HTML>
<html lang='pt-br'>
<head>
<meta charset='UTF-8'>
<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
<meta name='autor' content='A. Brandão Full Stack Developer 2020 / PHPOO, Composer, NodeJs, SASS, Gulp, MySQL, CSS, HTML5, JavaScript'>
<meta name='format-detection' content='telephone-no'>
<title>ERP - SMART</title>
<link rel='shortcut icon' href='../IMG/SICON.png'>
<link rel='stylesheet' href='https://cdn.brandev.site//CSS/bootstrap.css'>
<link href='https://cdn.brandev.site//css/all.css' rel='stylesheet'>
<link href='https://cdn.brandev.site//css/style.css' rel='stylesheet'>
<link href='https://cdn.brandev.site//style/estilo.css' rel='stylesheet'>
<script src="https://cdn.brandev.site//JS/jquery.js"></script>
<script src="https://cdn.brandev.site//JS/bootstrap.js"></script>
<script src="https://cdn.brandev.site//JS/popper.js"></script>
</head>
<body  class='text-center' onload="window.print()">
<!-- MARGEM DO CORPO -->
<div class='container-fluid'>
  <div class="row mt-2 mb-2">
    <div class="col-2"><img src="../img/smart.jpg" width="110%"></div>
    <div class="col-10"><h1>Minuta de Transferência</h1></div>
  </div>

<table class="table table-striped table-bordered" colspan='4'>
  <thead>
    <tr>    
        <th>OBSERVAÇÃO</th>
        <th>QTD</th>
        <th>DATA</th>
        <th>USUÁRIO</th>
        <th>MINUTA</th>
    </tr>  
    <tr>
    	<td></td>
    	<td><?php echo $total;?></td>
    	<td><?PHP echo $time;?></td>
      <td><?PHP echo $user;?></td>
      <td><?PHP echo $_SESSION['m2'];?></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>SMART</th>
      <th>MODELO</th>
      <th>LINHA</th>
      <th>COR</th>
      <th>DESTINO</th>
    </tr>
    <?php foreach($row_Busca as $row){?>
    <tr>
      <td><?php echo $row['smart'];?></td>
      <td><?php echo $row['modelo'];?></td>
      <td><?php echo $row['linha'];?></td>
      <td><?php echo $row['cor'];?></td>
      <td><?php echo $row['localsaida'];?></td>
    </tr>
    <?php } ?>
  </tbody>
  <?php  
    echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=saida.php'>";
    include('../CORE/Footer2.php')?>