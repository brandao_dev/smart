<?php 
include('../CORE/Header2.php');
include('leitura.php');
// include('mail.php')
?>
<script src="/PAGAMENTO/ajax.js"></script>
<div class="row mt-5 mb-3">
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-cyan h5"><i class="fas fa-user-cog mr-2"></i>Produzidos: <?php echo $_SESSION['total']?></div>
  </div>
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-cyan h5"><i class="fas fa-user-check mr-2"></i>Pré-aprovados: <?php echo($_SESSION['total'] - $pendente)?> </div>
  </div>
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-cyan h5" id="total"><i class="fas fa-user-graduate mr-2"></i>Pós-aprovados: 0 </div>
  </div>
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-indigo h5" id="pendente"><i class="fas fa-user-lock mr-2"></i>Pendentes: <?php echo (isset($pendente)) ? $pendente : 0?> </div>
  </div>
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-olive h5"><i class="fas fa-user-clock mr-2"></i>Referência: <?php echo $_SESSION['referencia']?></div>
  </div>
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-success h5"><i class="fas fa-user-alt-slash mr-2"></i>Chamados nulos: <?php echo $nulos?></div>
  </div>
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-success h5"><i class="fas fa-user-injured mr-2"></i>10.PartWait: <?php echo $partwait?></div>
  </div>
  <div class="col-6 col-xl-3">
    <div class="alert alert-block alert-success h5"><i class="fas fa-user-robot mr-2"></i>09.Scrap: <?php echo $scrap?></div>
  </div>
</div>

<form method="post" id="foda">

<table class="table table-hover table-bordered table-sm">
  <thead class="thead-dark">
    <tr>
      <th scope="col">RETORNO</th>
      <th scope="col">INTERVALO</th>
      <th scope="col">RMA</th>
      <th scope="col">AJUSTE</th>
      <th scope="col">SMART</th>
      <th scope="col">APROVADO?</th>
    </tr>
  </thead>
  <tbody><?php echo $tbody?>
  </tbody>
</table>

<div class="row">
  <div class="col-12">
        <button class="btn btn-block btn-success btn-lg shadow" type="submit" onclick="vibrate(200);play()"><i class="far fa-money-check-edit-alt mr-2"></i>Efetivar</button>
  </div>
</div>

</form>
<br>
<?php include('../CORE/Footer2.php')?>