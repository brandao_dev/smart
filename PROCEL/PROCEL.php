﻿<?PHP 
require('../CORE/PDO.php');

#DEFINIR QUAL BANCO USAR
$modelo = $_GET['modelo'];
#QUERY TABELA
$tabela = conex()->query("SELECT * FROM modelo WHERE modelo LIKE '$modelo'")->fetch(PDO::FETCH_ASSOC);
//FORNECEDOR
$fornecedor = ($tabela['fabricante'] == 'CONSUL')||($tabela['fabricante'] == 'BRASTEMP') ? 'Whirlpool' : '';

?>

<!doctype html>
<html>
	
<head>
<meta charset="utf-8">
<script>
	setTimeout(() => {
		window.close()
	}, 500);
</script>
<title>ERP SMART</title>
</head>
	
<style>
	.container {position: relative;}
	#image {position: absolute;}
	#imetro {position: absolute;left: 40px;top: 40px;}
	#energia {position: absolute;left: 390px;top: 20px;font-family: 'Arial Black', 'sans-serif';font-size: 60px}
	#volt {position: absolute;left: 800px;top: 35px;}
	#gas {position: absolute;left: 700px;top: 35px;}
	#linha {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 390px;top: 95px;}
	#fornecedor {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 390px;top: 130px;}	
	#marca {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 30px;left: 390px;top: 165px;}
	#modelo {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 40px;left: 390px;top: 205px;}
	#tipo {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 30px;left: 390px;top: 265px;}
	#queima {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 480px;top: 355px;}
	#forno {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 860px;top: 355px;}
	
	/*MAIS EFICIENTE*/
	#mais {position:absolute; font-family: Tahoma, Genova;font-size: 38px;left: 55px;top: 360px}
	
	#a {position: absolute;left: 820px;top: 412px;}
	#b {position: absolute;left: 820px;top: 408px;}
	#c {position: absolute;left: 820px;top: 594px;}
	#d {position: absolute;left: 820px;top: 680px;}
	#e {position: absolute;left: 820px;top: 767px;}
	#a2 {position: absolute;left: 600px;top: 322px;}
	#b2 {position: absolute;left: 600px;top: 508px;}
	#c2 {position: absolute;left: 600px;top: 614px;}
	#d2 {position: absolute;left: 600px;top: 680px;}
	#e2 {position: absolute;left: 600px;top: 767px;}
	
	/*MENOS EFICIENTE*/
	#menos {position:absolute; font-family: Tahoma, Genova;font-size: 38px;left: 55px;top: 875px}
	
	/*KWH*/
	#kwh {position: absolute;font-family:"Arial Black";font-size: 120px;left: 75px;top: 955px;}
	#mes {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 50px;left: 80px;top: 1105px;}
	#kwhi {position: absolute;left: 36px;top: 960px;}
	
	/*COMPARTIMENTO REFRIGERADO*/
	#refrii {position: absolute;left: 405px;top: 960px;}
	#refri {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 65px;left: 440px;top: 960px;}
	#refric {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 23px;left: 455px;top: 1045px;}
	
	/*COMPARTIMENTO CONGELADOR*/
	#congei {position: absolute;left: 690px;top: 960px;}
	#conge {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 65px;left: 730px;top: 960px;}
	#congec {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 23px;left: 750px;top: 1045px;}
	
	/*LOGO PROCEL*/
	#procel {position: absolute;left: 1px;top: 1240px;}
	
	/*LOGO PBR*/
	#PBE {position: absolute;left: 195px;top: 1249px;}
	
	/*QUADRO SEGURANÇA*/
	#quadro {position: absolute;left: 405px;top: 1150px;}
	#segura {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 27px;left: 460px;top: 1170px;}
	#registro {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 33px;left: 440px;top: 1245px;}
	
	/*QUADRO VOLUME TOTAL*/
	#quadro2 {position: absolute;left: 690px;top: 1150px;}
	#total {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 65px;left: 715px;top: 1150px;}
	#volume {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 27px;left: 752px;top: 1265px;}
	
	/*QUADRO TEMPERATURA*/
	#tempera {position: absolute;left: 405px;top: 1340px;}
	#temp {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 90px;left: 680px;top: 1330px;}
	
	/*INSTRUÇÔES*/
	#instrucoes {position: absolute;font-family: 'arial';font-size: 43px;left: 1px;top: 1480px;}
</style>
	
<body onload="window.print()">
<div class="container">
  <!--<img id="image" src="GABA2.jpg" width="100%">-->
  <label id="imetro"><img src="IMETRO.png" width="32%"></label>
  <label id="energia">ENERGIA</label>
  <label id="volt"><img src="<?PHP echo ($tabela['tensao'] == '220 V') ? '220.png' : '127.png';?>"></label>
  <label id="gas"><img src="<?PHP echo (!empty($tabela['gas'])) ? 'chama.png' : ''?>"></label>
  <label id="linha"><?PHP echo $tabela['linha'];?></label>
  <label id="fornecedor"><?PHP echo $fornecedor;?></label>
  <label id="marca">Marca: <?PHP echo $tabela['fabricante'];?></label>
  <label id="modelo">Modelo: <?PHP echo $tabela['modelo'];?></label>
  <label id="tipo">Tipo de Degelo: <?PHP echo $tabela['dgelo'];?></label>
	
	<!--SOMENTE PARA FOGÕES-->
	<?PHP if(!empty($tabela['gas'])) { ?>
  <label id="queima">Queimadores de Mesa</label>
  <label id="forno">Forno</label>
	<?PHP } ?>
	
  <label id="mais">Mais eficiente</label>
	
	<!--A-->
	<?PHP if(substr($tabela['eficiencia'],-1) == 'A') { ?>	
  <label id="a"><img src="A.jpg" width="75%"></label>
	
	<!--B-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'B') { ?>
  <label id="b"><img src="B.jpg" width="75%"></label>
	
	<!--C-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'C') { ?>
  <label id="c"><img src="C.jpg" width="75%"></label>
	
	<!--D-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'D') { ?>
  <label id="d"><img src="D.jpg" width="75%"></label>
	
	<!--E-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'E') { ?>
  <label id="e"><img src="E.jpg" width="75%"></label>
	
	<?PHP } if(substr($tabela['eficiencia'],1,-1) == 'A') { ?>
  <label id="a2"><img src="A.jpg" width="75%"></label>
	
	<?PHP } if(substr($tabela['eficiencia'],1,-1) == 'B') { ?>
  <label id="b2"><img src="B.jpg" width="75%"></label>
	
	<?PHP } if(substr($tabela['eficiencia'],1,-1) == 'C') { ?>
  <label id="c2"><img src="C.jpg" width="75%"></label>
	
	<?PHP } if(substr($tabela['eficiencia'],1,-1) == 'D') { ?>
  <label id="d2"><img src="D.jpg" width="75%"></label>
	
	<?PHP } if(substr($tabela['eficiencia'],1,-1) == 'E') { ?>
  <label id="e2"><img src="E.jpg" width="75%"></label>
	
	<?PHP } ?>
	
  <label id="menos">Menos eficiente</label>

	<!--KWHM-->
  <label id="kwh"><?PHP echo $tabela['kwhm'];?></label>
  <label id="mes">kWh/mês</label>
  <label id="kwhi"><img src="KWH.png" width="90%"></label>
	
	<!--REFRIGERADO-->
  <label id="refrii"><img src="KWH.png" width="68%"></label>
  <label id="refri"><?PHP echo str_replace(" L", "",$tabela['volbr']);?></label>
  <label id="refric">&nbsp;&nbsp;&nbsp;Volume do<br>compartimento<br>&nbsp;&nbsp;&nbsp;&nbsp;refrigerado</label>
	
	<!--CONGELADOR-->
  <label id="congei"><img src="KWH.png" width="85%"></label>
  <label id="conge"><?PHP echo str_replace(" L", "",$tabela['volbf']);?></label>
  <label id="congec">&nbsp;&nbsp;&nbsp;Volume do<br>compartimento<br>do congelador</label>
	
	<!--PROCEL PBE-->
  <label id="procel"><img src="ICON.png" width="60%"></label>
  <label id="PBE"><img src="PBE.png" width="200px"></label>
	
	<!--REGISTRO-->
  <label id="quadro"><img src="KWH.png" width="68%"></label>
  <label id="segura">&nbsp;Segurança<br>Desempenho</label>
  <label id="registro">No. Registro<br><?PHP echo $tabela['registro'];?></label>
	
	<!--VOLUME TOTAL-->
  <label id="quadro2"><img src="KWH.png" width="85%"></label>
  <label id="total"><?PHP echo str_replace(" L", "",$tabela['voltb']);?></label>
  <label id="volume">Volume total</label>
	
	<!--TEMPERATURA-->
  <label id="tempera"><img src="TEMP.png" width="96%"></label>
  <label id="temp"><?PHP echo $tabela['temperatura'];?>°C</label>
	
  <label id="instrucoes" align="center">Instruções de instalação e recomendações de uso, leia o Manual do aparelho</label>
</div>
</body>
</html>