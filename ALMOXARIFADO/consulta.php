<?php include '../CORE/Header2.php';?>
<script src="https://cdn.brandev.site/js/ag-grid-enterprise.min.js"></script>
<script src="ajax.js"></script>
<div id="player" style="display: none;"></div>
<!-- MOSTRA SELECTED -->
<?php if(($user == 'RICARDO') || ($equipe == 'ADM')) { ?>
	<div class="row">
		<div class="col-12">
			<!-- SEGUNDA LINHA DE FRASE DE APOIO *** /////////////////////////////////////////////////////////////////////////////////////// -->
			<div class="alert alert-info font-weight-bold" id="caso"><i class="fas fa-alien-monster mr-2"></i>Olá <?php echo $user?>!</div>
		</div>
		<div class="col-12 col-sm-6 col-md-3">
			<button class="btn btn-lg btn-block btn-success mb-3 shadow" id="insert" onclick="insert($(this).val())"><i class="fas fa-syringe mr-2"></i>Insere</button>
		</div>
		<div class="col-12 col-sm-6 col-md-3">
			<button class="btn btn-lg btn-block btn-primary mb-3 shadow" id="edit" onclick="edit($(this).val())"><i class="fas fa-eraser mr-2"></i>Edita</button>
		</div>
		<div class="col-12 col-sm-6 col-md-3">
			<button class="btn btn-lg btn-block btn-warning mb-3 shadow" onclick="location.reload()"><i class="fas fa-recycle mr-2"></i>Atualiza</button>
		</div>
		<div class="col-12 col-sm-6 col-md-3">
			<button class="btn btn-lg btn-block btn-danger mb-3 shadow" id="delete" onclick="excluir($(this).val())"><i class="fas fa-trash-restore-alt mr-2"></i>Deleta</button>
		</div>
	</div>
<?php } ?>
<div id="myGrid" style="position:absolute; width: 98%;height:80%" class="ag-theme-alpine"></div>
<script src="/AGGRID/agGrid.js"></script>
<?php include('../CORE/Footer2.php')?>