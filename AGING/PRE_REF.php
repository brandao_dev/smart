<?php 

################################# *** INCLUIR CONEXÃO E CABEÇALHO *** ###########################################################################
include '../PDO/PDO.php';

################################## *** FORMULARIO LEITURA VERIFICAR POST DE ID   ***   ###########################################################

if(isset($_POST['id']))
{
	$id = strtoupper(substr($_POST['id'],0,12));$_SESSION['id']= $id;
	$banco = ((substr($id,0,1)) == 'W') ? 'codigo' : 'lg';$_SESSION['banco']= $banco;
	
	//LE A DATA DE INICIO E FAZ O CALCULO
	$cli = $conn->query("SELECT REOPERADO,INICIO_TRIAGEM,smart,modelo,linha,etapa,serie1,fabricante,voltagem,cor,enge,1A,2A,3A,4A,1B,2B,3B,4B,OS,rma,obs,ALTA,CARGA FROM $banco WHERE smart = '$id'")->fetch(PDO::FETCH_ASSOC);
	$_SESSION['tipo'] = $conn->query("SELECT tipo FROM tabela WHERE LOCATE(modelo,'{$cli['modelo']}')")->fetch(PDO::FETCH_ASSOC)['tipo'];
	$A1 = $cli['1A'];
	$B1 = $cli['1B'];
	$A2 = $cli['2A'];
	$B2 = $cli['2B'];
	$A3 = $cli['3A'];
	$B3 = $cli['3B'];
	$A4 = $cli['4A'];
	$B4 = $cli['4B'];
	$os = $cli['OS'];
	$obs = $cli['obs'];
	$_SESSION['reoperado'] = $cli['REOPERADO'];
	$rma = $cli['rma'];$_SESSION['rma'] = $rma;
	
	
	#CRIA UM FORMATO DE DATA
	$create = ($cli['INICIO_TRIAGEM'] == '0000-00-00 00:00:00') ? date_create($NADA) : date_create($cli['INICIO_TRIAGEM']);
	
	#AJUSTA O FORMATO DE DATA
	$ini = date_format($create,"Y-m-d");
	
	
	$datetime1 = date_create($ini);
	$datetime2 = date_create($dia);
	$interval = date_diff($datetime1, $datetime2);
	$aging = $interval->format('%a');
	$_SESSION['aging'] = $aging;
	
	//TERNARIA	BLOQUEIO PARA ETAPAS INVALIDAS
	$caso = 'OK';
	$_SESSION['caso'] = $caso;
	$_SESSION['etapa'] = $cli['etapa'];
	$_SESSION['carga'] = $cli['CARGA'];
}

#################################################################################################################################################
########################################################*** SEGUNDA FASE ***#####################################################################



//////////////////////////////////*** UPDATE NA WIRLPOOL E INSERT NA LG ***////////////////////////////////////////

if(isset($_POST['cadastro']))
	{
		//RECEBIMENTO DAS TEMPERATURAS
		$A1 = $_POST['A1'];
		$B1 = $_POST['B1'];
		$A2 = $_POST['A2'];
		$B2 = $_POST['B2'];
		$A3 = $_POST['A3'];
		$B3 = $_POST['B3'];
		$A4 = $_POST['A4'];
		$B4 = $_POST['B4'];
		$A5 = $_POST['A5'];
		$B5 = $_POST['B5'];
		$os = $_POST['os'];
		$obs = strtoupper($_POST['obs']);
		$rma = $_POST['rma'];
		$caso = $_SESSION['caso'];
		$id = $_SESSION['id'];
		$banco = $_SESSION['banco'];
		$etapa = $_SESSION['etapa'];
		$aging = $_SESSION['aging'];
		$alta = ($_POST['i1']) ? 1 : 0;
	
	//SLIDE REFRIGERAÇÃO ///////////////////////////////////////////////
		
		
		if(isset($_POST['i0']))
		{
			if(empty($_SESSION['carga']))
			{
				if($_SESSION['tipo'] == 'R600a')
				{
					$carga = 'R134a';
				}
				elseif($_SESSION['tipo'] == 'R134a')
				{
					$carga = 'R600a';
				}
			}
			else
			{
				if($_SESSION['carga'] == 'R600a')
				{
					$carga = 'R134a';
				}
				elseif($_SESSION['carga'] == 'R134a')
				{
					$carga = 'R600a';
				}
			}
			
		}
		else
		{
			if(empty($_SESSION['carga']))
			{
				$carga = $_SESSION['tipo'];
			}
			else
			{
				$carga = $_SESSION['carga'];
			}
			
		}
	////////////////////////////////////////////////////////////////////	
		
		$constatado = strtoupper($_POST['constatado']);
		$obsac = strtoupper($_POST['obsac']);
		$peca = strtoupper($_POST['peca']);
		#QUINZENA 1/2 FUNIL DECIDIU QUE NÂO E NECESSARIO MANTER O STATUS RMA NA LISTA DE STANDBY 
		#$estado = ($etapa == '15.RMA') ? '15.RMA' : $_POST['etapa'];
		$estado = $_POST['etapa'];
	//SE O PRODUTO FOI APROVADO	
		if($caso == 'OK')
		{
	//SE O ESTADO FOI APROVADO		
			if($estado == '13.TESTING')
			{
				if($_SESSION['reoperado'] == '0000-00-00 00:00:00')
					
				#TESTING E REPARO COMUM &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&	
				{
					if($aging == 0)
					   {
							$conn->prepare("UPDATE $banco SET usertriagem=?, INICIO_TRIAGEM=?, etapa=?, 1A=?, 1B=? WHERE smart=?")->execute([$user, $time, '13.TESTING', $A1, $B1, $id]);
							$caso = 'dia0';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"etapa=13.TESTING, temps=$A1, $B1",$id,"OK");
							
					   }
					elseif($aging == 1)
					   {
							$conn->prepare("UPDATE $banco SET usertriagem=?, etapa=?, 2A=?, 2B=? WHERE smart=?")->execute([$user, '13.TESTING', $A2, $B2, $id]);
							$caso = 'dia1';
							$caso = 'dia0';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"13.TESTING, $A2, $B2",$id,"OK");
					   }
					elseif($aging > 1)
					   {
						
							$conn->prepare("UPDATE $banco SET datatriagem=?, usertriagem=?, etapa=?, 3A=?, 3B=?, ALTA=?,OS=?, obs=?, CARGA=? WHERE smart=?")->execute([$time, $user, '03.REPARADO', $A3, $B3, $alta, $os, $obs,$carga, $id]);
							$caso = 'COMPLETO';
				
							#CHAMA FUNCAO LOG	
							setLOG("UPDATE",$banco,"status=03.REPARADO, temps=$A3-$B3, alta=$alta, os=$os, obs=$obs, carga=$carga",$id,"OK");

							if(fnmatch('*.*',$_SESSION['rma']))
							{
								$add = $conn->query("SELECT SERVICO, CONSTATADO, OBS, PECA FROM sac WHERE SMART='$id' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
								$reparo = $add['SERVICO'].'_'.$time.'-'.$obs;
								$constatado = $add['CONSTATADO'].'_'.$time.'-'.$constatado;
								$obsac = $add['OBS'].'_'.$obsac;
								$peca = $add['PECA'].'_'.$peca;
								$conn->prepare("UPDATE sac SET OBS=?,CONSTATADO=?,SERVICO=?,PECA=? WHERE SMART=? ORDER BY id DESC LIMIT 1")->execute([$reparo,$constatado,$obsac,$peca,$id]);

								#CHAMA FUNCAO LOG	
								setLOG("UPDATE","SAC","serviço=$reparo,conatatado=$constatado,obs=$obsac,peça=$peca",$id,"AUTO");
						   }
						}
				}
				else
					
				#REOPERAÇÃO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
				{
					if($aging == 0)
					   {
							$conn->prepare("UPDATE $banco SET usertriagem=?, INICIO_TRIAGEM=?, etapa=?, 1A=?, 1B=? WHERE smart=?")->execute([$user, $time, $estado, $A1, $B1, $id]);
							$caso = 'dia0';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"$estado, $A1, $B1",$id,"OK");
					   }
					elseif($aging == 1)
					   {
							$conn->prepare("UPDATE $banco SET usertriagem=?, etapa=?, 2A=?, 2B=? WHERE smart=?")->execute([$user, '13.TESTING', $A2, $B2, $id]);
							$caso = 'dia1';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"13.TESTING, $A2, $B2",$id,"OK");
					   }
					elseif($aging == 2)
					   {
							$conn->prepare("UPDATE $banco SET usertriagem=?, etapa=?, 3A=?, 3B=? WHERE smart=?")->execute([$user, '13.TESTING', $A3, $B3, $id]);
							$caso = 'dia2';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"13.TESTING, $A3, $B3",$id,"OK");
					   }
					elseif($aging == 3)
						{
							$conn->prepare("UPDATE $banco SET usertriagem=?, etapa=?, 4A=?, 4B=?, ALTA=?,OS=?, obs=? WHERE smart=?")->execute([$user, '13.TESTING', $A4, $B4, $alta, $os, $obs, $id]);
							$caso = 'dia3';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"13.TESTING, $A4, $B4, $alta, $os, $obs",$id,"OK");

						}
					elseif($aging > 3)
						{
							$conn->prepare("UPDATE $banco SET  datatriagem=?, usertriagem=?, etapa=?, 4A=?, 4B=?, ALTA=?, OS=?, obs=?, CARGA=? WHERE smart=?")->execute([$time,$user, '03.REPARADO', $A5, $B5, $alta, $os, $obs, $carga, $id]);
							$caso = 'COMPLETO';
				
							#CHAMA FUNCAO LOG	
							setLOG("UPDATE",$banco,"etapa=03.REPARADO, temp=$A5-$B5, alta=$alta, os=$os, obs=$obs, carga=$carga",$id,"OK");

							if(fnmatch('*.*',$_SESSION['rma']))
							{
								$add = $conn->query("SELECT SERVICO, CONSTATADO, OBS, PECA FROM sac WHERE SMART='$id' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
								$reparo = $add['SERVICO'].'_'.$time.'-'.$obs;
								$constatado = $add['CONSTATADO'].'_'.$time.'-'.$constatado;
								$obsac = $add['OBS'].'_'.$obsac;
								$peca = $add['PECA'].'_'.$peca;
								$conn->prepare("UPDATE sac SET OBS=?,CONSTATADO=?,SERVICO=?,PECA=? WHERE SMART=? ORDER BY id DESC LIMIT 1")->execute([$reparo,$constatado,$obsac,$peca,$id]);

								#CHAMA FUNCAO LOG	
								setLOG("UPDATE","SAC","serviço=$reparo, constatado=$constatado, obs=$obsac, peça=$peca",$id,"AUTO");
							}

						}
				}
					
			}
			elseif($estado == '16.STANDBY')
			{
				if($_SESSION['reoperado'] == '0000-00-00 00:00:00')
				{
					if($aging > 1)
					{
						$conn->prepare("UPDATE $banco SET datatriagem=?, usertriagem=?, etapa=?, 3A=?, 3B=?, ALTA=?, OS=?, obs=?, CARGA=? WHERE smart=?")->execute([$time, $user, '03.REPARADO', $A3, $B3, $alta, $os, $obs, $carga, $id]);
						$caso = 'COMPLETO';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"03.REPARADO, $A3, $B3, $alta, $os, $obs, $carga",$id,"OK");

						if(fnmatch('*.*',$_SESSION['rma']))
						{
							$add = $conn->query("SELECT SERVICO, CONSTATADO, OBS, PECA FROM sac WHERE SMART='$id' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
							$reparo = $add['SERVICO'].'_'.$time.'-'.$obs;
							$constatado = $add['CONSTATADO'].'_'.$time.'-'.$constatado;
							$obsac = $add['OBS'].'_'.$obsac;
							$peca = $add['PECA'].'_'.$peca;
							$conn->prepare("UPDATE sac SET OBS=?,CONSTATADO=?,SERVICO=?,PECA=? WHERE SMART=? ORDER BY id DESC LIMIT 1")->execute([$reparo,$constatado,$obsac,$peca,$id]);
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE","SAC","serviço=$reparo, constatado=$constatado, obs=$obsac, peça=$peca",$id,"AUTO");
						}
					}
					else
					{
						$caso = 'NOSTAND';
					}
				}
				else
				{
					if($aging > 3)
					{
						$conn->prepare("UPDATE $banco SET datatriagem=?, usertriagem=?, etapa=?, 4A=?, 4B=?, ALTA=?, OS=?, obs=?, CARGA=? WHERE smart=?")->execute([$time, $user, '03.REPARADO', $A5, $B5, $alta, $os, $obs, $carga, $id]);
						$caso = 'COMPLETO';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"datatriagem, usertriagem, etapa, 4A, 4B, ALTA, OS, obs, CARGA",$id,"OK");

						if(fnmatch('*.*',$_SESSION['rma']))
						{
							$add = $conn->query("SELECT SERVICO, CONSTATADO, OBS, PECA FROM sac WHERE SMART='$id' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
							$reparo = $add['SERVICO'].'_'.$time.'-'.$obs;
							$constatado = $add['CONSTATADO'].'_'.$time.'-'.$constatado;
							$obsac = $add['OBS'].'_'.$obsac;
							$peca = $add['PECA'].'_'.$peca;
							$conn->prepare("UPDATE sac SET OBS=?,CONSTATADO=?,SERVICO=?,PECA=? WHERE SMART=? ORDER BY id DESC LIMIT 1")->execute([$reparo,$constatado,$obsac,$peca,$id]);
				
							#CHAMA FUNCAO LOG	
							setLOG("UPDATE","serviço=$reparo, constatado=$constatado, obs=$obsac, peça=$peca",$id,"AUTO");
						}
					}
					else
					{
						$caso = 'NOSTAND';
					}
				}
				
			}
			elseif($estado == '10.PARTWAIT')
			{
				$conn->prepare("UPDATE $banco SET INICIO_TRIAGEM=?, datatriagem=?, usertriagem=?, etapa=?, ALTA=?, OS=?, obs=?, 1A=?, 1B=?, 2A=?, 2B=?, 3A=?, 3B=?, 4A=?, 4B=? WHERE smart=?")->execute(['','', $user, '10.PARTWAIT', $alta, $os, $obs, '0', '0', '0', '0', '0', '0', '0', '0', $id]);
				$caso = 'PARTSCRAP';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"etapa=10.PARTWAIT, alta=$alta, os=$os, obs=$obs, zera temp",$id,"OK");
				
				if(fnmatch('*.*',$_SESSION['rma']))
				{
						$add = $conn->query("SELECT SERVICO, CONSTATADO, OBS, PECA FROM sac WHERE SMART='$id' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
						$reparo = $add['SERVICO'].'_'.$time.'-'.$obs;
						$constatado = $add['CONSTATADO'].'_'.$time.'-'.$constatado;
						$obsac = $add['OBS'].'_'.$obsac;
						$peca = $add['PECA'].'_'.$peca;
						$conn->prepare("UPDATE sac SET OBS=?,CONSTATADO=?,SERVICO=?,PECA=? WHERE SMART=? ORDER BY id DESC LIMIT 1")->execute([$reparo,$constatado,$obsac,$peca,$id]);
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE","SAC","serviço=$reparo, constatado=$constatado, obs=$obsac, peça=$peca",$id,"AUTO");
				}
			}
			elseif($estado == '14.REOPERACAO')
			{
				$conn->prepare("UPDATE $banco SET REOPERADO=?, INICIO_TRIAGEM=?, datatriagem=?, usertriagem=?, etapa=?, ALTA=?, OS=?, obs=?, 1A=?, 1B=?, 2A=?, 2B=?, 3A=?, 3B=?, 4A=?, 4B=? WHERE smart=?")->execute([$time,'','', $user, '14.REOPERACAO', $alta, $os, $obs, '0', '0', '0', '0', '0', '0', '0', '0', $id]);
				$caso = 'PARTSCRAP';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"etapa=14.REOPERACAO, alta=$alta, os=$os, obs=$obs, zera temp", $id,"OK");
			}
			elseif($estado == '09.SCRAP')
			{
				$conn->prepare("UPDATE $banco SET INICIO_TRIAGEM=?, datatriagem=?, usertriagem=?, etapa=?, ALTA=?, OS=?, obs=?, 1A=?, 1B=?, 2A=?, 2B=?, 3A=?, 3B=?, 4A=?, 4B=? WHERE smart=?")->execute(['',$time, $user, '09.SCRAP', $alta, $os, $obs, '0', '0', '0', '0', '0', '0', '0', '0', $id]);
				$caso = 'SCRAP';
				
				#CHAMA FUNCAO LOG	
				setLOG("UPDATE",$banco,"etapa=09.SCRAP, alta=$alta, os=$os, obs=$obs, zera temp",$id,"OK");
			}
			else
			{
				$caso = 'INVALIDA';
			}
		}
		$_SESSION['REOPERADO'] = '';
	}
?> 


<!--#######################################################***  INICIO DE HTML  ***#################################################################-->
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="description" content="Programa desenvolvido por Anderson Brandao">
	<title>ERP Smart</title>
	<link rel="stylesheet" type="text/css" href="../css/easyui.css">
	<link rel="stylesheet" type="text/css" href="../css/icon.css">
	<link rel="stylesheet" type="text/css" href="../css/demo.css">
	<link rel="stylesheet" type="text/css" href="../css/smart.css">
<style>	
	.switch {position: relative;display: inline-block;width: 90px;height: 39px;}.switch input {display:none;}
	.slider {position: absolute; cursor: pointer;top: 0;left: 0;right: 0;bottom: 0;background-color: #555;-webkit-transition: .4s;transition: .4s;}
	.slider:before {position: absolute;content: "";height: 30px;width: 30px;left: 3px;bottom: 3px;background-color: white;-webkit-transition: .4s;
	  transition: .4s;}
	input:checked + .slider {background-color: #f50;}
	input:focus + .slider {box-shadow: 0 0 10px #603;}
	input:checked + .slider:before {-webkit-transform: translateX(26px);-ms-transform: translateX(26px);transform: translateX(50px);}

	/* Rounded sliders */
	.slider.round {border-radius: 50px;}
	.slider.round:before {border-radius: 50%;}
	
		.button {
  		-webkit-transition-duration: 0.4s; /* Safari */
  		transition-duration: 0.4s;
  		background-color: #f50
		}
		.button:hover {
  		background-color: #555
		}
		.button {border-radius: 6px;}
		.button {font-size: 40px;}
		.button {padding: 3px 3px;}
		a:link 
		{ 
		text-decoration:none; 
		}
</style>	
</head>
<body>
<div id="suadiv">


<!--///////////////////////////////**** CABEÇALHO DE PAGINA ***//////////////////////////////////////////////////////////-->	

<div style="background-color: #ccc" class="dark">Registro de Aging<?php include '../MENU2.php';?></div>
<br>
	
<!--////////////////////////////////*** FRASES DE APOIO ***//////////////////////////////////////////////////////////////-->
<fieldset  class="round">
<legend>	
<table width="100%" cellpadding="3" cellspacing="0">
  <tr>
    <td>	
<?php 
		date_default_timezone_set('America/Sao_Paulo');
		$hr = date(" H ");
		if($hr >= 12 && $hr<18) {
		$resp = "Boa tarde";}
		else if ($hr >= 0 && $hr <12 ){
		$resp = "Bom dia";}
		else {
		$resp = "Boa noite";}
		if(isset($caso)){
			switch($caso)
					{
				case "BLOK":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ". $user."! Etapa do produto inválida. Avise o Cadastro!</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/BLOK.mp3' type='audio/mpeg'></audio>";
					break;		
				case "OK":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user."! Produto OK. Aguardando definição!</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/OK.mp3' type='audio/mpeg'></audio>";
					break;
				case "dia0":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Primeiro dia de teste!</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/dia0.mp3' type='audio/mpeg'></audio>";
					break;
				case "dia1":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Segundo dia de teste!</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/dia1.mp3' type='audio/mpeg'></audio>";
					break;	
				case "dia2":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Terceiro dia de teste</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/dia2.mp3' type='audio/mpeg'></audio>";
					break;
				case "dia3":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Quarto dia de teste.</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/dia3.mp3' type='audio/mpeg'></audio>";
					break;
				case "dia4":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Quinto dia de teste.</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/dia4.mp3' type='audio/mpeg'></audio>";
					break;
				case "INVALIDA":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Impossível registrar em etapa inválida.</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/INVALIDA.mp3' type='audio/mpeg'></audio>";
					break;
				case "NOSTAND":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Impossível Stand By. Aging incompleto!</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/NOSTAND.mp3' type='audio/mpeg'></audio>";
					break;
				case "COMPLETO":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Parabéns! Aging completo. Produto liberado para Aprovação.</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/COMPLETO.mp3' type='audio/mpeg'></audio>";
					break;
				case "PARTSCRAP":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Estado atualizado para ".$estado.". Aging suspenso.</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/PARTSCRAP.mp3' type='audio/mpeg'></audio>";
					break;
				case "RMA":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Atenção! RMA. Dê Prioridade na conclusão.</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/RMA.mp3' type='audio/mpeg'></audio>";
					break;
				case "SCRAP":
					echo "<div style ='font-size:30px;font-family: Arial black;color:#f50'>".$resp." ".$user.", Estado atualizado para ".$estado.". Aging suspenso.</div>";
					echo "<audio controls autoplay='autoplay' style='display:none'><source src='../som/AGE/PARTSCRAP.mp3' type='audio/mpeg'></audio>";
					break;
				}
		}
?>
</td>
</tr>
</table>
</legend>            										
<!--//////////////////////////////////***  SMART, ENGENHARIA, PRAZO E MARCA ***//////////////////////////////////////////--> 
<table width="100%" border="0" cellpadding="0" cellspacing="2">       
	<tr>
  		<td class="branco" align="center" bgcolor=#555 width="25%">Smart ID</td>
        
<form action="#" method="post">
	    <!-- BOTÃO DE BUSCA -->
        <td align="center" width="5%" class="main" rowspan="2">
			<button type="submit" class='button'>
        		<img width="110" height="90" src="../img/inicial.png">
			</button>	
		</td>
  		<td class="branco" width="20%" align="center" bgcolor=#555>ESTADO ATUAL</td>
  		<td class="branco" width="30%" align="center" bgcolor=#555>LINHA</td>
  		<td class="branco" width="20%" align="center" bgcolor=#555>SERIAL</td>
  		<td class="branco" width="30%" align="center" <?PHP echo ($_SESSION['reoperado'] != '0000-00-00 00:00:00') ? 'bgcolor=#f50' : 'bgcolor=#555';?>>REOPERAÇÃO</td>
	</tr>
	<tr>
		<!-- QUERY IDS DISPONIVEIS //////////////////////////////////////////////////////////////////////////-->		
		<td  class="main" align="center">
    		<input list="id" STYLE="width:99%;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF"
            class="main" name="id" type="text" autocomplete="off" placeholder="<?PHP echo $id;?>" required autofocus/>
			<datalist id="id">
				<?php
  				$sac = $conn->query("SELECT smart,id FROM codigo UNION ALL SELECT smart,id FROM lg ORDER BY id DESC");
				$resultado = $sac->fetch(PDO::FETCH_ASSOC);
				do
				{
					echo "<option value='{$resultado['smart']}'/>";
				}

				while($resultado = $sac->fetch(PDO::FETCH_ASSOC));
				?>
			</datalist>	
        </td>
</form>
		<td class="texto" align="center"><?PHP echo $cli['etapa'];?></td>		
		<td class="texto" align="center"><?PHP echo $cli['linha'];?></td>
                                   
      	<!--    SERIE    -->                              
      	<td class="texto" align="center"><?PHP echo $cli['serie1'];?></td> 
                                   
      	<!--    REOPERAÇÃO    -->                              
      	<td class="texto" align="center"><?PHP echo ($_SESSION['reoperado'] != '0000-00-00 00:00:00') ? $_SESSION['reoperado'] : 'NÃO';?></td> 
	</tr>
</table>
<br>	
<!--/////////////////////////////////////////*** MODELO TENSÃO E COR ***/////////////////////////////////////////////////-->	
<table class="main" width="100%" border="0" cellpadding="0" cellspacing="2">
	<tr>
		<td class="branco" align="center" bgcolor=#555>MODELO</td>
  		<td class="branco" align="center" bgcolor=#555>MARCA</td>
		<td class="branco" align="center" bgcolor=#555>COR</td>
		<td class="branco" align="center" bgcolor=#555>TENSÃO</td>
		<td class="branco" align="center" bgcolor=#555>ENGENHARIA</td>
		<td class="branco" align="center" bgcolor=#f50>AGING</td>
		<?PHP echo($inicio != '0000-00-00 00:00:00') ? '<td class="branco" align="center" bgcolor=#555>INICIO DA TRIAGEM</td>': '';?>
	<tr>
	</tr>	
		
		<!--   MODELO   -->
  		<td class="texto" align="center"><?PHP echo $cli['modelo'];?></td>
        											
        <!--   MARCA   -->
        <td class="texto" align="center"><?PHP echo $cli['fabricante'];?>
        </td>
        											
        <!--    COR    -->
        <td  align="center" class="texto"><?PHP echo $cli['cor'];?></td>
	
		<!-- TENSAO -->
        <td class="texto" align="center"><?PHP echo $cli['voltagem'];?></td>
	
		<!-- ENGENHARIA -->
        <td class="texto" align="center"><?PHP echo $cli['enge'];?></td>
	
		<!--   AGING    -->
        <td class="branco" align="center" bgcolor=#f50><?PHP echo $aging;?></td>
	
		<!--   AGING    -->
		<?PHP echo($inicio != '0000-00-00 00:00:00') ? '<td class="branco" align="center" bgcolor=#555>'.$inicio.'</td>': '';?>
    </tr>
</table>
</fieldset>	
<br>
<!--****************************  TEMPERATURAS  ************************************-->

<fieldset style="background-color: darkgray">
<table width="100%" border="0" cellpadding="0" cellspacing="2" bgcolor="#ccc">	
	<tr>
		<td align="center" class="branco" bgcolor=#555>TD1-RF
		</td>
  		<td align="center" class="branco" bgcolor=#555>TD1-FZ
		</td>
  		<td align="center" class="branco" bgcolor=#555>TD2-RF
		</td>
  		<td align="center" class="branco" bgcolor=#555>TD2-FZ
		</td>
  		<td align="center" class="branco" bgcolor=#555>TD3-RF
		</td>
  		<td align="center" class="branco" bgcolor=#555>TD3-FZ
		</td>
		<?PHP echo ($_SESSION['reoperado'] != "0000-00-00 00:00:00") ? 
			'
			<td align="center" class="branco" bgcolor="#555">TD4-RF
			</td>
			<td align="center" class="branco" bgcolor="#555">TD4-FZ
			</td>
			<td align="center" class="branco" bgcolor="#555">TD5-RF
			</td>
			<td align="center" class="branco" bgcolor="#555">TD5-FZ
			</td>
			' : '';
		?>
	<tr>
	</tr>	
	
<form action="#" method="post">
		<!-- PRAZO 1A-->
        <td class="texto" align="center" <?PHP if($aging == 0)echo 'bgcolor="#f50"';?>>
      		<input STYLE="width:100px;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;text-align: center"
        	class="main" type="number" placeholder="<?PHP echo $cli['1A'];?>" name="A1" <?PHP if($aging == 0)echo 'required';?>>
        </td>
	
		<!-- PRAZO 1B-->
        <td class="texto" align="center" <?PHP if($aging == 0)echo 'bgcolor="#f50"';?>>
      		<input STYLE="width:100px;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;text-align: center"
        	class="main" type="number" placeholder="<?PHP echo $cli['1B'];?>" name="B1" <?PHP if($aging == 0)echo 'required';?>>
        </td>
	
		<!-- PRAZO 2A-->
        <td class="texto" align="center" <?PHP if($aging == 1)echo 'bgcolor="#f50"';?>>
      		<input STYLE="width:100px;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;text-align: center"
        	class="main" type="number" placeholder="<?PHP echo $cli['2A'];?>" name="A2" <?PHP if($aging == 1)echo 'required';?>>
        </td>
	
		<!-- PRAZO 2B-->
        <td class="texto" align="center" <?PHP if($aging == 1)echo 'bgcolor="#f50"';?>>
      		<input STYLE="width:100px;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;text-align: center"
        	class="main" type="number" placeholder="<?PHP echo $cli['2B'];?>" name="B2" <?PHP if($aging == 1)echo 'required';?>>
        </td>
	
		<!-- PRAZO 3A-->
        <td class="texto" align="center" <?PHP if($aging == 2)echo 'bgcolor="#f50"';?>>
      		<input STYLE="width:100px;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;text-align: center"
        	class="main" type="number" placeholder="<?PHP echo $cli['3A'];?>" name="A3" <?PHP if($aging == 2)echo 'required';?>>
        </td>
	
		<!-- PRAZO 3B-->
        <td class="texto" align="center" <?PHP if($aging == 2)echo 'bgcolor="#f50"';?>>
      		<input STYLE="width:100px;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;text-align: center"
        	class="main" type="number" placeholder="<?PHP echo $cli['3B'];?>" name="B3" <?PHP if($aging == 2)echo 'required';?>>
        </td>
		<?PHP
			if($_SESSION['reoperado'] != '0000-00-00 00:00:00')
			{
				require('../AGING/REOPERA.php');
			}
		?>
	
	</tr>
</table>
<br/>	
 
<STYLE>
	input[type="radio"] {visibility: hidden;}
    label {display: block;border: 6px solid #CCC;width: 100px;height: 80px;background-color:#CCC}
	input[type="radio"]:checked+label {border-color:#555;width: 100px;height:80px; background-color: #F50;}
</STYLE>
	
<!-- /////////////////////////////////////*** ESTADO RMA OS ALTA ***///////////////////////////////////////////////////-->
<table width="100%" class="main" border="0" cellpadding="0" cellspacing="2" bgcolor="#ccc">
	<tr>
		<td class="branco" align="center" bgcolor=#555 width="30%">RMA</td>
		
     	<!--   TESTTING	-->
		<td class="main" align="center" rowspan="2">13.TESTING
			<input type="radio" name="etapa" id="i1" value="13.TESTING" checked/>
			<label for="i1"><img src="../img/AGE/TESTE.png" alt="" width="80"></label>
		</td>	
		
		<!--   PARTWAIT	-->
		<td class="main" align="center" rowspan="2">10.PARTWAIT
			<input type="radio" name="etapa" id="i2" value="10.PARTWAIT"/>
			<label for="i2"><img src="../img/TV/images.png" alt="" width="80"></label>
		</td>	
		
		<!--   SCRAP	-->
		<td class="main" align="center" rowspan="2">09.SCRAP
			<input type="radio" name="etapa" id="i3" value="09.SCRAP"/>
			<label for="i3"><img src="../img/scrap2.png" alt="" width="80"></label>
		</td>	
		
		<!--   REOPERA	-->
		<td class="main" align="center" rowspan="2">14.REOPERA
			<input type="radio" name="etapa" id="i4" value="14.REOPERACAO"/>
			<label for="i4"><img src="../img/AGE/REOPERA.png" alt="" width="80"></label>
		</td>	
		
		<!--   STAND	
		<td class="main" align="center" rowspan="2">16.STANDBY
			<input type="radio" name="etapa" id="i5" value="16.STANDBY"/>
			<label for="i5"><img src="../img/TV/CHECKED.png" alt="" width="80"></label>
		</td>
		-->
	</tr>
	<tr>
		<!-- RMA-->
        <td class="texto" align="center"><div style ='font-size:22px;font-family: Arial black;color:#f50'><?PHP echo $rma;?></div></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="3">	
	<tr>
  		<td class="branco" align="center" bgcolor=#555 width="25%">REFRIGERAÇÃO</td>
  		<td class="branco" align="center" bgcolor=#555 width="25%">SISTEMA DE ALTA</td>
		<td class="branco" align="center" bgcolor=#555>OS</td>
   		<td align="center" rowspan="4">   
			<button type="submit" name="cadastro" class='button'>
        		<img width="140" height="120" src="../img/BRANCA/SIMB.png">
			</button>	
		</td>
	</tr>
	<tr>
		<td class="branco" align="center" bgcolor="#CCC">
			<table>
				<tr>
					<td align="center">
						<div style ='font-size:22px;font-family: Arial black;color:#555'><?PHP $mostra = (empty($_SESSION['carga'])) ? $_SESSION['tipo'] : $_SESSION['carga']; echo $mostra;?></div>
					</td>
					<td>
						<label class="switch" width="10%">
							<input type="checkbox" name="i0">
							<span class="slider round"></span>
						</label>
					</td>
					<td align="center">
						<div style ='font-size:22px;font-family: Arial black;color:#555'><?PHP echo ($mostra == 'R600a') ? 'R134a' : 'R600a';?></div>
					</td>
				</tr>
			</table>	
		</td>
		<!-- SISTEMA DE ALTA-->
        <td align="center" bgcolor="#ccc">
			<table>
				<tr>
					<td align="center">
						<div style ='font-size:22px;font-family: Arial black;color:#555'>Não</div>
					</td>
					<td>
						<label class="switch" width="10%">
							<input type="checkbox" name="i1" <?PHP echo ($cli['ALTA'] == 1) ? 'checked' : '';?>>
							<span class="slider round"></span>
						</label>
					</td>
					<td align="center">
						<div style ='font-size:22px;font-family: Arial black;color:#555'>Sim</div>
					</td>
				</tr>
			</table>
		</td>
		
		<!-- OS-->
        <td class="texto" align="center">
      		<input STYLE="width:99%;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;"
        	class="main" type="text" placeholder="<?PHP echo $cli['OS'];?>"  value="<?PHP echo $cli['OS'];?>" name="os">
        </td>
    </tr>
	<tr>
		<td class="branco" align="left" bgcolor=#555 colspan="3">DESCRIÇÃO DO REPARO</td>
	</tr>
	<tr>
		<!-- DESCRICAO-->
        <td class="texto" align="center" colspan="3">
      		<input STYLE="width:100%;color: #609; font-family: Verdana; font-weight: bold; font-size: 22px;border: #FFFFFF;"
        	class="main" type="text" placeholder="<?PHP echo $cli['obs'];?>" value="<?PHP echo strtoupper($cli['obs']);?>" name="obs" 
					<?PHP 
						echo ((($aging > 1)&&($_SESSION['reoperado'] == '0000-00-00 00:00:00'))||(($aging > 3)&&($_SESSION['reoperado'] != '0000-00-00 00:00:00'))) ? 'required' : 'readonly';
					?>
			>
        </td>
	</tr>	
</table>
</fieldset>
</br>
<?PHP 
	
if(fnmatch('*.*',$rma))
{
	require('../AGING/SAC.php');
}
	
?>	
</form>
</div>
</body>
</html>