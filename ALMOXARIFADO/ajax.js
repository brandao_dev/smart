		
// COLUNAS //////////////////////////////////////////
var columnDefs = 
[
  {headerName: "DATA", field: "data", filter: "agDateColumnFilter"},
  {headerName: "CÓDIGO", field: "codigo", filter: "agTextColumnFilter"},
  {headerName: "DESCRIÇÃO", field: "descricao", filter: "agTextColumnFilter"},
  {headerName: "LOCAL", field: "local", filter: "agTextColumnFilter"},
  {headerName: "QTD", field: "quantidade", filter: "agNumberColumnFilter"},
  {headerName: "ALTERAÇÃO", field: "alteracao", filter: "agDateColumnFilter"},
  {headerName: "USUARIO", field: "user", filter: "agSetColumnFilter"}
];


// SELECIONAR ITENS //////////////////////////////
function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    selectedRows.forEach(function(selectedRow, index){
        if (index!==0) {
            selectedRowsString += ', ';
        }
		selectedRowsString += selectedRow.id+','+selectedRow.codigo+','+selectedRow.descricao+','+selectedRow.local+','+selectedRow.quantidade;
		sid = selectedRow.id
    });
	document.querySelector('#edit').value = selectedRowsString;
	document.querySelector('#delete').value = sid;
}

// CARREGAR JSON //////////////////////////////////
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    // do http request to get our sample data - not using any framework to keep the example self contained.
    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'get.php');
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResult = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResult);
        }
    };
});

// 8888888888      888 d8b 888                     
// 888             888 Y8P 888                     
// 888             888     888                     
// 8888888     .d88888 888 888888  8888b.  888d888 
// 888        d88" 888 888 888        "88b 888P"   
// 888        888  888 888 888    .d888888 888     
// 888        Y88b 888 888 Y88b.  888  888 888     
// 8888888888  "Y88888 888  "Y888 "Y888888 888   

function edit(id)
{
	//SE NÃO FOI ESCOLHIDO UMA LINHA
	if(id.length > 0)
	{
		array = id.split(',')
		Swal.fire({
			title: '<h2 class="text-primary"><i class="fas fa-edit mr-2"></i>Editar linha</h2>',
			html:
			'<form method="POST" id="form">'+
				'<div class="row text-left">'+
					'<div class="col-12 mt-2">'+
						'<label for="codigo">Código</label>'+
						'<input style="display: none" value="'+array[0]+'" name="id">'+
						'<input type="text" name="codigo" class="form-control form-lg font-weight-bold" value="'+array[1]+'">'+
					'</div>'+
					'<div class="col-12 mt-2">'+
						'<label for="descricao">Descrição</label>'+
						'<input type="text" name="descricao" class="form-control form-lg font-weight-bold" value="'+array[2]+'">'+
					'</div>'+
					'<div class="col-12 mt-2">'+
						'<label for="local">Local</label>'+
						'<input type="text" name="local" class="form-control form-lg font-weight-bold" value="'+array[3]+'">'+
					'</div>'+
					'<div class="col-12 mt-2">'+
						'<label for="quantidade">Quantidade</label>'+
						'<input type="number" name="quantidade" class="form-control form-lg font-weight-bold" value="'+array[4]+'">'+
					'</div>'+
				'</div>'+
			'</form>',
			showCancelButton: true,
			confirmButtonText: '<i class="fas fa-smile-wink mr-2"></i>Confirma',
			cancelButtonText: '<i class="far fa-angry mr-2"></i>Cancela',
			backdrop: `rgba(0,0,0,0.8)`,
			reverseButtons: true
		  }).then((result) => {
			if (result.isConfirmed) {
				let data = $('#form').serialize()
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "edita.php",
					async: true,
					data: data,
					success: function(result){
						$('#caso').html(result[0])
						$('#player').html(result[1])
						Swal.fire(
							'Item Editado',
							'Clique em <strong>ATUALIZA</strong> se precisar conferir essa alteração na tabela.',
							'info'
						)
						console.log(result.caso)
					},
					error: function(){
						console.log('erro')
					}
				})
			  }})

	}
	else
	{
		Swal.fire(
			'Atenção!',
			'Selecione uma <strong>linha</strong> na tabela para editar',
			'warning'
		)
	}
}

// 8888888                                    d8b         
//   888                                      Y8P         
//   888                                                  
//   888   88888b.  .d8888b   .d88b.  888d888 888 888d888 
//   888   888 "88b 88K      d8P  Y8b 888P"   888 888P"   
//   888   888  888 "Y8888b. 88888888 888     888 888     
//   888   888  888      X88 Y8b.     888     888 888     
// 8888888 888  888  88888P'  "Y8888  888     888 888  

function insert()
{
	Swal.fire({
		title: '<h2 class="text-success"><i class="fas fa-syringe mr-2"></i>Inserir linha</h2>',
		html:
		'<form method="POST" id="form">'+
			'<div class="row text-left">'+
				'<div class="col-12 mt-2">'+
					'<label for="codigo">Código</label>'+
					'<input style="display: none" name="id">'+
					'<input type="text" name="codigo" id="name" class="form-control form-lg font-weight-bold">'+
				'</div>'+
				'<div class="col-12 mt-2">'+
					'<label for="descricao">Descrição</label>'+
					'<input type="text" name="descricao" id="descricao" class="form-control form-lg font-weight-bold">'+
				'</div>'+
				'<div class="col-12 mt-2">'+
					'<label for="local">Local</label>'+
					'<input type="text" name="local" id="local" class="form-control form-lg font-weight-bold">'+
				'</div>'+
				'<div class="col-12 mt-2">'+
					'<label for="quantidade">Quantidade</label>'+
					'<input type="number" name="quantidade" id="quantidade" class="form-control form-lg font-weight-bold">'+
				'</div>'+
			'</div>'+
		'</form>',
		showCancelButton: true,
		confirmButtonText: '<i class="fas fa-smile-wink mr-2"></i>Confirma',
		confirmButtonColor: '#490',
		cancelButtonText: '<i class="far fa-angry mr-2"></i>Cancela',
		backdrop: `rgba(0,0,0,0.8)`,
		reverseButtons: true
	  }).then((result) => {
		if (result.isConfirmed) {
			let data = $('#form').serializeArray()
			//VALIDAÇÂO DOS DADOS
			if(data[1].value.length > 0)
			{
				if(data[2].value.length > 0)
				{
					if(data[3].value.length > 0)
					{
						if(data[4].value.length > 0 > 0)
						{
							$.ajax({
								type: "POST",
								dataType: "json",
								url: "insert.php",
								async: true,
								data: data,
								success: function(result){
									$('#caso').html(result[0])
									$('#player').html(result[1])
									Swal.fire(
										'Item adicionado',
										'Clique em <strong>ATUALIZA</strong> se precisar conferir essa adição na tabela',
										'success'
									)
									console.log('ok')
								},
								error: function(){
									console.log('erro')
								}
							})
						}
						else
						{
							Swal.fire(
								'Atenção!',
								'Informe a <strong>QUANTIDADE</strong> para prosseguir',
								'error'
							)
						}
					}
					else
					{
						Swal.fire(
							'Atenção!',
							'Informe o <strong>LOCAL</strong> para prosseguir',
							'error'
						)
					}
				}
				else
				{
					Swal.fire(
						'Atenção!',
						'Informe a <strong>DESCRIÇÃO</strong> para prosseguir',
						'error'
					)
				}
			}
			else
			{
				Swal.fire(
					'Atenção!',
					'Informe o <strong>CÓDIGO</strong> para prosseguir',
					'error'
				)
			}
		  }})

}

// 8888888888                   888          d8b         
// 888                          888          Y8P         
// 888                          888                      
// 8888888    888  888  .d8888b 888 888  888 888 888d888 
// 888        `Y8bd8P' d88P"    888 888  888 888 888P"   
// 888          X88K   888      888 888  888 888 888     
// 888        .d8""8b. Y88b.    888 Y88b 888 888 888     
// 8888888888 888  888  "Y8888P 888  "Y88888 888 888  

function excluir(id)
{
	//SE NÃO FOI ESCOLHIDO UMA LINHA
	if(id.length > 0)
	{
		console.log(id)
		Swal.fire({
			title: '<h2 class="text-danger"><i class="fas fa-trash-restore-alt mr-2"></i>Excluir linha</h2>',
			html:
				'<div class="row mt-3 mb-3">'+
					'<div class="col-12 mt-2">'+
						'Tem certeza que deseja excluir essa linha?'+
					'</div>'+
				'</div>',
			showCancelButton: true,
			confirmButtonText: '<i class="fas fa-smile-wink mr-2"></i>Confirma',
			confirmButtonColor: '#910',
			cancelButtonText: '<i class="far fa-angry mr-2"></i>Cancela',
			backdrop: `rgba(0,0,0,0.8)`,
			reverseButtons: true
		  }).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "deleta.php",
					async: true,
					data: 'id='+id,
					success: function(result){
						$('#caso').html(result[0])
						$('#player').html(result[1])
						Swal.fire(
							'Item excluído',
							'Clique em <strong>ATUALIZA</strong> se precisar conferir essa ação na tabela',
							'success'
						)
						console.log('ok')
					},
					error: function(){
						console.log('erro')
					}
				})
			  }})
	}
	else
	{
		Swal.fire(
			'Atenção!',
			'Selecione uma <strong>linha</strong> na tabela para excluir',
			'error'
		)
	}
}