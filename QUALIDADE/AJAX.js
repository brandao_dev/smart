
//        d8888      888 d8b          d8b                                    
//       d88888      888 Y8P          Y8P                                    
//      d88P888      888                                                     
//     d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
//    d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
//   d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
//  d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
// d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     

	$(function() 
		{			
    		$('#botao').on('click',function(e) 
				{
					e.preventDefault();
					var data = $('#formulario').serialize();
					var int = $('#cache').val();
					var smt = $('#smart').val();
					if(int == 2 && smt.length != 0)
					{    
						Swal.fire({
						  title: 'Aprova o produto de ID: ' + smt + " ?", 
						  showCancelButton: true,
						  confirmButtonColor: '#F55',
						  cancelButtonColor: '#5F5',
						  focusCancel: true,
						  confirmButtonText: 'Reprovar',
						  cancelButtonText: 'Aprovar'
						}).then((result) => {
						  if (result.value) {
								$.ajax
								(
									{
										type: 'POST',
										dataType: 'json',
										url: 'PROCESSA.php',
										async: true,
										data: {smart:smt, ex:'ex', int:2},
										success: function(data)
										{
											$('#cache').val(data.int);
											$('#smart').val('');
											$('#player').html(data.fala);
											$('#caso').html(data.caso);
											$('#total').html('<i class="far fa-calendar-alt mr-2"></i>'+data.total);
											$('#tab tbody').html('');
											for(var i = 0; i<data.sql.length; i++)
											{
												$("#tab tbody").append(
												"<tr><td>"
												+data.sql[i].smart+
												"</td><td>"
												+data.sql[i].modelo+
												"</td><td><button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data.sql[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td></tr>"
												);
											}			
										},
										error: function(){
											$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro ao excluir");
										}
									}
								);
						  }
						  else
						  {
							$.ajax
							(
								{
									type: 'POST',
									dataType: 'json',
									url: 'PROCESSA.php',
									async: true,
									data: data,
									success: function(data)
									{
										$('#cache').val(data.int);
										$('#smart').val('');
										$('#player').html(data.fala);
										$('#caso').html(data.caso);
										$('#total').html('<i class="far fa-calendar-alt mr-2"></i>'+data.total);
										$('#tab tbody').html('');
										for(var i = 0; i<data.sql.length; i++)
										{
											$("#tab tbody").append(
											"<tr><td>"
											+data.sql[i].smart+
											"</td><td>"
											+data.sql[i].modelo+
											"</td><td><button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data.sql[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td></tr>"
											);
										}			
									},
									error: function(){
										$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro2");
									}
								}
							)

						  }
						}) 
					}
					else
					{
						$.ajax
						(
							{
								type: 'POST',
								dataType: 'json',
								url: 'PROCESSA.php',
								async: true,
								data: data,
								success: function(data)
								{
									$('#cache').val(data.int);
									$('#smart').val('');
									$('#player').html(data.fala);
									$('#caso').html(data.caso);
									$('#total').html('<i class="far fa-calendar-alt mr-2"></i>'+data.total);

									//CONDIÇÂO COM USUARIO
									let grupo = sessionStorage.getItem('grupo');
									if(data.sql > 1 && grupo != 'ADM')
									{
										swal.fire(
											'Atenção!',
											'Produto voltou '+data.sql+' vezes. Informe à Supervisão',
											'alert'
										)
									}
									else
									{
										$('#tab tbody').html('');
										for(var i = 0; i<data.sql.length; i++)
										{
											$("#tab tbody").append(
											"<tr><td>"
											+data.sql[i].smart+
											"</td><td>"
											+data.sql[i].modelo+
											"</td><td><button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data.sql[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td></tr>"
											);
										}
									}			
								},
								error: function(){
									$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro. NOT int == 2 && smt.length != 0");
								}
							}
						)

					}
    			}
			);
		}
	)

// 8888888888                   888          d8b         
// 888                          888          Y8P         
// 888                          888                      
// 8888888    888  888  .d8888b 888 888  888 888 888d888 
// 888        `Y8bd8P' d88P"    888 888  888 888 888P"   
// 888          X88K   888      888 888  888 888 888     
// 888        .d8""8b. Y88b.    888 Y88b 888 888 888     
// 8888888888 888  888  "Y8888P 888  "Y88888 888 888  

function excluir(id){   
	var int = $('#cache').val();
	var f1 = (int == 1) ? 'Confirma o cancelamento da Qualidade do: ' : 'Confirma a reprovação do produto: ';     
	var f2 = (int == 1) ? 'Qualidade cancelada' : 'Produto reprovado';     
	Swal.fire({
	  title: f1 + id + " ?", 
	  showCancelButton: true,
	  confirmButtonColor: '#F55',
	  confirmButtonText: 'Confirma',
	  cancelButtonText: 'Cancela'
	}).then((result) => {
	  if (result.value) {            
		this.del(id);      
		Swal.fire(
		  'Eliminado!',
		  f2,
		  'success'
		)
	  }
	})                
} 
function del(id){
	var cache = $('#cache').val();
	$.ajax
	(
		{
			type: 'POST',
			dataType: 'json',
			url: 'PROCESSA.php',
			async: true,
			data: {smart:id, ex:'ex', int:cache},
			success: function(data)
			{
				$('#cache').val(data.int);
				$('#smart').val('');
				$('#player').html(data.fala);
				$('#caso').html(data.caso);
				$('#total').html('<i class="far fa-calendar-alt mr-2"></i>'+data.total);
				$('#tab tbody').html('');
				for(var i = 0; i<data.sql.length; i++)
				{
					$("#tab tbody").append(
					"<tr><td>"
					+data.sql[i].smart+
					"</td><td>"
					+data.sql[i].modelo+
					"</td><td><button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data.sql[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td></tr>"
					);
				}			
			},
			error: function(data){
				$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro ao excluir");
			}
		}
	);
} 

//        d8888 888                      888 d8b                           
//       d88888 888                      888 Y8P                           
//      d88P888 888                      888                               
//     d88P 888 888888 888  888  8888b.  888 888 88888888  8888b.  888d888 
//    d88P  888 888    888  888     "88b 888 888    d88P      "88b 888P"   
//   d88P   888 888    888  888 .d888888 888 888   d88P   .d888888 888     
//  d8888888888 Y88b.  Y88b 888 888  888 888 888  d88P    888  888 888     
// d88P     888  "Y888  "Y88888 "Y888888 888 888 88888888 "Y888888 888     

function lista(lista){
	$.ajax
	(
		{
			type: 'POST',
			dataType: 'json',
			url: 'ATUALIZA.php',
			async: true,
			data: {lista:lista},
			success: function(data)
			{
				if(data.int == 1)
				{
					$('#botao').html('<h5><i class="fas fa-hourglass-start mr-2 mr-2"></i>Iniciar</h5>')
					$('#tabela').html('<i class="fas fa-charging-station fa-lg mr-2"></i>')
				}
				else
				{
					$('#botao').html('<h5><i class="fas fa-hourglass-end mr-2 mr-2"></i>Finalizar</h5>')
					$('#tabela').html('<i class="fas fa-check-double mr-2"></i>')
				}
				$('#cache').val(data.int);
				$('#smart').val('');
				$('#player').html(data.fala);
				$('#caso').html(data.caso);
				$('#total').html('<i class="far fa-calendar-alt mr-2"></i>'+data.total);
				$('#tab tbody').html('');
				for(var i = 0; i<data.sql.length; i++)
				{
					$("#tab tbody").append(
					"<tr><td>"
					+data.sql[i].smart+
					"</td><td>"
					+data.sql[i].modelo+
					"</td><td><button class='btn btn-danger btn-block shadow' onclick='play(), vibrate(100), excluir(\""+data.sql[i].smart+"\")' ><i class='fas fa-trash-alt'></i></button></td></tr>"
					);
				}			
			},
			error: function(data){
				$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro ao carregar");
			}
		}
	);
}


// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

    function vibrate(ms){
		navigator.vibrate(ms);
	  }

// 888888b.                                           888          
// 888  "88b                                          888          
// 888  .88P                                          888          
// 8888888K.   8888b.  888d888  .d8888b  .d88b.   .d88888  .d88b.  
// 888  "Y88b     "88b 888P"   d88P"    d88""88b d88" 888 d8P  Y8b 
// 888    888 .d888888 888     888      888  888 888  888 88888888 
// 888   d88P 888  888 888     Y88b.    Y88..88P Y88b 888 Y8b.     
// 8888888P"  "Y888888 888      "Y8888P  "Y88P"   "Y88888  "Y8888  

    if(window.location.hash.substr(1,2) == "zx"){
        var bc = window.location.hash.substr(3);
        localStorage["barcode"] = decodeURI(window.location.hash.substr(3))
        window.close();
        self.close();
        window.location.href = "about:blank";//In case self.close isn't allowed
    }
    var changingHash = false;
    function onbarcode(event){
        switch(event.type){
            case "hashchange":{
                if(changingHash == true){
                    return;
                }
                var hash = window.location.hash;
                if(hash.substr(0,3) == "#zx"){
                    hash = window.location.hash.substr(3);
                    changingHash = true;
                    window.location.hash = event.oldURL.split("\#")[1] || ""
                    changingHash = false;
                    processBarcode(hash);
                }

                break;
            }
            case "storage":{
                window.focus();
                if(event.key == "barcode"){
                    window.removeEventListener("storage", onbarcode, false);
                    processBarcode(event.newValue);
                }
                break;
            }
            default:{
                console.log(event)
                break;
            }
        }
    }
    window.addEventListener("hashchange", onbarcode, false);

    function getScan(){
        var href = window.location.href;
        var ptr = href.lastIndexOf("#");
        if(ptr>0){
            href = href.substr(0,ptr);
        }
        window.addEventListener("storage", onbarcode, false);
        setTimeout('window.removeEventListener("storage", onbarcode, false)', 15000);
        localStorage.removeItem("barcode");
        //window.open  (href + "#zx" + new Date().toString());

        if(navigator.userAgent.match(/Firefox/i)){
            //Used for Firefox. If Chrome uses this, it raises the "hashchanged" event only.
            window.location.href =  ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
        }else{
            //Used for Chrome. If Firefox uses this, it leaves the scan window open.
            window.open   ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
        }
    }

    function processBarcode(bc){
			document.getElementById("smart").value = bc;
			$('html, body').animate({scrollTop:0},500);
	}