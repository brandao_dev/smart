<?php
include('../CORE/CONN.php');

$smart = strtoupper($_POST['smart']);
$fala = null;
$caso = null;

//VALIDA SMART SE EXISTE
if(conex()->query("SELECT id FROM codigo WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC))
{
    
    $sac = null;
    // PEGA ULTIMO SAC SE EXISTIR
    if(conex()->query("SELECT id FROM sac WHERE SMART = '$smart'")->fetch(PDO::FETCH_ASSOC))
    {
        $sac = conex()->query("SELECT id,SAC,COLETA FROM sac WHERE SMART = '$smart' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC)['SAC'];
    
        // ATUALIZA ETAPA NO CODIGO
        conex()->prepare("UPDATE codigo SET etapa = '02.AGUARD.REPARO' WHERE smart = '$smart'")->execute();
        
        $codigo = null;
        // PEGA INFORMAÇOES NO CODIGO
        $codigo = conex()->query("SELECT linha,modelo FROM codigo WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC);
        
        $ean = null;
        // PEGA EAN DO MODELO
        $ean = conex()->query("SELECT EAN,websac FROM modelo WHERE modelo = '{$codigo['modelo']}'")->fetch(PDO::FETCH_ASSOC);
    
        //ATUALIZA STATUS DE SAC
        conex()->prepare("UPDATE SAC SET STATUS = '18.LAB.RECEBIDO' WHERE SAC = '$sac'")->execute();
        
        $dia = date("Y-m-d H:i:s");
        // GERA NOVA LINHA DE PEDIDO
        conex()->prepare("INSERT INTO pedido (linha,modelo,ean,minuta,smart,pedido,reserva,user,destino,websac) VALUES ('{$codigo['linha']}','{$codigo['modelo']}','{$ean['EAN']}','1','$smart','$dia','$sac','{$_SESSION['MM_Username']}','GALATEA','{$ean['websac']}')")->execute();
    
        if(empty($sac['COLETA']))
        {
            $fala = "Atenção! Falha Operacional Logística: Recebimento não agendado";
            $caso = "<i class='fas fa-ban mr-2'></i>Atenção! Falha Operacional Logística: Recebimento não agendado";
        }
        else
        {
            $fala = "Recebimento registrado";
            $caso = "<i class='fas fa-anchor mr-2'></i>Recebimento registrado";
        }
    }
    else
    {  
        $fala = "Erro! Este produto não está em SAC";
        $caso = "<i class='fas fa-alien mr-2'></i>Erro! Este produto não está em SAC";
    }
}
else
{    
    $fala = "Smart ID não localizado";
    $caso = "<i class='fas fa-alien mr-2'></i>Smart ID não localizado";
}

include('tabela.php');

include('json.php');

?>