<?php include '../CORE/Header2.php';
?>

<script src="AJAX.JS"></script>
<script src="BADGE.js"></script>
<style> .swal {width:600px !important; } </style>

<!-- ****************************************************************************************************
######## ##    ## ######## ########     ###    ########     ###    
##       ###   ##    ##    ##     ##   ## ##   ##     ##   ## ##   
##       ####  ##    ##    ##     ##  ##   ##  ##     ##  ##   ##  
######   ## ## ##    ##    ########  ##     ## ##     ## ##     ## 
##       ##  ####    ##    ##   ##   ######### ##     ## ######### 
##       ##   ###    ##    ##    ##  ##     ## ##     ## ##     ## 
######## ##    ##    ##    ##     ## ##     ## ########  ##     ## 
 ***///////////////////////////////////////////////////////////////////////////////////////-->
 
<form id="formulario" action="" method="POST">
	<div class="row">
		<div class="col-12">
			<button type="button" class="btn btn-secondary btn-block" data-toggle="modal" data-target="#quanti">
				<h3 id="qtd"></h3>
			</button> 

			<!-- Modal -->
			<div class="modal fade" id="quanti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-eye mr-2"></i>Resumo da Produção</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h3 id="refrigeracao">Comece lendo um produto...</h3>
					<h3 id="lavanderia"></h3>
					<h3 id="coccao"></h3>
					<h3 id="outros"></h3>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-pink" data-dismiss="modal">OK</button>
				</div>
				</div>
			</div>
			</div> 

		</div>
		<div class="col-12 col-sm-6 col-lg-4 mt-3">
			<input type="text" class="form-control font-weight-bold form-control-lg" name="smart" id="smart" autocomplete="off" required autofocus list="lis" placeholder="Insira o ID">
			<datalist id="lis">
				<?php
					foreach((conex()->query("SELECT smart,id FROM codigo UNION ALL SELECT smart,id FROM lg ORDER BY id DESC")) as $result)
					{
						echo "<option value='".$result['smart']."'/>";
					}
				?>
			</datalist>
		</div>
		<div class="col-12 col-sm-6 col-lg-4 mt-3">
			<button class="btn btn-primary shadow btn-block" type="submit" id="botao" onclick="vibrate(200),play()"><h5 class="mt-1"><i class="fas fa-search mr-2"></i>Localiza</h5></button>
		</div>
</form>
		<div class="col-12 col-lg-4 mt-3">
			<button class="btn btn-success shadow btn-block" type="button" value="Scan" onclick="play(),getScan()"><h5 class="mt-1"><i class="fas fa-camera mr-2"></i>Camera</h5></button>
		</div>
	</div>

<!-- PLAYER DE AUDIO -->
<div style="display: none" id="player"></div>

<!-- BARCODE 1 -->
<INPUT id=barcode type=text style="display: none" >

<!-- SEGUNDA LINHA DE FRASE DE APOIO *** /////////////////////////////////////////////////////////////////////////////////////// -->
<div class="alert mt-3 alert-info" id="caso"><i class="fas fa-unlock-alt fa-lg"></i> Pronto para nova entrada!</div>

<!-- ***********************************************************************************
#### ##    ## ########  #######  ########  ##     ##    ###     ######   #######  ########  ######  
 ##  ###   ## ##       ##     ## ##     ## ###   ###   ## ##   ##    ## ##     ## ##       ##    ## 
 ##  ####  ## ##       ##     ## ##     ## #### ####  ##   ##  ##       ##     ## ##       ##       
 ##  ## ## ## ######   ##     ## ########  ## ### ## ##     ## ##       ##     ## ######    ######  
 ##  ##  #### ##       ##     ## ##   ##   ##     ## ######### ##       ##     ## ##             ## 
 ##  ##   ### ##       ##     ## ##    ##  ##     ## ##     ## ##    ## ##     ## ##       ##    ## 
#### ##    ## ##        #######  ##     ## ##     ## ##     ##  ######   #######  ########  ######  
**************************************************************************************** -->
<div class="row align-content-between">
	<div class="col-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2" id="cardEtapa">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-shoe-prints"></i> Etapa</h5>
				<h6 class="card-subtitle mb-2 text-muted" id="etapa"></h6>
			</div>
		</div>
	</div>

	<div class="col-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2" id="cardLinha">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-tape"></i> Linha</h5>
				<h6 class="card-subtitle mb-2 text-muted" id="linha"></h6>
			</div>
		</div>
	</div>

	<div class="col-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2" id="cardModelo">
			<div class="card-body">
				<h5 class="card-title"><i class="far fa-ruler-triangle"></i> Modelo</h5>
				<h6 class="card-subtitle mb-2 text-muted" id="modelo"></h6>
			</div>
		</div>
	</div>

	<div class="col-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-paw"></i> Marca</h5>
				<h6 class="card-subtitle mb-2 text-muted" id="marca"></h6>
			</div>
		</div>
	</div>

<div class="col-6 col-md-4 col-xl-2">
	<div class="card shadow p-1 mb-2">
		<div class="card-body">
			<h5 class="card-title"><i class="fas fa-user-hard-hat"></i> Enge</h5>
			<h6 class="card-subtitle mb-2 text-muted" id="engenharia"></h6>
		</div>
	</div>
</div>

<div class="col-6 col-md-4 col-xl-2">
	<div class="card shadow p-1 mb-2" id="cardRma">
		<div class="card-body">
			<h5 class="card-title"><i class="fas fa-fire-extinguisher"></i> RMA</h5>
			<h6 class="card-subtitle mb-2 text-muted" id="rma"></h6>
		</div>
	</div>
</div>

	<div class="col-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2" id="cardCor"  data-toggle="modal" data-target="#modalCor" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-palette"></i> Cor</h5>
				<h6 class="card-subtitle mb-2 text-muted" id="cor"></h6>
			</div>
		</div>
	</div>

<!-- Modal Cor REMOVIDO. TROCA DE COR É NA PINTURA-->
<!-- <div class="modal fade" id="modalCor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header"><i class="fas fa-palette fa-2x"></i>
			<h4 class="modal-title ml-2" id="exampleModalLabel">Trocar Cor</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body bg-light">		  
			<select name="personalizada" id="personalizada" class="form-control form-control-lg">
				<option value="">Selecione...</option>
				<option value="AZUL PERSO.">AZUL PERSO.</option>
				<option value="BRANCO PERSO.">BRANCO PERSO.</option>
				<option value="CINZA PERSO.">CINZA PERSO.</option>
				<option value="GRAFITE PERSO.">GRAFITE PERSO.</option>
				<option value="INOX PERSO.">INOX PERSO.</option>
				<option value="PRATA PERSO.">PRATA PERSO.</option>
				<option value="PRETO PERSO.">PRETO PERSO.</option>
				<option value="TITANIO PERSO.">TITANIO PERSO.</option>
				<option value="VERDE PERSO.">VERDE PERSO.</option>
				<option value="VERMELHO PERSO.">VERMELHO PERSO.</option>
			</select>
		</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="vibrate(100), play(), cor()">OK</button>
			</div>
		</div>
	</div>
</div> -->

	<div class="col-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2" id="cardTensao"  data-toggle="modal" data-target="#modalTensao" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-bolt"></i> Tensão</h5>
				<h6 class="card-subtitle mb-2 text-muted" id="tensao"></h6>
			</div>
		</div>
	</div>

	<!-- Modal Tensão-->
	<div class="modal fade" id="modalTensao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-bolt fa-2x"></i>
				<h4 class="modal-title ml-2" id="exampleModalLabel">Trocar tensão</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">		  
				<div class="btn-group btn-group-toggle shadow" data-toggle="buttons">
					<label class="btn btn-outline-secondary btn-lg" id="127">
						<input type="radio" name="tensao" value="127v" autocomplete="off" id="127v" onclick="v127(),vibrate(100),play()"> 127 V
					</label>
					<label class="btn btn-outline-secondary btn-lg" id="220">
						<input type="radio" name="tensao" value="220v" autocomplete="off" id="220v" onclick="v220(),vibrate(100),play()"> 220 V
					</label>
				</div>
				<h5 id="avisoTensao" class="mt-3"><i class="fas fa-lock-alt fa-2x mr-2"></i>Ops. No momento não existe tensão alternativa para este modelo.</h5>
			</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!------------------------------------------------------------------------
######## ######## ##     ## ########  ######## ########     ###    ######## ##     ## ########     ###     ######  
   ##    ##       ###   ### ##     ## ##       ##     ##   ## ##      ##    ##     ## ##     ##   ## ##   ##    ## 
   ##    ##       #### #### ##     ## ##       ##     ##  ##   ##     ##    ##     ## ##     ##  ##   ##  ##       
   ##    ######   ## ### ## ########  ######   ########  ##     ##    ##    ##     ## ########  ##     ##  ######  
   ##    ##       ##     ## ##        ##       ##   ##   #########    ##    ##     ## ##   ##   #########       ## 
   ##    ##       ##     ## ##        ##       ##    ##  ##     ##    ##    ##     ## ##    ##  ##     ## ##    ## 
   ##    ######## ##     ## ##        ######## ##     ## ##     ##    ##     #######  ##     ## ##     ##  ######  
------------------------------------------------------------------------->
<form action="" method="post" id="formFinal">

<!-- //INPUT DA TENSAO -->
<input type="text" id="tensaoInput" name="tensao" style="display: none">
<!-- //INPUT DA COR -->
<input type="text" id="cora" name="cora" style="display: none">
<!-- <input type="text" id="tensaoInput" name="tensao"> -->

<div class="row mt-3">
	<div class="col-6 mt-3">
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold" id="botaoRefri0"><i class="far fa-temperature-low"></i></span>
			</div>
			<select name="T0" id="temp0" class="custom-select font-weight-bold form-control-lg" required>
				<option value="">REFRIGERADOR</option>
				<option value="4">+4</option>
				<option value="3">+3</option>
				<option value="2">+2</option>
				<option value="1">+1</option>
				<option value="0">0</option>
				<option value="-1">-1</option>
				<option value="-2">-2</option>
				<option value="-3">-3</option>
				<option value="-4">-4</option>
				<option value="-5">-5</option>
				<option value="-6">-6</option>
				<option value="-7">-7</option>
				<option value="-8">-8</option>
			</select>
		</div>
	</div>
	<div class="col-6 mt-3">
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text font-weight-bold" id="botaoFreez0"><i class='fal fa-temperature-frigid'></i></span>
			</div>
			<select name="T1" id="temp1" class="custom-select font-weight-bold" required>
				<option value="">FREEZER</option>
				<option value="-8">-8</option>
				<option value="-9">-9</option>
				<option value="-10">-10</option>
				<option value="-11">-11</option>
				<option value="-12">-12</option>
				<option value="-13">-13</option>
				<option value="-14">-14</option>
				<option value="-15">-15</option>
				<option value="-16">-16</option>
				<option value="-17">-17</option>
				<option value="-18">-18</option>
				<option value="-19">-19</option>
				<option value="-20">-20</option>
				<option value="-21">-21</option>
				<option value="-22">-22</option>
				<option value="-23">-23</option>
				<option value="-24">-24</option>
				<option value="-25">-25</option>
				<option value="-26">-26</option>
			</select>
		</div>
	</div>
</div>

<hr>

<!-- -------------------------------------------------------------------------
########  ######## ######## #### ##    ## ####  ######     ###     #######  
##     ## ##       ##        ##  ###   ##  ##  ##    ##   ## ##   ##     ## 
##     ## ##       ##        ##  ####  ##  ##  ##        ##   ##  ##     ## 
##     ## ######   ######    ##  ## ## ##  ##  ##       ##     ## ##     ## 
##     ## ##       ##        ##  ##  ####  ##  ##       ######### ##     ## 
##     ## ##       ##        ##  ##   ###  ##  ##    ## ##     ## ##     ## 
########  ######## ##       #### ##    ## ####  ######  ##     ##  #######  
-------------------------------------------------------------------------------->
<div class="row btn-group d-flex" data-toggle="buttons">
		<div class="col-12 col-sm-6 col-lg-4">
			<label class="btn btn-outline-success shadow active w-100">
				<input type="radio" style="visibility:hidden;" name="etapa" id="etapa1" value="03.REPARADO" autocomplete="off" checked onclick="test(), vibrate(300)"><h5 class="mb-3"><i class="far fa-tachometer-alt-fastest mr-2"></i>03.Reparado</h5>
				<audio id="Atest" src="../som/reparado.mp3"></audio>	
			</label>
		</div>
		<div class="col-12 col-sm-6 col-lg-4">
				<label class="btn btn-outline-success shadow w-100">
				<input type="radio" style="visibility:hidden;" name="etapa" id="etapa2" value="10.PARTWAIT" autocomplete="off" onclick="partw(), vibrate(300)"><h5 class="mb-3"><i class="far fa-pause-circle mr-2"></i>10.PartWait</h5>
				<audio id="Apart" src="../som/part wait.mp3"></audio>	
			</label>	
		</div>
		<div class="col-12  col-lg-4">
				<label class="btn btn-outline-success shadow w-100">
				<input type="radio" style="visibility:hidden;" name="etapa" id="etapa3" value="09.SCRAP" autocomplete="off" onclick="scrap(), vibrate(300)"><h5 class="mb-3"><i class="fas fa-trash-restore-alt mr-2"></i>09.Scrap</h5>
				<audio id="Ascrap" src="../som/scrap.mp3"></audio>	
			</label>
		</div>
</div>

<!----------------------------------------------------------------------------------
 #######  ########   ######   #######  ########  ######  
##     ## ##     ## ##    ## ##     ## ##       ##    ## 
##     ## ##     ## ##       ##     ## ##       ##       
##     ## ########  ##       ##     ## ######    ######  
##     ## ##        ##       ##     ## ##             ## 
##     ## ##        ##    ## ##     ## ##       ##    ## 
 #######  ##         ######   #######  ########  ######  ** ---------------
----------------------------------------------------------------------------------->
<div class="row mt-2 justify-content-center">
	<div class="col-6 col-lg-4 mt-3">
		<label class="btn-block"><i class="fas fa-spray-can mr-2"></i>REFRIGERAÇÃO</label>
		<div class="btn-group btn-group-toggle shadow d-flex" data-toggle="buttons">
			<label class="btn btn-outline-primary btn-lg w-100" id="R600">
				<input type="radio" name="gas" value="R600a" autocomplete="off" id="R600a" onclick="vibrate(100),play()"> R600a
			</label>
			<label class="btn btn-outline-primary btn-lg w-100" id="R134">
				<input type="radio" name="gas" value="R134a" autocomplete="off" id="R134a" onclick="vibrate(100),play()"> R134a
			</label>
		</div>
	</div>
	<div class="col-6 col-lg-4 mt-3">
		<label class="btn-block"><i class="fas fa-container-storage mr-2"></i>SISTEMA ALTA</label>			
		<div class="btn-group btn-group-toggle shadow d-flex" data-toggle="buttons">
			<label class="btn btn-outline-primary btn-lg w-100" id="Alta0">
				<input type="radio" name="alta" id="alta0" value="0" autocomplete="off" onclick="vibrate(100),play()"> Não
			</label>
			<label class="btn btn-outline-primary btn-lg w-100" id="Alta1">
				<input type="radio" name="alta" id="alta1" value="1" autocomplete="off" onclick="vibrate(100),play()"> Sim
			</label>
		</div>
	</div>
	<div class="col-12 col-lg-4 mt-3 ">
		<!-- 
			<button class="btn btn-primary btn-lg" value="Scar" onclick="getScan()"><i class="fas fa-camera fa-lg ml-4 mr-4"></i></button>
			<input type="text" class="form-control-lg" placeholder="SERIAL + SKU" aria-label="Example text with button addon" aria-describedby="button-addon1"name="compBar" id="smarf" value=""> -->
			<label><i class="fas fa-curling mr-2"></i>COMPRESSOR</label>
			<div class="input-group mb-3 mt-1 input-group-lg">
				<div class="input-group-prepend">
					<button class="btn btn-primary" value="Scar" onclick="getScan()"><i class="fas fa-camera fa-lg ml-4 mr-4"></i></button>
				</div>
				<input type="text" class="form-control" placeholder="SERIAL + SKU" aria-label="SERIAL + SKU" aria-describedby="SERIAL + SKU" name="compBar" id="smarf" value="">
			</div>
	</div>
</div>

<!-- **********************************************************
 ######     ###     ######  
##    ##   ## ##   ##    ## 
##        ##   ##  ##       
 ######  ##     ## ##       
      ## ######### ##       
##    ## ##     ## ##    ## 
 ######  ##     ##  ######  
*************************************************************-->
<div id="saker">

	<div class="row mt-4">
		<div class="col">
			<h5 class="text-left"><i class="fas fa-fire-extinguisher"></i> Complemento do RMA</h5>
		</div>	
	</div>

	<!---------------------------------------------------------
	---------------- QUEIXA DO CLIENTE ------------------------
	---------------------------------------------------------->
	<div class="row align-content-between  mt-3">
		<div class="col-12">
			<div class="card shadow p-1 mb-2">
				<div class="card-body">
					<h5 class="text-left"><i class="fas fa-angry"></i> Queixa do Cliente</h5>
						<div id="queixa" class="text-left"></div>
				</div>
			</div>
		</div>
	</div>

	<!---------------------------------------------------------
	------------ CONSTATADO DO TECNICO ------------------------
	---------------------------------------------------------->
	<div class="row align-content-between  mt-3">
		<div class="col-12">
			<div class="card shadow p-1 mb-2">
				<div class="card-body">
					<h5 class="text-left"><i class="fas fa-glasses"></i> Constatação Técnica</h5>
						<div id="constatacao" class="text-left"></div>
					<!-- <input type="text" class="form-control font-weight-bold" placeholder="Nova constatação" required name="constatado" id="constatado"> -->
				</div>
			</div>
		</div>
	</div>

</div>

<!---------------------------------------------------------
------------ NOVA PEÇA ------------------------------------
---------------------------------------------------------->
<div class="row align-content-between mt-3">
	<div class="col-12">
		<div class="card shadow p-1 mb-2">
			<h5 class="text-left ml-3">
				<i class="fas fa-cogs mr-2"></i>Peça
			</h5>
			<div class="card-body">

				<div class="input-group mb-3">
					<input type="number" id="peca" name="peca" class="form-control form-control-lg font-weight-bold" placeholder="Código" aria-label="Código" aria-describedby="codigo">
					<div class="input-group-append font-weight-bold">
						<button class="btn btn-warning font-weight-bold" type="button" id="codigo" data-toggle="modal" data-target="#consulta"><i class="far fa-file-search mr-2"></i>Consulta</button>
					</div>
				</div>
					<!-- Modal -->
					<div class="modal fade" id="consulta" tabindex="-1" role="dialog" aria-labelledby="con" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="con"><i class="fas fa-bomb mr-2"></i>Vista Explodida</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body" id="pdf">
							<!-- GERADO POR AJAX -->
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
							</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>	
</div>

<hr>

<!-- SERVIÇO REALIZADO ANTERIORMENTE -->
<input type="text" id="falso" name='falso' value="" style="display: none">
<div class="row align-content-between mt-4">
	<div class="col text-left">
		<h5 class="mt-2" id="historico"><i class="fas fa-user-cog mr-2"></i>Ultimo reparo em:</h5>
	</div>
</div>
<!----------------------------------------------------------------------------------
--------- SETIMA LINHA DESCRIÇÃO DO SERVIÇO   ** -----------------------------------
----------------------------------------------------------------------------------->

<div class='row align-content-between mt-4' id="mostra"></div>

<hr>

<?php include('SERVICO.php')?>

<div class='row justify-content-end mt-5'>
	<div class='col-12 col-sm-6 col-md-4 col-xl-3'>
		<button class='btn btn-danger shadow btn-block' type='submit' id="finaliza" onclick='vibrate(200),play()'><h3><i class='fas fa-check-double mt-2 mr-2'></i>Confirma</h3></button>
	</div>
</div>

</form>
<br>

<?PHP include('../CORE/Footer2.php'); ?>