<?php			

include('../CORE/PDO.php');

// 888               d8b 888                              
// 888               Y8P 888                              
// 888                   888                              
// 888       .d88b.  888 888888 888  888 888d888  8888b.  
// 888      d8P  Y8b 888 888    888  888 888P"       "88b 
// 888      88888888 888 888    888  888 888     .d888888 
// 888      Y8b.     888 Y88b.  Y88b 888 888     888  888 
// 88888888  "Y8888  888  "Y888  "Y88888 888     "Y888888 

$id = strtoupper($_POST['id']);
$banco = banco($id);

#QUERY BANCO
if(!empty($banco))
{
	$sql = conex()->query("SELECT smart,fabricante,cor,linha,enge,modelo,voltagem,rma,grade,nlote,CARGA,modo,grupo FROM $banco WHERE smart = '$id'")->fetch(PDO::FETCH_ASSOC);
	
	#QUERY MODELO
	if($model = conex()->query("SELECT * FROM modelo WHERE modelo = '{$sql['modelo']}'")->fetch(PDO::FETCH_ASSOC))
	{
		//GRAVA GRUPO NO BANCO CASO ESTEJA VAZIO
		if(empty($sql['grupo']))
		{
			//REALIZA RELEITURA
			conex()->prepare("UPDATE $banco SET grupo = '{$model['procel']}' WHERE smart = '$id'")->execute();
			$sql = conex()->query("SELECT smart,fabricante,cor,linha,enge,modelo,voltagem,rma,grade,nlote,CARGA,modo,grupo FROM $banco WHERE smart = '$id'")->fetch(PDO::FETCH_ASSOC);
		}
	
		#SE MODELO FOR WHIRPOOL PEGA APENAS O 6 PRIMEIROS CHARS
		if($sql['modo'] != 'WP')
		{
			//SM OU LG
			$busca = $sql['modelo'];
		}
		else
		{
			//WHIRPOOL GENERICO
			$busca = substr($sql['modelo'],0,6);
		}
		
		$caso = '<i class="fas fa-print mr-2"></i>Etiqueta pronta para impressão';
		$fala = 'Etiqueta pronta para impressão';
	
		#BUSCA IMAGEM DO BOOK
		$img = glob('../BOOK/'.$id.'/*.jpg');
	}
	else
	{
		$sql = '';
		$model = '';
		$caso = '<i class="fas fa-heart-broken mr-2"></i>Modelo ausente';
		$fala = 'Modelo ausente';
	}
}
else
{
	$sql = '';
	$model = '';
	$caso = '<i class="fas fa-search-plus mr-2"></i>Escolha um ID...';
	$fala = '';
}

if($sql['rma'] == '11.LABORATORIO')
{
	$caso = '<i class="fas fa-stethoscope mr-2"></i>Atenção! Produto em Atendimento SAC';
	$fala = 'Atenção! Produto em Atendimento SAC';
}
	

//FALA
// $falas = htmlspecialchars(ucwords(strtolower($fala)));
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

//JSON
$dados = ['img'=>$img, 'sql'=>$sql, 'model'=>$model, 'caso'=>$caso, 'fala'=>$player];
print json_encode($dados, JSON_UNESCAPED_UNICODE);

?>