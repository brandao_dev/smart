<?php include '../CORE/Header2.php';

#SE NÃO TIVER SETADO MOSTRA O PADRÃO 180D
$pag = isset($_GET['M']) ? $_GET['M'] : '180';
#RECEBE MODELO OU PRODUTO COM FILTRO DO ANO
$table = ($pag == 'M') ? 'GET_MODEL.php' : 'GETA.php?M='.$pag;

?>
<script src="https://cdn.brandev.site/js/ag-grid-enterprise.min.js"></script>
	<!--MENU /////////////////////////////////////////////////////////////////////////////////////////////////////-->
	<div class="row">
		<div class="col-12 col-sm-6 col-md-4 col-xl-2 mt-3">
			<a href="GRID.php" class="btn btn-block btn-outline-primary <?php echo ($pag != "M") ? 'active' : ''?> shadow"><i class="far fa-refrigerator mr-2"></i>Produtos</a>
		</div>		
		<div class="col-12 col-sm-6 col-md-4 col-xl-2 mt-3">
			<a href="GRID.php?M=M" class="btn btn-block btn-outline-success <?php echo ($pag == "M") ? 'active' : ''?> shadow"><i class="fas fa-book mr-2"></i>Modelos</a>
		</div>
<a href=""></a>
<!-- SUPER GABIARRA BRABA -->
		<div class="col-12 col-sm-6 col-md-4 col-xl-2 mt-3">
			<button class="btn btn-block btn-outline-danger shadow" onclick="busca(<?php echo ($pag != 'M') ? 0 : 1?>)"><i class="fas fa-dna mr-2"></i><?php echo ($pag == 'M') ? 'Editar' : 'Info'?></button>
		</div>
<?PHP if($pag != 'M') { ?>
		<div class="col-12 col-sm-6 col-md-4 col-xl-2 mt-3">
			<button class="btn btn-block btn-outline-warning shadow" onclick="print()"><i class="fas fa-barcode-read mr-2"></i>Impressão</button>
		</div>
		<div class="col-12 col-md-8 col-xl-4 mt-3">
			<div class="btn-group shadow btn-block">
				<a href="GRID.php?M=21" class="btn btn-outline-brown <?php if($pag == '21') echo 'active'?>">2021</a>
				<a href="GRID.php?M=20" class="btn btn-outline-brown <?php if($pag == '20') echo 'active'?>">2020</a>
				<a href="GRID.php?M=19" class="btn btn-outline-brown <?php if($pag == '19') echo 'active'?>">2019</a>
				<a href="GRID.php?M=18" class="btn btn-outline-brown <?php if($pag == '18') echo 'active'?>">2018</a>
				<a href="GRID.php?M=17" class="btn btn-outline-brown <?php if($pag == '17') echo 'active'?>">2017</a>
			</div>
		</div>
<?php } ?>
		<div class="col-6 text-left font-weight-bold mt-3">
			<h1><div id="selectedRows"></div></h1>
		</div>
        <div class="col-6 font-weight-bold mt-3 text-right">
            <h1 class="text-secondary"><?php if($pag != 'M'){echo ($pag == '180') ? '<i class="fas fa-screwdriver mr-2"></i>Reparados em até 180 Dias' : '<i class="fas fa-person-dolly mr-2"></i>Reparados em 20'.$pag;} ?></h1>
        </div>
	</div>
	
	<!-- <span id="selectedRows"></span> -->
<div id="myGrid" style="position:absolute; width: 98%;height:80%" class="ag-theme-alpine"></div>

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++  BANCOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<?PHP if($pag != 'M') { ?>
<script type="text/javascript">		
// COLUNAS //////////////////////////////////////////
var columnDefs = 
[
  {headerName: "AGENTE", field: "agente", filter: "agSetColumnFilter", hide: true},
  {headerName: "ALTA", field: "ALTA", filter: "agSetColumnFilter", hide: true},
  {headerName: "ALTERAÇÃO", field: "agora", filter: "agDateColumnFilter", hide: true},
  {headerName: "ALT. HORA", field: "HORA", filter: "agTextColumnFilter", hide: true},
  {headerName: "COR", field: "cor", filter: "agTextColumnFilter", hide: true},
  {headerName: "DATA AGING", field: "INICIO_TRIAGEM", filter: "agDateColumnFilter", hide: true},
  {headerName: "DATA ENTRADA", field: "data", filter: "agDateColumnFilter", hide: true},
  {headerName: "DATA EXPEDIÇÃO", field: "dataexpedicao", filter: "agDateColumnFilter", hide: true},
  {headerName: "DATA HIGIENE", field: "higiene", filter: "agDateColumnFilter", hide: true},
  {headerName: "DATA PINTURA", field: "DATA_PINTURA", filter: "agDateColumnFilter", hide: true},
  {headerName: "DATA QUALIDADE", field: "qualidade", filter: "agDateColumnFilter", hide: true},
  {headerName: "DATA RMA", field: "datarma", filter: "agDateColumnFilter", hide: true},
  {headerName: "DATA TRIAGEM", field: "datatriagem", filter: "agDateColumnFilter"},
  {headerName: "ENGENHARIA", field: "enge", filter: "agTextColumnFilter", hide: true},
  {headerName: "ETAPA", field: "etapa", filter: "agSetColumnFilter"},
  {headerName: "FABRICANTE", field: "fabricante", filter: "agSetColumnFilter"},
  {headerName: "GRADE", field: "grade", filter: "agSetColumnFilter", hide: true},
  {headerName: "GRUPO", field: "grupo", filter: "agSetColumnFilter", hide: true},
  {headerName: "LINHA", field: "linha", filter: "agTextColumnFilter"},
  {headerName: "LIMPEZA", field: "limpeza", filter: "agSetColumnFilter", hide: true},
  {headerName: "LOCAL SAIDA", field: "localsaida", filter: "agTextColumnFilter", hide: true},
  {headerName: "LOTE", field: "nlote", filter: "agNumberColumnFilter", hide: true},
  {headerName: "MINUTA ENTRADA", field: "minuta", filter: "agNumberColumnFilter", hide: true},
  {headerName: "MINUTA SAIDA", field: "m2", filter: "agNumberColumnFilter", hide: true},
  {headerName: "MODELO", field: "modelo", filter: "agTextColumnFilter"},
  {headerName: "MODO", field: "modo", filter: "agSetColumnFilter", hide: true},
  {headerName: "PAGO", field: "pago", filter: "agDateColumnFilter", hide: true},
  {headerName: "PALETE", field: "palet", filter: "agTextColumnFilter", hide: true},
  {headerName: "PINTURA", field: "PINTURA", filter: "agTextColumnFilter", hide: true},
  {headerName: "PINTURA FASE", field: "pnt_local", filter: "agSetColumnFilter", hide: true},
  {headerName: "REFRI", field: "CARGA", filter: "agSetColumnFilter", hide: true},
  {headerName: "REPARO", field: "obs", filter: "agTextColumnFilter", hide: true},
  {headerName: "RMA", field: "rma", filter: "agTextColumnFilter"},
  {headerName: "SERIAL ORIGINAL", field: "serie1", filter: "agTextColumnFilter", hide: true},
  {headerName: "SERIAL SMART", field: "smart", filter: "agTextColumnFilter"},
  {headerName: "TENSAO", field: "voltagem", filter: "agTextColumnFilter"},
  {headerName: "USER HIGIENE", field: "higienico", filter: "agSetColumnFilter", hide: true},
  {headerName: "USER QUALIDADE", field: "user_qualidade", filter: "agSetColumnFilter", hide: true},
  {headerName: "USER TRIAGEM", field: "usertriagem", filter: "agSetColumnFilter", hide: true}
];

// SELECIONAR ITENS //////////////////////////////
function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    var imp = '';
    selectedRows.forEach( function(selectedRow, index) {
        if (index!==0) {
            selectedRowsString += ', ';
            imp += ', ';
        }
        selectedRowsString += selectedRow.smart;
        imp += selectedRow.smart;
    });
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
    document.myform.busca.value = selectedRowsString;
    document.formulario.imp.value = imp;
}
</script>
	
	
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++  MODELO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->	
<?PHP } else { ?>
<script type="text/javascript" charset="utf-8">	
// COLUNAS //////////////////////////////////////
var columnDefs = 
[
  {headerName: "DATA", field: "DATA", filter: "agDateColumnFilter"},
  {headerName: "ALTERACAO", field: "ALTERACAO", filter: "agDateColumnFilter", hide: true},
  {headerName: "MODELO", field: "modelo", filter: "agTextColumnFilter"},
  {headerName: "USUARIO", field: "USUARIO", filter: "agSetColumnFilter", hide: true},
  {headerName: "LINHA", field: "linha", filter: "agTextColumnFilter"},
  {headerName: "LETRA", field: "letra", filter: "agSetColumnFilter"},
  {headerName: "COR", field: "cor", filter: "agSetColumnFilter"},
  {headerName: "EMBALAGEM", field: "EMBALAGEM", filter: "agTextColumnFilter"},
  {headerName: "EAN", field: "EAN", filter: "agNumberColumnFilter"},
  {headerName: "TENSAO", field: "tensao", filter: "agSetColumnFilter"},
  {headerName: "GRUPO", field: "GRUPO", filter: "agSetColumnFilter"},
  {headerName: "FABRICANTE", field: "fabricante", filter: "agSetColumnFilter"},
  {headerName: "CARGA", field: "carga", filter: "agTextColumnFilter", hide: true},
  {headerName: "TIPO", field: "tipo", filter: "agSetColumnFilter", hide: true},
  {headerName: "CORRENTE", field: "corrente", filter: "agTextColumnFilter", hide: true},
  {headerName: "CAPACIDADE", field: "cap", filter: "agTextColumnFilter", hide: true},
  {headerName: "VOL.TB", field: "voltb", filter: "agTextColumnFilter", hide: true},
  {headerName: "VOL.BR", field: "volbr", filter: "agTextColumnFilter", hide: true},
  {headerName: "VOL.BF", field: "volbf", filter: "agTextColumnFilter", hide: true},
  {headerName: "VOL.TA", field: "volta", filter: "agTextColumnFilter", hide: true},
  {headerName: "VOL.AR", field: "volar", filter: "agTextColumnFilter", hide: true},
  {headerName: "VOL.AF", field: "volaf", filter: "agTextColumnFilter", hide: true},
  {headerName: "PTC. DEGELO", field: "degelo", filter: "agTextColumnFilter", hide: true},
  {headerName: "DEGELO", field: "dgelo", filter: "agSetColumnFilter", hide: true},
  {headerName: "POTENCIA", field: "potencia", filter: "agTextColumnFilter", hide: true},
  {headerName: "GAS", field: "gas", filter: "agSetColumnFilter"},
  {headerName: "EFICIÊNCIA", field: "eficiencia", filter: "agSetColumnFilter"},
  {headerName: "KWH/MÊS", field: "kwhm", filter: "agTextColumnFilter"},
  {headerName: "TEMPERATURA", field: "temperatura", filter: "agTextColumnFilter"},
  {headerName: "REGISTRO", field: "registro", filter: "agTextColumnFilter"},
  {headerName: "PROCEL", field: "procel", filter: "agSetColumnFilter"},
];
	
// PEGAR SMART ////////////////////////////////////
function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    var selectedid = '';
    selectedRows.forEach( function(selectedRow, index) {
        if (index!==0) {
            selectedRowsString += ', ';
            selectedid += ', ';
        }
        selectedRowsString += selectedRow.modelo;
        selectedid += selectedRow.id;
    });
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
    document.myform.busca.value = selectedRowsString;
}
</script>
<?PHP } ?>		

<script>
// CARREGAR JSON //////////////////////////////////
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    // do http request to get our sample data - not using any framework to keep the example self contained.
    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', '<?PHP echo $table;?>');
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResult = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResult);
        }
    };
});

//BUSCA
function busca(variavel)
{
	var busca = $('#selectedRows').html()
	if(busca.length != 0)
	{  
		if(variavel == 0)
		{			
			window.open('../CONSULTA/BUSCAR.php?smart='+busca)
		}
		else
		{			
			window.open('../MODELO/modelo.php?modelo='+busca)
		}
	}
	else
	{        
		Swal.fire({
			title: 'Atenção',
			html: '<div class="row"><div class="col-12">Selecione um ID para exibir os detalhes</div></div>',          
			type: 'warning',
			confirmButtonText: 'Ok'
		})

	}
}
//IMPRESSAO
function print()
{
	var print = $('#selectedRows').html()
	if(print.length != 0)
	{
		window.open('../IMPRESSAO/IMP.php?imp='+print)
	}
	else
	{        
		Swal.fire({
			title: 'Atenção',
			html: '<div class="row"><div class="col-12">Selecione um ID para imprimir a Etiqueta</div></div>',          
			type: 'warning',
			confirmButtonText: 'Ok'
		})
	}
}
</script>

<script src="/AGGRID/agGrid.js"></script>
<?PHP include('../CORE/Footer2.php')?>