<?php include('../CORE/PDO.php');

$id = $_POST['id'];
if($banco = banco($id))
{
    $retorno = conex()->query("SELECT modelo, linha, voltagem, cor, rma, etapa FROM $banco WHERE smart = '$id'")->fetch(PDO::FETCH_ASSOC);

    #RETORNO RMA
    if(!fnmatch('*.*', $retorno['rma']))
    {
        #ETAPA INVALIDA
        if($retorno['etapa'] == '03.REPARADO' || $retorno['etapa'] == '04.HIGIENIZADO'|| $retorno['etapa'] == '05.QUALIDADE')
        {
            $caso = '<i class="fas fa-thumbs-up mr-2"></i>Ok. Informe o agente!';
            $fala = $id;
        }
        else
        {
            $caso = '<i class="fas fa-sad-tear mr-2"></i>Atenção! Produto sem REPARO';
            $fala = 'Atenção! Produto sem REPARO';
        }
    }
    else
    {
        $rma = strtolower($retorno['rma']);
        $caso = '<i class="fas fa-bomb mr-2"></i>Atenção! Produto RMA: '.$retorno['rma'];
        $fala = 'Atenção! Produto RMA: '.$rma;
    }
}
else
{
    $caso = '<i class="fas fa-thumbs-down mr-2"></i>ID não encontrado. Certifique';
    $fala = 'ID não encontrado. Certifique';
}


//FALA
// $falas = (!empty($fala)) ? htmlspecialchars($fala) : htmlspecialchars('');
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

$dados = ['sql'=> $retorno, 'caso'=>$caso, 'fala'=>$player];
print json_encode($dados, JSON_UNESCAPED_UNICODE);
?>