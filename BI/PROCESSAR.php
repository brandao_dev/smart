﻿<?php			
include('../CORE/Header2.php');

#FUNÇÃO SELECTPROCESSAR
function selectProcessar($etapa1,$etapa2,$fabricante1,$fabricante2,$linha1,$linha2,$local)
{
  return conex()->query("SELECT id FROM codigo WHERE (etapa LIKE '$etapa1%' OR etapa LIKE '$etapa2%') AND (fabricante LIKE '$fabricante1' OR fabricante LIKE '$fabricante2') AND (linha LIKE '$linha1%' OR linha LIKE '$linha2%') AND (localsaida LIKE '%$local')")->rowCount();
}
#FUNÇÃO OUTROS
function selectOutros($etapa1,$etapa2,$local)
{
   return conex()->query("SELECT id FROM codigo WHERE (etapa LIKE '$etapa1%' OR etapa LIKE '$etapa2%') AND (fabricante LIKE 'BRASTEMP' OR fabricante LIKE 'CONSUL' OR fabricante LIKE 'KITCHENAID') AND (linha NOT LIKE 'REFR%' AND linha NOT LIKE 'ADEG%' AND linha NOT LIKE 'FREEZ%' AND linha NOT LIKE 'FRIGO%' AND linha NOT LIKE 'LAVADORA%' AND linha NOT LIKE 'FOG%' AND linha NOT LIKE 'COO%' AND linha NOT LIKE 'CERV%') AND (localsaida LIKE '%$local')")->rowCount();
}

?>
    <table class="table table-bordered table-dark table-hover">
        <tr>
            <td rowspan="2">Mapa dos Produtos Whirlpool</td>
            <td COLSPAN="3">Produtos a Serem Processados</td>
            <td rowspan="2">TOTAL</td>
            <td>Part Wait</td>
        </tr>
        <tr>
            <td>GALATEA</td>
            <td>CAPITÃO</td>
            <td>CAMARES</td>
            <td>GALATEA</td>
        </tr>
<form name="ajaxform" id="ajaxform" method="POST" class="form-group" action="<?php echo DIRPAGE.'BI/DETALHE_PROCESSAR.php'?>">
        <tr class="mediocinza" align="center" bgcolor="#ccc">
            <td>REFRIGERADORES/CERV</td>
            <td><button type="submit" class="mediocinza" name="nome" value="refri_galatea"><?PHP echo $_SESSION['refri_galatea'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','REFRI','CERVE','GALATEA');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="refri_capitao"><?PHP echo $_SESSION['refri_capitao'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','REFRI','CERVE','CAPITAO');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="refri_camares"><?PHP echo $_SESSION['refri_camares'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','REFRI','CERVE','CAMARES');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="refri_tudo"><?PHP echo $_SESSION['refri_tudo'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','REFRI','CERVE','');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="refri_pw"><?PHP echo $_SESSION['refri_pw'] = selectProcessar('10.PART','10.PART','BRASTEMP','CONSUL','REFRI','CERVE','GALATEA');?></button></td>
        </tr>
        <tr class="mediocinza" align="center" bgcolor="#ccc">
            <td>FREEZERS H/V</td>
            <td><button type="submit" class="mediocinza" name="nome" value="free_galatea"><?PHP echo $_SESSION['free_galatea'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FREEZ','FREEZ','GALATEA');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="free_capitao"><?PHP echo $_SESSION['free_capitao'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FREEZ','FREEZ','CAPITAO');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="free_camares"><?PHP echo $_SESSION['free_camares'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FREEZ','FREEZ','CAMARES');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="free_tudo"><?PHP echo $_SESSION['free_tudo'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FREEZ','FREEZ','');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="free_pw"><?PHP echo $_SESSION['free_pw'] = selectProcessar('10.PART','10.PART','BRASTEMP','CONSUL','FREEZ','FREEZ','GALATEA');?></button></td>
        </tr>
        <tr class="mediocinza" align="center" bgcolor="#ccc">
            <td>FRIGOBAR/ADEGA</td>
            <td><button type="submit" class="mediocinza" name="nome" value="frigo_galatea"><?PHP echo $_SESSION['frigo_galatea'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FRIGO','ADEG','GALATEA');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="frigo_capitao"><?PHP echo $_SESSION['bfrigo_capitao'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FRIGO','ADEG','CAPITAO');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="frigo_camares"><?PHP echo $_SESSION['frigo_camares'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FRIGO','ADEG','CAMARES');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="frigo_tudo"><?PHP echo $_SESSION['frigo_tudo'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FRIGO','ADEG','');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="frigo_pw"><?PHP echo $_SESSION['frigo_pw'] = selectProcessar('10.PART','10.PART','BRASTEMP','CONSUL','FRIGO','ADEG','GALATEA');?></button></td>
        </tr>
        <tr class="mediocinza" align="center" bgcolor="#ccc">
            <td>LAVADORAS</td>
            <td><button type="submit" class="mediocinza" name="nome" value="lava_galatea"><?PHP echo $_SESSION['lava_galatea'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','LAVADORA','LAVADORA','GALATEA');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="lava_capitao"><?PHP echo $_SESSION['lava_capitao'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','LAVADORA','LAVADORA','CAPITAO');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="lava_camares"><?PHP echo $_SESSION['lava_camares'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','LAVADORA','LAVADORA','CAMARES');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="lava_tudo"><?PHP echo $_SESSION['lava_tudo'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','LAVADORA','LAVADORA','');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="lava_pw"><?PHP echo $_SESSION['lava_pw'] = selectProcessar('10.PART','10.PART','BRASTEMP','CONSUL','LAVADORA','LAVADORA','GALATEA');?></button></td>
        </tr>
        <tr class="mediocinza" align="center" bgcolor="#ccc">
            <td>FOGÕES/COOKTOP</td>
            <td><button type="submit" class="mediocinza" name="nome" value="fog_galatea"><?PHP echo $_SESSION['fog_galatea'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FOG','COO','GALATEA');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="fog_capitao"><?PHP echo $_SESSION['fog_capitao'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FOG','COO','CAPITAO');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="fog_camares"><?PHP echo $_SESSION['fog_camares'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FOG','COO','CAMARES');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="fog_tudo"><?PHP echo $_SESSION['fog_tudo'] = selectProcessar('02.AGUA','01','BRASTEMP','CONSUL','FOG','COO','');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="fog_pw"><?PHP echo $_SESSION['fog_pw'] = selectProcessar('10.PART','10.PART','BRASTEMP','CONSUL','FOG','COO','GALATEA');?></button></td>
        </tr>
        <tr class="mediocinza" align="center" bgcolor="#ccc">
            <td>KITCHENAID</td>
            <td><button type="submit" class="mediocinza" name="nome" value="kit_galatea"><?PHP echo $_SESSION['kit_galatea'] = selectProcessar('02.AGUA','01','KITCHENAID','KITCHENAID','','','GALATEA');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="kit_capitao"><?PHP echo $_SESSION['kit_capitao'] = selectProcessar('02.AGUA','01','KITCHENAID','KITCHENAID','','','CAPITAO');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="kit_camares"><?PHP echo $_SESSION['kit_camares'] = selectProcessar('02.AGUA','01','KITCHENAID','KITCHENAID','','','CAMARES');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="kit_tudo"><?PHP echo $_SESSION['kit_tudo'] = selectProcessar('02.AGUA','01','KITCHENAID','KITCHENAID','','','');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="kit_pw"><?PHP echo $_SESSION['kit_pw'] = selectProcessar('10.PART','10.PART','KITCHENAID','KITCHENAID','','','GALATEA');?></button></td>
        </tr>
        <tr class="mediocinza" align="center" bgcolor="#ccc">
            <td>OUTROS</td>
            <td><button type="submit" class="mediocinza" name="nome" value="out_galatea"><?PHP echo $_SESSION['out_galatea'] = selectOutros('02.AGUA','01','GALATEA');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="out_capitao"><?PHP echo $_SESSION['out_capitao'] = selectOutros('02.AGUA','01','CAPITAO');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="out_camares"><?PHP echo $_SESSION['out_camares'] = selectOutros('02.AGUA','01','CAMARES');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="out_tudo"><?PHP echo $_SESSION['out_tudo'] = selectOutros('02.AGUA','01','');?></button></td>
            <td><button type="submit" class="mediocinza" name="nome" value="out_pw"><?PHP echo $_SESSION['out_pw'] = selectOutros('10.PART','10.PART','GALATEA');?></button></td>
        </tr>
        <tr class="mediobranco" align="center" bgcolor="#555">
            <td>TOTAL</td>
            <td><button type="submit" class="mediobranco" name="nome" value="total_galatea"><?PHP echo $_SESSION['refri_galatea']+$_SESSION['free_galatea']+$_SESSION['frigo_galatea']+$_SESSION['lava_galatea']+$_SESSION['fog_galatea']+$_SESSION['kit_galatea']+$_SESSION['out_galatea'];?></button></td>
            <td><button type="submit" class="mediobranco" name="nome" value="total_capitao"><?PHP echo $_SESSION['refri_capitao']+$_SESSION['free_capitao']+$_SESSION['bfrigo_capitao']+$_SESSION['lava_capitao']+$_SESSION['fog_capitao']+$_SESSION['kit_capitao']+$_SESSION['out_capitao'];?></button></td>
            <td><button type="submit" class="mediobranco" name="nome" value="total_camares"><?PHP echo $_SESSION['refri_camares']+$_SESSION['free_camares']+$_SESSION['frigo_camares']+$_SESSION['lava_camares']+$_SESSION['fog_camares']+$_SESSION['kit_camares']+$_SESSION['out_camares'];?></button></td>
            <td><button type="submit" class="mediobranco" name="nome" value="total_tudo"><?PHP echo $_SESSION['refri_tudo']+$_SESSION['free_tudo']+$_SESSION['frigo_tudo']+$_SESSION['lava_tudo']+$_SESSION['fog_tudo']+$_SESSION['kit_tudo']+$_SESSION['out_tudo'];?></button></td>
            <td><button type="submit" class="mediobranco" name="nome" value="total_pw"><?PHP echo $_SESSION['refri_pw']+$_SESSION['free_pw']+$_SESSION['frigo_pw']+$_SESSION['lava_pw']+$_SESSION['fog_pw']+$_SESSION['kit_pw']+$_SESSION['out_pw'];?></button></td>
        </tr>
</form>
    </table>
<?PHP include('../CORE/Footer.php'); ?>