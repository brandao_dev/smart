$(function(){
    $('#smart').on("keyup",function(){
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'LEITURA.php',
            async: true,
            data: 'id='+$(this).val(),
            success: function(dados){
                $('#info').empty();
                $('#linha').html('<i class="fas fa-tape mr-2"></i>'+dados.sql.linha);
                $('#tensao').html('<i class="fas fa-bolt mr-2"></i>'+dados.sql.voltagem);
                $('#modelo').html('<i class="far fa-ruler-triangle mr-2"></i>'+dados.sql.modelo);
                $('#cor').html('<i class="fas fa-palette mr-2"></i>'+dados.sql.cor);
                $('#caso').html(dados.caso);
                $('#player').html(dados.fala)
            },
            error: function(){
                console.log('caca')
            }
        })
    })
})

function pessoa(pessoa){
    id = $('#smart').val();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        async: true,
        url: 'PROCESSA.php',
        data: {pessoa:pessoa,id:id},
        success: function(data){
            $('#caso').html(data.caso);
            $('#player').html(data.fala);
            $('#'+pessoa).html(pessoa+' - '+data.contagem);
            $('#total').html('<i class="fas fa-users mr-2"></i>'+data.total)

            //DESTQUE PARA O ESCOLHIDO
            $('#1ANA').removeClass('bg-warning');
            $('#1ELVIS').removeClass('bg-warning');
            $('#1GINAURA').removeClass('bg-warning');
            $('#1INGRID').removeClass('bg-warning');
            $('#1LAIDI').removeClass('bg-warning');
            $('#1MARLY').removeClass('bg-warning');
            $('#1'+pessoa).addClass('bg-warning');
        },
        error: function(){
            console.log('merda')
        }
    })
}

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

function vibrate(ms){
    navigator.vibrate(ms);
  }

// 888888b.                                           888          
// 888  "88b                                          888          
// 888  .88P                                          888          
// 8888888K.   8888b.  888d888  .d8888b  .d88b.   .d88888  .d88b.  
// 888  "Y88b     "88b 888P"   d88P"    d88""88b d88" 888 d8P  Y8b 
// 888    888 .d888888 888     888      888  888 888  888 88888888 
// 888   d88P 888  888 888     Y88b.    Y88..88P Y88b 888 Y8b.     
// 8888888P"  "Y888888 888      "Y8888P  "Y88P"   "Y88888  "Y8888  

if(window.location.hash.substr(1,2) == "zx"){
    var bc = window.location.hash.substr(3);
    localStorage["barcode"] = decodeURI(window.location.hash.substr(3))
    window.close();
    self.close();
    window.location.href = "about:blank";//In case self.close isn't allowed
}
var changingHash = false;
function onbarcode(event){
    switch(event.type){
        case "hashchange":{
            if(changingHash == true){
                return;
            }
            var hash = window.location.hash;
            if(hash.substr(0,3) == "#zx"){
                hash = window.location.hash.substr(3);
                changingHash = true;
                window.location.hash = event.oldURL.split("\#")[1] || ""
                changingHash = false;
                processBarcode(hash);
            }

            break;
        }
        case "storage":{
            window.focus();
            if(event.key == "barcode"){
                window.removeEventListener("storage", onbarcode, false);
                processBarcode(event.newValue);
            }
            break;
        }
        default:{
            console.log(event)
            break;
        }
    }
}
window.addEventListener("hashchange", onbarcode, false);

function getScan(){
    var href = window.location.href;
    var ptr = href.lastIndexOf("#");
    if(ptr>0){
        href = href.substr(0,ptr);
    }
    window.addEventListener("storage", onbarcode, false);
    setTimeout('window.removeEventListener("storage", onbarcode, false)', 15000);
    localStorage.removeItem("barcode");
    //window.open  (href + "#zx" + new Date().toString());

    if(navigator.userAgent.match(/Firefox/i)){
        //Used for Firefox. If Chrome uses this, it raises the "hashchanged" event only.
        window.location.href =  ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
    }else{
        //Used for Chrome. If Firefox uses this, it leaves the scan window open.
        window.open   ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
    }
}

function processBarcode(bc){
    document.getElementById("smart").value = '';
    document.getElementById("smart").value = bc;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'LEITURA.php',
        async: true,
        data: 'id='+bc,
        success: function(dados){
            $('#info').empty();
            $('#linha').html('<i class="fas fa-tape mr-2"></i>'+dados.sql.linha);
            $('#tensao').html('<i class="fas fa-bolt mr-2"></i>'+dados.sql.voltagem);
            $('#modelo').html('<i class="far fa-ruler-triangle mr-2"></i>'+dados.sql.modelo);
            $('#cor').html('<i class="fas fa-palette mr-2"></i>'+dados.sql.cor);
            $('#caso').html(dados.caso);
            $('#player').html(dados.fala);
        },
        error: function(){
            console.log('caca')
        }
    })
}