<?php
include_once '../CORE/PDO.php';

$servico = (isset($_POST['servico'])) ? $_POST['servico'] : '';
$cor = (isset($_POST['cor'])) ? $_POST['cor'] : '';
$int = $_POST['int'];
$smart = (isset($_POST['smart'])) ? strtoupper($_POST['smart']) : '';
$ex = (isset($_POST['ex'])) ? true : false;

//CERTIFICA SE EXISTE
$banco = banco($smart);
if($original = conex()->query("SELECT smart,cor,modelo FROM $banco WHERE smart = '$smart'")->fetch(PDO::FETCH_ASSOC))
{
    //BUSCA A ORIGINAL SE SOLICITADO
    if($cor == 'ORIGINAL')
    {
        $cor = conex()->query("SELECT cor FROM modelo WHERE modelo = '{$original['modelo']}'")->fetch(PDO::FETCH_ASSOC)['cor'];
    }
    elseif(empty($cor))
    {
        $original['cor'];
    }
    
    //TRIO
    switch($int)
    {
        case 1:
            if($ex == true)
            {
                conex()->prepare("UPDATE $banco SET DATA_PINTURA = '', PINTURA = '', pnt_local = 0 WHERE smart = '$smart'")->execute();
                $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Registro excluído!';
                $fala = 'Registro excluído!';
                #CHAMA FUNCAO LOG	
                setLOG("PINTURA",'*',"Excluido",$smart,"*");
            }
            else
            {
                if($servico != '')
                {
                    conex()->prepare("UPDATE $banco SET DATA_PINTURA = '$time', PINTURA = '$servico', cor = '$cor', pnt_local = 1 WHERE smart = '$smart'")->execute();
                    $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Restauração registrada!';
                    $fala = 'Restauração registrada!';
                    #CHAMA FUNCAO LOG	
                    setLOG("PINTURA",'*',"registrado",$smart,"*");
                }
                else
                {
                    $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Serviço não definido';
                    $fala = 'Serviço não definido';
                }
            }
            $data = conex()->query("SELECT smart, modelo FROM codigo WHERE pnt_local = 1 AND DATA_PINTURA LIKE '$dia%' UNION ALL SELECT smart, modelo FROM lg WHERE pnt_local = 1 AND DATA_PINTURA LIKE '$dia%'")->fetchAll(PDO::FETCH_ASSOC);
        break;

        case 2:
            if($ex == true)
            {
                conex()->prepare("UPDATE $banco SET DATA_PINTURA = '', PINTURA = '', pnt_local = 0 WHERE smart = '$smart'")->execute();
                $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Saída cancelada!';
                $fala = 'Saída cancelada!';
                #CHAMA FUNCAO LOG	
                setLOG("PINTURA",'*',"Saida cancelada",$smart,"*");
            }
            else
            {
                if($servico != '')
                {
                    conex()->prepare("UPDATE $banco SET DATA_PINTURA = '$time', PINTURA = '$servico', cor = '$cor', pnt_local = 2 WHERE smart = '$smart'")->execute();
                    $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Saída registrada!';
                    $fala = 'Saída registrada!';
                    #CHAMA FUNCAO LOG	
                    setLOG("PINTURA",'*',"Saida registrada",$smart,"*");
                }
                else
                {
                    $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Serviço não definido';
                    $fala = 'Serviço não definido';
                }

            }
            $data = conex()->query("SELECT smart, modelo FROM codigo WHERE pnt_local = 2 UNION ALL SELECT smart, modelo FROM lg WHERE pnt_local = 2")->fetchAll(PDO::FETCH_ASSOC);
        break;

        case 3:
            if(conex()->query("SELECT id FROM $banco WHERE smart = '$smart' AND pnt_local = 2")->fetch(PDO::FETCH_ASSOC))
            {
                conex()->prepare("UPDATE $banco SET pnt_local = 3 WHERE smart = '$smart'")->execute();
                $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Recebimento concluído!';
                $fala = 'Recebimento concluído!';
                #CHAMA FUNCAO LOG	
                setLOG("PINTURA",'*',"Recebimento concluido",$smart,"*");
            }
            else
            {
                $caso = '<i class="fas fa-edit fa-lg mr-2"></i>Esse ID não teve saída!';
                $fala = 'Esse ID não teve saída!';
                #CHAMA FUNCAO LOG	
                setLOG("PINTURA",'*',"Recebimento concluido",$smart,"*");
            }
            $data = conex()->query("SELECT smart, modelo FROM codigo WHERE pnt_local = 2 UNION ALL SELECT smart, modelo FROM lg WHERE pnt_local = 2")->fetchAll(PDO::FETCH_ASSOC);                
        break;
    }
}
else
{       
    $caso = '<i class="fas fa-edit fa-lg mr-2"></i>ID não existe';
    $fala = 'ID nao existe';
    $data = '';
}



//FALA
// $falas = htmlspecialchars(ucwords(strtolower($fala)));
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

//JSON
$mes = date('Y-m');
$total = Count(conex()->query("SELECT id FROM codigo WHERE DATA_PINTURA LIKE '$mes%' UNION SELECT id FROM lg WHERE DATA_PINTURA LIKE '$mes%'")->fetchAll(PDO::FETCH_ASSOC));

$data = array_merge(['sql'=>$data, 'caso'=>$caso, 'fala'=>$player, 'total'=>$total, 'int'=>$int]);
print json_encode($data, JSON_UNESCAPED_UNICODE);