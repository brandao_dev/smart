$(function(){
    $(document).ajaxStart(function(){
      Swal.fire({
          title: 'Aguarde...',
          html: '<h4 class="text-left"><i class="fas fa-check mr-2 text-success"></i>Gerando arquivo Excel</h4>',
          imageUrl: '../IMG/PAGAMENTO/3.gif',
          imageWidth: 200,
          imageHeight: 200,
          showConfirmButton: false,
          backdrop: `rgba(0,0,0,0.8)`,
      }
      )
    });
    $('#foda').on('submit', function(event){
        event.preventDefault()
        
        let data = $(this).serialize()
        $.ajax({
            dataType: 'json',
            type: 'POST',
            async: true,
            data: data,
            url: '/PAGAMENTO/processa.php',
            success: function(dados){
                console.log(dados)
                $('#total').html('<i class="fas fa-user-graduate mr-2"></i>Pós-aprovados: '+dados)
                $('#total').removeClass('alert-cyan')
                $('#total').addClass('alert-danger')
                let recusa = sessionStorage.getItem('pendente') - dados
                $('#pendente').html('<i class="fas fa-user-alien mr-2"></i>Recusados: '+recusa)
                $('#pendente').removeClass('alert-cyan')
                $('#pendente').addClass('alert-danger')
                Swal.fire({
                        icon: 'success',
                        title: '<h1>Pronto!<h1>',
                        html: '<h5>Clique para baixar a planilha!</h5>',
                        confirmButtonText:'<a href="/PAGAMENTO/PLANILHAS/fechamento.xlsx" class="text-light" style="text-decoration: none;" onclick="vibrate(200);play()"><i class="fas fa-cloud-download-alt mr-2"></i>Donwload</a>',
                        backdrop: `rgba(0,0,0,0.8)`,
                        heightAuto: true
                }).then((result)=>{$('html, body').animate({scrollTop:0},500)})
            },
            error: function(dados){
                console.log(dados)
                console.log(sessionStorage.getItem('total'))
            }
        })
        
    })
})

//VIBRAR
function vibrate(ms){
    navigator.vibrate(ms);
    }