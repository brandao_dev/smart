<?php include '../CORE/Header2.php';?>

<script src="https://brandev.tech/FRAMEWORK/js/ag-grid-enterprise.js"></script>
	
<div id="myGrid" style="position:absolute; width: 99%;height:80%" class="ag-theme-balham"></div>


<script type="text/javascript" charset="utf-8">		
// COLUNAS //////////////////////////////////////////
var columnDefs = 
[
  {headerName: "DATA EXPEDIÇÃO", field: "dataexpedicao", filter: "agDateColumnFilter"},
  {headerName: "LOCAL SAIDA", field: "localsaida", filter: "agTextColumnFilter", hide: true},
  {headerName: "COR", field: "cor", filter: "agTextColumnFilter", hide: true},
  {headerName: "ETAPA", field: "etapa", filter: "agSetColumnFilter", hide: true},
  {headerName: "FABRICANTE", field: "fabricante", filter: "agSetColumnFilter"},
  {headerName: "GRADE", field: "grade", filter: "agSetColumnFilter", hide: true},
  {headerName: "LINHA", field: "linha", filter: "agTextColumnFilter"},
  {headerName: "MINUTA SAIDA", field: "m2", filter: "agNumberColumnFilter", hide: true},
  {headerName: "MODELO", field: "modelo", filter: "agTextColumnFilter"},
  {headerName: "SERIAL SMART", field: "smart", filter: "agTextColumnFilter"},
  {headerName: "TENSAO", field: "voltagem", filter: "agTextColumnFilter"},
  {headerName: "USER", field: "userexpedicao", filter: "agSetColumnFilter"},
];

// OPÇÔES DA GRID E PAINEL LATERAL /////////////////
var gridOptions = 
{
	statusBar: {
        statusPanels: [
            { statusPanel: 'agTotalAndFilteredRowCountComponent', align: 'left' },
            { statusPanel: 'agFilteredRowCountComponent' },
            { statusPanel: 'agSelectedRowCountComponent' },
            { statusPanel: 'agAggregationComponent' }
        ]
    },
	defaultColDef: {sortable: true, filterParams: {clearButton: true,inRangeInclusive:true,includeBlanksInEquals:false,includeBlanksInRange:false,includeBlanksANTreaterThan:false,includeBlanksInLessThan:false,
			comparator: function (filterLocalDateAtMidnight, cellValue) 
			{
			   var dateAsString = cellValue;
			   if (dateAsString == null) return 0;
			   // In the example application, dates are stored as dd/mm/yyyy
			   // We create a Date object for comparison against the filter date
			   var dateParts = dateAsString.split("\/");
			   var day = Number(dateParts[2]);
			   var month = Number(dateParts[1]) - 1;
			   var year = Number(dateParts[0]);
			   var cellDate = new Date(day, month, year);
			   // Now that both parameters are Date objects, we can compare
			   if (cellDate < filterLocalDateAtMidnight) {return -1;} else if (cellDate > filterLocalDateAtMidnight) {return 1;} else {return 0;}
			}}},
	columnDefs: columnDefs,
    enableRangeSelection: true,
    rowData: null,
	floatingFilter: true,
	suppressCsvExport: true,
	rowSelection: 'multiple',
	onSelectionChanged: onSelectionChanged,
	animateRows: true,
	sideBar: 
	{
		toolPanels: 
		[{
			id: 'columns',
			labelDefault: 'Columns',
			labelKey: 'columns',
			iconKey: 'columns',
			toolPanel: 'agColumnsToolPanel',
			toolPanelParams: 
			{
				suppressPivots: true,
				suppressPivotMode: true,
				suppressValues: true,
				suppressRowGroups: true
			}
		}]
},
// TRADUÇÂO DOS MENUS ////////////////////////////////////////////////////////////////////////////////////////	
	localeText : {
	// for filter panel
	page: 'Página',
	more: 'Mais',
	to: 'Para',
	of: 'A',
	next: 'Proximo',
	last: 'Ultimo',
	first: 'Primeiro',
	previous: 'Anterior',
	loadingOoo: 'Carregando...',

	// for set filter
	selectAll: 'Selecione Tudo',
	searchOoo: 'Procurar...',
	blanks: 'Branco',

	// for number filter and text filter
	filterOoo: 'Filtro...',
	applyFilter: 'Aplicar Filtro...',
	equals: 'Igual',
	notEqual: 'Diferente',

	// for number filter
	lessThan: 'Menor que',
	greaterThan: 'Maior que',
	lessThanOrEqual: 'Menor ou Igual a',
	greaterThanOrEqual: 'Maior ou igual a',
	inRange: 'Entre',

	// for text filter
	contains: 'Contém',
	notContains: 'Nao Contém',
	startsWith: 'Começa com',
	endsWith: 'Termina com',

	// filter conditions
	andCondition: 'E',
	orCondition: 'OU',

	// the header of the default group column
	group: 'Grupo',

	// tool panel
	columns: 'Colunas',
	filters: 'Filtros',
	rowGroupColumns: 'Grupo de Colunas',
	rowGroupColumnsEmptyMessage: 'Arrastar Coluna para Grupo',
	valueColumns: 'Valor da Coluna',
	pivotMode: 'Modo Pivo',
	groups: 'Grupos',
	values: 'Valores',
	pivots: 'Pivos',
	valueColumnsEmptyMessage: 'Arrastar Coluna para Agregar',
	pivotColumnsEmptyMessage: 'Arraste aqui para Pivo',
	toolPanelButton: 'Painel de Ferramentas',

	// other
	noRowsToShow: 'Sem linhas',

	// enterprise menu
	pinColumn: 'Fixar Coluna',
	valueAggregation: 'Valor Agregado',
	autosizeThiscolumn: 'Ajustar esta Coluna',
	autosizeAllColumns: 'Ajustar todas as Colunas',
	groupBy: 'Agrupado por',
	ungroupBy: 'Desagrupado por',
	resetColumns: 'Resetar Colunas',
	expandAll: 'Expandir Tudo',
	collapseAll: 'Colapsar Tudo',
	toolPanel: 'Painel de Ferramentas',
	export: 'Exportar',
	csvExport: 'laCSV Exportp',
	excelExport: 'Exportar Planilha Excel (.XLSX)',
	excelXmlExport: 'Exportar (.XML)',

	// enterprise menu (charts)
	chartRange: 'laChart Range',

	columnRangeChart: 'laColumn',
	groupedColumnChart: 'laGrouped',
	stackedColumnChart: 'laStacked',
	normalizedColumnChart: 'la100% Stacked',

	barRangeChart: 'laBar',
	groupedBarChart: 'laGrouped',
	stackedBarChart: 'laStacked',
	normalizedBarChart: 'la100% Stacked',

	lineRangeChart: 'laLine',

	pieRangeChart: 'laPie',
	pieChart: 'laPie',
	doughnutChart: 'laDoughnut',

	areaRangeChart: 'laArea',
	areaChart: 'laArea',
	stackedAreaChart: 'laStacked',
	normalizedAreaChart: 'la100% Stacked',

	// enterprise menu pinning
	pinLeft: 'Fixar a Esquerda <<',
	pinRight: 'Fixar a Direita >>',
	noPin: 'Desfixar <>',

	// enterprise menu aggregation and status bar
	sum: 'Soma',
	min: 'Menor Valor',
	max: 'Maior Valor',
	none: 'Nada',
	count: 'Contagem',
	average: 'Média',
	filteredRows: 'Linhas Filtradas',
	selectedRows: 'Linhas Selecionadas',
	totalRows: 'Linhas Totais',
	totalAndFilteredRows: 'Linhas Totais Filtradas',

	// standard menu
	copy: 'Copiar',
	copyWithHeaders: 'Copiar com Cabeçalho',
	ctrlC: 'Ctrl + C',
	paste: 'Colar',
	ctrlV: 'Ctrl + V',

	// charts
	settings: 'laSettings',
	data: 'laData',
	format: 'laFormat',
	categories: 'laCategories',
	series: 'laSeries',
	axis: 'laAxis',
	color: 'laColor',
	thickness: 'laThickness',
	xRotation: 'laX Rotation',
	yRotation: 'laY Rotation',
	ticks: 'laTicks',
	width: 'laWidth',
	length: 'laLength',
	padding: 'laPadding',
	chart: 'laChart',
	title: 'laTitle',
	font: 'laFont',
	top: 'laTop',
	right: 'laRight',
	bottom: 'laBottom',
	left: 'laLeft',
	labels: 'laLabels',
	size: 'laSize',
	legend: 'laLegend',
	position: 'laPosition',
	markerSize: 'laMarker Size',
	markerStroke: 'laMarker Stroke',
	markerPadding: 'laMarker Padding',
	itemPaddingX: 'laItem Padding X',
	itemPaddingY: 'laItem Padding Y',
	strokeWidth: 'laStroke Width',
	offset: 'laOffset',
	tooltips: 'laTooltips',
	offsets: 'laOffsets',
	callout: 'laCallout',
	markers: 'laMarkers',
	shadow: 'laShadow',
	blur: 'laBlur',
	xOffset: 'laX Offset',
	yOffset: 'laY Offset',
	lineWidth: 'laLine Width',
	normal: 'laNormal',
	bold: 'laBold',
	italic: 'laItalic',
	boldItalic: 'laBold Italic',
	fillOpacity: 'laFill Opacity',
	strokeOpacity: 'laLine Opacity',
	groupedColumnTooltip: 'laGrouped',
	stackedColumnTooltip: 'laStacked',
	normalizedColumnTooltip: 'la100% Stacked',
	groupedBarTooltip: 'laGrouped',
	stackedBarTooltip: 'laStacked',
	normalizedBarTooltip: 'la100% Stacked',
	pieTooltip: 'laPie',
	doughnutTooltip: 'laDoughnut',
	lineTooltip: 'laLine',
	groupedAreaTooltip: 'laGrouped',
	stackedAreaTooltip: 'laStacked',
	normalizedAreaTooltip: 'la100% Stacked'
}
};

// SELECIONAR ITENS //////////////////////////////
function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    var imp = '';
    selectedRows.forEach( function(selectedRow, index) {
        if (index!==0) {
            selectedRowsString += ', ';
            imp += ', ';
        }
        selectedRowsString += selectedRow.smart;
        imp += selectedRow.smart;
    });
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
    document.myform.busca.value = selectedRowsString;
    document.formulario.imp.value = imp;
}
</script>		

<script>
// CARREGAR JSON //////////////////////////////////
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    // do http request to get our sample data - not using any framework to keep the example self contained.
    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'GET.php');
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResult = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResult);
        }
    };
});
</script>
<?php include('../CORE/Footer2.php')?>