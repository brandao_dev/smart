<?php include("../CORE/Header2.php");

if($user == 'PAINEL')
{ ?>
<script>
setTimeout(function() {
location.reload();
}, 60000);
</script>
<?php } 

$mes = date("Y-m");
$total = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%' UNION ALL SELECT id FROM lg WHERE higiene LIKE '$mes%'")->fetchAll(PDO::FETCH_ASSOC));
$MARLY = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%' AND higienico LIKE 'MARLY' UNION ALL SELECT id FROM lg WHERE higiene LIKE '$mes%' AND higienico LIKE 'MARLY'")->fetchAll(PDO::FETCH_ASSOC));
$ANA = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%' AND higienico LIKE 'ANA' UNION ALL SELECT id FROM lg WHERE higiene LIKE '$mes%' AND higienico LIKE 'ANA'")->fetchAll(PDO::FETCH_ASSOC));
$GINAURA = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%' AND higienico LIKE 'GINAURA' UNION ALL SELECT id FROM lg WHERE higiene LIKE '$mes%' AND higienico LIKE 'GINAURA'")->fetchAll(PDO::FETCH_ASSOC));
$LAIDI = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%' AND higienico LIKE 'LAIDI' UNION ALL SELECT id FROM lg WHERE higiene LIKE '$mes%' AND higienico LIKE 'LAIDI'")->fetchAll(PDO::FETCH_ASSOC));
$ELVIS = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%' AND higienico LIKE 'ELVIS' UNION ALL SELECT id FROM lg WHERE higiene LIKE '$mes%' AND higienico LIKE 'ELVIS'")->fetchAll(PDO::FETCH_ASSOC));
$INGRID = Count(conex()->query("SELECT id FROM codigo WHERE higiene LIKE '$mes%' AND higienico LIKE 'INGRID' UNION ALL SELECT id FROM lg WHERE higiene LIKE '$mes%' AND higienico LIKE 'INGRID'")->fetchAll(PDO::FETCH_ASSOC));
?>     
<!--Sweet Alert 2 -->     
<!--Código custom -->          
<script src="AJAX.js"></script>   
<div id="player" style="display: none;"></div>

<!-- 
       d8888                   d8b          
      d88888                   Y8P          
     d88P888                                
    d88P 888 88888b.   .d88b.  888  .d88b.  
   d88P  888 888 "88b d88""88b 888 d88""88b 
  d88P   888 888  888 888  888 888 888  888 
 d8888888888 888 d88P Y88..88P 888 Y88..88P 
d88P     888 88888P"   "Y88P"  888  "Y88P"  
             888                            
             888                            
             888                            
 -->

<div class="row">
    <div class="col-8">
        <!-- SEGUNDA LINHA DE FRASE DE APOIO *** //////////////////////// -->
        <div class="alert mt-3 alert-info font-weight-bold btn-block" id="caso"><i class="fas fa-handshake fa-lg mr-2"></i><?php echo 'Olá Sr. '.$user.'!';?></div>
    </div>
    <div class="col-4">
        <!-- TOTAL *** /////////////////////////// -->
        <div class="alert alert-dark mt-3 font-weight-bold" id="total"><i class="fas fa-users mr-2"></i><?php echo $total?></div>
    </div>
</div>

<!-- 
       d8888      888 d8b          d8b                                    
      d88888      888 Y8P          Y8P                                    
     d88P888      888                                                     
    d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
   d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
  d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
 d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     
 -->

<div class="row">
    <div class="col-7 mt-2">
        <input type="text" class="form-control form-control-lg font-weight-bold" name="smart" id="smart" autocomplete="off" required autofocus list="lis" placeholder="Insira o ID">
        <datalist id="lis">
            <?php
                foreach((conex()->query("SELECT smart,ALTERACAO FROM codigo UNION ALL SELECT smart,ALTERACAO FROM lg ORDER BY ALTERACAO DESC")) as $result)
                {
                    echo "<option value='".$result['smart']."'/>";
                }
            ?>
        </datalist>
    </div>
    <div class="col-5 mt-2">
        <button class="btn btn-success shadow btn-block" type="button" value="Scan" onclick="play(),vibrate(100),getScan()"><h5 class="mt-1"><i class="fas fa-camera mr-2"></i>Camera</h5></button>
    </div>
</div>

<!-- PLAYER DE AUDIO -->
<div style="display: none" id="click"></div>

<!-- BARCODE 1 -->
<INPUT id=barcode type=text style="display: none" >

<hr>
<!-- 

88888888888          888               888          
    888              888               888          
    888              888               888          
    888      .d88b.  88888b.   .d88b.  888  8888b.  
    888     d8P  Y8b 888 "88b d8P  Y8b 888     "88b 
    888     88888888 888  888 88888888 888 .d888888 
    888     Y8b.     888 d88P Y8b.     888 888  888 
    888      "Y8888  88888P"   "Y8888  888 "Y888888  

 -->
 <div class="row mt-2">
    <div class="col-12 col-sm-6 col-xl-3">
        <div class="alert alert-brown" id="linha"><i class="fas fa-tape mr-2"></i>Linha</div>
    </div>
    <div class="col-12 col-sm-6 col-xl-3">
        <div class="alert alert-brown" id="modelo"><i class="far fa-ruler-triangle mr-2"></i>Modelo</div>
    </div>
    <div class="col-6 col-xl-3">
        <div class="alert alert-brown" id="cor"><i class="fas fa-palette mr-2"></i>Cor</div>
    </div>
    <div class="col-6 col-xl-3">
        <div class="alert alert-brown" id="tensao"><i class="fas fa-bolt mr-2"></i>Tensão</div>
    </div>
 </div>
        
 <hr>
<div class="row">
    <div class="col-6 col-sm-4 col-lg-3 col-xl-2 mt-4">
        <div class="card shadow" onclick="pessoa('ANA'), vibrate(100),play()" id="1ANA">
        <img class="card-img-top" src="../IMG/HIGI/ANA.jpg" alt="Ana">
            <div class="card-body">
                <h5 class="card-text" id="ANA">ANA - <?php echo $ANA?></h5>
            </div>
        </div>                  
    </div>
    <div class="col-6 col-sm-4 col-lg-3 col-xl-2 mt-4">
        <div class="card shadow" onclick="pessoa('ELVIS'), vibrate(100),play()" id="1ELVIS">
        <img class="card-img-top" src="../IMG/HIGI/ELVIS.jpg" alt="ELVIS">
            <div class="card-body">
                <h5 class="card-text" id="ELVIS">ELVIS - <?php echo $ELVIS?></h5>
            </div>
        </div>                  
    </div>
    <div class="col-6 col-sm-4 col-lg-3 col-xl-2 mt-4">
        <div class="card shadow" onclick="pessoa('GINAURA'), vibrate(100),play()" id="1GINAURA">
        <img class="card-img-top" src="../IMG/HIGI/GINAURA.jpg" alt="Ginaura">
            <div class="card-body">
                <h5 class="card-text" id="GINAURA">GINAURA - <?php echo $GINAURA?></h5>
            </div>
        </div>                  
    </div>
    <div class="col-6 col-sm-4 col-lg-3 col-xl-2 mt-4">
        <div class="card shadow" onclick="pessoa('INGRID'), vibrate(100),play()" id="1INGRID">
        <img class="card-img-top" src="../IMG/HIGI/INGRID.jpg" alt="Ingrid">
            <div class="card-body">
                <h5 class="card-text" id="INGRID">INGRID - <?php echo $INGRID?></h5>
            </div>
        </div>                  
    </div>
    <div class="col-6 col-sm-4 col-lg-3 col-xl-2 mt-4">
        <div class="card shadow" onclick="pessoa('LAIDI'), vibrate(100),play()" id="1LAIDI">
        <img class="card-img-top" src="../IMG/HIGI/LAIDI.jpg" alt="Laide">
            <div class="card-body">
                <h5 class="card-text" id="LAIDI">LEIDE - <?php echo $LAIDI?></h5>
            </div>
        </div>                  
    </div>
    <div class="col-6 col-sm-4 col-lg-3 col-xl-2 mt-4">
        <div class="card shadow" onclick="pessoa('MARLY'), vibrate(100),play()" id="1MARLY">
        <img class="card-img-top" src="../IMG/HIGI/MARLY.jpg" alt="Marly">
            <div class="card-body">
                <h5 class="card-text" id="MARLY">MARLY - <?php echo $MARLY?></h5>
            </div>
        </div>                  
    </div>
</div>
<hr>

<?php include("../CORE/Footer2.php")?>