<?php include_once('../CORE/PDO.php');

$sac = $_GET['min'];

$minuta = conex()->query("SELECT * FROM sac WHERE SAC = '$sac'")->fetch(PDO::FETCH_ASSOC);
$banco = banco($minuta['SMART']);
$sql = conex()->query("SELECT enge,linha,grade,cor,voltagem,fabricante FROM $banco WHERE smart = '{$minuta['SMART']}'")->fetch(PDO::FETCH_ASSOC);


// DEFINIR DESENHO
if($sql['linha'] == 'FREEZER H.')
{
  $prod = 'FRE';
}
elseif(substr($sql['linha'],0,3) == 'FOG')
{
  $prod = 'FOG';
}
elseif(substr($sql['linha'],0,6) == 'LAVA E' || substr($sql['linha'],0,6) == 'LAVADO')
{
  $prod = 'LAV';
}
elseif(fnmatch('*LOU*', $sql['linha']))
{
  $prod = 'LOC';
}
else
{
  $prod = 'REF';
}

?>

<!DOCTYPE HTML>
<html lang='pt-br'>
<head>
<meta charset='UTF-8'>
<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
<meta name='autor' content='A. Brandão Full Stack Developer 2020 / PHPOO, Composer, NodeJs, SASS, Gulp, MySQL, CSS, HTML5, JavaScript'>
<meta name='format-detection' content='telephone-no'>
<title>ERP - SMART</title>
<link rel='shortcut icon' href='../IMG/SICON.png'>
<link rel='stylesheet' href='https://cdn.brandev.site/CSS/bootstrap.css'>
<link href='https://cdn.brandev.site/css/all.css' rel='stylesheet'>
<link href='https://cdn.brandev.site/css/style.css' rel='stylesheet'>
<link href='https://cdn.brandev.site/style/estilo.css' rel='stylesheet'>
<script src="https://cdn.brandev.site/js/jquery.js"></script>
<script src="https://cdn.brandev.site/js/bootstrap.js"></script>
<script src="https://cdn.brandev.site/js/popper.js"></script>
</head>
<body  class='text-center' onload="window.print()">
<!-- MARGEM DO CORPO -->
<div class='container-fluid'>
  <div class="row mt-2 mb-2">
    <div class="col-2"><img src="../IMG/smart.jpg" width="110%"></div>
    <div class="col-10"><h1>Ordem de Serviço nº <?php echo $minuta['SAC']?></h1></div>
  </div>

<div class="row">
  <div class="col text-left font-weight-bold"><h4><i class="fas fa-store-alt mr-2"></i>Loja</h4></div>
</div>  
<table class="table table-striped table-bordered text-left" colspan='4' style="border: 2px solid  #111 !important;">
  <tr>
    <th>Atendimento</th>
    <th>Agendamento</th>
    <th>Origem</th>
  </tr>
  <tr>
    <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?php echo $minuta['DATA'];?></td>
    <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?php echo substr($minuta['AGENDA'], -10);?></td>
    <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?php echo $minuta['LOCAL'];?></td>
  </tr>
</table>

<div class="row">
  <div class="col text-left font-weight-bold"><h4><i class="far fa-address-card mr-2"></i>Cliente</h4></div>
</div>  
<table class="table table-striped table-bordered text-left" colspan='4' style="border: 2px solid  #111 !important;">
    <tr>    
        <th>Cliente</th>
        <th>Endereço</th>
        <th>Bairro</th>
        <th>Cidade</th>
    </tr>  
    <tr>
    	<td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['CLIENTE'];?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['ENDERECO'].', '.$minuta['NUMERO'];?></td>
    	<td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['BAIRRO'];?></td>
    	<td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?php echo $minuta['CIDADE'];?></td>
    </tr>
      <th>CEP</th>
      <th>Complemento</th>
      <th>Telefone 1</th>
      <th>Telefone 2</th>
    </tr>  
    <tr>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['CEP'];?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['COMPLEMENTO']?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['TEL']?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['TEL2']?></td>
    </tr>
</table>

<div class="row">
  <div class="col text-left font-weight-bold"><h4><i class="far fa-refrigerator mr-2"></i>Produto</h4></div>
</div> 
<table class="table table-striped table-bordered text-left" colspan='4' style="border: 2px solid  #111 !important;">
    <tr>    
        <th>Linha</th>
        <th>Marca</th>
        <th>Smart ID</th>
        <th>Engenharia</th>
    </tr>  
    <tr>
    	<td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?php echo $sql['linha'];?></td>
    	<td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $sql['fabricante'];?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['SMART'];?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $sql['enge']?></td>
    </tr>
      <th>Modelo</th>
      <th>Grade</th>
      <th>Voltagem</th>
      <th>Cor</th>
    </tr>  
    <tr>
    	<td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?php echo $minuta['MODELO'];?></td>
    	<td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $sql['grade'];?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $sql['voltagem'];?></td>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $sql['cor']?></td>
    </tr>
</table>

<div class="row">
  <div class="col text-left font-weight-bold"><h4><i class="fas fa-clipboard-check mr-2"></i>Check-List</h4></div>
</div> 

  <img src="../IMG/SAC/<?php echo $prod?>.png" alt="" class="w-100">

<div style="page-break-after: always;"></div>
<div class="row">
  <div class="col text-left font-weight-bold"><h4><i class="fas fa-search mr-2"></i>Observações</h4></div>
</div> 
<table class="table table-striped table-bordered text-left mt-3" colspan='4' style="border: 2px solid  #111 !important;">
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>

<div class="row">
  <div class="col text-left font-weight-bold"><h4><i class="fas fa-thumbtack mr-2"></i>Registros</h4></div>
</div> 
<table class="table table-striped table-bordered text-left" colspan='4' style="border: 2px solid  #111 !important;">
    <tr>    
        <th>Queixa(s)</th>
    </tr>
    <tr>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?php echo $minuta['QUEIXA'];?></td>
    </tr>
    </tr>
        <th>Constatado(s)</th> 
    </tr>
    <tr>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['CONSTATADO'];?></td>
    </tr>
    <tr>
        <th>Reparo(s)</th>
    </tr>
    <tr>
      <td style="background-color: #ccc !important;-webkit-print-color-adjust: exact"><?PHP echo $minuta['SERVICO'];?></td>
    </tr> 
</table>
<div class="row mt-5">
  <div class="col-12 mb-3">
    <h4>
      <p>Declaro que o servico foi devidamente executado, estando o produto de minha propriedade em perfeita ordem e
      funcionamento.</p><p>É de meu conhecimento que as informações complementares poderão ser obtidas através do manual de instruções do meu
      produto que se encontra em meu poder.</p>
    </h4>
  </div>
  <hr>
  <div class="col-6 mt-5">
    <p>____________________________________________</p>
    <p>Assinatura Técnico</p>
  </div>
  <div class="col-6 mt-5">
    <p>_____________________________________________</p>
    <p>Assinatura Cliente</p>
  </div>
  <div class="col-12 mt-5">
    <h3>TERMO DE INTERESSE</h3>
  </div>
  <div class="col-12 mt-5">
    <h4>
      <p>Declaro não ter interesse em reter a(s) peça(s) substituída(s) para manutenção do produto descrito nesta Ordem de
      Servico.</p>
    </h4>
  </div>
  <br><br>
  <div class="col-12 mt-5">
      <p>__________________________________________</p>
      <p>Visto do Cliente/Consumidor
      </p>
    <h4><p>"O cliente é a pessoa mais importante em qualquer negócio."</p><p>De acordo com o Codigo de Defesa do Consumidor o Serviço tem até 30 dias para consertar o produto.</p>
    </h4>
  </div>
</div>
  
  <?php echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=SAC.php'>";include('../CORE/Footer2.php')?>