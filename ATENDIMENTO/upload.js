// 888     888 d8b          888                 888888b.            888          888    d8b               
// 888     888 Y8P          888                 888  "88b           888          888    Y8P               
// 888     888              888                 888  .88P           888          888                      
// Y88b   d88P 888 .d8888b  888888  8888b.      8888888K.   .d88b.  888  .d88b.  888888 888 88888b.d88b.  
//  Y88b d88P  888 88K      888        "88b     888  "Y88b d88""88b 888 d8P  Y8b 888    888 888 "888 "88b 
//   Y88o88P   888 "Y8888b. 888    .d888888     888    888 888  888 888 88888888 888    888 888  888  888 
//    Y888P    888      X88 Y88b.  888  888 d8b 888   d88P Y88..88P 888 Y8b.     Y88b.  888 888  888  888 
//     Y8P     888  88888P'  "Y888 "Y888888 88P 8888888P"   "Y88P"  888  "Y8888   "Y888 888 888  888  888 
//                                          8P                                                            
//                                          "                                                             
function abre(tipo,modelo)
{    
    if(tipo == 'VISTA')
    {
        titulo = '<i class="fas fa-project-diagram mr-2"></i>Vista Explodida do '+modelo
    }
    else
    {
        titulo = '<i class="far fa-newspaper mr-2"></i>Boletim Técnico do '+modelo
    }
    window.open('https://docs.google.com/gview?embedded=true&url=https://smart.brandev.tech/'+tipo+'/'+modelo+'.pdf', '_blank')
    // webview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=https://smart.brandev.tech/"+tipo+"/"+modelo+".pdf");

    // Swal.fire({
    //     title: titulo,
    //     html: 
    //     '<embed type="application/pdf" src="../'+tipo+'/'+modelo+'.pdf" width="100%" frameborder="0" height="500" style="height: 85vh;"></embed>',
    //     customClass: 'swal',
    //   })
}

// .d88888b.               888                        
// d88P" "Y88b              888                        
// 888     888              888                        
// 888     888 888d888  .d88888  .d88b.  88888b.d88b.  
// 888     888 888P"   d88" 888 d8P  Y8b 888 "888 "88b 
// 888     888 888     888  888 88888888 888  888  888 
// Y88b. .d88P 888     Y88b 888 Y8b.     888  888  888 
//  "Y88888P"  888      "Y88888  "Y8888  888  888  888 
function order(id)
{
    //seletor para os checkbox com name mcheckbox selecionados
    var checkbox = $('input:checkbox[name^=ordem]:checked');
    //verifica se existem checkbox selecionados
    var val = checkbox.length
    // MARCA ORDEM CONFORME CHECADOS
    if(val != 0)
    {
        $('#o'+id).html(val)
    }
    // SE DESMARCAR RETIRA NUMERO
    if($("#i"+id+":checked").length == 0)
    {
        $('#o'+id).html('')
    }

    
}


// 8888888b.           888             
// 888   Y88b          888             
// 888    888          888             
// 888   d88P  .d88b.  888888  8888b.  
// 8888888P"  d88""88b 888        "88b 
// 888 T88b   888  888 888    .d888888 
// 888  T88b  Y88..88P Y88b.  888  888 
// 888   T88b  "Y88P"   "Y888 "Y888888 

function percurso(caminho)
{
    //seletor para os checkbox com name mcheckbox selecionados
    var checkbox = $('input:checkbox[name^=ordem]:checked');
    //verifica se existem checkbox selecionados
    var val = checkbox.length
    if(val > 5)
    {
        Swal.fire(
            'Atenção!',
            'Agende 5 atendimentos no máximo',
            'warning'
        )
    }
    else
    {
        //verifica se existem checkbox selecionados
        if(val > 1)
        {
            //array para armazenar os valores
            var escolhidos = [];
            var ordem = [];
            var nova = [];
            var i = 0;
            //função each para pegar os selecionados
            checkbox.each(function(){
                escolhidos.push($(this).val());
                ordem.push($('#o'+escolhidos[i]).html())
                i++
            });
            var distintos = new Set(ordem)
            if(distintos.size !== ordem.length)
            {
                Swal.fire(
                    'Atenção!',
                    'Ordem de atendimento repetida',
                    'error'
                )

            }
            else
            {
                let rota = [];
                for(a = 0; nova.length < ordem.length; a++)
                {
                    nova.push(escolhidos[ordem[a]-1])
                }
                for(a = 0; rota.length < ordem.length; a++)
                {
                    rota.push(caminho[nova[a]])
                }

                let result = rota.join("/")
                window.open('https://www.google.com.br/maps/dir/R.+Galatea,+1560+-+Carandiru,+São+Paulo+-+SP,+02068-000,+Brasil/'+result)
                
            }
        } 
        else
        {
                Swal.fire(
                    'Atenção!',
                    'Selecione pelo menos <strong>2</strong> atendimentos para gerar uma rota',
                    'warning'
                )
        }

    }

}

//        d8888 888                           888 d8b      888          
//       d88888 888                           888 Y8P      888          
//      d88P888 888                           888          888          
//     d88P 888 888888  .d88b.  88888b.   .d88888 888  .d88888  .d88b.  
//    d88P  888 888    d8P  Y8b 888 "88b d88" 888 888 d88" 888 d88""88b 
//   d88P   888 888    88888888 888  888 888  888 888 888  888 888  888 
//  d8888888888 Y88b.  Y8b.     888  888 Y88b 888 888 Y88b 888 Y88..88P 
// d88P     888  "Y888  "Y8888  888  888  "Y88888 888  "Y88888  "Y88P"  

function atendido(id)
{      
      swal.fire({
        title: '<i class="far fa-clipboard-list-check mr-2"></i>Conclusão do Atendimento<br>ID: '+id,
        html: 
        "<div class='row'>"+
            "<div class='col-12 col-sm-6 mt-4'>"+
                "<button class='btn btn-lg btn-block shadow btn-outline-success' onclick='play(),vibrate(100),conclusao(\"03.PARTWAIT\",\""+id+"\")'><i class='fas fa-pause-circle mr-2'></i>Part Wait</button>"+
            "</div>"+
            "<div class='col-12 col-sm-6 mt-4'>"+
                "<button class='btn btn-lg btn-block shadow btn-outline-success' onclick='play(),vibrate(100),conclusao(\"11.LABORATORIO\",\""+id+"\")'><i class='fas fa-microscope mr-2'></i>Laboratório</button>"+
            "</div>"+
            "<div class='col-12 col-sm-6 mt-4'>"+
                "<button class='btn btn-block btn-lg shadow btn-outline-success' onclick='play(),vibrate(100),conclusao(\"05.OBSERVACAO\",\""+id+"\")'><i class='fas fa-eye mr-2'></i>Observação</button>"+
            "</div>"+
            "<div class='col-12 col-sm-6 mt-4'>"+
                "<button class='btn btn-block btn-lg shadow btn-outline-success' onclick='play(),vibrate(100),conclusao(\"15.FINALIZADO\",\""+id+"\")'><i class='fas fa-flag-checkered mr-2'></i>Finalizado</button>"+
            "</div>"+
            "<div class='col-12 col-sm-6 mt-4'>"+
                "<button class='btn btn-block btn-lg shadow btn-outline-success' onclick='play(),vibrate(100),conclusao(\"09.TRATATIVA\",\""+id+"\")'><i class='fas fa-handshake mr-2'></i>Tratativa Comercial</button>"+
            "</div>"+
            "<div class='col-12 col-sm-6 mt-4'>"+
                "<button class='btn btn-block btn-lg shadow btn-outline-success' onclick='play(),vibrate(100),conclusao(\"06.ORCAMENTO\",\""+id+"\")'><i class='fas fa-cash-register mr-2'></i>Orçamento</button>"+
            "</div>"+
        "</div>",
        showConfirmButton: true,
        confirmButtonText: '<i class="fas fa-arrow-alt-circle-left mr-2"></i>Voltar',
        customClass: 'swal',
        backdrop: `rgba(0,0,0,0.8)`,
      })

}

//  .d8888b.                             888                                     
// d88P  Y88b                            888                                     
// 888    888                            888                                     
// 888         .d88b.  88888b.   .d8888b 888 888  888 .d8888b   8888b.   .d88b.  
// 888        d88""88b 888 "88b d88P"    888 888  888 88K          "88b d88""88b 
// 888    888 888  888 888  888 888      888 888  888 "Y8888b. .d888888 888  888 
// Y88b  d88P Y88..88P 888  888 Y88b.    888 Y88b 888      X88 888  888 Y88..88P 
//  "Y8888P"   "Y88P"  888  888  "Y8888P 888  "Y88888  88888P' "Y888888  "Y88P"  
                                                                     

function conclusao(option,id)
{
    
    switch(option)
    {
        case '03.PARTWAIT':
            titulo = '<i class="fas fa-pause-circle mr-2"></i>Part Wait'
            reparo = ''
            pecas = 
            '<div class="row text-left">'+
                '<div class="col-12 mb-1">'+
                    '<i class="fas fa-cogs mr-2"></i>Peça(s)'+
                '</div>'+
                '<div class="col-12 mb-2">'+
                    '<input type="text" id="peca" name="peca" class="form-control form-control-lg font-weight-bold" required placeholder="Insira o(s) código(s) " >'+
                '</div>'+
            '</div>'
        break;
        case '11.LABORATORIO':
            titulo = "<i class='fas fa-microscope mr-2'></i>Laboratório"
            reparo = ''
            pecas = 
            '<div class="row text-left">'+
                '<div class="col-12 mb-1">'+
                    '<i class="fas fa-cogs mr-2"></i>Peça(s)'+
                '</div>'+
                '<div class="col-12 mb-2">'+
                    '<input type="text" id="peca" name="peca" class="form-control form-control-lg font-weight-bold" required placeholder="Insira o(s) código(s) " >'+
                '</div>'+
            '</div>'
        break;
        case '05.OBSERVACAO':
            titulo = "<i class='fas fa-eye mr-2'></i>Observação"
            reparo = ''
            pecas = 
            '<div class="row text-left">'+
                '<div class="col-12 mb-1">'+
                    '<i class="fas fa-cogs mr-2"></i>Peça(s)'+
                '</div>'+
                '<div class="col-12 mb-2">'+
                    '<input type="text" id="peca" name="peca" class="form-control form-control-lg font-weight-bold" required placeholder="Insira o(s) código(s) " >'+
                '</div>'+
            '</div>'
        break;
        case '15.FINALIZADO':
            titulo = "<i class='fas fa-flag-checkered mr-2'></i>Finalizado"
            reparo = '<div class="row mt-2 justify-content-center"> <div class="col-12 col-sm-6 col-lg-4 mt-3"> <label class="btn-block"><i class="fas fa-spray-can mr-2"></i>REFRIGERAÇÃO</label> <div class="btn-group btn-group-toggle shadow d-flex" data-toggle="buttons"> <label class="btn btn-outline-primary btn-lg w-100" id="R600"> <input type="radio" name="gas" value="R600a" autocomplete="off" id="R600a" onclick="vibrate(100),play()"> R600a </label> <label class="btn btn-outline-primary btn-lg w-100" id="R134"> <input type="radio" name="gas" value="R134a" autocomplete="off" id="R134a" onclick="vibrate(100),play()"> R134a </label> </div> </div> <div class="col-12 col-sm-6 col-lg-4 mt-3"> <label class="btn-block"><i class="fas fa-container-storage mr-2"></i>SISTEMA ALTA</label> <div class="btn-group btn-group-toggle shadow d-flex" data-toggle="buttons"> <label class="btn btn-outline-primary btn-lg w-100" id="Alta0"> <input type="radio" name="alta" id="alta0" value="0" autocomplete="off" onclick="vibrate(100),play()"> Não </label> <label class="btn btn-outline-primary btn-lg w-100" id="Alta1"> <input type="radio" name="alta" id="alta1" value="1" autocomplete="off" onclick="vibrate(100),play()"> Sim </label> </div> </div> </div> </div><div class="row mt-3"> <div class="col-6 mt-3"> <div class="input-group"> <div class="input-group-prepend"> <span class="input-group-text font-weight-bold" id="botaoRefri0"><i class="far fa-temperature-low"></i></span> </div> <select name="T0" id="temp0" class="custom-select font-weight-bold form-control-lg" required> <option value="">REFRIGERADOR</option> <option value="4">+4</option> <option value="3">+3</option> <option value="2">+2</option> <option value="1">+1</option> <option value="0">0</option> <option value="-1">-1</option> <option value="-2">-2</option> <option value="-3">-3</option> <option value="-4">-4</option> <option value="-5">-5</option> <option value="-6">-6</option> <option value="-7">-7</option> <option value="-8">-8</option> </select> </div> </div> <div class="col-6 mt-3"> <div class="input-group"> <div class="input-group-prepend"> <span class="input-group-text font-weight-bold" id="botaoFreez0"><i class="fal fa-temperature-frigid"></i></span> </div> <select name="T1" id="temp1" class="custom-select font-weight-bold" required> <option value="">FREEZER</option> <option value="-8">-8</option> <option value="-9">-9</option> <option value="-10">-10</option> <option value="-11">-11</option> <option value="-12">-12</option> <option value="-13">-13</option> <option value="-14">-14</option> <option value="-15">-15</option> <option value="-16">-16</option> <option value="-17">-17</option> <option value="-18">-18</option> <option value="-19">-19</option> <option value="-20">-20</option> <option value="-21">-21</option> <option value="-22">-22</option> <option value="-23">-23</option> <option value="-24">-24</option> <option value="-25">-25</option> <option value="-26">-26</option> </select> </div> </div> </div> <hr><div class="row justify-content-center" id="servico"> <!-- ADAPTADO --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#adaptado" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-wrench"></i> Adaptado</h5> <span class="badge badge-danger" id="spanAdaptado"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="adaptado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-wrench mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Adaptado...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelCondensador"> <input type="checkbox" autocomplete="off" name="servico[]" value="ADAPTADO_CONDENSADOR" id="condensador" onclick="span(), play()"> Condensador </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelFabricador"> <input type="checkbox" autocomplete="off" name="servico[]" value="ADAPTADO_FABRICADOR_DE_GELO" id="fabricador" onclick="span(),play()"> Fabricador de Gelo </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelLinhadealta"> <input type="checkbox" autocomplete="off" name="servico[]" value="ADAPTADO_LINHA_DE_ALTA" id="linhadealta" onclick="span(),play()"> Linha de Alta </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button> </div> </div> </div> </div> <!-- APLICADO--> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#aplicado" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-syringe"></i> Aplicado</h5> <span class="badge badge-danger" id="spanAplicado"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="aplicado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-syringe mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Aplicado...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelHaste"> <input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_HASTE" id="haste" onclick="span(), play()"> Haste </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelManta"> <input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_MANTA" id="manta" onclick="span(), play()"> Manta </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelRodizio"> <input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_RODIZIO" id="rodizio" onclick="span(), play()"> Rodízio </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelPresilhas"> <input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_PRESILHAS" id="presilhas" onclick="span(), play()"> Presilhas </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelCargagas"> <input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_CARGA_DE_GAS" id="cargagas" onclick="span(), play()"> Carga de Gás </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelTapafuga"> <input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_TAPA_FUGA" id="tapafuga" onclick="span(), play()"> Tapa Fuga </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button> </div> </div> </div> </div> <!-- REOPERADO --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#reoper" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-user-md"></i> Reoperado</h5> <span class="badge badge-danger" id="spanReoperado"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="reoper" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-user-md mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Reoperado...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelCompressor"> <input type="checkbox" autocomplete="off" name="servico[]" value="REOPERADO_COMPRESSOR"  id="compressor" onclick="span(), play()"> Compressor </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button> </div> </div> </div> </div> <!-- REFEITO --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#refeito" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-puzzle-piece"></i> Refeito</h5> <span class="badge badge-danger" id="spanRefeito"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="refeito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-puzzle-piece mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Refeito...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelBaixa"> <input type="checkbox" autocomplete="off" name="servico[]" value="REFEITO_BAIXA" id="baixa" onclick="span(), play()"> Baixa </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="play()">OK</button> </div> </div> </div> </div> <!-- REPARADO --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#reparado" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-tools"></i> Reparado</h5> <span class="badge badge-danger" id="spanReparado"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="reparado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-tools mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Reparado...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelPlaca"> <input type="checkbox" autocomplete="off" name="servico[]" value="REPARADO_PLACA" id="placa" onclick="span(), play()"> Placa </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelCano"> <input type="checkbox" autocomplete="off" name="servico[]" value="REPARADO_CANO" id="cano" onclick="span(), play()"> Tubulação </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelBase"> <input type="checkbox" autocomplete="off" name="servico[]" value="REPARADO_BASE" id="base" onclick="span(), play()"> Base </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-danger" id="labelSem"> <input type="checkbox" autocomplete="off" name="servico[]" value="SEM_REPARO" id="sem" onclick="span(), play()"> Sem Reparo </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button> </div> </div> </div> </div> <!-- TROCADO --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#trocado" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-sync-alt"></i> Trocado</h5> <span class="badge badge-danger" id="spanTrocado"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="trocado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-sync-alt mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Trocado...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-between"> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelCapatraseira"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_CAPA_TRASEIRA" id="capatraseira" onclick="span(), play()"> Capa Traseira </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelCompressor"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_COMPRESSOR" id="tcompressor" onclick="span(), play()"> Compressor </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelCondensadora"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_CONDENSADORA" id="condensadora" onclick="span(), play()"> Condensadora </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelDamper"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_DAMPER" id="damper" onclick="span(), play()"> Damper </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelDobradica"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_DOBRADIÇA" id="dobradica" onclick="span(), play()"> Dobradiça </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelEvaporador"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_EVAPORADOR" id="trocaevaporador" onclick="span(), play()"> Evaporador </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelInterface"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_INTERFACE" id="interface" onclick="span(), play()"> Interface </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelInterruptor"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_INTERRUPTOR" id="interruptor" onclick="span(), play()"> Interruptor de Porta </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelLed"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_LED" id="led" onclick="span(), play()"> Led </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelPenivelador"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PE_NIVELADOR" id="penivelador" onclick="span(), play()"> Pé Nivelador </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelPlacapotencia"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PLACA_POTENCIA" id="placapotencia" onclick="span(), play()"> Placa Potência </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelPorta"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PORTA" id="porta" onclick="span(), play()"> Porta </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelProtetortermico"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PROTETOR_TERMICO" id="protetortermico" onclick="span(), play()"> Protetor Térmico </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelRele"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_RELE" id="rele" onclick="span(), play()"> Relê </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelResistencia"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_RESISTENCIA" id="resistencia" onclick="span(), play()"> Resistência </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelRodizio"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_RODIZIO" id="trodizio" onclick="span(), play()"> Rodízio </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelSensordegelo"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_SENSOR_DEGELO" id="sensordegelo" onclick="span(), play()"> Sensor Degelo </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelSensortemperatura"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_SENSOR_TEMPERATURA" id="sensortemperatura" onclick="span(), play()"> Sensor Temperatura </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelTermofuzivel"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_TERMOFUZIVEL" id="termofuzivel" onclick="span(), play()"> Termofuzível </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelVentilador"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_VENTILADOR" id="ventilador" onclick="span(), play()"> Ventilador </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelGaxeta"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_GAXETA" id="gaxeta" onclick="span(), play()"> Gaxeta </label> </div> <div class="btn-group-toggle mt-2 col-6" data-toggle="buttons"> <label class="btn btn-block btn-outline-success" id="labelFiltro"> <input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_FILTRO" id="filtro" onclick="span(), play()"> Filtro </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal">OK</button> </div> </div> </div> </div> <!-- PRESSURIZADO --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#pressurizado" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-compress-arrows-alt"></i> Pressurizado</h5> <span class="badge badge-danger" id="spanPressurizado"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="pressurizado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-compress-arrows-alt mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Pressurizado...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelSistema"> <input type="checkbox" autocomplete="off" value="PRESSURIZADO_SISTEMA" name="servico[]" id="sistema" onclick="span(), play()"> Sistema </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button> </div> </div> </div> </div> <!-- 8888888888 888 888 8888888     .d88b.   .d88b.   8888b.   .d88b. 888        d88""88b d88P"88b     "88b d88""88b 888        888  888 888  888 .d888888 888  888 888        Y88..88P Y88b 888 888  888 Y88..88P 888         "Y88P"   "Y88888 "Y888888  "Y88P" 888 Y8b d88P "Y88P" --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#fogao" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="far fa-fire-alt"></i> Fogão</h5> <span class="badge badge-danger" id="spanFogao"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="fogao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="far fa-fire-alt mt-2 mr-2"></i> <h5 class="modal-title" id="exampleModalLabel">Fogão...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelVidro"> <input type="checkbox" autocomplete="off" value="VIDRO" name="servico[]" id="vidro" onclick="span(), play()"> Vidro </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelInjetor"> <input type="checkbox" autocomplete="off" value="INJETOR" name="servico[]" id="injetor" onclick="span(), play()"> Injetor </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelSpu"> <input type="checkbox" autocomplete="off" value="SPU" name="servico[]" id="spu" onclick="span(), play()"> SPU </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelTrempe"> <input type="checkbox" autocomplete="off" value="TREMPE" name="servico[]" id="trempe" onclick="span(), play()"> Trempe </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelQueimador"> <input type="checkbox" autocomplete="off" value="QUEIMADOR" name="servico[]" id="queimador" onclick="span(), play()"> Queimador </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelEspalhador"> <input type="checkbox" autocomplete="off" value="ESPALHADOR" name="servico[]" id="espalhador" onclick="span(), play()"> Espalhador </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelChao"> <input type="checkbox" autocomplete="off" value="CHAO" name="servico[]" id="chao" onclick="span(), play()"> Chão </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button> </div> </div> </div> </div> <!-- 888                                      888 888                                      888 888                                      888 888       8888b.  888  888  8888b.   .d88888  .d88b.  888d888  8888b. 888          "88b 888  888     "88b d88" 888 d88""88b 888P"       "88b 888      .d888888 Y88  88P .d888888 888  888 888  888 888     .d888888 888      888  888  Y8bd8P  888  888 Y88b 888 Y88..88P 888     888  888 88888888 "Y888888   Y88P   "Y888888  "Y88888  "Y88P"  888     "Y888888 --> <div class="col-12 col-sm-6 col-md-4"> <div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#lavadora" onclick="vibrate(100),play()"> <div class="card-body"> <h5 class="card-title"><i class="fas fa-dryer-alt mr-2"></i>Lavadora</h5> <span class="badge badge-danger" id="spanLavadora"></span> <h6 class="card-subtitle mb-2 text-muted"></h6> </div> </div> </div> <div class="modal fade" id="lavadora" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"><i class="fas fa-dryer-alt mr-2 mt-2"></i> <h5 class="modal-title" id="exampleModalLabel">Lavadora...</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body bg-light"> <div class="row justify-content-center"> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelAnelHidro"> <input type="checkbox" autocomplete="off" value="ANEL_HIDRO" name="servico[]" id="anelHidro" onclick="span(), play()"> Anel Hidro </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelAnelTanque"> <input type="checkbox" autocomplete="off" value="ANEL_TANQUE" name="servico[]" id="anelTanque" onclick="span(), play()"> Anel Tanque </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelAtuador"> <input type="checkbox" autocomplete="off" value="ATUADOR" name="servico[]" id="atuador" onclick="span(), play()"> Atuador </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelCapacitor"> <input type="checkbox" autocomplete="off" value="CAPACITOR" name="servico[]" id="capacitor" onclick="span(), play()"> Capacitor </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelCesto"> <input type="checkbox" autocomplete="off" value="CESTO" name="servico[]" id="cesto" onclick="span(), play()"> Cesto </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelChicote"> <input type="checkbox" autocomplete="off" value="CHICOTE" name="servico[]" id="chicote" onclick="span(), play()"> Chicote </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelConsole"> <input type="checkbox" autocomplete="off" value="CONSOLE" name="servico[]" id="console" onclick="span(), play()"> Console </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelEletrobomba"> <input type="checkbox" autocomplete="off" value="ELETROBOMBA" name="servico[]" id="eletrobomba" onclick="span(), play()"> Eletrobomba </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelGabinete"> <input type="checkbox" autocomplete="off" value="GABINETE" name="servico[]" id="gabinete" onclick="span(), play()"> Gabinete </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelManupulo"> <input type="checkbox" autocomplete="off" value="MANIPULO" name="servico[]" id="manipulo" onclick="span(), play()"> Manipulo </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelMotor"> <input type="checkbox" autocomplete="off" value="MOTOR" name="servico[]" id="motor" onclick="span(), play()"> Motor </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelPlacaInterface"> <input type="checkbox" autocomplete="off" value="PLACA_INTERFACE" name="servico[]" id="placaInterface" onclick="span(), play()"> Placa Interface </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelPlacaPotencia"> <input type="checkbox" autocomplete="off" value="PLACA_POTENCIA" name="servico[]" id="placaPotencia" onclick="span(), play()"> Placa Potência </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelPressostato"> <input type="checkbox" autocomplete="off" value="PRESSOSTATO" name="servico[]" id="pressostato" onclick="span(), play()"> Pressostato </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelSuporte"> <input type="checkbox" autocomplete="off" value="SUPORTE" name="servico[]" id="suporte" onclick="span(), play()"> Suporte </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelTampaFixa"> <input type="checkbox" autocomplete="off" value="TAMPA_FIXA" name="servico[]" id="tampaFixa" onclick="span(), play()"> Tampa Fixa </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelTampaMovel"> <input type="checkbox" autocomplete="off" value="TAMPA_MOVEL" name="servico[]" id="tampaMovel" onclick="span(), play()"> Tampa Móvel </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelTanque"> <input type="checkbox" autocomplete="off" value="TANQUE" name="servico[]" id="tanque" onclick="span(), play()"> Tanque </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelTirante"> <input type="checkbox" autocomplete="off" value="TIRANTE" name="servico[]" id="tirante" onclick="span(), play()"> Tirante </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelValvula"> <input type="checkbox" autocomplete="off" value="VALVULA" name="servico[]" id="valvula" onclick="span(), play()"> Valvula </label> </div> <div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons"> <label class="btn btn-outline-success" id="labelVaraSuspensao"> <input type="checkbox" autocomplete="off" value="VARA_SUSPENSAO" name="servico[]" id="varaSuspensao" onclick="span(), play()"> Vara Suspensão </label> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button> </div> </div> </div> </div> </div>'
            pecas = ''
        break;
        case '09.TRATATIVA':
            titulo = "<i class='fas fa-handshake mr-2'></i>Tratativa Comercial"
            reparo = ''
            pecas = ''
        break;
        case '06.ORCAMENTO':
            titulo = "<i class='fas fa-cash-register mr-2'></i>Orçamento"
            reparo = ''
            pecas = 
            '<div class="row text-left">'+
                '<div class="col-12 mb-1">'+
                    '<i class="fas fa-cogs mr-2"></i>Peça(s)'+
                '</div>'+
                '<div class="col-12 mb-2">'+
                    '<input type="text" id="peca" name="peca" class="form-control form-control-lg font-weight-bold" required placeholder="Insira o(s) código(s) " >'+
                '</div>'+
            '</div>'
        break;
        case '02.NEGADO':
            titulo = "<i class='fas fa-comment-slash mr-2'></i>Não Atendido"
            reparo = ''
            pecas = ''
        break;
    }
    swal.fire({
        title: titulo,
        showConfirmButton: false,
        html: 
        '<span id="file-name">Primeiro capture a imagem</span>'+
        '<form method="post" action="" enctype="multipart/form-data" id="myform">'+ 
            '<div class="row">'+               
                '<div class="col-6">'+
                    '<label for="file" class="btn btn-lg btn-block btn-success shadow mt-3" onclick="play(),vibrate(100)"><i class="fas fa-camera mr-2"></i>Câmera</label>'+
                    '<input type="file" id="file" name="file"  style="display:none"/>'+ 
                '</div>'+
                '<div class="col-6">'+
                    '<button type="submit" class="btn btn-lg btn-block btn-success shadow mt-3" onclick="play(),vibrate(100)"><i class="fas fa-save fa-lg mr-2"></i>Salvar</button>'+ 
                '</div>'+
            '</div>'+
        '</form>'+
        '<hr>'+
        '<span id="file-audio">Primeiro grave o audio</span>'+
        '<form method="post" action="" enctype="multipart/form-data" id="audio">'+
            '<div class="row">'+
                '<div class="col-6">'+
                    '<label for="ad" class="btn btn-lg btn-block btn-pink shadow mt-3"  onclick="play(),vibrate(100)"><i class="fas fa-microphone-alt mr-2"></i>Mic</label>'+
                    '<input id="ad" type="file" accept="audio/*" capture style="display:none">'+ 
                '</div>'+
                '<div class="col-6">'+
                    '<button type="submit" class="btn btn-lg btn-block btn-pink shadow mt-3" onclick="play(),vibrate(100)"><i class="fas fa-save fa-lg mr-2"></i>Salvar</button>'+ 
                '</div>'+
            '</div>'+
        '</form>'+
        '<hr>'+
        '<form method="post" id="formulario">'+
        '<div class="row text-left" id="avalia">'+
            '<div class="col-12 mb-3">'+
               '<label for="constatado"><i class="fas fa-binoculars mr-2"></i>Constatado</label>'+
                '<input type="text" id="constatado" name="constatado" class="form-control form-control-lg font-weight-bold" required placeholder="Descreva...">'+
            '</div>'+
            '<div class="col-12 mb-3">'+
                '<div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons">'+
                '<label class="btn btn-outline-primary active">'+
                    '<input class="w-100" type="radio" name="tecnico" id="DC" value="DEFEITO CONFERE>" onclick="play(),vibrate(100)" autocomplete="off" checked ><i class="fas fa-user-check mr-2"></i>Defeito Confere'+
                '</label>'+
                '<label class="btn btn-outline-primary">'+
                    '<input class="w-100" type="radio" name="tecnico" id="SD" value="SEM DEFEITO>" onclick="play(),vibrate(100)" autocomplete="off"  ><i class="fas fa-user-times mr-2"></i>Sem Defeito'+
                '</label>'+
                '<label class="btn btn-outline-primary">'+
                    '<input class="w-100" type="radio" name="tecnico" id="OD" value="OUTRO DEFEITO>" onclick="play(),vibrate(100)" autocomplete="off"  ><i class="fas fa-user-edit mr-2"></i>Outro Defeito'+
                '</label>'+
                '</div>'+
            '</div>'+
        '</div>'+
        reparo+
        pecas+           
        '<input type="hidden" name="id" value="'+id+'"></input>'+ 
        '<input type="hidden" name="option" value="'+option+'"></input>'+ 
        '<div class="row mb-2 mt-5 justify-content-center">'+
            '<div class="col-6">'+
                '<button type="button" class="btn btn-lg btn-block btn-secondary shadow" onclick="play(),vibrate(100), atendido(\''+id+'\')"><i class="fas fa-arrow-alt-circle-left mr-2"></i>Volta</button>'+
            '</div>'+
            '<div class="col-6">'+
                '<button type="submit" class="btn btn-lg btn-block btn-danger shadow" onclick="play(),vibrate(100)"><i class="fas fa-concierge-bell mr-2"></i>OK</button>'+
            '</div>'+
        '</div>'+
        '</form>',
        customClass: 'swal',
        backdrop: `rgba(0,0,0,0.8)`,
    }) 
    

    var $input = document.getElementById('file');
    var $input2 = document.getElementById('ad');
    $fileName = document.getElementById('file-name');
    $fileName2 = document.getElementById('file-audio');
    
    $input.addEventListener('change', function(){
    $fileName.textContent = 'Ok. Agora clique em Salvar';
    });
    $input2.addEventListener('change', function(){
    $fileName2.textContent = 'Ok. Agora clique em Salvar';
    });

    // ==================================================================================
    // Image
    // ==================================================================================

    $(function(){
    
        $("#myform").on('submit',function(e){
            e.preventDefault()
    
            var fd = new FormData();
            var files = $('#file')[0].files;
            
            // Check file selected or not
            if(files.length > 0 ){
               fd.append('file',files[0]);
               fd.append('id',id)
    
               $.ajax({
                  url: 'imagem.php',
                  type: 'post',
                  data: fd,
                  contentType: false,
                  processData: false,
                  success: function(){
                    $('#file-name').html('Pronto! Imagem salva!')
                  },
               });
            }else{
               alert("Capture a foto antes de salvar.");
            }
        });
    });

    // =============================================================================
    // Audio
    // =============================================================================

    $(function(){
    
        $("#audio").on('submit',function(e){
            e.preventDefault()
    
            var fd = new FormData();
            var files = $('#ad')[0].files;
            
            // Check file selected or not
            if(files.length > 0 ){
               fd.append('file',files[0]);
               fd.append('id',id)
    
               $.ajax({
                  url: 'audio.php',
                  type: 'post',
                  data: fd,
                  contentType: false,
                  processData: false,
                  success: function(){
                    $('#file-audio').html('Pronto! Audio salvo!')
                  },
               });
            }else{
               alert("Grave o audio antes de salvar.");
            }
        });
    });

    // ==============================================================================
    // CONCLUIR
    // ==============================================================================

    $(function(){    
        $("#formulario").on('submit',function(e){
            e.preventDefault()
            let fd = $(this).serialize()
               $.ajax({
                  url: 'processa.php',
                  type: 'post',
                  data: fd,
                  dataType: 'json',
                  async: true,
                  success: function(retorno){
                        swal.fire({
                            title: 'Parabéns!',
                            text: 'Atendimento concluído com sucesso!',
                          }).then((result)=>{
                            location.reload();
                          })
                        //   ESCONDE O CHAMADO
                        //   $('#'+retorno.smart).fadeOut()
                  },
                  error: function()
                  {
                      console.log('erro')
                  }
               });
        });
    });
}

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

function vibrate(ms){
    navigator.vibrate(ms);
  }