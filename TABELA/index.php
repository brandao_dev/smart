<?php include '../CORE/Header2.php';

$pag = $_GET['M'];
$table = ($pag != 'M') ? 'GET.php' : 'GET_MODEL.php';

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/alasql/0.5.5/alasql.min.js"></script>
<script src="https://cdn.brandev.site/js/ag-grid-enterprise.min.js"></script>
	<!--MENU /////////////////////////////////////////////////////////////////////////////////////////////////////-->
	<div class="row">
		<div class="col-12 col-sm-6 col-md-3 mt-2">
			<a href="GRID.php?M=C" class="btn btn-block btn-outline-primary <?php echo ($pag != "M") ? 'active' : ''?> shadow"><i class="far fa-refrigerator mr-2"></i>Produtos</a>
		</div>		
		<div class="col-12 col-sm-6 col-md-3 mt-2">
			<a href="GRID.php?M=M" class="btn btn-block btn-outline-success <?php echo ($pag == "M") ? 'active' : ''?> shadow"><i class="fas fa-book mr-2"></i>Modelos</a>
		</div>
<a href=""></a>
<!-- SUPER GABIARRA BRABA -->
		<div class="col-12 col-sm-6 col-md-3 mt-2">
			<button class="btn btn-block btn-outline-danger shadow" onclick="busca(<?php echo ($pag != 'M') ? 0 : 1?>)"><i class="fas fa-dna mr-2"></i>Detalhes</button>
		</div>
<?PHP if($pag != 'M') { ?>
		<div class="col-12 col-sm-6 col-md-3 mt-2">
			<button class="btn btn-block btn-outline-warning shadow" onclick="print()"><i class="fas fa-barcode-read mr-2"></i>Impressão</button>
		</div>
<?php } ?>
		<div class="col-12 text-left text-secondary font-weight-bold mt-2">
			<h1><div id="selectedRows"></div></h1>
		</div>
	</div>
<div id="myGrid" style="position:absolute; width: 99%;height:80%" class="ag-theme-alpine-dark"></div>

<script src="/TABELA/fakeServer.js"></script>
<script src="/TABELA/main.js"></script>

    
<?PHP include('../CORE/Footer2.php')?>