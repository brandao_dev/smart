<!DOCTYPE html>

<html lang="pt-br">
<head>
	<title>SMART-ERP</title>
	<meta charset="UTF-8">
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="IMG/SICONP.png"/>
<!--===============================================================================================-->
	<link rel='stylesheet' href='https://cdn.brandev.site/custom/css/custom.css'>
<!--===============================================================================================-->
	<link href='https://cdn.brandev.site/css/all.css' rel='stylesheet'>
<!--===============================================================================================-->
	<script src="https://cdn.brandev.site/custom/js/jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="https://cdn.brandev.site/custom/js/bootstrap.min.js"></script>
	<script src="https://cdn.brandev.site/custom/js/popper.min.js"></script>
<!--===============================================================================================-->
	<script src="/LOGIN/AJAX.js"></script>
	<style>
		.swal {width:980px !important;
		}
		body, html {
  		height: 100%;
		}
		.bg {
		/* BACKGROUD ALEATORIO */
		background-image: url('<?php $a = array('01','02','03','04','05','06','07','08','09','10','11','12'); echo 'IMG/LOGIN/'.$a[array_rand($a)].'.jpg';?>');

		/* Full height */
		height: 100%;

		/* Center and scale the image nicely */
		background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		footer { 
		position:absolute;
		bottom:0;
		z-index: 3;
		}
	</style>
	
</head>
	<body>
	<!-- RETORNO DE AUDIO -->
	<div id="player" style="display: none;"></div>

	<!-- CORPO -->
	<div class="container-fluid bg d-flex justify-content-center bg-dark">
		<div class="row align-self-center ">
			<form action="" method="POST" id="login">

				<!-- QUADRO LOGIN -->
				<div class="card border-cyan shadow-lg bg-dark" id="quadro">
					<div class="card-body">
						<div class="col text-center mt-3">
							<img src="IMG/4.gif" width="50%" alt="SMART">
						</div>
						<div class="col mt-5">
							<span class="text-secondary">
								<i class="fas fa-user mr-2"></i>Nome
							</span>
						</div>
						<div class="col mt-1">
							<input class="form-control form-control-lg font-weight-bold" type="text" name="nome" id="nome">
						</div>
						<div class="col mt-3">
							<span class="text-secondary">
								<i class="fas fa-key mr-2"></i>Senha
							</span>
						</div>
						<div class="col mt-1">
							<input class="form-control form-control-lg font-weight-bold" type="password" name="senha" id="senha">
						</div>
						<div class="col text-center  mt-2 text-danger font-weight-bold" id="erro"></div>
						<div class="col mt-5">
							<button class="btn btn-lg btn-block btn-warning" type="submit" id="botao">
								<i class="fas fa-door-open mr-2"></i>Entre...
							</button>
						</div>
						<div class="card-footer text-secondary text-center mt-5">
						<i class="fas fa-sad-tear mr-2"></i>
							<a href="#" class="text-cyan" style="text-decoration: none;" onclick="telegram($('#nome').val())">Esqueci minha senha</a>
						</div>
					</div>
				</div>

				<!-- QUADRO WAIT -->
				<div class="card shadow-lg bg-dark" id="wait" style="display: none;">
					<img class="card-img-top" src='../IMG/CUBE.gif' class="w-100">
					<div class="card-body">
						<h4 class="card-title text-light">Estamos carregando seu perfil. Aguarde...</h4>
					</div>					
				</div>
			</form>
		</div>
		<footer class="mt-auto font-weight-bold text-light">
          <p>Comercial Smart Outlet, by Br@ndeveloper 2021.</p>
      	</footer>
	</div>
</body>
</html>