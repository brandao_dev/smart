<?php include '../CORE/PDO.php';
$sac = (isset($_POST['sac'])) ? $_POST['sac'] : '';


// SE SAC EXISTE
if($sql = conex()->query("SELECT * FROM sac WHERE SAC = '$sac'")->fetch(PDO::FETCH_ASSOC))
{
    $banco = banco($sql['SMART']);
    $enge = conex()->query("SELECT enge,peca,obs,grupo,datatriagem FROM $banco WHERE smart = '{$sql['SMART']}'")->fetch(PDO::FETCH_ASSOC);

    if(($sql['STATUS'] == '04.AGENDADO' || $sql['STATUS'] == '02.NEGADO') && $equipe == 'AGE')
    {
        $fala = 'Proibido registrar agendamentos nessa tela';
        $caso = '<i class="fas fa-poop mr-2"></i>Proibido registrar agendamentos nessa tela'; 
        $img = '';
        $produzido = 0;
        $retorno = 0;
        $grava = '';
        $repair = '';
        $sons = '';
    }
    else
    {
    
        #VER SE HOUVE REPARO NESTE DIA ANTES DO SAC
        $repair = date("d",strtotime($enge['datatriagem']));
        $hj = date('d');
        $repair = ($repair == $hj) ? 1 : 0;
    
        #BUSCA IMAGEM DO BOOK
        $img = glob('../BOOK/'.$sql['SMART'].'/*.jpg');
    
        #BUSCA GRAVAÇÔES REQUER PHP 7
        $tudo = glob('../AUDIO/ATENDIMENTO/'.$sql['SMART'].'/*.wav');
        $sons = Count($tudo);
        $grava = null;
        foreach($tudo as $item)
        {        
            $grava .= "<p><audio controls><source src='{$item}' type='audio/wav'></audio></p>";
        }
        if($grava == false) { $grava = '' ;}
    
        
        $fala = 'Editar atendimento '.$sac;
        $caso = '<i class="fas fa-edit mr-2"></i>Editar atendimento '.$sac; 
    
        #CHART
        $ano = date("Y");
        $produzido = Count(conex()->query("SELECT id FROM $banco WHERE MODELO LIKE '{$sql['MODELO']}' AND datatriagem LIKE '$ano%' AND rma NOT LIKE '%.%'")->fetchAll(PDO::FETCH_ASSOC));
        $retorno = Count(conex()->query("SELECT id FROM $banco WHERE MODELO LIKE '{$sql['MODELO']}' AND rma LIKE '%.%' AND datatriagem LIKE '$ano%'")->fetchAll(PDO::FETCH_ASSOC));
    
        $total = $produzido + $retorno;
        if($total != 0)
        {
            $produzido = ceil(($produzido * 100)/$total);
            $retorno = ceil(100 - $produzido);
        }  
        else
        {        
            $retorno = 100;
        }
    }

}
else
{
    $fala = 'Ops. Atendimento '.$sac.' inexistente';
    $caso = '<i class="fas fa-ghost mr-2"></i>Ops. Atendimento '.$sac.' inexistente!';
    $img = '';
    $produzido = 0;
    $retorno = 0;
    $grava = '';
    $repair = '';
    $sons = '';
    $enge = null;
    $sql = false;
}

//FALA
// $falas = (!empty($fala)) ? htmlspecialchars($fala) : htmlspecialchars('Ops. atenção no preenchimento');
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player ='';

$soma = ['caso'=>$caso, 'img'=>$img, 'sql'=>$sql, 'fala'=>$player, 'enge'=>$enge,'produzido'=>$produzido,'retorno'=>$retorno, 'grava'=>$grava, 'repair'=>$repair, 'sons'=>$sons];
print json_encode($soma, JSON_UNESCAPED_UNICODE);

?>