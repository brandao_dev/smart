<?php include_once('../CORE/PDO.php');

$min = $_GET['min'];

$minuta = conex()->query("SELECT * FROM pedido WHERE minuta = '$min'")->fetchAll(PDO::FETCH_ASSOC);
$total = Count($minuta);

?>

<link rel='shortcut icon' href='../IMG/SICON.png'>
<link rel='stylesheet' href='https://cdn.brandev.site/css/bootstrap.css'>
<link href='https://cdn.brandev.site/css/all.css' rel='stylesheet'>
<link href='https://cdn.brandev.site/css/style.css' rel='stylesheet'>
<!-- <link href='https://cdn.brandev.site/style/estilo.css' rel='stylesheet'> -->
<script src="https://cdn.brandev.site/js/jquery.js"></script>
<script src="https://cdn.brandev.site/js/bootstrap.js"></script>
<script src="https://cdn.brandev.site/js/popper.js"></script>
<script>
    window.onafterprint=function(){ window.close();}
</script>

<body onload="window.print()">
<!-- MARGEM DO CORPO -->
<div class='container-fluid'>
  <div class="row mt-2 mb-2">
    <div class="col-4"><img src="../IMG/smart.jpg" width="150px"></div>
    <div class="col-8 text-left"><h1>Minuta de Transferência</h1></div>
  </div>

<table class="table table-striped table-bordered text-center">
  <thead>
    <tr>    
        <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">OBSERVAÇÃO</th>
        <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">QTD</th>
        <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">DATA</th>
        <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">USUÁRIO</th>
        <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">MINUTA</th>
    </tr> 
  </thead>
  <tbody>
    <tr>
    	<td></td>
    	<td><?php echo $total;?></td>
    	<td><?PHP echo $time;?></td>
      <td><?PHP echo $user;?></td>
      <td><?PHP echo $min?></td>
    </tr>
  </tbody>
</table>
<table class="table table-striped table-bordered text-center">
  <thead>
    <tr>
      <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">SMART</th>
      <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">MODELO</th>
      <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">LINHA</th>
      <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">EAN</th>
      <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">DESTINO</th>
      <th style="background-color: #ccc !important;-webkit-print-color-adjust: exact">RMA</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($minuta as $row){?>
    <tr>
      <td><?php echo $row['smart'];?></td>
      <td><?php echo $row['modelo'];?></td>
      <td><?php echo $row['linha'];?></td>
      <td><?php echo $row['ean'];?></td>
      <td><?php echo $row['destino'];?></td>
      <td><?php echo $row['reserva'];?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</body>
</html>