<!DOCTYPE html>

<html lang="pt-br">
<head>
	<title>SMART-ERP</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
		<link rel="icon" type="image/png" href="../IMG/WHAT.png"/>
        <meta name="description" content="Author: Brandeveloper, Category: Service App, Length: 2 pages">
        <!-- ================================================================== -->

        <!-- Add to homescreen for Chrome on Android ==========================-->
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="Smart">
        <!-- ================================================================= -->

        <!-- Add to homescreen for Safari on iOS ==============================-->
        <meta name="apple-mobile-web-app-status-bar-style" content="white">
        <meta name="apple-mobile-web-app-title" content="Smart">
        <link rel="apple-touch-icon" href="IMG/SICONP.png">
        <!-- ================================================================= -->

        <!-- Tile for Win8 ====================================================-->
        <meta name="msapplication-TileColor" content="#343A40">
        <meta name="msapplication-TileImage" content="IMG/SICONP.png">
        <!-- ================================================================= -->

        <!-- OG FACEBOOK ===================================================== -->
        <meta property="og:locale" content="pt-br"/>
        <meta property="og:url" content="https://smart.brandev.site/"/>
        <meta property="og:title" content="Smart"/>
        <meta property="og:site_name" content="Smart"/>
        <meta property="og:description" content="Smart"/>
        <meta property="og:image" content="https://smart.brandev.site/IMG/SICONP.png"/>
        <meta property="og:image:type" content="image/png"/>
        <meta property="og:image:width" content="200"/>
        <meta property="og:image:height" content="200"/>
        <meta property="og:type" content="product"/>
        <!-- ================================================================= -->

        <!-- TWITTER ========================================================= -->
        <meta name="twitter:url" content="https://smart.brandev.site/">
        <meta name="twitter:title" content="Smart">
        <meta name="twitter:description" content="Smart">
        <meta name="twitter:image" content="https://smart.brandev.site/IMG/SICONP.png">
        <meta charset="UTF-8">
        <!-- ================================================================= -->
<!--===============================================================================================-->
	<link rel='stylesheet' href='/cdn/custom/css/custom.css'>
<!--===============================================================================================-->
	<link href='/cdn/css/all.css' rel='stylesheet'>
<!--===============================================================================================-->
	<script src="/cdn/custom/js/jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="/cdn/custom/js/bootstrap.min.js"></script>
	<script src="/cdn/custom/js/popper.min.js"></script>
<!--===============================================================================================-->
	<style>
		.swal {width:980px !important;
		}
		body, html {
  		height: 100%;
		}
		.bg {
		/* BACKGROUD ALEATORIO */
		background-image: url('https://images.unsplash.com/photo-1553095066-5014bc7b7f2d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8d2FsbCUyMGJhY2tncm91bmR8ZW58MHx8MHx8&w=1000&q=80');

		/* Full height */
		height: 100%;

		/* Center and scale the image nicely */
		background-position: center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		footer { 
		position:absolute;
		bottom:0;
		z-index: 3;
		}
	</style>
	
</head>
	<body>
	<!-- RETORNO DE AUDIO -->
	<div id="player" style="display: none;"></div>

	<!-- CORPO -->
	<div class="container-fluid bg d-flex justify-content-center bg-dark">
		<div class="row align-self-center "><div class="jumbotron">
		<h1 class="display-4"><i class="fa-solid fa-triangle-exclamation mr-2"></i>Sistema SMART</h1>
		<p class="lead">Para garantir o funcionamento adequado de todos os serviços disponíveis, optamos por interromper o acesso por tempo indeterminado.</p>
		<hr class="my-4">
		<p>Estamos realizando manutenção de rotina para atualizações de plug-ins e APIs.</p>
		</div>
		</div>
		<footer class="mt-auto font-weight-bold text-light">
          <p>Comercial Smart Outlet, by <a href="http://brandev.site">Br@ndev</a> 2022.</p>
      	</footer>
	</div>
</body>
</html>