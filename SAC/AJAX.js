// CARREGA AUTOMATICO URL VAR
$(()=>
{
	window.onload = function()
	{
		const get = window.location.search.split('=')[1]
		if(get != undefined)
		{
			editar(get)
		}
	}
})

// 888b     d888               888          888          
// 8888b   d8888               888          888          
// 88888b.d88888               888          888          
// 888Y88888P888  .d88b.   .d88888  .d88b.  888  .d88b.  
// 888 Y888P 888 d88""88b d88" 888 d8P  Y8b 888 d88""88b 
// 888  Y8P  888 888  888 888  888 88888888 888 888  888 
// 888   "   888 Y88..88P Y88b 888 Y8b.     888 Y88..88P 
// 888       888  "Y88P"   "Y88888  "Y8888  888  "Y88P"  

$(function(){
	$("#model").on('click',function(){
		$.ajax({
		type: "POST",
		dataType: 'json',
		url: "search.php",
		data:'keyword='+$('#smart').val(),
		success: function(data)
		{
			//RESET SAC
			$("#sac").val('')
			//RESET LOCAIS
			$("#loja").html('<i class="fas fa-store-alt mr-2"></i>Loja');
			$("#locais").val('');
			//RESET STATUS
			$("#status").html('<i class="fas fa-bell mr-2"></i>Status');
			$("#01").prop('checked',false);
			$("#03").prop('checked',false);
			$("#04").prop('checked',false);
			$("#05").prop('checked',false);
			$("#07").prop('checked',false);
			$("#11").prop('checked',false);
			$("#12").prop('checked',false);
			$("#14").prop('checked',false);
			$("#15").prop('checked',false);
			$("#06").prop('checked',false);

			//ZERA GRUPO DE DATAS
			$('#datas input').val('');
			$('#agendamento').html('Sem agendamento');
			$('#comprado').html('Não informada');
			$('#nota').val('');
			$('#engenharia').html('Código');
			$("#cliente input").val('');
			$(".temp").remove();
			$(".temo").remove();
			$(".tema").remove();

			//PADRAO TECNICO
			$('#DC').prop('checked',true);
			$('#SD').prop('checked',false);
			$('#OD').prop('checked',false);
			$('#peca').val('Indefinida');

			//IMAGENS
			$('#im').html('');
            
			// //LIMPA LABEL 
			// $('label').removeClass('active')
			// //LIMPA RADIOS
			// $('input[type=radio]').prop('checked',false);
			// //LIMPAR SESSÂO
			// sessionStorage.clear()

			//APAGAR NUMERO ABERTO
			$('#aberto').html('<i class="fas fa-retweet mr-2"></i>RETORNOS');
			$('#player').html(data.fala);
			$('#caso').html(data.caso);
			$('#barra').empty();
			$('#barra').append('<div class="progress-bar progress-bar-animated progress-bar-striped font-weight-bold bg-success" style="width: '+data.produzido+'%;" role="progressbar" aria-valuemin="0" aria-valuemax="100"><h4><i class="fas fa-thumbs-up mr-2"></i>'+data.produzido+'%</h4></div>'+
			'<div class="progress-bar progress-bar-animated progress-bar-striped font-weight-bold bg-secondary" style="width: '+data.retorno+'%;" role="progressbar"  aria-valuemin="0" aria-valuemax="100"><h4><i class="fas fa-thumbs-down mr-2"></i>'+data.retorno+'%</h4></div>');
			// let total = data.produzido+data.retorno;
			// $("#ind").html('<i class="fas fa-chart-pie mr-2"></i>Reparo='+total+' / '+'Retorno='+data.retorno);

			if(data.sql.modelo == null ||data.sql.modelo == undefined || data.sql.modelo == '')
			{
				$('#log').html('<i class="fas fa-pastafarianism mr-2"></i>ERRO NO CADASTRO!');
				Swal.fire(
					{
						title: 'Erro no cadastro!',
						html: 'Enviei um alerta para a administração corrigir o modelo',
						imageUrl: '../IMG/SAC/ERRO.jpg',
						imageWidth: 400,
						imageHeight: 200,
						allowOutsideClick: false
					}
				)	
				//TELEGRAM
				var msg = "Erro no cadastro do modelo do ID: "+data.sql.smart;
				var token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
				//SMART LOGIN CHANNEL
				var chat = '-1001210942378';
				async function file_get_contents(uri, callback) {
					let res = await fetch(uri),
						ret = await res.text(); 
					return callback ? callback(ret) : ret; // a Promise() actually.
				}
				file_get_contents("https://api.telegram.org/bot"+token+"/sendMessage?chat_id="+chat+"&text="+msg);
			}
			else
			{
				$("#log").html('<i class="far fa-ruler-triangle mr-2"></i>'+data.sql.modelo);
			}
			$("#modelo").val(data.sql.modelo);

			console.log(data.sql.voltagem)
			if(data.sql.voltagem == null || data.sql.voltagem == undefined || data.sql.voltagem == '')
			{
				$('#lok').html('<i class="fas fa-pastafarianism mr-2"></i>ERRO NO CADASTRO!');
				Swal.fire(
					{
						title: 'Erro no cadastro!',
						html: 'Enviei um alerta para a administração corrigir a voltagem',
						imageUrl: '../IMG/SAC/ERRO.jpg',
						imageWidth: 400,
						imageHeight: 200,
						allowOutsideClick: false
					}
				)
				//TELEGRAM
				var msg = "Erro no cadastro da voltagem do ID: "+data.sql.smart;
				var token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
				//SMART LOGIN CHANNEL
				var chat = '-1001210942378';
				async function file_get_contents(uri, callback) {
					let res = await fetch(uri),
						ret = await res.text(); 
					return callback ? callback(ret) : ret; // a Promise() actually.
				}
				file_get_contents("https://api.telegram.org/bot"+token+"/sendMessage?chat_id="+chat+"&text="+msg);
			}
			else
			{
				$("#lok").html('<i class="fas fa-bolt mr-2"></i>'+data.sql.voltagem);
			}
			$("#voltagem").val(data.sql.voltagem);
			$("#engenharia").html('<i class="fal fa-user-hard-hat mr-2"></i>'+data.sql.enge);
		}
		});
	});
});

// .d8888b.  8888888888 8888888b.  
// d88P  Y88b 888        888   Y88b 
// 888    888 888        888    888 
// 888        8888888    888   d88P 
// 888        888        8888888P"  
// 888    888 888        888        
// Y88b  d88P 888        888        
//  "Y8888P"  8888888888 888        

function limpa_formulário_cep() {
	//Limpa valores do formulário de cep.
	document.getElementById('rua').value=("");
	document.getElementById('bairro').value=("");
	document.getElementById('cidade').value=("");
}

function meu_callback(conteudo) {
if (!("erro" in conteudo)) {
	//Atualiza os campos com os valores.
	document.getElementById('rua').value=(conteudo.logradouro);
	document.getElementById('bairro').value=(conteudo.bairro);
	document.getElementById('cidade').value=(conteudo.localidade);
} //end if.
else {
	//CEP não Encontrado.
	limpa_formulário_cep();
	Swal.fire(
		{
			title: 'CEP não localizado!',
			text: 'Por favor, verifique se o CEP digitado esta correto',
			imageUrl: '../IMG/SAC/CEP.jpg',
			imageWidth: 400,
			imageHeight: 200,
			allowOutsideClick: false
		})
}
}

function pesquisacep(valor) {

//Nova variável "cep" somente com dígitos.
var cep = valor.replace(/\D/g, '');

//Verifica se campo cep possui valor informado.
if (cep != "") {

	//Expressão regular para validar o CEP.
	var validacep = /^[0-9]{8}$/;

	//Valida o formato do CEP.
	if(validacep.test(cep)) {

		//Preenche os campos com "..." enquanto consulta webservice.
		document.getElementById('rua').value="...";
		document.getElementById('bairro').value="...";
		document.getElementById('cidade').value="...";

		//Cria um elemento javascript.
		var script = document.createElement('script');

		//Sincroniza com o callback.
		script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

		//Insere script no documento e carrega o conteúdo.
		document.body.appendChild(script);

	} //end if.
	else {
		//cep é inválido.
		limpa_formulário_cep();
		Swal.fire(
			{
				title: 'Formato do CEP inválido!',
				text: 'O CEP digitado está fora do padrão!',
				imageUrl: '../IMG/SAC/CEP.jpg',
				imageWidth: 400,
				imageHeight: 200,
				allowOutsideClick: false
			})
	}
} //end if.
else {
	//cep sem valor, limpa formulário.
	limpa_formulário_cep();
}
};

// .d8888b.  888             888                      
// d88P  Y88b 888             888                      
// Y88b.      888             888                      
//  "Y888b.   888888  8888b.  888888 888  888 .d8888b  
//     "Y88b. 888        "88b 888    888  888 88K      
//       "888 888    .d888888 888    888  888 "Y8888b. 
// Y88b  d88P Y88b.  888  888 Y88b.  Y88b 888      X88 
//  "Y8888P"   "Y888 "Y888888  "Y888  "Y88888  88888P' 

function sts(stau)
{
	$('#status').html(stau);
}
function lojas(loj,locais)
{
	$('#loja').html(loj);
	//RESERVA PARA LEVAR PARA UPDATE SEM NECESSIDADE DE ESCOLHER NOVAMENTE
	$('#locais').val(locais);
}

// 88888888888          888           .d888                            
//     888              888          d88P"                             
//     888              888          888                               
//     888      .d88b.  888  .d88b.  888888  .d88b.  88888b.   .d88b.  
//     888     d8P  Y8b 888 d8P  Y8b 888    d88""88b 888 "88b d8P  Y8b 
//     888     88888888 888 88888888 888    888  888 888  888 88888888 
//     888     Y8b.     888 Y8b.     888    Y88..88P 888  888 Y8b.     
// 	   888      "Y8888  888  "Y8888  888     "Y88P"  888  888  "Y8888  
	
//MASK FUNCTION
$(function() {
	VMasker($("#telefone1")).maskPattern('(99)99999-9999');
	VMasker($("#telefone2")).maskPattern('(99)99999-9999');
});


// 8888888b.           888                      
// 888  "Y88b          888                      
// 888    888          888                      
// 888    888  8888b.  888888  8888b.  .d8888b  
// 888    888     "88b 888        "88b 88K      
// 888    888 .d888888 888    .d888888 "Y8888b. 
// 888  .d88P 888  888 Y88b.  888  888      X88 
// 8888888P"  "Y888888  "Y888 "Y888888  88888P' 

jQuery(function($){
	$.datepicker.regional['pt-BR'] = {
			closeText: 'Fechar',
			prevText: 'Mes Anterior',
			nextText: 'Proximo Mes;',
			currentText: 'Hoje',
			monthNames: ['Janeiro','Fevereiro','Marco','Abril','Maio','Junho',
			'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
			'Jul','Ago','Set','Out','Nov','Dez'],
			dayNames: ['Domingo','Segunda-feira','Terca-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
			weekHeader: 'Sm',
			dateFormat: 'dd/mm/yy',
			firstDay: 0,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

$( function() {
  $( "#date1" ).datepicker();
  $( "#date2" ).datepicker();
} );

// 8888888888      888 d8b 888                     
// 888             888 Y8P 888                     
// 888             888     888                     
// 8888888     .d88888 888 888888  8888b.  888d888 
// 888        d88" 888 888 888        "88b 888P"   
// 888        888  888 888 888    .d888888 888     
// 888        Y88b 888 888 Y88b.  888  888 888     
// 8888888888  "Y88888 888  "Y888 "Y888888 888    

function editar(sac)
{
	$.ajax
	(
		{
			type: 'POST',
			dataType: 'json',
			url: 'LEITURA.php',
			async: true,
			data: {sac:sac},
			success: function(data)
			{
				// ESVAZIAR APPEND
				$('#grava').empty()

				//LIMPA PRIMEIRO ANTES DE PREENCHER
				$('html, body').animate({scrollTop:0},200);
				$('#smart').val('');
				$('#barra').empty();
				// $("#ind").html('Índice de retorno do modelo');
				$("#log").html('<i class="far fa-ruler-triangle mr-2"></i>Modelo');
				$('#modelo').val('');
				$("#lok").html('<i class="fas fa-bolt mr-2"></i>Voltagem');
				$('#voltagem').val('');
				$("#constatado").val('');

				//RESET LOCAIS
				$("#loja").html('<i class="fas fa-store-alt mr-2"></i>Loja');
				$("#locais").val('');

				//ZERA GRUPO DE DATAS
				$('#datas input').val('');
				$('#agendamento').html('Sem agendamento');
				$('#comprado').html('Não informada');
				$('#nota').val('');
				$('#engenharia').html('Código');
				$("#cliente input").val('');
				$(".temp").remove();
				$(".temo").remove();
				$(".tema").remove();

				//IMAGENS
				$('#im').html('');

				//RESET CHECLIST
				$("#status").html('<i class="fas fa-bell mr-2"></i>Status');
				$("#01").prop('checked',false);
				$("#03").prop('checked',false);
				$("#04").prop('checked',false);
				$("#05").prop('checked',false);
				$("#07").prop('checked',false);
				$("#11").prop('checked',false);
				$("#12").prop('checked',false);
				$("#14").prop('checked',false);
				$("#15").prop('checked',false);
				$("#06").prop('checked',false);
				$("#00").prop('checked',false);

				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//COMEÇA A PREENCHER
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
				//RETORNO BÁSICO
				$('#player').html(data.fala);
				$('#caso').html(data.caso);


				if(data.sql != false)
				{
					//WHATSAPP E TELEFONE ============================================================================================================
					if(data.sql.TEL.length > 12)
					{
						$('#whats1').empty()
						$('#whats1').append('<a href="https://api.whatsapp.com/send?phone=55'+data.sql.TEL.replace(/([^\d])+/gim, '')+'" target="_blank" class="btn btn-success"><i class="fab fa-whatsapp fa-lg"></i></a><a href="tel:55'+data.sql.TEL.replace(/([^\d])+/gim, '')+'" target="_blank" class="btn btn-purple"><i class="far fa-phone-rotary fa-lg"></i></a>')
					}
					if(data.sql.TEL2.length > 12)
					{
						$('#whats2').empty()
						$('#whats2').append('<a href="https://api.whatsapp.com/send?phone=55'+data.sql.TEL2.replace(/([^\d])+/gim, '')+'" target="_blank" class="btn btn-success"><i class="fab fa-whatsapp fa-lg"></i></a><a href="tel:55'+data.sql.TEL2.replace(/([^\d])+/gim, '')+'" target="_blank" class="btn btn-purple"><i class="far fa-phone-rotary fa-lg"></i></a>')
					}			
	
					//PROIBIDO TECNICO REGISTRAR AGENDAMENTO CONDIÇÃO
					if(data.caso == '<i class="fas fa-poop mr-2"></i>Proibido registrar agendamentos nessa tela')
					{
						Swal.fire(
							{
								title: 'Atenção!',
								html: 'Os agendamentos devem ser registrados somente na tela de <a href="../ATENDIMENTO/index.php">ATENDIMENTO</a> no mesmo dia!',
								imageUrl: '../IMG/SAC/ATENDIMENTO.jpg',
								imageWidth: 400,
								imageHeight: 200,
								allowOutsideClick: false
							}).then((result) => {
							  if (result.isConfirmed) {
								location.reload();
							  }
							})
					}
					else
					{
	
						//PADRAO TECNICO
						$('#DC').prop('checked',true);
						$('#SD').prop('checked',false);
						$('#OD').prop('checked',false);
						$('#peca').val('Indefinida');
	
						//GRAVAÇÔES
						if(data.grava != '')
						{
							if(data.sons != 0)
							{
								$('#pil').html('<i class="fas fa-waveform-path mr-2"></i>GRAVAÇÕES<span class="ml-2 badge badge-pill badge-danger count">'+data.sons+'</span>')
							}
							$('#grava').append(
							'<div class="modal-header">'+
								'<h5 class="modal-title" id="exampleModalLabel">Gravações disponíveis</h5>'+
								'<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">'+
								'<span aria-hidden="true">&times;</span>'+
								'</button>'+
							'</div>'+
							'<div class="modal-body">'+
								data.grava+
							'</div>'+
							'<div class="modal-footer">'+
								'<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>'+
							'</div>')
						}
						else
						{
							$('#grava').append(
							'<div class="modal-header">'+
								'<h5 class="modal-title" id="exampleModalLabel">Nada para se Ouvir</h5>'+
								'<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">'+
								'<span aria-hidden="true">&times;</span>'+
								'</button>'+
							'</div>'+
							'<div class="modal-body">'+
								'<img src="../IMG/SAC/GRAVACAO.jpg" class="w-100" alt="">'+
								'Nenhuma gravação disponível para esse atendimento.'+
							'</div>'+
							'<div class="modal-footer">'+
								'<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>'+
							'</div>')
						};
		
						//MOSTRAR NUMERO ABERTO
						$('#aberto').html('<i class="fas fa-retweet mr-2"></i>'+data.sql.ABERTO+' X')
						//SESSAO PARA DATA REPARO
						sessionStorage.setItem('reparo',data.repair);
						$.each(data.img,function(index,element){
							$('#im').append('<div class="col-12 col-sm-6 col-md-4 col-xl-2 mb-3"><img src="'+element+'" class="w-100 rounded shadow"></div>');
						})
						// #CONFIRMA PREENCHIMENTO DO SAC PORQUE PODE TER ORIGEM EXTERNA
						$('#sac').val(data.sql.SAC);
						$('#smart').val(data.sql.SMART);
		
						//GRAFICO
						$('#barra').append('<div class="progress-bar progress-bar-animated progress-bar-striped font-weight-bold bg-success" style="width: '+data.produzido+'%;" role="progressbar" aria-valuemin="0" aria-valuemax="100"><h4><i class="fas fa-thumbs-up mr-2"></i>'+data.produzido+'%</h4></div>'+
						'<div class="progress-bar progress-bar-animated progress-bar-striped font-weight-bold bg-secondary" style="width: '+data.retorno+'%;" role="progressbar"  aria-valuemin="0" aria-valuemax="100"><h4><i class="fas fa-thumbs-down mr-2"></i>'+data.retorno+'%</h4></div>');
		
						if(data.sql.MODELO != undefined)
						{
							$('#log').html('<i class="far fa-ruler-triangle mr-2"></i>'+data.sql.MODELO);
							$("#modelo").val(data.sql.MODELO);
							$('#lok').html('<i class="fas fa-bolt mr-2"></i>'+data.sql.VOLTAGEM);
							$("#voltagem").val(data.sql.VOLTAGEM);
						}
						else
						{
							$("#log").html('<i class="far fa-ruler-triangle mr-2"></i>Modelo');
							$("#lok").html('<i class="fas fa-bolt mr-2"></i>Voltagem');
						}
						
						// INICAR COM DATA AGENDA, CASO SEJA NOVOS LABS COLOCA DATA COLETA
						$('#agendamento').html(data.sql.AGENDA.replace('_','').slice(-10));

						// ESCOLHA DE ICONES PARA STATUS
						switch(data.sql.STATUS)
						{
							case '01.ABERTO':
								$('#status').html('<i class=\'fas fa-door-open mr-2\'></i>'+data.sql.STATUS);
								$('#01').prop('checked',true);
							break;
							case '03.PARTWAIT':
								$('#status').html('<i class=\'fas fa-pause-circle mr-2\'></i>'+data.sql.STATUS);
								$('#03').prop('checked',true);
							break;
							case '04.AGENDADO':
								$('#status').html('<i class=\'fas fa-home mr-2\'></i>'+data.sql.STATUS);
								$('#04').prop('checked',true);
							break;
							case '05.OBSERVACAO':
								$('#status').html('<i class=\'fas fa-eye mr-2\'></i>'+data.sql.STATUS);
								$('#05').prop('checked',true);
							break;
							case '07.ACORDO':
								$('#status').html('<i class=\'fas fa-handshake mr-2\'></i>'+data.sql.STATUS);
								$('#07').prop('checked',true);
							break;
							case '11.LABORATORIO':
								$('#status').html('<i class=\'fas fa-microscope mr-2\'></i>'+data.sql.STATUS);
								$('#11').prop('checked',true);
							break;
							case '12.DEVOLVIDO':
								$('#status').html('<i class=\'fas fa-undo-alt mr-2\'></i>'+data.sql.STATUS);
								$('#12').prop('checked',true);
							break;
							case '14.CANCELADO':
								$('#status').html('<i class=\'fas fa-bell-slash mr-2\'></i>'+data.sql.STATUS);
								$('#14').prop('checked',true);
							break;
							case '15.FINALIZADO':
								$('#status').html('<i class=\'fas fa-flag-checkered mr-2\'></i>'+data.sql.STATUS);
								$('#15').prop('checked',true);
							break;
							case '06.ORCAMENTO':
								$('#status').html('<i class=\'fas fa-cash-register mr-2\'></i>'+data.sql.STATUS);
								$('#06').prop('checked',true);
							break;
							case '00.REPARADO':
								$('#status').html('<i class=\'fas fa-wrench mr-2\'></i>'+data.sql.STATUS);
								$('#00').prop('checked',true);
							break;
							case '02.NEGADO':
								$('#status').html('<i class=\'fas fa-comment-slash mr-2\'></i>'+data.sql.STATUS);
							break;
							case '09.TRATATIVA':
								$('#status').html('<i class=\'fas fa-handshake mr-2\'></i>'+data.sql.STATUS);
							break;
							case '17.LAB.AGENDADO':
							case '18.LAB.RECEBIDO':
								$('#status').html('<i class=\'fas fa-truck mr-2\'></i>'+data.sql.STATUS);
								$('#agendamento').html(data.sql.COLETA);
							break;
							case undefined:
								$('#status').html('<i class="fas fa-bell mr-2"></i>Status');
							break;
							default:
								$('#status').html('<i class=\'fas fa-scroll-old mr-2\'></i>'+data.sql.STATUS);
								// Swal.fire(
								// 	'Atenção!',
								// 	'<i class="far fa-tired mr-2"></i>Por favor inicie escolhendo um novo Status.',
								// 	'warning'
								// )
						}
						if(data.sql.LOCAL.length != 0)
						{
							$('#loja').html('<i class="fas fa-store-alt mr-2"></i>'+data.sql.LOCAL);
							//RECHECAR LOCAL CONFORME LEITURA
							$('#locais').val(data.sql.LOCAL);
						}
						else
						{
							$("#loja").html('<i class="fas fa-store-alt mr-2"></i>Loja');	
						}
						
						$('#comprado').html(data.sql.COMPRA);
						$('#nota').val(data.sql.NOTA);
						$('#engenharia').html('<i class="fal fa-user-hard-hat mr-2"></i>'+data.enge.enge);
						$('#nome').val(data.sql.CLIENTE);
						$('#telefone1').val(data.sql.TEL);
						$('#telefone2').val(data.sql.TEL2);
						$('#cep').val(data.sql.CEP);
						$('#rua').val(data.sql.ENDERECO);
						$('#bairro').val(data.sql.BAIRRO);
						$('#numero').val(data.sql.NUMERO);
						$('#complemento').val(data.sql.COMPLEMENTO);
						$('#cidade').val(data.sql.CIDADE);
						var queixa = data.sql.QUEIXA.split('/');
						//EACH REVERSE
						$.each
						(
							queixa,function(index,element)
							{
								let q = index+1;
								if(element.length != 0)
								{
									$('#cliente').append('<div class="temp col-12 mb-2"><i class="far fa-angry mr-2"></i>'+q+'º Queixa<div class="alert alert-block alert-danger">'+element+'</div></div>');
								}
							}
						)
						var avalia = data.sql.CONSTATADO.split('/');
						$.each
						(
							avalia,function(index,element)
							{
								let a = index+1;
								if(element.length != 0)
								{
									$('#avalia').append('<div class="temo col-12 mb-2"><i class="far fa-search-plus mr-2"></i>'+a+'º Avaliação<div class="alert alert-block alert-info">'+element+'</div></div>');
								}
							}
						)
						var reparo = data.enge.obs.split('/');
						$.each
						(
							reparo,function(index,element)
							{
								let r = index+1;
								if(element.length != 0)
								{
									$('#reparo').append('<div class="tema col-12 mb-2"><i class="fas fa-hammer mr-2"></i>'+r+'º Reparo<div class="alert alert-block alert-success">'+element+'</div></div>');
								}
							}
						)
						$('#peca').val(data.sql.PECA);
					}
				}
			}
		}
	)
}

// 8888888b.          d8b          888    
// 888   Y88b         Y8P          888    
// 888    888                      888    
// 888   d88P 888d888 888 88888b.  888888 
// 8888888P"  888P"   888 888 "88b 888    
// 888        888     888 888  888 888    
// 888        888     888 888  888 Y88b.  
// 888        888     888 888  888  "Y888 

function printar()
{
	print = $('#sac').val();
	if(print.length != 0)
	{
		window.location="minuta.php?min="+print	
	}
	else
	{  
		Swal.fire(
			{
				title: 'Não entendi seu pedido!',
				html: 'Que tal abrir uma OS antes de imprimir?',
				imageUrl: '../IMG/SAC/PRINT.jpg',
				imageWidth: 400,
				imageHeight: 200,
				allowOutsideClick: false
			}
		)
	}
}    

// 888     888          888 d8b      888                                     
// 888     888          888 Y8P      888                                     
// 888     888          888          888                                     
// Y88b   d88P  8888b.  888 888  .d88888  8888b.   .d8888b  8888b.   .d88b.  
//  Y88b d88P      "88b 888 888 d88" 888     "88b d88P"        "88b d88""88b 
//   Y88o88P   .d888888 888 888 888  888 .d888888 888      .d888888 888  888 
//    Y888P    888  888 888 888 Y88b 888 888  888 Y88b.    888  888 Y88..88P 
// 	   Y8P     "Y888888 888 888  "Y88888 "Y888888  "Y8888P "Y888888  "Y88P"  
	
$(function() 
	{	

		$('#botao').on('click',function() 
			{
				//IMPEDIR REGISTRO DE TECNICO SEM REPARO ANTERIOR
				let reparo = sessionStorage.getItem('reparo');
				if(reparo == 0 && $('#constatado').val() != '')
				{ 
					Swal.fire(
						{
							title: 'Alerta! Faltou o reparo',
							html: 'Registre o REPARO do produto antes de registrar o SAC!',
							imageUrl: '../IMG/SAC/ATENDIMENTO.jpg',
							imageWidth: 400,
							imageHeight: 200,
							allowOutsideClick: false,
							showConfirmButton: true,
							confirmButtonColor: '#09d',
							confirmButtonText:'<a href="../AGING/REFRIGERADOR.php" style="text-decoration:none; color:#fff"><i class="far fa-laugh-wink mr-2"></i>Agora</a>',
							showCancelButton: true,
							cancelButtonColor: '#d56',
							cancelButtonText:'<i class="far fa-tired mr-2"></i>Depois',
						}
					)
				}
				else
				{

					let cas = $('#caso').html();
					//EM ATENDIMENTO
					if(cas.substring(0,26) == '<i class="fas fa-ambulance')
					{ 
						Swal.fire(
							{
								title: 'Produto em atendimento!',
								html: 'Finalize o chamado anterior antes de abrir um novo chamado com esse produto!',
								imageUrl: '../IMG/SAC/ATENDIMENTO.jpg',
								imageWidth: 400,
								imageHeight: 200,
								allowOutsideClick: false
							}
						)	
					}
					else
					{
						//SMART
						if($('#smart').val() == '')
						{ 
							Swal.fire(
								{
									title: 'Atenção!',
									html: 'Para registrar um chamado novo você precisa definir o ID Smart',
									imageUrl: '../IMG/SAC/ETIQUETA.jpg',
									imageWidth: 400,
									imageHeight: 200,
									allowOutsideClick: false
								}
							)
						}
						else
						{
							//VOLTAGEM
							if($('#voltagem').val() == '')
							{
								Swal.fire(
									{
										title: 'Erro no Registro!',
										html: 'A VOLTAGEM está com falha no registro. Comunique a Supervisão',
										imageUrl: '../IMG/SAC/ERRO.jpg',
										imageWidth: 400,
										imageHeight: 200,
										allowOutsideClick: false
									}
								)
							}
							else
							{
								//MODELO
								if($('#modelo').val() == '')
								{
									Swal.fire(
										{
											title: 'Erro no Registro!',
											html: 'O MODELO está com falha no registro. Comunique a Supervisão',
											imageUrl: '../IMG/SAC/ERRO.jpg',
											imageWidth: 400,
											imageHeight: 200,
											allowOutsideClick: false
										}
									)
								}
								else
								{
									//STATUS DE RADIO CONCATENADO
									if(
										$("#01:checked").length == 0 &&
										$("#03:checked").length == 0 &&
										$("#04:checked").length == 0 &&
										$("#05:checked").length == 0 &&
										$("#07:checked").length == 0 &&
										$("#11:checked").length == 0 &&
										$("#12:checked").length == 0 &&
										$("#14:checked").length == 0 &&
										$("#15:checked").length == 0 &&
										$("#00:checked").length == 0 &&
										$("#06:checked").length == 0
										)
									{
										Swal.fire(
											{
												title: 'Ops!',
												html: 'Precisamos Confirmar o STATUS desse chamado...',
												imageUrl: '../IMG/SAC/ETAPA.jpg',
												imageWidth: 400,
												imageHeight: 200,
												allowOutsideClick: false
											}
										)
									}
									else
									{
										//LOJA
										if($('#loja').html() == '<i class="fas fa-store-alt mr-2"></i>Loja')
										{
											Swal.fire(
												{
													title: 'Atenção!',
													html: 'É importante definir a loja de origem!',
													imageUrl: '../IMG/SAC/LOJA.jpg',
													imageWidth: 400,
													imageHeight: 200,
													allowOutsideClick: false
												}
											)
										}
										else
										{
											//REQUIRED DATA AGENDAMENTO (SE O STATUS FOR AGENDAMENTO E NÃO TEM AGENDAMENTO AINDA GRAVADO)
											if($("#04:checked").length != 0 && $('#date1').val() == '' && $('#agendamento').html() == 'Sem agendamento')
											{
												Swal.fire(
													{
														title: 'Atenção!',
														html: 'Sempre que definir o Status como 04.AGENDADO, escolha uma data de agendamento',
														imageUrl: '../IMG/SAC/AGENDA.jpg',
														imageWidth: 400,
														imageHeight: 200,
														allowOutsideClick: false
													}
												)
											}
											else
											{
												// console.log($('#date1').val().length)
												//REQUIRED STATUS AGENDADO SE FOR MARCADO DATA DE AGENDAMENTO
												if($('#user').html() == 'SAC' && $("#04:checked").length == 0 && $('#date1').val().length != 0)
												{
													Swal.fire(
														{
															title: 'Atenção!',
															html: 'Proibido informar data de <strong>agendamento</strong> com status diferente de <strong>04.AGENDADO</strong>',
															imageUrl: '../IMG/SAC/AGENDA.jpg',
															imageWidth: 400,
															imageHeight: 200,
															allowOutsideClick: false
														}
													)
												}
												else
												{
													//SE SAC NOVO
													if($('#sac').val() == '')
													{ 
														//SE NOTA ESTIVER VAZIA
														if($('#nota').val() == '')
														{
															Swal.fire(
																{
																	title: 'Atenção!',
																	html: 'Informe a Nota Fiscal!',
																	imageUrl: '../IMG/SAC/NOTA.jpg',
																	imageWidth: 400,
																	imageHeight: 200,
																	allowOutsideClick: false
																}
															)
														}
														else
														{
															//SE DATACOMPRA VAZIO E DEVOLUÇÃO NÃO ESCOLHIDA
															if($('#date2').val() == '' && $("#12:checked").length == 0)
															{
																Swal.fire(
																	{
																		title: 'Atenção!',
																		html: 'Informe a data da compra para registrar o estado de garantia do produto.',
																		imageUrl: '../IMG/SAC/COMPRA.jpg',
																		imageWidth: 400,
																		imageHeight: 200,
																		allowOutsideClick: false
																	}
																)
															}
															else
															{
																//COMPARAR DATAS PARA ALERTA DE GARANTIA
																let inicial = $('#date2').val();
																let arr = inicial.split('/');
																let compra = new Date(arr[2],arr[1]-1,arr[0]);
																let agora = new Date();
																var timeDiff = Math.abs(agora.getTime() - compra.getTime());
																var dif = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
						
																if(dif > 180)
																{
																	Swal.fire(
																		{
																			title: 'Atenção!',
																			html: 'Este produto esta fora de garantia. Confirma esse atendimento?',
																			imageUrl: '../IMG/SAC/GARANTIA.jpg',
																			imageWidth: 400,
																			imageHeight: 200,
																			showCancelButton: true,
																			confirmButtonText: '<i class="far fa-thumbs-up mr-2"></i>Sim, confirmo!',
																			cancelButtonText: '<i class="far fa-thumbs-down mr-2"></i>Melhor não...',
																			allowOutsideClick: false
																		}
																	).then
																	(
																	(result) =>
																	{
																		if(result.isConfirmed) 
																		{
																			continuar();
																		}
																	}
																	)
																}
																else
																{
																	continuar();
																}
									
															}
														}
					
													}
													else
													{
														continuar();
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		)
	}
)

// .d8888b.                    888    d8b                                    
// d88P  Y88b                   888    Y8P                                    
// 888    888                   888                                           
// 888         .d88b.  88888b.  888888 888 88888b.  888  888  8888b.  888d888 
// 888        d88""88b 888 "88b 888    888 888 "88b 888  888     "88b 888P"   
// 888    888 888  888 888  888 888    888 888  888 888  888 .d888888 888     
// Y88b  d88P Y88..88P 888  888 Y88b.  888 888  888 Y88b 888 888  888 888     
//  "Y8888P"   "Y88P"  888  888  "Y888 888 888  888  "Y88888 "Y888888 888     

window.continuar = function()
{	
	//REQUIRED CLIENTE
	if($('#nome').val() == '')
	{
		Swal.fire(
			{
				title: 'Atenção!',
				html: 'Quer registrar um chamado sem o nome do cliente?',
				imageUrl: '../IMG/SAC/CLIENTE.jpg',
				imageWidth: 400,
				imageHeight: 200,
				allowOutsideClick: false
			}
		)
	}
	else
	{
		//REQUIRED TELEFONE
		if($('#telefone1').val() == '' && $('#user').html() != 'AGE')
		{
			Swal.fire(
				{
					title: 'Atenção!',
					html: 'Faltou pelo menos um telefone do cliente. Se for devolução da loja, complete com zeros..',
					imageUrl: '../IMG/SAC/TELEFONE.jpg',
					imageWidth: 400,
					imageHeight: 200,
					allowOutsideClick: false
				}
			)
		}
		else
		{
			//REQUIRED CEP 
			if($('#cep').val() == '' && $('#user').html() != 'AGE')
			{
				Swal.fire(
					{
						title: 'Atenção!',
						html: 'CEP Obrigatório para comprovação do endereço. Se não souber o CEP <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="blank">procure aqui</a>',
						imageUrl: '../IMG/SAC/CEP.jpg',
						imageWidth: 400,
						imageHeight: 200,
						allowOutsideClick: false
					}
				)
			}
			else
			{
				//REQUIRED RUA
				if($('#rua').val() == '' && $('#user').html() != 'AGE')
				{
					Swal.fire(
						{
							title: 'Atenção!',
							html: 'Faltou a rua. Ja procurou pelo CEP? Se não souber o CEP <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="blank">procure aqui</a>',
							imageUrl: '../IMG/SAC/RUA.jpg',
							imageWidth: 400,
							imageHeight: 200,
							allowOutsideClick: false
						}
					)
				}
				else
				{
					//REQUIRED NUMERO
					if($('#numero').val() == '' && $('#user').html() != 'AGE')
					{
						Swal.fire(
							{
								title: 'Atenção!',
								html: 'Faltou o número da casa. Não pode colocar no mesmo campo que a rua ok...',
								imageUrl: '../IMG/SAC/NUMERO.jpg',
								imageWidth: 400,
								imageHeight: 200,
								allowOutsideClick: false
							}
						)
					}
					else
					{
						//REQUIRED BAIRRO
						if($('#bairro').val() == '' && $('#user').html() != 'AGE')
						{
							Swal.fire(
								{
									title: 'Atenção!',
									html: 'Faltou o bairro.  Ja procurou pelo CEP? Se não souber o CEP <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="blank">procure aqui</a>',
									imageUrl: '../IMG/SAC/BAIRRO.jpg',
									imageWidth: 400,
									imageHeight: 200,
									allowOutsideClick: false
								}
							)
						}
						else
						{
							//REQUIRED CIDADE
							if($('#cidade').val() == '' && $('#user').html() != 'AGE')
							{
								Swal.fire(
									{
										title: 'Atenção!',
										html: 'Faltou preencher a cidade. Ja procurou pelo CEP? Se não souber o CEP <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="blank">procure aqui</a>',
										imageUrl: '../IMG/SAC/CIDADE.jpg',
										imageWidth: 400,
										imageHeight: 200,
										allowOutsideClick: false
									}
								)
							}
							else
							{
								//REQUIRED QUEIXA SE CASO NÂO FOR ATENDIMENTO ENCONTRADO. SINAL QUE É NOVO 
								if(($('#queixa').val() == '')&&($('#sac').val() == ''))
								{
									Swal.fire(
										{
											title: 'Atenção!',
											text: 'Para NOVO chamado é necessário preencher Queixa.',
											imageUrl: '../IMG/SAC/QUEIXA.jpg',
											imageWidth: 400,
											imageHeight: 200,
											allowOutsideClick: false
										}
									)
								}
								else
								{
									//PROIBIDO USAR A PALAVRA URGENTE OU URGENCIA 
									let urgente = $('#queixa').val()
									let baixa = urgente.toLowerCase()
									if((baixa.search("urg") != -1))
									{
										Swal.fire(
											{
												title: 'Atenção!',
												html: 'Não utilize o termo <strong>URGENTE</strong> na queixa. O SAC possui uma ordem própria de atendimento. Descreva apenas o que for importante para o <strong>TÉCNICO</strong> entender o problema',
												imageUrl: '../IMG/SAC/QUEIXA.jpg',
												imageWidth: 400,
												imageHeight: 200,
												allowOutsideClick: false
											}
										)
									}
									else
									{
										//REQUIRED CONSTATADO SE TECNICO
										if($('#constatado').val() == '' & $('#user').html() == 'AGE')
										{
											Swal.fire(
												{
													title: 'Por favor Técnico!',
													html: 'Descreva a(s) falha(s) que você identificou no produto',
													imageUrl: '../IMG/SAC/STATUS.jpg',
													imageWidth: 400,
													imageHeight: 200,
													allowOutsideClick: false
												}
											)
										}
										else
										{
											//CONDIÇÔES PARA DECLARAÇÂO: USER NÂO LIMA E NÃO FINALIZADO
											if($("#15:checked").length != 0 || $('#user').html() == 'AGE')
											{
												adicionar();		
											}
											else
											{
												//  .d8888b.  888                        888      888      d8b          888    
												// d88P  Y88b 888                        888      888      Y8P          888    
												// 888    888 888                        888      888                   888    
												// 888        88888b.   .d88b.   .d8888b 888  888 888      888 .d8888b  888888 
												// 888        888 "88b d8P  Y8b d88P"    888 .88P 888      888 88K      888    
												// 888    888 888  888 88888888 888      888888K  888      888 "Y8888b. 888    
												// Y88b  d88P 888  888 Y8b.     Y88b.    888 "88b 888      888      X88 Y88b.  
												//  "Y8888P"  888  888  "Y8888   "Y8888P 888  888 88888888 888  88888P'  "Y888 
							
												let usuario = $('#usuario').html();
												Swal.fire(
													{
														title: 'Antes de concluir...',
														text: "Eu, "+usuario+", declaro que certifiquei com o cliente todas as dúvidas de uso inadequado, instalação, informações de operação presentes no site oficial ou manual do produto afim de garantir que o chamado técnico seja aberto somente após esgotadas todas as tentativas possíveis de solução pelo próprio cliente. Estou ciente que a ligação para agendamento de atendimento à domicílio será gravada e poderá ser auditada a qualquer tempo.",
														imageUrl: '../IMG/SAC/TEMPERATURA.jpg',
														imageWidth: 400,
														imageHeight: 200,
														showCancelButton: true,
														confirmButtonText: '<i class="far fa-thumbs-up mr-2"></i>Sim, confirmo!',
														cancelButtonText: '<i class="far fa-thumbs-down mr-2"></i>Ainda não...',
														allowOutsideClick: false
													}
												).then
												(
													(result) =>
													{
														if(result.isConfirmed) 
														{
															Swal.fire(
																{
																	title: 'Obrigado pela Declaração!',
																	text: 'Vamos finalizar esse registro',
																	imageUrl: '../IMG/SAC/APROVADO.jpg',
																	imageWidth: 400,
																	imageHeight: 200,
																	allowOutsideClick: false
																}
															);
							
															adicionar();
														}
														else if 
															(
																result.dismiss === Swal.DismissReason.cancel
															) 
														{
															Swal.fire(
																{
																	title: 'Pensando bem...!',
																	text: 'É melhor checarmos algumas possibilidades',
																	imageUrl: '../IMG/SAC/DUVIDA.jpg',
																	imageWidth: 400,
																	imageHeight: 200,
																	allowOutsideClick: false
																})
															//RECEBE NUMERO DO SPAN E REMOVE 1
															// let atual = $('#span'+grupo).html();atual--;
															//ATUALIZA NUMERO DO SPAN
															// $('#span'+grupo).html(atual);
															//SE CONTAGEM SPAN É ZERO ENTÃO NÃO MOSTRA NADA
															// if(atual == 0){$('#span'+grupo).html('');};
															//LIMPA O CHECADO
															// $('#'+posicao).prop('checked',false);
															//LIMPA O ATIVO
															// $('#label'+posicao).removeClass('active');
														}
													}	
												)
											
											}
										}
									}

								}

							}

						}

					}

				}

			}

		}

	}
}
//        d8888      888 d8b          d8b                                    
//       d88888      888 Y8P          Y8P                                    
//      d88P888      888                                                     
//     d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
//    d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
//   d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
//  d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
// d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888 

//FUNÇÂO GLOBAL POR OBJETO WINDOW
window.adicionar = function()
{	
	//LOADING
	$(document).ajaxStart(function(){
		Swal.fire(
			{
				title: 'Por favor aguarde...',
				html: 'Estamos registrando seu chamado',
				showConfirmButton: false,
				allowOutsideClick: false,
				backdrop: 'rgba(0,0,0,0.8)',
                willOpen: () => {
                    Swal.showLoading()
				},
			}
		)
	});
	$(document).ajaxStop(function(){
	  swal.close()
	}); 	

	var data = $('#insert').serialize();
	$.ajax
	(
		{
			type: 'POST',
			dataType: 'json',
			url: 'PROCESSA.php',
			async: true,
			data: data,
			success: function(data)
			{
				$('#aberto').html('<i class="fas fa-retweet mr-2"></i>RETORNOS');
            
				// //LIMPA LABEL 
				// $('label').removeClass('active')
				// //LIMPA INPUTS
				// $(':input').val('')
				// //LIMPA RADIOS
				// $('input[type=radio]').prop('checked',false);
				// //LIMPAR SESSÂO
				// sessionStorage.clear()

				// ESVAZIAR APPEND
				$('#grava').empty()
				$('#grava').append(
				'<div class="modal-header">'+
					'<h5 class="modal-title" id="exampleModalLabel">Nada para se Ouvir</h5>'+
					'<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">'+
					'<span aria-hidden="true">&times;</span>'+
					'</button>'+
				'</div>'+
				'<div class="modal-body">'+
					'<img src="../IMG/SAC/GRAVACAO.jpg" class="w-100" alt="">'+
					'Desculpe. Recurso ainda não disponível. Previsão para conclusão desse complemento em 12/2020'+
				'</div>'+
				'<div class="modal-footer">'+
					'<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>'+
				'</div>');
				$('#barra').empty();
				$('#im').html('');
				$('#player').html(data.fala);
				$('#caso').html(data.caso);	
				$('#smart').val('');
				$('#sac').val('');
				// $('#piechart').html('<img src="../IMG/SAC/GRAP.jpg" width="250px" alt="">');
				// $("#ind").html('Índice de retorno do modelo');
				$("#log").html('<i class="far fa-ruler-triangle mr-2"></i>Modelo');
				$('#modelo').val('');
				$("#lok").html('<i class="fas fa-bolt mr-2"></i>Voltagem');
				$('#voltagem').val('');
				$('#constatado').val('');

				//RESET CHECLIST
				$("#status").html('<i class="fas fa-bell mr-2"></i>Status');
				$("#01").prop('checked',false);
				$("#03").prop('checked',false);
				$("#04").prop('checked',false);
				$("#05").prop('checked',false);
				$("#07").prop('checked',false);
				$("#11").prop('checked',false);
				$("#12").prop('checked',false);
				$("#14").prop('checked',false);
				$("#15").prop('checked',false);
				$("#06").prop('checked',false);

				//RESET LOCAIS
				$("#loja").html('<i class="fas fa-store-alt mr-2"></i>Loja');
				$("#locais").val('');

				//ZERA GRUPO DE DATAS
				$('#datas input').val('');
				$('#agendamento').html('Sem agendamento');
				$('#comprado').html('Não informada');
				$('#nota').html('');
				$('#engenharia').html('Código');
				$("#cliente input").val('');
				$(".temp").remove();
				$(".temo").remove();
				$(".tema").remove();

				//PADRAO TECNICO
				$('#DC').prop('checked',true);
				$('#SD').prop('checked',false);
				$('#OD').prop('checked',false);
				$('#peca').val('');
				// $('html, body').animate({scrollTop:0},500);
			},
			error: function(){
				$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro. Contate o Administrador");
			}
		}
	)	

}
// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

    function vibrate(ms){
		navigator.vibrate(ms);
	  }


//        d8888                                     888             
//       d88888                                     888             
//      d88P888                                     888             
//     d88P 888 888  888 .d8888b   .d88b.  88888b.  888888  .d88b.  
//    d88P  888 888  888 88K      d8P  Y8b 888 "88b 888    d8P  Y8b 
//   d88P   888 888  888 "Y8888b. 88888888 888  888 888    88888888 
//  d8888888888 Y88b 888      X88 Y8b.     888  888 Y88b.  Y8b.     
// d88P     888  "Y88888  88888P'  "Y8888  888  888  "Y888  "Y8888  

function explodida()
{

	//CERTIFICA SE PDF EXISTE
	let modelo = $('#modelo').val();
	$.ajax({
		url: '../VISTA/'+modelo+'.pdf',
		type: 'HEAD',
		success: function()
		{
			Swal.fire(
				{
					title: modelo,
					width: '100%',
					height: '100%',
					html: '<embed type="application/pdf" src="../VISTA/'+modelo+'.pdf" width="100%" frameborder="0" height="100%" style="height: 85vh;"></embed>',
				})
		},
		error: function()
		{
			if(modelo.length == 0)
			{
				Swal.fire(
					{
						title: 'Erro!',
						html: 'Nenhum modelo informado. Abra uma OS existente ou inicie uma nova para abrir a vista explodida.',
						imageUrl: '../IMG/SAC/VAZIO.jpg',
						imageWidth: 400,
						imageHeight: 200
					}
				)
			}
			else
			{
				Swal.fire(
					{
						title: 'Desculpe, documento ausente!',
						html: 'Não se preocupe! Enviei agora um alerta para a administração informando seu pedido. Aguarde o retorno!',
						imageUrl: '../IMG/SAC/AUSENTE.jpg',
						imageWidth: 400,
						imageHeight: 200
					}
				)
				//TELEGRAM
				var msg = "Olá. Gostaria de obter a Vista Expolodida do modelo: "+modelo;
				var token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
				//SMART LOGIN CHANNEL
				var chat = '-1001210942378';
				async function file_get_contents(uri, callback) {
					let res = await fetch(uri),
						ret = await res.text(); 
					return callback ? callback(ret) : ret; // a Promise() actually.
				}
				file_get_contents("https://api.telegram.org/bot"+token+"/sendMessage?chat_id="+chat+"&text="+msg);
			}
		}
	});
}
