//LEITURA
$(function(){
    $('#nome').on('change', function(){
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'PermissaoLeitura.php',
            async: true,
            data: 'nome='+$(this).val(),
            success: function(data){
                //INFORMA SUCESSO
                console.log('sucesso');
                //FEEDBACK AUDIO
                $('#player').html(data.play);
                //FEEDBACK ESCRITO
                $('#caso').html('<i class="fas fa-user-cog mr-2"></i>Editar '+data[0].nome);

                
                //CHECKBOX LIMPAR TODOS
                $('input:checkbox').prop('checked',false);
                //LABEL CLASS SECONDARY
                $('label').removeClass('btn-outline-danger').removeClass('btn-outline-purple').addClass('btn-outline-secondary');
                //LABEL DESATIVA
                $('label').removeClass('active');

                //RADIO DO ATIVO/INATIVO
                let status  = (data[0].status == 1) ? 'active' : 'blocked';
                $("#"+status).prop('checked',true);
                $("label[for='"+status+"']").removeClass('btn-outline-secondary');
                $("label[for='"+status+"']").addClass('btn-outline-danger');
                $("label[for='"+status+"']").addClass('active');
                
                //CHECKBOX DO PADRÃO INSERIR ESPECIFICO
                let input = data[0].padrao+data[0].padrao;
                $('#'+input).prop("checked", true);
                //DESABILITAR TRAVAR TODOS
                $('#'+input).attr("disabled",true);
                //GUARDA INPUT NA SESSÃO PARA USAR DEPOIS
                sessionStorage.setItem("input",data[0].padrao);
                
                //LABEL DO PADRÃO INSERIR ESPECIFICO
                $('label[for="'+input+'"]').addClass('active');
                $('label[for="'+input+'"]').removeClass('btn-outline-secondary');
                $('label[for="'+input+'"]').addClass('btn-outline-danger');

                //CONVERTE PERMISSAO EM ARRAY
                let permissao = data[0].permissao.split('.');
                $.each(permissao, function(index,element)
                {
                    //CHECKBOX DA PERMISSAO
                    $('#'+element).prop("checked",true);
                    //LABEL DA PERMISSÃO
                    $('label[for="'+element+'"]').addClass("active");
                    $('label[for="'+element+'"]').removeClass("btn-outline-secondary");
                    $('label[for="'+element+'"]').addClass("btn-outline-purple");
                    //REABILITAR PADRÔES QUE TEM PAGINA ATRIBUIDA
                    $('#'+element+element).attr("disabled",false);
                })
                //SENHA
                $('#senha').val(data[0].senha);
            },
            error: function(){
                console.log('deu ruim')
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    })
})

// COLOR
function radio(id)
{
    let input = sessionStorage.getItem('input')
    //DESABILITAR PADRÃO DA SESSÃO QUANDO TROCAR
    $('#'+input+input).attr("disabled",false)
    $('label[for="'+input+input+'"]').removeClass('active')
    $('#'+input+input).prop("checked",false)
    //GUARDA NA SESSÂO NOVA INTENÇÂO DE PADRÂO APENAS O ID BASE (NAO DUPLICADO)
    let metade = id.substring(0,2);
    sessionStorage.setItem('input',metade);
}

//REABILITAR PADRAO
function reabilita(id)
{
    if($('#'+id+':checked').length > 0)
    {
        $('#'+id+id).attr("disabled",false);
    }
}

//SUBMETER
$(function(){
    $("#form").on('submit',function(e){
        e.preventDefault();

        //VALIDAÇÂO NOME
        if($('#nome').val() == '')
        {
            Swal.fire(
            'Atenção!!',
            'Nenhum NOME foi escolhido na lista',
            'question'
          )
        }
        else
        {
            if($('#senha').val() == '')
            {
                Swal.fire(
                'Atenção!!',
                'Necessário definir uma SENHA',
                'warning'
              )
            }
            else
            {
                if($('input[type=checkbox]:checked').length < 2)
                {
                    Swal.fire(
                    'Atenção!!',
                    'Necessário definir pelo menos UMA TELA INICIAL e UM ACESSO',
                    'warning'
                  ) 
                }
                else
                {
                    let dados = $(this).serialize();
                    $.ajax({
                        dataType: 'json',
                        url: 'PermissaoGravacao.php',
                        async: true,
                        data: dados,
                        type: 'POST',
                        success: function(data){
                            //INFORMA SUCESSO
                            console.log('sucesso');
                            //FEEDBACK AUDIO
                            $('#player').html(data.play);
                            //FEEDBACK ESCRITO
                            $('#caso').html('<i class="fas fa-user-cog mr-2"></i>Usuário atualizado com sucesso!'); 
            
                                
                            //CHECKBOX LIMPAR TODOS
                            $('input:checkbox').prop('checked',false);
                            //LABEL CLASS SECONDARY
                            $('label').removeClass('btn-outline-danger').removeClass('btn-outline-purple').addClass('btn-outline-secondary');
                            //LABEL DESATIVA
                            $('label').removeClass('active');
            
                            //RADIO DO ATIVO/INATIVO
                            let status  = (data[0].status == 1) ? 'active' : 'blocked';
                            $("#"+status).prop('checked',true);
                            $("label[for='"+status+"']").removeClass('btn-outline-secondary');
                            $("label[for='"+status+"']").addClass('btn-outline-danger');
                            $("label[for='"+status+"']").addClass('active');
                            
                            //CHECKBOX DO PADRÃO INSERIR ESPECIFICO
                            let input = data[0].padrao+data[0].padrao;
                            $('#'+input).prop('checked', true);
                            //DESABILITAR TRAVAR
                            $('#'+input).attr("disabled",true);
                            //GUARDA INPUT NA SESSÃO PARA USAR DEPOIS
                            sessionStorage.setItem('input',data[0].padrao);
                            
                            //LABEL DO PADRÃO INSERIR ESPECIFICO
                            $('label[for="'+input+'"]').addClass('active');
                            $('label[for="'+input+'"]').removeClass('btn-outline-secondary');
                            $('label[for="'+input+'"]').addClass('btn-outline-danger');
            
                            //CONVERTE PERMISSAO EM ARRAY
                            let permissao = data[0].permissao.split('.');
                            $.each(permissao, function(index,element)
                            {
                                //CHECKBOX DA PERMISSAO
                                $('#'+element).prop('checked',true);
                                //LABEL DA PERMISSÃO
                                $('label[for="'+element+'"]').addClass('active');
                                $('label[for="'+element+'"]').removeClass('btn-outline-secondary');
                                $('label[for="'+element+'"]').addClass('btn-outline-purple');
                                //REABILITAR PADRÔES QUE TEM PAGINA ATRIBUIDA
                                $('#'+element+element).attr("disabled",false);
                            })
                            //SENHA
                            $('#senha').val(data[0].senha);    
                        },
                        error: function (){
                            console.log('deu ruim');
                        }
                    })
                }
            }

        }
    })

})

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

function vibrate(ms){
    navigator.vibrate(ms);
  }