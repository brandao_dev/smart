<?php $body = '
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Pagamento Serviço Terceiro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body style="margin: 0; padding: 0">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse: collapse" >
            <tr>
              <td>
                <img src="https://smart.brandev.tech/IMG/MAIL/TOP.jpg" alt="SMART" width="100%" style="display: block" />
              </td>
            </tr>
            <tr>
              <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td  align="left" style=" color: #808; font-family: Arial black, sans-serif; font-size: 28px; " >
                      Ref.: '.$_SESSION['referencia'].'
                    </td>
                  </tr>
                  <tr>
                    <td  align="left" style=" color: #0586ff; font-family: Arial black, sans-serif; font-size: 24px; " >
                      Lista de Produtos Reparados - Equipe Lima
                    </td>
                  </tr>
                  <tr>
                    <td  align="left" style=" padding: 10px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px; " >
                      Segue lista de produtos reparados pela equipe Lima. Este é um email automático gerado por sistema de gestão integrada. As informações constantes na planilha em anexo foram pré-aprovados pela Supervisão da Smart que recebe cópia.
                      <p>
                        <br>
                        <br>
                        <h4 style="color: #0586ff;">Clique para baixar arquivo</h4>
                      <a href="https://smart.brandev.tech/PAGAMENTO/PLANILHAS/'.$dia.'.xlsx" download><img src="https://smart.brandev.tech/IMG/MAIL/XLS.png" width="50px" alt="DOWNLOAD"></a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <tr>
                          <td width="260" valign="top">
                          </td>
                          <td
                            style="font-size: 0; line-height: 0"
                            width="20"
                          ></td>
                          <td width="260" style="vertical-align: top">
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
                <td>
                    <img src="https://smart.brandev.tech/IMG/MAIL/DOWN.jpg" alt="SMART" width="100%" style="display: block" />
                </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>';
?>