<?php include '../CORE/Header2.php';?>

<script src="https://cdn.brandev.site/js/ag-grid-enterprise.min.js"></script>
	
<div id="myGrid" style="position:absolute; width: 98%;height:80%" class="ag-theme-alpine"></div>


<script type="text/javascript" charset="utf-8">		
// COLUNAS //////////////////////////////////////////
var columnDefs = 
[
  {headerName: "ALTERAÇÃO", field: "alteracao", filter: "agDateColumnFilter"},
  {headerName: "DATA", field: "pedido", filter: "agTextColumnFilter", hide: true},
  {headerName: "SMART", field: "smart", filter: "agTextColumnFilter"},
  {headerName: "LINHA", field: "linha", filter: "agTextColumnFilter"},
  {headerName: "MODELO", field: "modelo", filter: "agTextColumnFilter"},
  {headerName: "MINUTA", field: "minuta", filter: "agNumberColumnFilter"},
  {headerName: "SAC", field: "reserva", filter: "agSetColumnFilter"},
  {headerName: "DESTINO", field: "destino", filter: "agSetColumnFilter"},
  {headerName: "EAN", field: "ean", filter: "agTextColumnFilter"},
  {headerName: "WEBSAC", field: "websac", filter: "agTextColumnFilter"},
  {headerName: "USUARIO", field: "user", filter: "agSetColumnFilter", hide: true}
];

// SELECIONAR ITENS //////////////////////////////
function onSelectionChanged() {
    var selectedRows = gridOptions.api.getSelectedRows();
    var selectedRowsString = '';
    var imp = '';
    selectedRows.forEach( function(selectedRow, index) {
        if (index!==0) {
            selectedRowsString += ', ';
            imp += ', ';
        }
        selectedRowsString += selectedRow.smart;
        imp += selectedRow.smart;
    });
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
    document.myform.busca.value = selectedRowsString;
    document.formulario.imp.value = imp;
}
</script>		

<script>
// CARREGAR JSON //////////////////////////////////
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    // do http request to get our sample data - not using any framework to keep the example self contained.
    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'GET.php');
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResult = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResult);
        }
    };
});
</script>
<script src="/AGGRID/agGrid.js"></script>
<?php include('../CORE/Footer2.php')?>