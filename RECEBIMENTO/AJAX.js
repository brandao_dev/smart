
//        d8888      888 d8b          d8b                                    
//       d88888      888 Y8P          Y8P                                    
//      d88P888      888                                                     
//     d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
//    d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
//   d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
//  d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
// d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     

	$(function() 
		{			
    		$('#botao').on('click',function(e) 
				{e.preventDefault();
					var data = $('#insert').serialize();

					if($('#marca').val() == '')
					{  
						Swal.fire(
							'Atenção!',
							'Faltou MARCA/FABRICANTE.',
							'warning'
						)
					}
					else
					{

						if($('#modelo').val() == '')
						{  
							Swal.fire(
								'Atenção!',
								'Faltou MODELO.',
								'warning'
							)
						}
						else
						{

							if($('#serial').val() == '')
							{  
								Swal.fire(
									'Atenção!',
									'Faltou SERIAL.',
									'warning'
								)
							}
							else
							{

								if($('#linha').val() == '')
								{  
									Swal.fire(
										'Atenção!',
										'Faltou LINHA.',
										'warning'
									)
								}
								else
								{

									if($('#cor').val() == '')
									{  
										Swal.fire(
											'Atenção!',
											'Faltou COR.',
											'warning'
										)
									}
									else
									{
										$.ajax
										(
											{
												type: 'POST',
												dataType: 'json',
												url: 'PROCESSA.php',
												async: true,
												data: data,
												success: function(data)
												{
													$('#nota').val('');
													$('#marca').val('');
													$('#modelo').val('');
													$('#linha').val('');
													$('#serial').val('');
													$('#cor').val('');
													$('#obs').val('');
													$('#enge').val('');
													$('#enge').on();
													$('#cod').val('');
													$('#player').html(data.fala);
													$('#caso').html(data.caso);
													$('#total').html('<i class="fas fa-box mr-2"></i>'+data.total);
													$('#tab tbody').html('');
													for(var i = 0; i<data.total; i++)
													{
														$("#tab tbody").append(
														"<tr><td>"
														+data.data[i].smart+
														"</td><td>"
														+data.data[i].modelo+
														"</td><td><button class='btn btn-warning btn-block shadow' onclick='play(), vibrate(100), menu(\""+data.data[i].modelo+"\",\""+data.data[i].enge+"\",\""+data.data[i].cor+"\",\""+data.data[i].voltagem+"\",\""+data.data[i].fabricante+"\",\""+data.data[i].linha+"\",\""+data.data[i].smart+"\",\""+data.data[i].id+"\")'><i class='fas fa-folder-open'></i></button></td></tr>"
														);
													}			
												},
												error: function(){
													$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro. Contate o Administrador");
												}
											}
										)
									}
								}
							}
						}
					
					}
				}
			);
		}
	)

// 8888888888                   888          d8b         
// 888                          888          Y8P         
// 888                          888                      
// 8888888    888  888  .d8888b 888 888  888 888 888d888 
// 888        `Y8bd8P' d88P"    888 888  888 888 888P"   
// 888          X88K   888      888 888  888 888 888     
// 888        .d8""8b. Y88b.    888 Y88b 888 888 888     
// 8888888888 888  888  "Y8888P 888  "Y88888 888 888  

function excluir(smart){        
	Swal.fire({
	  title: 'Confirma a exclusão do registro: '+smart+" ?", 
	  showCancelButton: true,
	  confirmButtonText: 'Confirma',
	  cancelButtonText: 'Cancela'
	}).then((result) => {
	  if (result.value) {            
		this.del(smart);             
		//y mostramos un msj sobre la eliminación  
		Swal.fire(
		  'Eliminado!',
		  'O ID foi excluído do Sistema.',
		  'success'
		)
	  }
	})                
} 
//Procedimiento BORRAR.
function del(smart){
	$.ajax
	(
		{
			type: 'POST',
			dataType: 'json',
			url: 'PROCESSA.php',
			async: true,
			data: {smart:smart,ex:'ex'},
			success: function(data)
			{
				$('#enge').val('');
				$('#enge').on();
				$('#code').val('');
				$('#player').html(data.fala);
				$('#caso').html(data.caso);
				$('#total').html('<i class="fas fa-box mr-2"></i>'+data.total);
				$('#tab tbody').html('');
				for(var i = 0; i<data.total; i++)
				{
					$("#tab tbody").append(
					"<tr><td>"
					+data.data[i].smart+
					"</td><td>"
					+data.data[i].modelo+
					"</td><td><button class='btn btn-warning btn-block shadow' onclick='play(), vibrate(100), menu(\""+data.data[i].modelo+"\",\""+data.data[i].enge+"\",\""+data.data[i].cor+"\",\""+data.data[i].voltagem+"\",\""+data.data[i].fabricante+"\",\""+data.data[i].linha+"\",\""+data.data[i].smart+"\",\""+data.data[i].id+"\")'><i class='fas fa-folder-open'></i></button></td></tr>"
					);
				}			
			},
			error: function(){
				$("#caso").html("<i class='fas fa-exclamation-triangle mr-2'></i>Erro. Contate o Administrador");
			}
		}
	);
}

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888     

    function vibrate(ms){
		navigator.vibrate(ms);
	  }

// 888888b.                                           888          
// 888  "88b                                          888          
// 888  .88P                                          888          
// 8888888K.   8888b.  888d888  .d8888b  .d88b.   .d88888  .d88b.  
// 888  "Y88b     "88b 888P"   d88P"    d88""88b d88" 888 d8P  Y8b 
// 888    888 .d888888 888     888      888  888 888  888 88888888 
// 888   d88P 888  888 888     Y88b.    Y88..88P Y88b 888 Y8b.     
// 8888888P"  "Y888888 888      "Y8888P  "Y88P"   "Y88888  "Y8888  

if(window.location.hash.substr(1,2) == "zx"){
	var bc = window.location.hash.substr(3);
	localStorage["barcode"] = decodeURI(window.location.hash.substr(3))
	window.close();
	self.close();
	window.location.href = "about:blank";//In case self.close isn't allowed
}
var changingHash = false;
function onbarcode(event){
	switch(event.type){
		case "hashchange":{
			if(changingHash == true){
				return;
			}
			var hash = window.location.hash;
			if(hash.substr(0,3) == "#zx"){
				hash = window.location.hash.substr(3);
				changingHash = true;
				window.location.hash = event.oldURL.split("\#")[1] || ""
				changingHash = false;
				processBarcode(hash);
			}

			break;
		}
		case "storage":{
			window.focus();
			if(event.key == "barcode"){
				window.removeEventListener("storage", onbarcode, false);
				processBarcode(event.newValue);
			}
			break;
		}
		default:{
			console.log(event)
			break;
		}
	}
}
window.addEventListener("hashchange", onbarcode, false);

function getScan(){
	var href = window.location.href;
	var ptr = href.lastIndexOf("#");
	if(ptr>0){
		href = href.substr(0,ptr);
	}
	window.addEventListener("storage", onbarcode, false);
	setTimeout('window.removeEventListener("storage", onbarcode, false)', 15000);
	localStorage.removeItem("barcode");
	//window.open  (href + "#zx" + new Date().toString());

	if(navigator.userAgent.match(/Firefox/i)){
		//Used for Firefox. If Chrome uses this, it raises the "hashchanged" event only.
		window.location.href =  ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
	}else{
		//Used for Chrome. If Firefox uses this, it leaves the scan window open.
		window.open   ("zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}"));
	}
}

function processBarcode(bc){
	if(bc.length > 20 && bc.length < 24)
	{
		document.getElementById("cod").value = bc;
	}
	else
	{
		Swal.fire(
			'Ops!',
			'Erro na captura. Tente novamente ou digite à mão.',
			'error'

		)
	}
   
	//put your code in place of the line above.
}
	// 8888888888 888    d8b                            888             
	// 888        888    Y8P                            888             
	// 888        888                                   888             
	// 8888888    888888 888  .d88888 888  888  .d88b.  888888  8888b.  
	// 888        888    888 d88" 888 888  888 d8P  Y8b 888        "88b 
	// 888        888    888 888  888 888  888 88888888 888    .d888888 
	// 888        Y88b.  888 Y88b 888 Y88b 888 Y8b.     Y88b.  888  888 
	// 8888888888  "Y888 888  "Y88888  "Y88888  "Y8888   "Y888 "Y888888 
	// 						   888                                   
	// 						   888                                   
	// 						   888                                   
	
	function printar(modelo,enge,cor,voltagem,fabricante,linha,smart)
	{ 
		$(function(){
		$("#1").barcode(smart,"code128",{barWidth:2,barHeight:70,fontSize: 0});
		});   
		Swal.fire({
			html: "<table class='table table-borderless bg-warning' onclick='cont()' id='print'><tr><td><div style='font-size: 20px; font-family: impact'>"+linha+" - "+fabricante+" - "+cor+" "+voltagem+"</div></td></tr><tr><td><div style='font-size: 41px; font-family: Calibri'>"+modelo+" - "+enge+"</div></td></tr><tr><td><div style='margin: -18px; margin-top: 1px' id='1'></div></td></tr><tr><td><div style='font-size: 54px; font-family: Calibri; margin-top:8px'>"+smart+"</div></td></tr></table>",
			showCancelButton: true,
			showConfirmButton: false,
			cancelButtonText: 'Cancela'
		})
	}
	   
	function cont(){
		var conteudo = document.getElementById('print').innerHTML;
		tela_impressao = window.open('about:blank');
		tela_impressao.document.write(conteudo);
		tela_impressao.window.print();
		tela_impressao.window.close();
	}
	// 8888888b.                                                       
	// 888  "Y88b                                                      
	// 888    888                                                      
	// 888    888  .d88b.  .d8888b   .d8888b  8888b.  888d888  8888b.  
	// 888    888 d8P  Y8b 88K      d88P"        "88b 888P"       "88b 
	// 888    888 88888888 "Y8888b. 888      .d888888 888     .d888888 
	// 888  .d88P Y8b.          X88 Y88b.    888  888 888     888  888 
	// 8888888P"   "Y8888   88888P'  "Y8888P "Y888888 888     "Y888888 
	
	function descara(id)
	{
		window.open('ID.php?id='+id)
	}

	// 888b     d888                            
	// 8888b   d8888                            
	// 88888b.d88888                            
	// 888Y88888P888  .d88b.  88888b.  888  888 
	// 888 Y888P 888 d8P  Y8b 888 "88b 888  888 
	// 888  Y8P  888 88888888 888  888 888  888 
	// 888   "   888 Y8b.     888  888 Y88b 888 
	// 888       888  "Y8888  888  888  "Y88888 

	function menu(modelo,enge,cor,voltagem,fabricante,linha,smart,id)
	{
		Swal.fire({
			html: 
			"<tr>"+
				"<td>"+
					"<button class='btn btn-warning btn-block shadow mt-3' onclick='play(), vibrate(100), printar(\""+modelo+"\",\""+enge+"\",\""+cor+"\",\""+voltagem+"\",\""+fabricante+"\",\""+linha+"\",\""+smart+"\")'><h5><i class='fas fa-ticket-alt mr-2'></i>Etiqueta ID</h5></button>"+
				"</td>"+
				"<td>"+
					"<button class='btn btn-primary btn-block shadow mt-3' onclick='play(), vibrate(100), descara(\""+id+"\")'><h5><i class='fas fa-print mr-2'></i>Descaracterização</h5></button>"+
				"</td>"+
				"<td>"+
					"<button class='btn btn-success btn-block shadow mt-3' onclick='play(), vibrate(100), window.open(\"editar.php?id="+smart+"\")'><h5><i class='fas fa-camera mr-2'></i>Digitalização</h5></button>"+
				"</td>"+
				"<td>"+
					"<button class='btn btn-danger btn-block shadow mt-3' onclick='play(), vibrate(100), excluir(\""+smart+"\")' ><h5><i class='fas fa-trash-alt mr-2'></i>Excluir</h5></button>"+
				"</td>"+
			"</tr>",
			showCancelButton: true,
			showConfirmButton: false,
			cancelButtonText: 'Cancela'
		})

	}