<?php 
function conex()
{
    $banco = 'cep';
    $user = 'root';
    $senha = '6364';

    try
    {
        $conn = new PDO("mysql:host=localhost;dbname=$banco",$user,$senha);
        $conn->exec('SET names utf8, time_zone = "-3:00";');
        return $conn;
    }
    catch(PDOException $e)
    {
        echo 'ERRO DE CONEXÂO = '.$e->getMessage();
    }
}

$ceps = conex()->query("SELECT * FROM cep");
$table = null;
foreach($ceps as $cep)
{
    $table .=   '<tr>
                    <td id="cep" onblur="pesquisacep(this.value);">
                        '.$cep['CEP'].'
                    </td>
                </tr>';
}


?>


<script>
// .d8888b.  8888888888 8888888b.  
// d88P  Y88b 888        888   Y88b 
// 888    888 888        888    888 
// 888        8888888    888   d88P 
// 888        888        8888888P"  
// 888    888 888        888        
// Y88b  d88P 888        888        
//  "Y8888P"  8888888888 888        

function limpa_formulário_cep() {
	//Limpa valores do formulário de cep.
	document.getElementById('rua').value=("");
	document.getElementById('bairro').value=("");
	document.getElementById('cidade').value=("");
}

function meu_callback(conteudo) {
if (!("erro" in conteudo)) {
	//Atualiza os campos com os valores.
	document.getElementById('rua').value=(conteudo.logradouro);
	document.getElementById('bairro').value=(conteudo.bairro);
	document.getElementById('cidade').value=(conteudo.localidade);
} //end if.
else {
	//CEP não Encontrado.
	limpa_formulário_cep();
	Swal.fire(
		{
			title: 'CEP não localizado!',
			text: 'Por favor, verifique se o CEP digitado esta correto',
			imageUrl: '../IMG/SAC/CEP.jpg',
			imageWidth: 400,
			imageHeight: 200,
			allowOutsideClick: false
		})
}
}

function pesquisacep(valor) {

//Nova variável "cep" somente com dígitos.
var cep = valor.replace(/\D/g, '');

//Verifica se campo cep possui valor informado.
if (cep != "") {

	//Expressão regular para validar o CEP.
	var validacep = /^[0-9]{8}$/;

	//Valida o formato do CEP.
	if(validacep.test(cep)) {

		//Preenche os campos com "..." enquanto consulta webservice.
		document.getElementById('rua').value="...";
		document.getElementById('bairro').value="...";
		document.getElementById('cidade').value="...";

		//Cria um elemento javascript.
		var script = document.createElement('script');

		//Sincroniza com o callback.
		script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

		//Insere script no documento e carrega o conteúdo.
		document.body.appendChild(script);

	} //end if.
	else {
		//cep é inválido.
		limpa_formulário_cep();
		Swal.fire(
			{
				title: 'Formato do CEP inválido!',
				text: 'O CEP digitado está fora do padrão!',
				imageUrl: '../IMG/SAC/CEP.jpg',
				imageWidth: 400,
				imageHeight: 200,
				allowOutsideClick: false
			})
	}
} //end if.
else {
	//cep sem valor, limpa formulário.
	limpa_formulário_cep();
}
};
</script>



<div>
    <div>
        <table>
            <!-- <input name="nex" id="minut" type="number" value="" style="display: none;"> -->
            <thead>
                <tr>
                    <th>CEP</th>
                    <th>RUA</th>
                    <th>BAIRRO</th>
                    <th>CIDADE</th>
                    <th>ESTADO</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $table; ?>
            </tbody>
        </table>
    </div>
</div>