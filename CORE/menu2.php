﻿<?php

$foto = (file_exists('../PERFIL/'.$_SESSION['MM_Username'].'.jpg')) ? '<img width="40px" style="border-radius: 50%;" class="mr-2" src="../PERFIL/'.$_SESSION['MM_Username'].'.jpg">' :  '<i class="fas fa-user mr-2"></i>';

$itens = '';
$menu = '';

#CRIAR ITENS DO MENU CONFORME PERMISSÃO
function permissao($permissao, $grupo, $list)
{
  if(conex()->query("SELECT id FROM menu WHERE code = '$permissao' AND grupo = '$grupo' AND lista = '$list'")->fetch(PDO::FETCH_ASSOC) == true)
  {
    return '<a class="dropdown-item h5" href="/'.itemMenu($permissao)['url'].'"><i class="'.itemMenu($permissao)['ico2'].' mr-2"></i> '.itemMenu($permissao)['opcao'].'</a>';
  }
}

#TITULO DO SUBMENU
function titulo($grupo)
{
  $ico = conex()->query("SELECT ico1 FROM menu WHERE grupo = '$grupo'")->fetch(PDO::FETCH_ASSOC);
  return '<i class="'.$ico['ico1'].' mr-2"></i>';
}

#STRING PARA ARRAY
$permissoes = explode('.',$_SESSION['permissao']);

#SELEÇÃO DE GRUPOS PERMITIDOS
$codes = conex()->query("SELECT DISTINCT(grupo) FROM menu WHERE LOCATE(code,'{$_SESSION['permissao']}') ORDER BY lista,id")->fetchAll(PDO::FETCH_ASSOC);


//PRA CADA GRUPO EX: MOVIMENTAR, SERVIÇO, EDITAR...
foreach($codes as $grupo)
{
  //SELECIONAR AS LISTAS EXISTENTES EM CADA GRUPO
  $listas = conex()->query("SELECT DISTINCT(lista) FROM menu WHERE grupo = '{$grupo['grupo']}' ORDER BY lista,id")->fetchAll(PDO::FETCH_ASSOC);

  //SEPARAR GRUPOS EM LISTAS (LINHAS BRANCA, MARROM...)
  foreach($listas as $list)
  {
    //PRA CADA PERMISSÂO EM CADA GRUPO EM CADA LISTA EX: A1 DO MOVIMENTAR DA LISTA 1
    foreach($permissoes as $permissao)
    {
      //UM ITEM RECEBE UM LINK DE ACESSO
      $itens .= permissao($permissao, $grupo['grupo'], $list['lista']);
    }

    //ADICIONAR BARRA SE INTENS NÂO ESTIVER VAZIO E NÃO TER TERMINADO COM BARRA
    if((!empty($itens)) && (substr($itens, -11) != 'der"></div>'))
    {
      //ADICIONADO UMA SEPARAÇÃO
      $itens .= '<div class="dropdown-divider"></div>';
    }
  
  }
  
  if(!empty($itens))
  {
    //MENU CONCATENA CADA GRUPO DE ITENS SEPARADOS PELAS LISTAS
    $menu .= '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle text-left" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.titulo($grupo['grupo']).$grupo['grupo'].'</a><div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">'.$itens.'</div></li>';
    $itens = '';
  }
}

//CONSTRUÇÂO FINAL DO MENU
$_SESSION['ACESSO'] = '
<nav class="navbar navbar-expand-xl navbar-dark bg-dark h5">
  <a class="navbar-brand" href="#"><img src="../IMG/4.gif" width="100px"></a>
    
    <button class="navbar-toggler align-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mr-auto">'.
          $menu.'
        </ul>
      </div>

      <div>
        <ul class="navbar-nav">     
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-left" href="#" id="navba" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.
            $foto.$_SESSION['MM_Username'].'
          </a>
          <div class="dropdown-menu" aria-labelledby="navba">
            <a class="dropdown-item h5" href="#" onclick="perfil()"><i class="fas fa-portrait mr-2"></i>PERFIL</a>
            <a class="dropdown-item h5" href="/LOGIN/sair.php"><i class="fas fa-sign-out-alt mr-2"></i>LOGOFF</a>
          </div>
        </li>
        </ul>
      </div>
</nav>';

?>