<?php include('../CORE/PDO.php');

//////////////////////////////////////////////////////////////////////////////////
// SE A SESSÂO FOR OK ///////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// $_SESSION['caso'] = 'fa-thumbs-up';

if(fnmatch('*fa-thumbs-up*',$_SESSION['caso']))
{
	//COR
	if(!empty($_POST['cora']))
	{
		$cor = $_POST['cora'];
	}
	else
	{
		$cor = $_SESSION['cli']['cor'];
	}

	//ETAPA
	$etapa = $_POST['etapa'];

	//INICIO PADRÃO
	$inicio = $_SESSION['cli']['INICIO_TRIAGEM'];
	
	//TEMPERATURAS
	$T0 =  (!empty($_POST['T0'])) ? $_POST['T0'] : '';
	$T1 =  (!empty($_POST['T1'])) ? $_POST['T1'] : '';
	$temperatura = $T0.'.'.$T1;

	//GAS
	$gas = isset($_POST['gas']) ? $_POST['gas'] : '';

	//ALTA
	$alta = isset($_POST['alta']) ? $_POST['alta'] : '';

	//TENSAO
	$tensao = $_POST['tensao'];
	
	// $modelo = ($tensao == '127 V') ? $_SESSION['base127'] : $_SESSION['base220'];

	//SE NUMERO DO COMPRESSOR FOI INSERIDO
	$compBar = (!empty($_POST['compBar'])) ? $_POST['compBar'] : '';
	if(!empty($compBar)) $desc .= 'TROCADO_COMPRESSOR; ';

	//PEÇA
	$peca = $_POST['peca'];

	//SE HOOUVER RMA
	if(fnmatch('*.*',($_SESSION['cli']['rma'])))
	{
		$rma = true;
	}
	else
	{
		$rma = false;
	}

	//SE VIER DA PINTURA, A DATA DE REPARO MANTEM A ORIGINAL
	$triagem = ($_SESSION['cli']['etapa'] == '11.PINTURA') ? $_SESSION['cli']['datatriagem'] : $time;
			
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// ***   DEFINIR ETAPAS  ***   //////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////


		//VERIFICA SE EXISTE A LETRA PROCEL NO BANCO MODELO, CASO CONTRARIO NÃO REGISTRA
		$mods = $_SESSION['cli']['modelo'];
		$grupo = ($teste = conex()->query("SELECT procel FROM modelo WHERE modelo = '$mods'")->fetch(PDO::FETCH_ASSOC)) ? $teste['procel'] : '';
		
		#SE GRUPO NÂO ESTA VAZIO
		if(!empty($grupo))
		{
			# TEMPERATURA VAZIA PARA REFRIGERAÇÂO
			if(($grupo == 'R')&&(empty($T0))&&(empty($T10)))
			{
				$_SESSION['caso'] = '<i class="fas fa-hippo fa-lg mr-2"></i>';
				$fala = 'Atenção '.NOME_MINUSCULO."!! Falta TEMPERATURA!";

			}
			else
			{
				switch($etapa)
				{
					case '03.REPARADO':
						$_SESSION['caso'] = '<i class="fas fa-handshake fa-lg mr-2"></i>';
						$fala = 'Parabens '.NOME_MINUSCULO."!! Produto reparado!";
						$etapa = '03.REPARADO';
						$opt = 'TESTADO_AUTO';
	
						#GRAVA INSTRUÇÔES DO SAC
						if($rma == true)
						{
							conex()->prepare("UPDATE sac SET PECA=? WHERE SMART=? ORDER BY id DESC LIMIT 1")->execute([$peca, $_SESSION['id']]);
						}
	
						// // MANDA TELEGRAM
						// $msg = $user." , O ".$_SESSION['cli']['linha'].", ".$_SESSION['cli']['modelo'].", ID ".$_SESSION['cli']['smart'].", reparo: ".$desc." foi aprovado com êxito!";
						// $token = $_SESSION['token'];
						// $chat = $_SESSION['chat'];
						// file_get_contents("https://api.telegram.org/bot$token/sendMessage?chat_id=$chat&text=$msg");			
					break;
					
					//APAGA TEMPERATURAS E REINICIA INICIO DE TRIAGEM EM REOPERADO, PARTWAIT E SCRAP
					case '10.PARTWAIT':
						$_SESSION['caso'] = '<i class="fas fa-paperclip mr-2"></i>';
						$fala = "OK ".NOME_MINUSCULO.", produto alterado para Part Wait";
						$temperatura = '';						
						$opt = 'PARTWAIT';
					break;
					case '09.SCRAP':
						$_SESSION['caso'] = '<i class="fas fa-trash-restore mr-2"></i>';
						$fala = "OK ".NOME_MINUSCULO.", produto alterado para Scrap";
						$temperatura = '';
						$opt = 'SCRAP';
					break;
				}
	

				//SE EXISTE E NÃO FOI APAGADA A DESCRIÇÃO ORIGINAL
				$desc = $_POST['falso'];
				//BARRA PARA SEPARAR FUTURO ARRAY SE TIVER ALGUMA DESCRIÇÃO
				$barra = (empty($desc)) ? '' : '/';
				//SE NOVO SERVIÇO ESTA VAZIO COLOCA TESTADO AUTO, SE NÃO COLOCA O NOVO SERVIÇO
				$desc .= (empty($_POST['servico'])) ? $barra.$time.'='.$opt.'; ' : $barra.$time.'='.implode('; ',$_POST['servico']).'; ';
				
				// NÃO É POSSIVEL MAIS MUDAR MODELO NESSA TELA
				conex()->prepare("UPDATE {$_SESSION['banco']} SET datatriagem=?, usertriagem=?, voltagem=?, INICIO_TRIAGEM=?, etapa=?, temperatura=?, ALTA=?, obs=?, CARGA=?, COMPRESSOR=?, cor=?, grupo=?, peca=? WHERE smart=?")->execute([$time, $user, $tensao, $inicio, $etapa, $temperatura, $alta, $desc, $gas, $compBar, $cor, $grupo, $peca, $_SESSION['id']]);
			
				#CHAMA FUNCAO LOG	
				setLOG("REPARO",$_SESSION['cli']['rma'],"tensao=$tensao, temp=$temperatura, alta=$alta, desc=$desc, gas=$gas, comp=$compBar, cor=$cor, grupo=$grupo, peça=$peca",$_SESSION['id'],$etapa);

			}

		}
		else
		{
			#CHAMA FUNCAO LOG	(log-filtro-banco-ação-id-modulo)
			setLOG("REPARO",$_SESSION['cli']['rma'],"Falha. Falta letra de Procel no modelo",$_SESSION['id'],$etapa);
			$_SESSION['caso'] = '<i class="fas fa-hippo fa-lg mr-2"></i>';
			$fala = 'Falhou '.NOME_MINUSCULO."!! Falta PROCEL no modelo!";
		}
}

//////////////////////////////////////////////////////////////////////////////
//SESSION NÃO É OK	//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
elseif(fnmatch('*fa-comment-exclamation*',$_SESSION['caso']))
{
	#CHAMA FUNCAO LOG	
	setLOG("REPARO",$_SESSION['cli']['rma'],"Tentativa frustrada de registro, Etapa invalida",$_SESSION['id'],$etapa);
	$_SESSION['caso'] = '<i class="fas fa-hippo fa-lg mr-2"></i>';
	$fala = "ATENÇÃO!".NOME_MINUSCULO.", operação indevida.  Etapa invalida";
}

//////////////////////////////////////////////////////////////////////////////
//SESSION NÃO É OK	//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
elseif(fnmatch('*fa-exclamation-triangle*',$_SESSION['caso']))
{
	#CHAMA FUNCAO LOG	
	setLOG("REPARO",$_SESSION['cli']['rma'],"Tentativa frustrada de registro, Linha inválida",$_SESSION['id'],$etapa);
	$_SESSION['caso'] = '<i class="fas fa-hippo fa-lg mr-2"></i>';
	$fala = "ATENÇÃO!".NOME_MINUSCULO.", operação indevida. Linha inválida";
}


//LER O SAC 
$sac = ($sac = conex()->query("SELECT id, SAC FROM sac WHERE SMART = '{$_SESSION['id']}' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC)) ? $sac['SAC'] : '';


#RESETAR AS SESSOES, EXCETO CASO
unset($_SESSION['id'],$_SESSION['banco'],$_SESSION['aging'], $_SESSION['cli'], $_SESSION['base127'], $_SESSION['base220']);

// $falas = (!empty($fala)) ? htmlspecialchars($fala) : htmlspecialchars('Ops. atenção no preenchimento');
// $falas=rawurlencode($falas);
// $html=file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q='.$falas.'&tl=pt-IN');
// $player="<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></audio>";
$player = '';

// CRIAR OBJETO
$cli = array_merge(array('caso'=>$_SESSION['caso'], 'fala'=>$fala, 'player'=>$player, 'grupo'=>$grupo, 'sac'=>$sac));

echo json_encode($cli);

?>