<!--*********************************************************
                                                               
 .d8888b.                            d8b                   
d88P  Y88b                           Y8P                   
Y88b.                                                      
 "Y888b.    .d88b.  888d888 888  888 888  .d8888b  .d88b.  
    "Y88b. d8P  Y8b 888P"   888  888 888 d88P"    d88""88b 
      "888 88888888 888     Y88  88P 888 888      888  888 
Y88b  d88P Y8b.     888      Y8bd8P  888 Y88b.    Y88..88P 
 "Y8888P"   "Y8888  888       Y88P   888  "Y8888P  "Y88P"

************************************************************-->
	
<div class="row justify-content-center" id="servico">

	<!-- ADAPTADO -->	
	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#adaptado" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-wrench"></i> Adaptado</h5>
				<span class="badge badge-danger" id="spanAdaptado"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="adaptado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-wrench mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Adaptado...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelCondensador">
							<input type="checkbox" autocomplete="off" name="servico[]" value="ADAPTADO_CONDENSADOR" id="condensador" onclick="span(), play()"> Condensador
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelFabricador">
							<input type="checkbox" autocomplete="off" name="servico[]" value="ADAPTADO_FABRICADOR_DE_GELO" id="fabricador" onclick="span(),play()"> Fabricador de Gelo
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelLinhadealta">
							<input type="checkbox" autocomplete="off" name="servico[]" value="ADAPTADO_LINHA_DE_ALTA" id="linhadealta" onclick="span(),play()"> Linha de Alta
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
			</div>
			</div>
		</div>
	</div>

	<!-- APLICADO-->
	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#aplicado" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-syringe"></i> Aplicado</h5>
				<span class="badge badge-danger" id="spanAplicado"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="aplicado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-syringe mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Aplicado...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelHaste">
							<input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_HASTE" id="haste" onclick="span(), play()"> Haste
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelManta">
							<input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_MANTA" id="manta" onclick="span(), play()"> Manta
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelRodizio">
							<input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_RODIZIO" id="rodizio" onclick="span(), play()"> Rodízio
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelPresilhas">
							<input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_PRESILHAS" id="presilhas" onclick="span(), play()"> Presilhas
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelCargagas">
							<input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_CARGA_DE_GAS" id="cargagas" onclick="span(), play()"> Carga de Gás
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelTapafuga">
							<input type="checkbox" autocomplete="off" name="servico[]" value="APLICADO_TAPA_FUGA" id="tapafuga" onclick="span(), play()"> Tapa Fuga
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
			</div>
			</div>
		</div>
	</div>

	<!-- REOPERADO -->
	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#reoper" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-user-md"></i> Reoperado</h5>
				<span class="badge badge-danger" id="spanReoperado"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="reoper" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-user-md mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Reoperado...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelCompressor">
							<input type="checkbox" autocomplete="off" name="servico[]" value="REOPERADO_COMPRESSOR"  id="compressor" onclick="span(), play()"> Compressor
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
			</div>
			</div>
		</div>
	</div>

	<!-- REFEITO -->
	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#refeito" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-puzzle-piece"></i> Refeito</h5>
				<span class="badge badge-danger" id="spanRefeito"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="refeito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-puzzle-piece mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Refeito...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelBaixa">
							<input type="checkbox" autocomplete="off" name="servico[]" value="REFEITO_BAIXA" id="baixa" onclick="span(), play()"> Baixa
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="play()">OK</button>
			</div>
			</div>
		</div>
	</div>

	<!-- REPARADO -->
	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#reparado" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-tools"></i> Reparado</h5>
				<span class="badge badge-danger" id="spanReparado"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="reparado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-tools mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Reparado...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelPlaca">
							<input type="checkbox" autocomplete="off" name="servico[]" value="REPARADO_PLACA" id="placa" onclick="span(), play()"> Placa
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelCano">
							<input type="checkbox" autocomplete="off" name="servico[]" value="REPARADO_CANO" id="cano" onclick="span(), play()"> Tubulação
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelBase">
							<input type="checkbox" autocomplete="off" name="servico[]" value="REPARADO_BASE" id="base" onclick="span(), play()"> Base
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-danger" id="labelSem">
							<input type="checkbox" autocomplete="off" name="servico[]" value="SEM_REPARO" id="sem" onclick="span(), play()"> Sem Reparo
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
			</div>
			</div>
		</div>
	</div>

	<!-- TROCADO -->
	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#trocado" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-sync-alt"></i> Trocado</h5>
				<span class="badge badge-danger" id="spanTrocado"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="trocado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-sync-alt mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Trocado...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-between">
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelCapatraseira">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_CAPA_TRASEIRA" id="capatraseira" onclick="span(), play()"> Capa Traseira
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelCompressor">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_COMPRESSOR" id="tcompressor" onclick="span(), play()"> Compressor
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelCondensadora">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_CONDENSADORA" id="condensadora" onclick="span(), play()"> Condensadora
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelDamper">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_DAMPER" id="damper" onclick="span(), play()"> Damper
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelDobradica">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_DOBRADIÇA" id="dobradica" onclick="span(), play()"> Dobradiça
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelEvaporador">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_EVAPORADOR" id="trocaevaporador" onclick="span(), play()"> Evaporador
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelInterface">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_INTERFACE" id="interface" onclick="span(), play()"> Interface
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelInterruptor">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_INTERRUPTOR" id="interruptor" onclick="span(), play()"> Interruptor de Porta
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelLed">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_LED" id="led" onclick="span(), play()"> Led
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelPenivelador">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PE_NIVELADOR" id="penivelador" onclick="span(), play()"> Pé Nivelador
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelPlacapotencia">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PLACA_POTENCIA" id="placapotencia" onclick="span(), play()"> Placa Potência
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelPorta">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PORTA" id="porta" onclick="span(), play()"> Porta
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelProtetortermico">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_PROTETOR_TERMICO" id="protetortermico" onclick="span(), play()"> Protetor Térmico
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelRele">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_RELE" id="rele" onclick="span(), play()"> Relê
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelResistencia">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_RESISTENCIA" id="resistencia" onclick="span(), play()"> Resistência
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelRodizio">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_RODIZIO" id="trodizio" onclick="span(), play()"> Rodízio
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelSensordegelo">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_SENSOR_DEGELO" id="sensordegelo" onclick="span(), play()"> Sensor Degelo
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelSensortemperatura">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_SENSOR_TEMPERATURA" id="sensortemperatura" onclick="span(), play()"> Sensor Temperatura
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelTermofuzivel">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_TERMOFUZIVEL" id="termofuzivel" onclick="span(), play()"> Termofuzível
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelVentilador">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_VENTILADOR" id="ventilador" onclick="span(), play()"> Ventilador
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelGaxeta">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_GAXETA" id="gaxeta" onclick="span(), play()"> Gaxeta
						</label>
					</div>
					<div class="btn-group-toggle mt-2 col-6" data-toggle="buttons">
						<label class="btn btn-block btn-outline-success" id="labelFiltro">
							<input type="checkbox" autocomplete="off" name="servico[]" value="TROCADO_FILTRO" id="filtro" onclick="span(), play()"> Filtro
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal">OK</button>
			</div>
			</div>
		</div>
	</div>

	<!-- PRESSURIZADO -->
	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#pressurizado" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-compress-arrows-alt"></i> Pressurizado</h5>
				<span class="badge badge-danger" id="spanPressurizado"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="pressurizado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-compress-arrows-alt mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Pressurizado...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelSistema">
							<input type="checkbox" autocomplete="off" value="PRESSURIZADO_SISTEMA" name="servico[]" id="sistema" onclick="span(), play()"> Sistema
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
			</div>
			</div>
		</div>
	</div>

<!-- 
8888888888                                     
888                                            
888                                            
8888888     .d88b.   .d88b.   8888b.   .d88b.  
888        d88""88b d88P"88b     "88b d88""88b 
888        888  888 888  888 .d888888 888  888 
888        Y88..88P Y88b 888 888  888 Y88..88P 
888         "Y88P"   "Y88888 "Y888888  "Y88P"  
                         888                   
                    Y8b d88P                   
                     "Y88P"                    
-->

	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#fogao" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="far fa-fire-alt"></i> Fogão</h5>
				<span class="badge badge-danger" id="spanFogao"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="fogao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="far fa-fire-alt mt-2 mr-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Fogão...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelVidro">
							<input type="checkbox" autocomplete="off" value="VIDRO" name="servico[]" id="vidro" onclick="span(), play()"> Vidro
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelInjetor">
							<input type="checkbox" autocomplete="off" value="INJETOR" name="servico[]" id="injetor" onclick="span(), play()"> Injetor
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelSpu">
							<input type="checkbox" autocomplete="off" value="SPU" name="servico[]" id="spu" onclick="span(), play()"> SPU
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelTrempe">
							<input type="checkbox" autocomplete="off" value="TREMPE" name="servico[]" id="trempe" onclick="span(), play()"> Trempe
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelQueimador">
							<input type="checkbox" autocomplete="off" value="QUEIMADOR" name="servico[]" id="queimador" onclick="span(), play()"> Queimador
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelEspalhador">
							<input type="checkbox" autocomplete="off" value="ESPALHADOR" name="servico[]" id="espalhador" onclick="span(), play()"> Espalhador
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelChao">
							<input type="checkbox" autocomplete="off" value="CHAO" name="servico[]" id="chao" onclick="span(), play()"> Chão
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
			</div>
			</div>
		</div>
	</div>

<!-- 
888                                      888                           
888                                      888                           
888                                      888                           
888       8888b.  888  888  8888b.   .d88888  .d88b.  888d888  8888b.  
888          "88b 888  888     "88b d88" 888 d88""88b 888P"       "88b 
888      .d888888 Y88  88P .d888888 888  888 888  888 888     .d888888 
888      888  888  Y8bd8P  888  888 Y88b 888 Y88..88P 888     888  888 
88888888 "Y888888   Y88P   "Y888888  "Y88888  "Y88P"  888     "Y888888 	
-->

	<div class="col-12 col-sm-6 col-md-4 col-xl-2">
		<div class="card shadow p-1 mb-2"  data-toggle="modal" data-target="#lavadora" onclick="vibrate(100),play()">
			<div class="card-body">
				<h5 class="card-title"><i class="fas fa-dryer-alt mr-2"></i>Lavadora</h5>
				<span class="badge badge-danger" id="spanLavadora"></span>
				<h6 class="card-subtitle mb-2 text-muted"></h6>
			</div>
		</div>
	</div>
	<div class="modal fade" id="lavadora" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header"><i class="fas fa-dryer-alt mr-2 mt-2"></i>
				<h5 class="modal-title" id="exampleModalLabel">Lavadora...</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body bg-light">
				<div class="row justify-content-center">
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelAnelHidro">
							<input type="checkbox" autocomplete="off" value="ANEL_HIDRO" name="servico[]" id="anelHidro" onclick="span(), play()"> Anel Hidro
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelAnelTanque">
							<input type="checkbox" autocomplete="off" value="ANEL_TANQUE" name="servico[]" id="anelTanque" onclick="span(), play()"> Anel Tanque
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelAtuador">
							<input type="checkbox" autocomplete="off" value="ATUADOR" name="servico[]" id="atuador" onclick="span(), play()"> Atuador
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelCapacitor">
							<input type="checkbox" autocomplete="off" value="CAPACITOR" name="servico[]" id="capacitor" onclick="span(), play()"> Capacitor
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelCesto">
							<input type="checkbox" autocomplete="off" value="CESTO" name="servico[]" id="cesto" onclick="span(), play()"> Cesto
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelChicote">
							<input type="checkbox" autocomplete="off" value="CHICOTE" name="servico[]" id="chicote" onclick="span(), play()"> Chicote
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelConsole">
							<input type="checkbox" autocomplete="off" value="CONSOLE" name="servico[]" id="console" onclick="span(), play()"> Console
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelEletrobomba">
							<input type="checkbox" autocomplete="off" value="ELETROBOMBA" name="servico[]" id="eletrobomba" onclick="span(), play()"> Eletrobomba
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelGabinete">
							<input type="checkbox" autocomplete="off" value="GABINETE" name="servico[]" id="gabinete" onclick="span(), play()"> Gabinete
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelManupulo">
							<input type="checkbox" autocomplete="off" value="MANIPULO" name="servico[]" id="manipulo" onclick="span(), play()"> Manipulo
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelMotor">
							<input type="checkbox" autocomplete="off" value="MOTOR" name="servico[]" id="motor" onclick="span(), play()"> Motor
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelPlacaInterface">
							<input type="checkbox" autocomplete="off" value="PLACA_INTERFACE" name="servico[]" id="placaInterface" onclick="span(), play()"> Placa Interface
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelPlacaPotencia">
							<input type="checkbox" autocomplete="off" value="PLACA_POTENCIA" name="servico[]" id="placaPotencia" onclick="span(), play()"> Placa Potência
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelPressostato">
							<input type="checkbox" autocomplete="off" value="PRESSOSTATO" name="servico[]" id="pressostato" onclick="span(), play()"> Pressostato
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelSuporte">
							<input type="checkbox" autocomplete="off" value="SUPORTE" name="servico[]" id="suporte" onclick="span(), play()"> Suporte
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelTampaFixa">
							<input type="checkbox" autocomplete="off" value="TAMPA_FIXA" name="servico[]" id="tampaFixa" onclick="span(), play()"> Tampa Fixa
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelTampaMovel">
							<input type="checkbox" autocomplete="off" value="TAMPA_MOVEL" name="servico[]" id="tampaMovel" onclick="span(), play()"> Tampa Móvel
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelTanque">
							<input type="checkbox" autocomplete="off" value="TANQUE" name="servico[]" id="tanque" onclick="span(), play()"> Tanque
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelTirante">
							<input type="checkbox" autocomplete="off" value="TIRANTE" name="servico[]" id="tirante" onclick="span(), play()"> Tirante
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelValvula">
							<input type="checkbox" autocomplete="off" value="VALVULA" name="servico[]" id="valvula" onclick="span(), play()"> Valvula
						</label>
					</div>
					<div class="btn-group-toggle mt-2 mr-2" data-toggle="buttons">
						<label class="btn btn-outline-success" id="labelVaraSuspensao">
							<input type="checkbox" autocomplete="off" value="VARA_SUSPENSAO" name="servico[]" id="varaSuspensao" onclick="span(), play()"> Vara Suspensão
						</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
			</div>
			</div>
		</div>
	</div>

</div>