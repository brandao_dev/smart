
//GRAFICO HORIZONTAL
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart2);

      function drawChart2() {
		  var JUNHO = 10;
		  var JULHO = 11;
		  var AGOSTO = 44;
		  var SETEMBRO = 74;
		  var OUTUBRO = 14;
		  var NOVEMBRO = 14;
		  var DEZEMBRO = 11;
		  var JANEIRO = 78;
        var data2 = google.visualization.arrayToDataTable([
          ['MES', ''],
          ['SET',  85],
          ['OUT',  77],
          ['NOV',  55],
          ['DEZ',  55],
          ['JAN',  58],
        ]);

        var options2 = {
            legend: { position: "none" },
		    vAxis : {textStyle: {color: '#fff'}},
		    hAxis : {textStyle: {color: '#000',fontSize: 20}},
		    chartArea: {width: '90%',height: '75%'}, 
        };

        var chart2 = new google.visualization.AreaChart(document.getElementById('2'));
        chart2.draw(data2, options2);
      }		

//GRAFICO NANOMETRO      
	   
      google.charts.load('current2', {'packages':['gauge']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
		var gaunge = 81;  
        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Semana', gaunge]
        ]);

        var options = {
          width: 250, height: 250,
          greenFrom: 67, greenTo: 100,
          yellowFrom:33, yellowTo: 67,
          redFrom:0, redTo: 33,
          minorTicks: 1
        };

        var chart = new google.visualization.Gauge(document.getElementById('1'));

        chart.draw(data, options);
      }

//GRAFICO VERTICAL

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);
		
function drawMultSeries()
{
    var DIA1 = 27;		
    var DIA2 = 20;		
    var DIA3 = 15;		
    var DIA4 = 18;		
    var DIA5 = 3;			
    var data3 = google.visualization.arrayToDataTable([
    ['City', 'Testing', {role:'style'}, {role:'annotation'}],
    ['1D', DIA1, 'red', DIA1],
    ['2D', DIA2, 'orange', DIA2],
    ['3D', DIA3, 'yellow', DIA3],
    ['4D', DIA4, 'green', DIA4],
    ['5D', DIA5, 'blue', DIA5]
    ]);

    var options3 = 
    {         
        legend: { position: "none" },
        chartArea: {width: '85%',height: '100%'}, 
        vAxis : {textStyle: {color: '#555',fontSize: 15,bold: true,}}
    };

    var chart3 = new google.visualization.BarChart(document.getElementById('aging'));
    chart3.draw(data3, options3);

    google.visualization.events.addListener(chart3, 'select', select);
    function select() 
    {
        var selection = chart3.getSelection();
        for (var i = 0; i < selection.length; i++) 
        {
            var item = selection[i];
            if (item.row != null && item.column != null) 
            {
                var str = data3.getFormattedValue(item.row, item.column);
            }
        }
        let corpo
        str = parseInt(str-1) 
            $.each(sql, function(index,value){      
                corpo += 
                '<tr>'+
                    '<th>'+sql[index].smart+'</th>'+
                    '<th>'+sql[index].linha+'</th>'+
                    '<th>'+sql[index].modelo+'</th>'+
                    '<th>'+sql[index].enge+'</th>'+
                '</tr>';
                
                //LIMITE de EACH
                return index < str
            })
        let titulo;
        switch(str)
        {
            case 26:
                titulo = '1 - Um dia'
            break;
            case 19:
                titulo = '2 - Dois dias'
            break;
            case 14:
                titulo = '3 - Três dias'
            break;
            case 17:
                titulo = '4 - Quatro dias'
            break;
            case 2:
                titulo = '5 - Cinco dias'
            break;
        }        
        Swal.fire(
            {
                title: titulo,
                html:'<table class="table table-bordered table-hover table-striped table-sm w-100">'+
                        '<thead class="thead-dark">'+
                            '<tr>'+
                                '<th>SMART</th>'+
                                '<th>LINHA</th>'+
                                '<th>MODELO</th>'+
                                '<th>ENGENHARIA</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>'+
                            corpo+
                        '</tbody>'+
                    '</table>',
                customClass: 'swal',
                backdrop: `
                  rgba(0,0,0,0.8)
                `
            }
        );
    }
}
