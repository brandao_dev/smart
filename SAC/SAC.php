<?php include("../CORE/Header2.php");
$sc = (isset($_GET['sc'])) ? 'value="'.$_GET['sc'].'"' : '';
?>
<!-- JS SCRIPTS -->
<script src="AJAX.js?<?php echo filemtime('AJAX.js')?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdn.brandev.site/js/masker.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/blitzer/jquery-ui.css">
<!-- DIVS OCULTAS -->
<div id="player" style="display: none;"><?php echo $player?></div>
<div style="display: none;" id="user"><?php echo $equipe?></div>
<div style="display: none;" id="usuario"><?php echo $user?></div>
<!-- BADGE -->
<style>
    .count{
  vertical-align:middle;
  margin-left:-15px;
  margin-top: -20px;
  font-size:13px;
}
</style>

<!--
8888888888      888 d8b 888                     
888             888 Y8P 888                     
888             888     888                     
8888888     .d88888 888 888888  8888b.  888d888 
888        d88" 888 888 888        "88b 888P"   
888        888  888 888 888    .d888888 888     
888        Y88b 888 888 Y88b.  888  888 888     
8888888888  "Y88888 888  "Y888 "Y888888 888     
 -->
<form action="" method="POST" id="insert">

<div class="row mb-2">
    <!-- FRASE DE APOIO *** //////////////////////// -->
    <div class="col-12">
        <div class="alert mt-3 alert-info font-weight-bold btn-block" id="caso"><i class="fas fa-handshake fa-lg mr-2"></i>Bem vindo(a)! <?php echo $user?></div>
    </div>

    <!-- INPUT DO SAC *** //////////////////////// -->
    <div class="col-6 col-md-3 mb-3">
        <input list="sak" type="text" name="sac" id="sac" class="form-control form-control-lg font-weight-bold" placeholder="Nº SAC" autocomplete="off" autofocus <?php echo $sc?> ><datalist id="sak" data-toggle="tooltip" >
            <?php $not = conex()->query("SELECT SAC,ALTERACAO FROM sac ORDER BY ALTERACAO DESC")->fetchAll(PDO::FETCH_ASSOC);
                foreach($not as $no)
                {
                    echo '<option value="'.$no['SAC'].'"/>';
                }
            ?>
        </datalist>
    </div>
    
    <!-- BOTAO EDITAR *** //////////////////////// -->
    <div class="col-6 col-md-3 mb-3 font-weight-bold">
        <button class="btn btn-block btn-lg btn-danger shadow" data-toggle="tooltip"  onclick="play(),vibrate(),editar($('#sac').val())"><i class="fas fa-user-edit mr-2"></i>Editar</button>
    </div>
    
    <!-- INPUT DO SMART *** //////////////////////// -->
    <div class="col-6 col-md-3 mb-3">
        <input type="text" list='sm' id='smart' name="smart" placeholder="Smart ID"  autocomplete="off" required class="form-control form-control-lg font-weight-bold" <?php if($equipe == 'AGE') echo 'readonly'?>><datalist id='sm'>
            <?php $sm = conex()->query("SELECT smart,id FROM codigo UNION ALL SELECT smart,id FROM lg ORDER BY id DESC")->fetchAll(PDO::FETCH_ASSOC);
                foreach($sm as $sma)
                {
                    echo '<option value="'.$sma['smart'].'"/>';
                }
            ?>
        </datalist>
    </div>
    
    <!-- BOTAO FICHA DO ID *** //////////////////////// -->
    <div class="col-6 col-md-3 mb-3 font-weight-bold">
        <button type="button" class="btn btn-block btn-lg btn-warning shadow" id="model" onclick="play(),vibrate(100)"><i class="fas fa-user-plus mr-2"></i>Novo</button>
    </div>
</div>

<!--
8888888b.                        888          888             
888   Y88b                       888          888             
888    888                       888          888             
888   d88P 888d888  .d88b.   .d88888 888  888 888888  .d88b.  
8888888P"  888P"   d88""88b d88" 888 888  888 888    d88""88b 
888        888     888  888 888  888 888  888 888    888  888 
888        888     Y88..88P Y88b 888 Y88b 888 Y88b.  Y88..88P 
888        888      "Y88P"   "Y88888  "Y88888  "Y888  "Y88P"  
 -->

<div class="row mb-2">

    <!-- BARRA DE PROGRESSO *** //////////////////////// -->
    <div class="col-12 mt-3">
        <div class="progress shadow h-75" id="barra" data-toggle="tooltip">
            <div class="progress-bar progress-bar-striped font-weight-bold bg-secondary" style="width: 100%;" role="progressbar"  aria-valuemin="0" aria-valuemax="100"><h4>0%</h4></div>
        </div>
    </div>

    <!-- INFO DE RETORNO *** //////////////////////// -->
    <div class="col-12 col-sm-4 col-md-3 mt-3">
        <div class="alert alert-orange font-weight-bold" id="aberto"><i class="fas fa-retweet mr-2"></i>RETORNOS</div>
    </div>

    <!-- INFO DE MODELO *** //////////////////////// -->
    <div class="col-12 col-sm-4 col-md-3 mt-3">
        <div class="alert alert-success font-weight-bold" id="log"><i class="far fa-ruler-triangle mr-2"></i>MODELO</div>
        <input type="text" style="display: none;" id="modelo" name="modelo">
    </div>

    <!-- INFO DE VOLTAGEM *** //////////////////////// -->
    <div class="col-12 col-sm-4 col-md-3 mt-3">
        <div class="alert alert-success font-weight-bold" id="lok"><i class="fas fa-bolt mr-2"></i>VOLTAGEM</div>
        <input type="text" style="display: none;" id="voltagem" name="voltagem">
    </div>

    <!-- INFO DE ENGENHARIA *** //////////////////////// -->
    <div class="col-12 col-sm-6 col-md-3 mt-3">
        <div class="alert alert-success font-weight-bold" id="engenharia"><i class="fal fa-user-hard-hat mr-2"></i>ENGENHARIA</div>
    </div>
    <div class="col-12 col-sm-6 col-md-4 mt-3">
        <button class="btn btn-block btn-success font-weight-bold shadow btn-lg"  data-toggle="modal" data-target="#modalExemplo" id="pil">
            <i class="fas fa-waveform-path mr-2"></i>GRAVAÇÕES
        </button>

        <!-- Modal -->
        <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="grava">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nada para se Ouvir</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="../IMG/SAC/GRAVACAO.jpg" class="w-100" alt="">
                        Nenhuma gravação disponível para esse atendimento.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- 
 .d8888b.  888             888                      
d88P  Y88b 888             888                      
Y88b.      888             888                      
 "Y888b.   888888  8888b.  888888 888  888 .d8888b  
    "Y88b. 888        "88b 888    888  888 88K      
      "888 888    .d888888 888    888  888 "Y8888b. 
Y88b  d88P Y88b.  888  888 Y88b.  Y88b 888      X88 
 "Y8888P"   "Y888 "Y888888  "Y888  "Y88888  88888P'  
-->

    <div class="col-12 col-sm-6 col-md-4 mb-2 mt-3" data-toggle="tooltip">
        <button class="btn btn-lg btn-info btn-block shadow" onclick="play(),vibrate()" data-toggle="modal" data-target="#stat" id="status" >
            <i class="fas fa-bell mr-2"></i>Status
        </button>        
        <div class="modal fade" id="stat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-bell mr-2"></i>Status...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <div class="row" id="etapa">

                        <!-- APENAS EQUIPE ADM E SAC TEM ACESSO A BOTOES DESSE GRUPO -->
                            <?PHP if(($equipe == 'ADM')||($equipe == 'SAC')) { ?>
                            
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="04" value="04.AGENDADO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-home mr-2\'></i>04.AGENDADO')"><i class="fas fa-home mr-2"></i>04.AGENDADO
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="14" value="14.CANCELADO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-bell-slash mr-2\'></i>14.CANCELADO')"><i class="fas fa-bell-slash mr-2"></i>14.CANCELADO
                                </label>
                            </div>

                            <?php } 
                            
                            if(($equipe != 'LOJA')) { ?>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="03" value="03.PARTWAIT"  onclick="play(),vibrate(),sts('<i class=\'fas fa-pause-circle mr-2\'></i>03.PARTWAIT')"><i class="fas fa-pause-circle mr-2"></i>03.PARTWAIT
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="05" value="05.OBSERVACAO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-eye mr-2\'></i>05.OBSERVAÇÃO')"><i class="fas fa-eye mr-2"></i>05.OBSERVAÇÃO
                                </label>
                            </div>
                            <?php }

                            if($equipe != 'AGE') { ?>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="01" value="01.ABERTO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-door-open mr-2\'></i>01.ABERTO')"><i class="fas fa-door-open mr-2"></i>01.ABERTO
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="07" value="07.ACORDO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-handshake mr-2\'></i>07.ACORDO')"><i class="fas fa-handshake mr-2"></i>07.ACORDO
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="11" value="11.LABORATORIO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-microscope mr-2\'></i>11.LABORATORIO')"><i class="fas fa-microscope mr-2"></i>11.LABORATÓRIO
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="12" value="12.DEVOLVIDO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-undo-alt mr-2\'></i>12.DEVOLVIDO')"><i class="fas fa-undo-alt mr-2"></i>12.DEVOLVIDO
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="15" value="15.FINALIZADO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-flag-checkered mr-2\'></i>15.FINALIZADO')"><i class="fas fa-flag-checkered mr-2"></i>15.FINALIZADO
                                </label>
                            </div>
                            <?php } ?>

                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="06" value="06.ORCAMENTO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-cash-register mr-2\'></i>06.ORCAMENTO')"><i class="fas fa-cash-register mr-2"></i>06.ORÇAMENTO
                                </label>
                            </div>

                            <?php if($equipe == 'AGE' ||$equipe == 'ADM') { ?>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                    <input type="radio" autocomplete="off" class="d-none" name="status" id="00" value="00.REPARADO"  onclick="play(),vibrate(),sts('<i class=\'fas fa-wrench mr-2\'></i>00.REPARADO')"><i class="fas fa-wrench mr-2"></i>00.REPARADO
                                </label>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- 
888                  d8b          
888                  Y8P          
888                               
888       .d88b.    8888  8888b.  
888      d88""88b   "888     "88b 
888      888  888    888 .d888888 
888      Y88..88P    888 888  888 
88888888  "Y88P"     888 "Y888888 
                     888          
                    d88P          
                  888P"            
-->

    <div class="col-12 col-sm-6 col-md-4 mb-2 mt-3" data-toggle="tooltip">
        <button class="btn btn-lg btn-info btn-block shadow" onclick="play(),vibrate()" data-toggle="modal" data-target="#loj" id="loja" <?php if($equipe == 'AGE') echo 'disabled'?>><i class="fas fa-store-alt mr-2"></i>Loja</button>        
        <div class="modal fade" id="loj" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel2"><i class="fas fa-store-alt mr-2"></i>Loja...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bg-light">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <div class="row">
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                <input type="radio" autocomplete="off" class="d-none" name="loja" value="MOGI" id="MOGI" onclick="play(),vibrate(),lojas('<i class=\'fas fa-store-alt mr-2\'></i>MOGI','MOGI')"><i class="fas fa-store-alt mr-2"></i>MOGI
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                <input type="radio" autocomplete="off" class="d-none" name="loja" value="VILA MARIA" id="VILA_MARIA" onclick="play(),vibrate(),lojas('<i class=\'fas fa-store-alt mr-2\'></i>VILA MARIA','VILA MARIA')"><i class='fas fa-store-alt mr-2'></i>VILA MARIA</label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                <input type="radio" autocomplete="off" class="d-none" name="loja" value="GALATEA" id="GALATEA" onclick="play(),vibrate(),lojas('<i class=\'fas fa-store-alt mr-2\'></i>GALATEA','GALATEA')"><i class='fas fa-store-alt mr-2'></i>GALATEA</label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                <input type="radio" autocomplete="off" class="d-none" name="loja" value="CHICO PONTES" id="CHICO_PONTES" onclick="play(),vibrate(),lojas('<i class=\'fas fa-store-alt mr-2\'></i>CHICO PONTES','CHICO PONTES')"><i class='fas fa-store-alt mr-2'></i>CHICO PONTES</label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                <input type="radio" autocomplete="off" class="d-none" name="loja" value="VILA GUILHERME" id="VILA_GUILHERME" onclick="play(),vibrate(),lojas('<i class=\'fas fa-store-alt mr-2\'></i>VILA GUILHERME','VILA GUILHERME')"><i class='fas fa-store-alt mr-2'></i>VILA GUILHERME</label>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-4 mt-3">
                                <label class="btn btn-outline-info btn-lg btn-block shadow">
                                <input type="radio" autocomplete="off" class="d-none" name="loja" value="TATUAPE" id="TATUAPE" onclick="play(),vibrate(),lojas('<i class=\'fas fa-store-alt mr-2\'></i>TATUAPE','TATUAPE')"><i class='fas fa-store-alt mr-2'></i>TATUAPÉ</label>
                            </div>
                        </div>
                    </div>

                    <!-- INPUT DO LOCAL -->
                    <input type="text" style="display: none;" id="locais" name="locais">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" onclick="vibrate(100),play()">OK</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
  
  <!-- 
  8888888                                                            
    888                                                              
    888                                                              
    888   88888b.d88b.   8888b.   .d88b.   .d88b.  88888b.  .d8888b  
    888   888 "888 "88b     "88b d88P"88b d8P  Y8b 888 "88b 88K      
    888   888  888  888 .d888888 888  888 88888888 888  888 "Y8888b. 
    888   888  888  888 888  888 Y88b 888 Y8b.     888  888      X88 
  8888888 888  888  888 "Y888888  "Y88888  "Y8888  888  888  88888P' 
                                      888                            
                                 Y8b d88P                            
                                  "Y88P"                              
  -->
  <div class="col-12" id="im"></div>

<!-- 
8888888b.           888                      
888  "Y88b          888                      
888    888          888                      
888    888  8888b.  888888  8888b.  .d8888b  
888    888     "88b 888        "88b 88K      
888    888 .d888888 888    .d888888 "Y8888b. 
888  .d88P 888  888 Y88b.  888  888      X88 
8888888P"  "Y888888  "Y888 "Y888888  88888P'  
-->
    <div class="col-12">
        <div class="form-row mt-2 text-left" id='datas'>
            <div class="form-group col-12 col-sm-6 col-xl-3">
                <label for="date1"><i class="far fa-calendar-exclamation mr-2"></i>Agendamento</label>
                <input name="agenda" placeholder="Calendário" class="form-control form-control-lg font-weight-bold" autocomplete="off" <?php echo (($equipe == 'AGE')||($equipe == 'LOJA')) ? 'readonly' : 'id="date1"'?>>
            </div>
            <div class="col-12 col-sm-6 col-xl-2">  
                <label><i class="far fa-calendar-exclamation mr-2"></i>Agendado para:</label>
                <div class="alert alert-primary font-weight-bold" id="agendamento">Sem agendamento</div>        
            </div>
            <div class="form-group col-12 col-sm-6 col-xl-3">
                <label for="date2"><i class="far fa-calendar-exclamation mr-2"></i>Data da Compra</label>
                <input id="date2" name="compra" placeholder="Calendário"  class="form-control form-control-lg font-weight-bold" autocomplete="off" <?php echo ($equipe == 'AGE') ? 'readonly' : 'id="date2"'?>>
            </div>
            <div class="col-12 col-sm-6 col-xl-2">  
                <label for="comprado"><i class="far fa-calendar-exclamation mr-2"></i>Comprado em:</label>
                <div class="alert alert-primary font-weight-bold" id="comprado">Não informada</div>        
            </div>
            <div class="col-12 col-xl-2">  
                <label for="nota"><i class="far fa-file-invoice-dollar mr-2"></i>Nota Fiscal:</label>
                <input type="number" name="nota" id="nota" autocomplete="off"  class="form-control form-control-lg font-weight-bold">       
            </div>
        </div>
    </div>


<!-- 
 .d8888b.  888 d8b                   888             
d88P  Y88b 888 Y8P                   888             
888    888 888                       888             
888        888 888  .d88b.  88888b.  888888  .d88b.  
888        888 888 d8P  Y8b 888 "88b 888    d8P  Y8b 
888    888 888 888 88888888 888  888 888    88888888 
Y88b  d88P 888 888 Y8b.     888  888 Y88b.  Y8b.     
 "Y8888P"  888 888  "Y8888  888  888  "Y888  "Y8888   
-->
    <div class="col-12">
        <div class="card bg-light shadow mt-4">
            <div class="card-body">
            <h2 class="card-title text-left">Cliente</h2>
                <div class="row text-left" id="cliente">
                    <div class="col-12 col-lg-4 col-xl-4 mb-2">
                        <label for="nome"><i class="far fa-id-card mr-2"></i>Nome</label>
                        <input type="text" id="nome" name="nome" class="form-control form-control-lg font-weight-bold" required placeholder="Do cliente"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 mb-2">
                        <label for="telefone1"><i class="far fa-phone-square-alt mr-2"></i>Telefone 1</label>
                            <div class="input-group">
                            <input type="tel" id="telefone1" name="telefone1" class="form-control form-control-lg font-weight-bold" required placeholder="Com DDD"<?php if($equipe == 'AGE') echo 'readonly'?> aria-describedby="whats1">
                            <div class="input-group-append" id="whats1">
                                <a href="" onclick="return false;" class="btn btn-outline-success"><i class="fab fa-whatsapp fa-lg"></i></a>
                                <a href="" onclick="return false;" class="btn btn-outline-purple"><i class="far fa-phone-rotary fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 mb-2">
                        <label for="telefone2"><i class="far fa-phone-square-alt mr-2"></i>Telefone 2</label>
                            <div class="input-group">
                            <input type="tel" id="telefone2" name="telefone2" class="form-control form-control-lg font-weight-bold" required placeholder="Com DDD"<?php if($equipe == 'AGE') echo 'readonly'?> aria-describedby="whats2">
                            <div class="input-group-append" id="whats2">
                                <a href="" onclick="return false;" class="btn btn-outline-success"><i class="fab fa-whatsapp fa-lg"></i></a>
                                <a href="" onclick="return false;" class="btn btn-outline-purple"><i class="far fa-phone-rotary fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 mb-2">
                        <label for="cep"><i class="fas fa-mail-bulk mr-2"></i>CEP</label>
                        <input type="number" id="cep" name="cep" class="form-control form-control-lg font-weight-bold" required maxlength="9" placeholder="Pesquise" value="" onblur="pesquisacep(this.value);" autocomplete="off"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <div class="col-12 col-lg-6 mb-2">
                        <label for="rua"><i class="fas fa-map-marked mr-2"></i>Rua</label>
                        <input type="text" id="rua" name="rua" class="form-control form-control-lg font-weight-bold" required placeholder="Automático se tiver CEP"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-2 mb-2">
                        <label for="numero"><i class="fas fa-sort-numeric-up-alt mr-2"></i>Numero</label>
                        <input type="number" id="numero" name="numero" class="form-control form-control-lg font-weight-bold" required placeholder="Residencial"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 col-xxl-4 mb-2">
                        <label for="bairro"><i class="fas fa-city mr-2"></i>Bairro</label>
                        <input type="text" id="bairro" name="bairro" class="form-control form-control-lg font-weight-bold" required placeholder="Automático se tiver CEP"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 col-xxl-4 mb-2">
                        <label for="complemento"><i class="fas fa-paperclip mr-2"></i>Complemento</label>
                        <input type="text" id="complemento" name="complemento" class="form-control form-control-lg font-weight-bold" required placeholder="AP,Bloco,etc"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4 col-xxl-4 mb-2">
                        <label for="cidade"><i class="fas fa-globe-americas mr-2"></i>Cidade</label>
                        <input type="text" id="cidade" name="cidade" class="form-control form-control-lg font-weight-bold" required placeholder="Automático se tiver CEP"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <!-- DEFEITOS MANUAIS TEMPORARIOS -->
                    <div class="col-12">
                        <label for="queixa"><i class="fas fa-frown mr-2"></i>Queixa</label>
                        <input type="text" id="queixa" name="queixa" class="form-control form-control-lg font-weight-bold" required placeholder="Descreva o defeito informado"<?php if($equipe == 'AGE') echo 'readonly'?>>
                    </div>
                    <!-- FASE DE ESCOLHA DE DEEITOS ADIADA 
                        <div class="col-12 mb-2" id="qu">
                    </div> -->
                </div>
            </div>
        </div>
    </div>

<!-- 
88888888888                            d8b                   
    888                                Y8P                   
    888                                                      
    888      .d88b.   .d8888b 88888b.  888  .d8888b  .d88b.  
    888     d8P  Y8b d88P"    888 "88b 888 d88P"    d88""88b 
    888     88888888 888      888  888 888 888      888  888 
    888     Y8b.     Y88b.    888  888 888 Y88b.    Y88..88P 
    888      "Y8888   "Y8888P 888  888 888  "Y8888P  "Y88P"   
-->

    <div class="col-12">
        <div class="card bg-light shadow mt-5">
            <div class="card-body">
            <h2 class="card-title text-left">Técnico</h2>
                <div class="row text-left" id="avalia">
                    <div class="col-12 mb-3">
                        <label for="constatado"><i class="fas fa-binoculars mr-2"></i>Constatado</label>
                        <input type="text" id="constatado" name="constatado" class="form-control form-control-lg font-weight-bold" required placeholder="Descreva o defeito constatado" <?php if(($equipe != 'AGE')&&($equipe != 'ADM')) echo 'readonly'?>>
                    </div>
                    <div class="col-12 mb-3">
                        <div class="btn-group btn-group-toggle d-flex shadow" data-toggle="buttons">
                        <label for="DC" class="btn btn-outline-primary active">
                            <input type="radio" name="tecnico" id="DC" value="DEFEITO CONFERE>" autocomplete="off" checked <?php if(($equipe != 'AGE')&&($equipe != 'ADM'))  echo 'disabled'?>><i class="fas fa-user-check mr-2"></i>Defeito Confere
                        </label>
                        <label for="SD" class="btn btn-outline-primary">
                            <input type="radio" name="tecnico" id="SD" value="SEM DEFEITO>" autocomplete="off"  <?php if(($equipe != 'AGE')&&($equipe != 'ADM'))  echo 'disabled'?>><i class="fas fa-user-times mr-2"></i>Sem Defeito
                        </label>
                        <label for="OD" class="btn btn-outline-primary">
                            <input type="radio" name="tecnico" id="OD" value="OUTRO DEFEITO>" autocomplete="off"  <?php if(($equipe != 'AGE')&&($equipe != 'ADM'))  echo 'disabled'?>><i class="fas fa-user-edit mr-2"></i>Outro Defeito
                        </label>
                        </div>
                    </div>
                </div>
                <div class="row text-left" id="reparo">
                </div>
                <div class="row text-left">
                    <div class="col-12 mb-1">
                        <i class="fas fa-cogs mr-2"></i>Peça(s)
                    </div>
                    <div class="col-12 col-sm-6 mb-2">
                        <input type="text" id="peca" name="peca" class="form-control form-control-lg font-weight-bold" required placeholder="Insira o(s) código(s) " <?php if(($equipe != 'AGE')&&($equipe != 'ADM')) echo 'readonly'?>>
                    </div>
                    <div class="col-12 col-sm-6 mb-2">
                        <button class="btn btn-lg btn-outline-success font-weight-bold btn-block shadow" type="button" id="codigo" onclick="explodida()"><i class="far fa-file-search mr-2"></i>Vista Explodida</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- 
8888888888 d8b                   888 
888        Y8P                   888 
888                              888 
8888888    888 88888b.   8888b.  888 
888        888 888 "88b     "88b 888 
888        888 888  888 .d888888 888 
888        888 888  888 888  888 888 
888        888 888  888 "Y888888 888  
-->


    <div class="col-6 mt-5 mb-5" >
        <button class="btn btn-lg btn-block btn-secondary font-weight-bold shadow" onclick="printar()"><i class="fas fa-print mr-5"></i>Imprimir</button>
    </div>
    <div class="col-6 mt-5 mb-5">
        <button class="btn btn-lg btn-block btn-danger font-weight-bold shadow" type="button" id="botao"><i class="fas fa-thumbtack mr-2"></i>Registrar</button>
    </div>

</div>

</form>


<?php include("../CORE/Footer2.php")?>