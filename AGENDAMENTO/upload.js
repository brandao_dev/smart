function upload(id,tipo,modelo)
{
    if(tipo == 'BOLETIM')
    {
        titulo = 'Boletim Técnico'
        titulo2 = 'Boletim carregado'
    }
    else
    {
        titulo = 'Vista Explodida'
        titulo2 = 'Vista Explodida carregada'
    }
    
    Swal.fire({
        title: '<span id="file-name">Carregar '+titulo+' Modelo: '+modelo+'</span>',
        html: 
        '<div align="center">'+
            '<form method="post" action="" enctype="multipart/form-data" id="myform">'+ 
                '<div class="row">'+ 
                    '<div class="col-12">'+
                        '<label for="file" class="btn btn-lg btn-block btn-primary shadow mt-3"><i class="fas fa-search fa-lg mr-2"></i>Procurar</label>'+
                        '<input type="file" id="file" name="file"  style="display:none" />'+ 
                    '</div>'+
                    '<div class="col-12">'+
                        '<button type="submit" class="btn btn-lg btn-block btn-purple shadow mt-3"><i class="fas fa-save fa-lg mr-2"></i>Confirmar</button>'+ 
                    '</div>'+
                '</div>'+ 
            '</form>'+ 
        '</div>',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        showConfirmButton: false
      })

      var $input = document.getElementById('file'),
      $fileName = document.getElementById('file-name');
      
      $input.addEventListener('change', function(){
      $fileName.textContent = titulo2+'! Só falta confirmar';
      });


      $(function() { 
        $("#myform").on('submit',function(e) {
            e.preventDefault()
            var fd = new FormData(); 
            var files = $('#file')[0].files[0]; 
            fd.append('file', files); 
            fd.append('modelo',modelo);
            fd.append('tipo',tipo);
    
            $.ajax({ 
                url: 'upload.php', 
                type: 'post', 
                data: fd, 
                contentType: false, 
                processData: false,
                async: true, 
                success: function(resp)
                { 
                    // DEI RÉ NO KIBE E MANDEI RECARREGAR
                    location.reload()
                    console.log(resp)
                    // if(resp == "VISTA")
                    // {
                    //     $('#'+id).html("<button type='button' class='btn btn-success shadow' onclick='abre(\"VISTA\",\"{$linha['MODELO']}\")'>SIM</button>")
                    // }
                    // else
                    // {
                    //     $('#b'+id).html("<button type='button' class='btn btn-success shadow' onclick='abre(\"BOLETIM\",\"{$linha['MODELO']}\")'>SIM</button>")
                    // }
                    // Swal.close()
                },
                error: function()
                {
                    alert('Erro. Tente novamente'); 
                } 
            }); 
        }); 
    }); 
}

function abre(tipo,modelo)
{    
    if(tipo == 'VISTA')
    {
        titulo = 'Vista Explodida do '+modelo
    }
    else
    {
        titulo = 'Boletim Técnico do '+modelo
    }
    // WebView.getSettings().setJavaScriptEnabled(true);
    // webView.setWebChromeClient(new WebChromeClient())
    window.open('http://docs.google.com/gview?embedded=true&url=https://smart.brandev.tech/'+tipo+'/'+modelo+'.pdf', '_blank')
    // webView.loadUrl("file:///smart.brandev.tech/"+tipo+"/"+modelo+".pdf");

}