﻿<?php include('../CORE/Header2.php');?>
<script src="https://cdn.brandev.site/js/jquery-barcode.js"></script>
<script src="https://cdn.brandev.site/js/masker.js"></script>
<div id="player" style="display: none;"></div>
<script type="text/JavaScript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.js"></script>
<!-- PRINT RETRATO/PAISAGEM -->
<style>@page {size: auto;}</style>
<div class="row">

	<!-- ALERTA -->
	<div class="col-12">	
		<div class="alert alert-block alert-info" id="caso"><i class="fas fa-gift mr-2"></i>Bem vindo(a) <?php echo $user?></div>
	</div>

	<!-- GRUPO 1 -->
	<div class="col-12 col-xl-6">
		<div class="row">

			<!-- INPUT -->
			<div class="col-12 mb-3">
				<input type="text" list="imp" class="form-control form-control-lg font-weight-bold" placeholder="Insira o ID" name="imp" autofocus autocomplete="off" id="etiqueta"><datalist id="imp">
					<?php
						$sac = conex()->query("SELECT smart, ALTERACAO FROM codigo UNION ALL SELECT smart, ALTERACAO FROM lg ORDER BY ALTERACAO DESC")->fetchAll(PDO::FETCH_ASSOC);
						foreach($sac as $res)
						{
							echo "<option value=".$res['smart'].">";
						}
					?>
				</datalist>
			</div>

			<!-- BOTOES -->
			<div class="col-12 col-xl-4 mb-3">
				<button type="button" class="btn btn-primary btn-block btn-lg shadow" onclick="vibrate(), play(), grade('A',$('#etiqueta').val())"><i class="far fa-grin-beam mr-2"></i>A</button>
			</div>
			<div class="col-12 col-xl-4 mb-3">
				<button type="button" class="btn btn-success btn-block btn-lg shadow" onclick="vibrate(), play(), grade('B',$('#etiqueta').val())"><i class="far fa-smile mr-2"></i>B</button>
			</div>
			<div class="col-12 col-xl-4 mb-3">
				<button type="button" class="btn btn-danger btn-block btn-lg shadow" onclick="vibrate(), play(), grade('C',$('#etiqueta').val())"><i class="far fa-meh mr-2"></i>C</button>
			</div>

			<!-- ETIQUETAS -->
			<div class="col-12 mb-3">
				<!-- PRINT -->
				<div class="card shadow mt-4" onclick="$('#print').print()">
					<div class="card-body" id="print">
						<div class="row text-left">
							<div class="col-12" style ='font-size:27px;font-family: Impact' id="linha">
							LINHA, FABRICANTE E COR
							</div>
							<div class="col-12" style ='font-size:30px;font-family: Calibri' id="modelo">
							MODELO
							</div>
							<div class="col-12">
								<div id="2" id="barra1" style="margin-left: -28px;"></div>
							</div>
							<div class="col-12" style ='font-size:30px;font-family: Calibri' id="serial">
							SERIAL
							</div>

							<!-- BARRA E GRADE -->
							<div class="col-12">
								<div class="row">
									<div class="col">
										<div id="3" id="barra2" style="margin-left: -28px;"></div>
									</div>
									<div class="col">
										<div id="grade"></div>
									</div>
								</div>
							</div>

							<!-- EAN -->
							<div class="col-12">
								<div class="row">
									<div class="col" style ='font-size:30px;font-family: Calibri' id="ean" >
										EAN
									</div>

									<!-- VOLTAGEM -->
									<div class="col" style ='font-size:30px;font-family: arial black' id="voltagem">
									VOLTAGEM
									</div>
								</div>
							</div>

							<!-- TABELA -->
							<div class="col-12">
								<table class="table table-bordered table-hover table-sm" id="tabela" style="font-family: Verdana; border: 2px solid  #111 !important; width: auto;">
								</table>
							</div>

							<!-- FINAL -->
							<div class="col-12">
								Não remova a etiqueta. Risco de perca de garantia. Revisado por:<img src="../IMG/smart.jpg" width="100">
							</div>
						</div>
						
						<!-- <table class="table table-borderless table-sm w-100">
							<tr>
								<td class="text-left" colspan="6">
									<div style ='font-size:27px;font-family: Impact' id="linha">LINHA, FABRICANTE E COR</div>
								</td>
							</tr>
							<tr>
								<td class="text-left" colspan="6">
									<div style ='font-size:30px;font-family: Calibri' id="modelo">MODELO</div>	
								</td>
							</tr>
							<tr>
								<td class="text-left" colspan="6">
									<div id="2" id="barra1" style="margin-left: -28px;"></div>
								</td>
							</tr>
							<tr>
								<td class="text-left" colspan="6">
									<div style ='font-size:30px;font-family: Calibri' id="serial">SERIAL</div>
								</td>
							</tr>
							<tr>
								<td class="text-left" colspan="4">
									<div id="3" id="barra2" style="margin-left: -28px;"></div>
								</td>
								<td class="text-left"  colspan="2">
									<div id="grade"></div>
								</td>
							</tr>
							<tr>
								<td class="text-left" colspan="4">
									<div style ='font-size:30px;font-family: Calibri' id="ean">EAN</div>
								</td>
								<td class="text-left"  colspan="2">
									<div style ='font-size:30px;font-family: arial black' id="voltagem">VOLTAGEM</div>	
								</td>
							</tr>
						</table>	 -->

						<!-- <table class="table table-bordered table-hover table-sm" id="tabela" style="font-family: Verdana; border: 2px solid  #111 !important; width: auto;">
						</table> -->

						<!-- <table class="table table-borderless table-hover table-sm w-100" style="font-family: Verdana;font-size:12px; width">	
							<tr>
								<td class="text-left">Não remova a etiqueta. Risco de perca de garantia. Revisado por:<img src="../IMG/smart.jpg" width="100"></td>
							</tr>
						</table> -->
					</div>			
				</div>
			</div>
			
			<!-- IMAGENS -->
			<div class="col-12">
				<div class="mt-4" id="im"></div>
			</div>

		</div>
	</div>

	<!-- GRUPO 2 -->
	<div class="col-12 col-xl-6">
		<div class="row">

			<!-- INPUT MODELO -->
			<div class="col-9 mb-3">
				<input type="text" class="form-control form-control-lg font-weight-bold" id="modeloProcel" placeholder="Insira um Modelo existente" autocomplete="off" list="model">
				<datalist id="model">
				<?php
					$modelos = conex()->query("SELECT modelo FROM modelo ORDER BY modelo")->fetchAll(PDO::FETCH_ASSOC);
					foreach($modelos as $modelo)
					{
						echo "<option value='{$modelo['modelo']}'/>";
					}				
				?>
				</datalist>
			</div>

			<!-- BOTÂO NOVO -->
			<div class="col-3">
				<button class="btn btn-lg btn-purple btn-block shadow" onclick="window.open('../MODELO/modelo.php','_blank')"><i class="fas fa-plus-circle mr-2"></i>Novo</button>
			</div>

			<!-- PROCEL -->
			<div class="col-12 text-left">
				<div class="card shadow">
					<div class="card-body">
						<form action="" method="POST" id="form">
							<div class="form-row" id="procel">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script src="AJAX.js?<?php echo filemtime('AJAX.js')?>"></script>
<?PHP include('../CORE/Footer2.php'); ?>