<?php include "../CORE/PDO.php";?>
<!DOCTYPE HTML>
<html lang='pt-br'>
<head>
	<meta charset='UTF-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
	<meta name='autor' content='A. Brandão Full Stack Developer 2020 / PHPOO, Composer, NodeJs, SASS, Gulp, MySQL, CSS, HTML5, JavaScript'>
	<meta name='format-detection' content='telephone-no'>
	<title>ERP - SMART</title>
	<link rel='shortcut icon' href='../IMG/SICON.png'>
<!--===============================================================================================-->
	<link rel='stylesheet' href='https://cdn.brandev.site/custom/css/custom.css'>
<!--===============================================================================================-->
	<link href='https://cdn.brandev.site/css/all.css' rel='stylesheet'>
<!--===============================================================================================-->
	<script src="https://cdn.brandev.site/custom/js/jquery.min.js"></script>
<!--===============================================================================================-->
	<script src="https://cdn.brandev.site/custom/js/popper.min.js"></script>
<!-- ============================================================================================= -->
	<script src="https://cdn.brandev.site/custom/js/bootstrap.min.js"></script>
<!-- ============================================================================================= -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.9/dist/sweetalert2.all.min.js"></script>
	<!-- PERFIL AJAX -->
	<script src="/PERFIL/upload.js"></script>
</head>
<body  class='text-center'>

<!-- AUDIO PADRÂO -->
<audio id="click" src="../som/click.mp3"></audio>
<script>
      function play() {
        var click = document.getElementById("click");
        click.play();
      }
</script>
<!-- MARGEM DO CORPO -->
<div class='container-fluid'>
<?PHP

	#SE NÃO ESTIVER LOGADO VOLTA PARA O INDEX
	if(!isset($_SESSION['MM_Username']))
	{
		header('location:../index.php');
	}

	#SE NÃO TIVER AUTORIZAÇÃO PARA A URL, VOLTA PARA INDEX
	$code = conex()->query("SELECT code FROM menu WHERE url LIKE '$url'")->fetch(PDO::FETCH_ASSOC)['code'];
	if (!fnmatch("*$code*",$_SESSION['permissao']))
	{
		header('location:../index.php');
	}

	#MOSTRA MENU
	echo $_SESSION['ACESSO'];

	#REALCE DO GRUPO
	$titulo = conex()->query("SELECT grupo,opcao,code FROM menu WHERE url LIKE '$url'")->fetch(PDO::FETCH_ASSOC);
?>

<div class='text-left alert alert-warning h5 font-weight-bold'>
	<i class='<?php echo itemMenu($titulo['code'])['ico1']?>'></i> 
	<?php echo $titulo['grupo'].'/ <i class="'.itemMenu($titulo['code'])['ico2'].'"></i> '.$titulo['opcao']?>
</div>