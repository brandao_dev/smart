<?php include('leitura.php');?>
<!--Código custom -->          
<script src="ajax.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/blitzer/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- TAMANHO DE SWAL -->
<style>.swal {width:950px !important;}</style>

<!-- PLAYER -->
<div id="player" style="display: none;"></div>


<!-- 
       d8888      888 d8b          d8b                                    
      d88888      888 Y8P          Y8P                                    
     d88P888      888                                                     
    d88P 888  .d88888 888  .d8888b 888  .d88b.  88888b.   8888b.  888d888 
   d88P  888 d88" 888 888 d88P"    888 d88""88b 888 "88b     "88b 888P"   
  d88P   888 888  888 888 888      888 888  888 888  888 .d888888 888     
 d8888888888 Y88b 888 888 Y88b.    888 Y88..88P 888  888 888  888 888     
d88P     888  "Y88888 888  "Y8888P 888  "Y88P"  888  888 "Y888888 888     
 -->
<div class="row">
    <div class="col">
        <div class="alert alert-info font-weight-bold btn-block" id="caso"><i class="fas fa-handshake fa-lg mr-2"></i>Bem vindo(a)! <?php echo $user?></div>
    </div>
</div>
<form action="" method="POST" id="form">
    <div class="row">
        <div class="col-12 col-sm-6 mt-2">
            <input type="text" class="form-control form-control-lg font-weight-bold" name="smart" id="smart" autocomplete="off" required autofocus list="lis" placeholder="Insira o ID">
            <datalist id="lis">
                <?php echo $id; ?>
            </datalist>
        </div>
        <div class="col-12 col-sm-6 mt-2">
            <button class="btn btn-primary btn-lg shadow btn-block" type="submit" onclick="play(),vibrate(100)"><i class="fas fa-hand-receiving mr-2"></i>Receber</button>
        </div>
    </div>
</form>

<!-- PLAYER DE AUDIO -->
<div style="display: none" id="click"></div>

<!-- BARCODE 1 -->
<INPUT id=barcode type=text style="display: none" >

<hr>
<!--
88888888888          888               888
    888              888               888
    888              888               888
    888      .d88b.  88888b.   .d88b.  888  8888b.
    888     d8P  Y8b 888 "88b d8P  Y8b 888     "88b
    888     88888888 888  888 88888888 888 .d888888
    888     Y8b.     888 d88P Y8b.     888 888  888
    888      "Y8888  88888P"   "Y8888  888 "Y888888
 -->
<form action="" method="post" id="agendas">
    <div class="row mt-2 mb-3">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover" id="tab">
                <!-- <input name="nex" id="minut" type="number" value="" style="display: none;"> -->
                <thead>
                    <tr class="bg-secondary text-light">
                        <th><i class="fas fa-barcode-read fa-lg"></i></th>
                        <th class="d-none d-sm-table-cell"><i class="fas fa-user-headset fa-lg"></i></th>
                        <th class="d-none d-lg-table-cell"><i class="fas fa-store-alt fa-lg"></i></th>
                        <th><i class="far fa-clock fa-lg"></i></th>
                        <th class="d-none d-md-table-cell"><i class="far fa-calendar-day fa-lg"></i></th>
                        <th class="d-none d-xl-table-cell"><i class="fas fa-directions fa-lg"></i></th>
                        <th class="d-none d-xl-table-cell"><i class="fas fa-fire-extinguisher fa-lg"></i></th>
                    </tr>
                </thead>
                <tbody id="table">
                    <?php echo $table; ?>
                </tbody>
            </table>
        </div>
        <div class="col-12">
            <button class="btn btn-success btn-lg shadow btn-block" type="submit" id="btn" value="Scan" onclick="play(),vibrate(100)"><i class="far fa-calendar-day mr-2"></i>Agendar</button>
        </div>
    </div>
</form>

<?php include('../CORE/Footer2.php')?>