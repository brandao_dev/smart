﻿<?PHP 
require('../CORE/PDO.php');

#DEFINIR QUAL BANCO USAR
$modelo = $_GET['modelo'];
#QUERY TABELA
$tabela = conex()->query("SELECT * FROM modelo WHERE modelo LIKE '$modelo'")->fetch(PDO::FETCH_ASSOC);
//FORNECEDOR
$fornecedor = ($tabela['fabricante'] == 'CONSUL')||($tabela['fabricante'] == 'BRASTEMP') ? 'Whirlpool' : '';

?>

<!doctype html>
<html>
	
<head>
<script>
	setTimeout(() => {
		window.close()
	}, 500);
</script>
<meta charset="utf-8">
<title>ERP SMART</title>
</head>
	
<style>
	.container {position: relative;}
	#image {position: absolute;}
	#imetro {position: absolute;left: 40px;top: 40px;}
	#energia {position: absolute;left: 390px;top: 20px;font-family: 'Arial Black', 'sans-serif';font-size: 60px}
	#volt {position: absolute;left: 850px;top: 41px;}
	#gas {position: absolute;left: 700px;top: 35px;}
	#linha {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 390px;top: 95px;}
	#fornecedor {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 390px;top: 130px;}	
	#marca {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 30px;left: 390px;top: 165px;}
	#modelo {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 40px;left: 390px;top: 205px;}
	#queima {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 480px;top: 355px;}
	#forno {position: absolute;font-family: Tahoma, Geneva, sans-serif;font-size: 27px;left: 860px;top: 355px;}
	
	/*MAIS EFICIENTE*/
	#mais {position:absolute; font-family: Tahoma, Genova;font-size: 38px;left: 55px;top: 360px}
	
	#a {position: absolute;left: 820px;top: 412px;}
	#b {position: absolute;left: 820px;top: 408px;}
	#c {position: absolute;left: 820px;top: 594px;}
	#d {position: absolute;left: 820px;top: 680px;}
	#e {position: absolute;left: 820px;top: 767px;}
	#a2 {position: absolute;left: 580px;top: 412px;}
	#b2 {position: absolute;left: 600px;top: 408px;}
	#c2 {position: absolute;left: 600px;top: 594px;}
	#d2 {position: absolute;left: 600px;top: 680px;}
	#e2 {position: absolute;left: 600px;top: 767px;}
	
	/*MENOS EFICIENTE*/
	#menos {position:absolute; font-family: Tahoma, Genova;font-size: 38px;left: 55px;top: 875px}
	
	/*RENDIMENTO*/
	#kwh {position: absolute;font-family:"Arial Black";font-size: 120px;left: 75px;top: 955px;}
	#mes {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 23px;left: 110px;top: 1115px;}
	#kwhi {position: absolute;left: 36px;top: 960px;}
	
	/*COMPARTIMENTO REFRIGERADO*/
	#refrii {position: absolute;left: 405px;top: 960px;}
	#refri {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 65px;left: 440px;top: 960px;}
	#refric {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 23px;left: 475px;top: 1065px;}
	
	/*COMPARTIMENTO CONGELADOR*/
	#congei {position: absolute;left: 690px;top: 960px;}
	#conge {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 65px;left: 730px;top: 960px;}
	#congec {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 23px;left: 730px;top: 1090px;}
	#kgh {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 40px;left: 790px;top: 1025px;}
	
	/*LOGO PROCEL*/
	#procel {position: absolute;left: 105px;top: 1240px;}
	
	/*LOGO PBR*/
	#PBE {position: absolute;left: 395px;top: 1249px;}
	
	/*REGISTRO */
	#quadro2 {position: absolute;left: 690px;top: 1200px;}
	#segura {position: absolute;font-family: 'Arial Black', 'sans-serif';font-size: 35px;left: 725px;top: 1215px;}
	#N {position: absolute;left: 725px;top: 1265px;}
	#registro {position: absolute;font-family: 'Arial', 'sans-serif';font-size: 30px;left: 735px;top: 1375px;}
	
	/*INSTRUÇÔES*/
	#instrucoes {position: absolute;font-family: 'arial';font-size: 26px;left: 55px;top: 1480px;}
</style>
	
<body onload="window.print()">
<div class="container">
  <!-- <img id="image" src="GABA2.jpg" width="100%"> -->
  <label id="imetro"><img src="IMETRO.png" width="32%"></label>
  <label id="energia">ENERGIA</label>
  <label id="volt"><img src="<?PHP echo $tabela['gas'].'.png';?>" width="132px"></label>
  <label id="gas"><img src="<?PHP echo (!empty($tabela['gas'])) ? 'chama.png' : ''?>"></label>
  <label id="linha"><?PHP echo $tabela['linha'];?></label>
  <label id="fornecedor"><?PHP echo $fornecedor;?></label>
  <label id="marca">Marca: <?PHP echo $tabela['fabricante'];?></label>
  <label id="modelo">Modelo: <?PHP echo $tabela['modelo'];?></label>
	
	<!--SOMENTE PARA FOGÕES-->
	<?PHP if(!empty($tabela['gas'])) { ?>
  <label id="queima">Queimadores de Mesa</label>
  <label id="forno">Forno</label>
	<?PHP } ?>
	
  <label id="mais">Mais eficiente</label>
	
	<!--SEGUNDA CAMADA-->
	
	<!--A-->
	<?PHP if(substr($tabela['eficiencia'],-1) == 'A') { ?>	
  <label id="a"><img src="A.jpg" width="75%"></label>
	
	<!--B-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'B') { ?>
  <label id="b"><img src="B.jpg" width="75%"></label>
	
	<!--C-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'C') { ?>
  <label id="c"><img src="C.jpg" width="75%"></label>
	
	<!--D-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'D') { ?>
  <label id="d"><img src="D.jpg" width="75%"></label>
	
	<!--E-->
	<?PHP } if(substr($tabela['eficiencia'],-1) == 'E') { ?>
  <label id="e"><img src="E.jpg" width="75%"></label>
	
	
	<!--PRIMEIRA CAMADA-->
	
	<!--0A-->
	<?PHP } if(substr($tabela['eficiencia'],0,1) == 'A') { ?>
  <label id="a2"><img src="A.jpg" width="75%"></label>
	
	<!--0B-->
	<?PHP } if(substr($tabela['eficiencia'],0,1) == 'B') { ?>
  <label id="b2"><img src="B.jpg" width="75%"></label>
	
	<!--0C-->
	<?PHP } if(substr($tabela['eficiencia'],0,1) == 'C') { ?>
  <label id="c2"><img src="C.jpg" width="75%"></label>
	
	<!--0D-->
	<?PHP } if(substr($tabela['eficiencia'],0,1) == 'D') { ?>
  <label id="d2"><img src="D.jpg" width="75%"></label>
	
	<!--0E-->
	<?PHP } if(substr($tabela['eficiencia'],0,1) == 'E') { ?>
  <label id="e2"><img src="E.jpg" width="75%"></label>
	
	<?PHP } ?>
	
  <label id="menos">Menos eficiente</label>

	<!--RENDIMENTO-->
  <label id="kwh"><?PHP echo $tabela['carga'];?>%</label>
  <label id="mes" align="center">Rendimento médio<br>dos queimadores</label>
  <label id="kwhi"><img src="KWH.png" width="90%"></label>
	
	<!--VOLUME FORNO-->
  <label id="refrii"><img src="KWH.png" width="68%"></label>
  <label id="refri"><?PHP echo $tabela['cap'];?>L</label>
  <label id="refric" align="center">Volume do<br>forno</label>
	
	<!--CONSUMO-->
  <label id="congei"><img src="KWH.png" width="85%"></label>
  <label id="conge"><?PHP echo $tabela['corrente'];?></label>
  <label id="kgh">kg/h</label>
  <label id="congec">Consumo do forno</label>
	
	<!--PROCEL PBE-->
  <label id="procel"><img src="CONPET.jpg" width="200px"></label>
  <label id="PBE"><img src="PBE.png" width="200px"></label>
	
	<!--REGISTRO-->
  <label id="segura">Segurança</label>
  <label id="N"><img src="N.png"></label>
  <label id="registro" align="center">Registro N°<br><?PHP echo $tabela['registro'];?></label>
  <label id="quadro2"><img src="KWH.png" width="280px" height="260px"></label>
	
  <label id="instrucoes">Instruções de instalação e recomendações de uso, leia o Manual do aparelho</label>
</div>
</body>
</html>